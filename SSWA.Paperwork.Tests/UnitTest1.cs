using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Models;
using SSWA.Paperwork.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static SSWA.Paperwork.Common.Notification;

namespace SSWA.Paperwork.Tests
{
    public class UnitTest1
    {

        [Fact]
        public async void Test1()
        {


            IExternalUserService externalUserService = new Auth0UserService(
                   new Auth0UserServiceConfiguration()
                   {
                       AppId = "jjtzavPmJcQknLfSmoSyDLaoQD2eZCAN",
                       ClientId = "eLtq22FSfsB5gW8xXDzSoqQhKm6Nquot",
                       ClientSecret = "L9y-IXVpmkUUjeeSyaYdkXHVqwm-uNutUnOE0H3VrgQCQN5DBZCZN2rY-b1qZVg6",
                       Domain = "sswa.au.auth0.com",
                       Connection = "Username-Password-Authentication",
                       SigningKey = "123456",
                       SendgridApiKey = "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",
                       AppName = "SSWA Platform",
                       
                   });


            var users = await externalUserService.GetAllUsersAsync();

            string output = "";
            foreach (var user in users)
            {
                var roles = await externalUserService.GetUserRolesAsync(user.Id);
                if (roles.Any(x => x.Name == Roles.ADMIN))
                {
                    output += user.Name + "\t" + user.Email + Environment.NewLine;
                }
            }

            Console.WriteLine(output);

            //var emailService = new EmailService("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", new ListLogger<EmailService>());
            //var notificationService = new NotificationService(new ListLogger<NotificationService>(), null, emailService, null, externalUserService);
            //var person = new Core.User()
            //{
            //    Id = "auth0|59e6f464d3fd9c639a5c7de6",
            //    Email = "liamw@promatiq.com",
            //    Name = "Liam Wallace"
            //};

            //var formJson = @"";

            //JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            //{
            //    ContractResolver = new CamelCasePropertyNamesContractResolver(),
            //    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            //    Converters = new List<JsonConverter>() { new FormConverter() }
            //};

            //var form = new PlantEntryRequestForm()
            //{
            //    Id = "test-id",
            //    Kind = new FormKind("PER", "Plant Entry Request"),
            //    CreatedBy = person,
            //    Status = FormState.PENDING_APPROVAL,
            //    Approvers = new List<FormApproval>()
            //    {
            //        new FormApproval()
            //        {
            //            User = person,
            //            Role = new FormApproval.FormApprovalRole()
            //            {
            //                Id = "paperwork_maintenance_manager",
            //                Name = "Maintenance Manager"
            //            },
            //            IsApproved = null
            //        }
            //    }

            //};



            //var recipient = form.CreatedBy;
            //var requestor = await externalUserService.GetUserAsync(form.CreatedBy.Id);

            //notificationService.Handle(new Common.Events.FormSubmittedEvent(form), new System.Threading.CancellationToken()).Wait();

            //var userNotification = new Notification()
            //{
            //    CreatedAt = DateTime.UtcNow,
            //    UpdatedAt = DateTime.UtcNow,
            //    RecipientId = recipient.Id,
            //    Recipient = recipient,
            //    Subject = $"{form.FriendlyId} Approved",
            //    Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been approved." +
            //              $"<br /><br />Request: <em>{form.Description}</em>",
            //    Action = new NotificationAction()
            //    {
            //        Domain = "https://sswa.azurewebsites.net",
            //        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
            //        Display = "View Form"
            //    }
            //};

            //if (form.Kind.Code == "PER")
            //{
            //    var perForm = form as PlantEntryRequestForm;
            //    var pdfAttachment = await GetPdfAttachment();
            //    var calendarAttachment = GetCalendarAttachment(perForm, perForm.FriendlyId + " Start", perForm.Description);

            //    await emailService.SendAsync(userNotification, calendarAttachment);
            //    //await client.AddAsync(userNotification);

            //    calendarAttachment = GetCalendarAttachment(perForm, "Southern Seawater Desalination Plant Visit", "");

            //    foreach (var visitor in perForm.Visitors)
            //    {
            //        if (string.IsNullOrWhiteSpace(visitor.Email))
            //            continue;

            //        var awarenessTraining = "";
            //        if (perForm.AwarenessTraining.Any())
            //        {
            //            awarenessTraining = " as well as the following training: <i>";
            //            awarenessTraining += string.Join(" / ", perForm.AwarenessTraining);
            //            awarenessTraining = "</i>";
            //        }

            //        var inductionTraining = "Visitor";
            //        switch (perForm.RequestType)
            //        {
            //            case "STW":
            //                inductionTraining = "Short Term Works";
            //                break;
            //            case "PCW":
            //                inductionTraining = "Period Contract Works";
            //                break;
            //            case "MW":
            //                inductionTraining = "Marine Works";
            //                break;
            //        }

            //        var description = $@"
            //            Dear {visitor.Name},
            //            <br /><br />
            //            {requestor.Name} from Southern Seawater Desalination Plant has received management approval for your entry to the Plant as per your agreed time. 
            //            <br /><br />
            //            This calendar entry can be used to help with scheduling of your visit to the Plant. 
            //            <br /><br />
            //            Please see the attached pre-arrival information in preparation for your entry to the Plant. Safety is at the forefront of our minds in everything we do at the Plant so take careful note of this information. 
            //            <br /><br />
            //            <b>Our motto is �No job is so urgent or minor that it cannot be done safely�. Any person at the Plant is authorised to stop any job at any time to review and ensure correct safe job planning. 
            //            <br /><br />
            //            Please assist us in our commitment to safety, in the first instance by planning and communicating well any jobs you are involved with, personally ensuring you have all the necessary and appropriate resources.</b>
            //            <br /><br />
            //            You are required to complete a <i>{inductionTraining}</i> induction{awarenessTraining}. This will be done on site.
            //            <br /><br />
            //            We look forward to seeing you at the Plant. Please feel free to check with {requestor.Name} (Email: <a href=""mailto:{requestor.Email}"">{requestor.Email}</a>, Phone: <a href=""tel:{requestor.Phone}"">{requestor.Phone}</a>) if any clarification is required.
            //            <br /><br />
            //            Regards,
            //            <br /><br />
            //            The Southern Seawater Desalination Plant team 
            //            ";


            //        await emailService.SendAdhocAsync(visitor.Email, "Upcoming SSWA Plant Visit", description, calendarAttachment, pdfAttachment);
            //    }

            //}
            //else
            //{
            //    //await emailService.SendAsync(userNotification);
            //}

        }

        [Fact]
        public void TestApproverCanApproveInMoreThanOneRole()
        {
            var person = new Core.User()
            {
                Id = "test",
                Email = "liamw@promatiq.com",
                Name = "Liam Wallace"
            };

            var person2 = new Core.User()
            {
                Id = "test2",
                Email = "liamw@promatiq.com",
                Name = "Liam Wallace"
            };

            var person3 = new Core.User()
            {
                Id = "test3",
                Email = "liamw@promatiq.com",
                Name = "Liam Wallace"
            };

            var currentUserService = new InMemoryCurrentUserService(person.Id, "paperwork_maintenance_manager");
            var formAuthorisation = new FormAuthorisation(currentUserService);


            var form = new PlantEntryRequestForm()
            {
                Id = "test-id",
                Status = FormState.PENDING_APPROVAL,
                Approvers = new List<FormApproval>()
                {
                    new FormApproval()
                    {
                        User = person,
                        Role = new FormApproval.FormApprovalRole()
                        {
                            Id = "paperwork_maintenance_manager",
                            Name = "Maintenance Manager"
                        },
                        IsApproved = null
                    },
                    new FormApproval()
                    {
                        User = person2,
                        Role = new FormApproval.FormApprovalRole()
                        {
                            Id = "paperwork_operations_manager",
                            Name = "Operations Manager"
                        },
                        IsApproved = true
                    },
                    new FormApproval()
                    {
                        User = person3,
                        Role = new FormApproval.FormApprovalRole()
                        {
                            Id = "paperwork_alliance_manager",
                            Name = "Alliance Manager"
                        },
                        IsApproved = null
                    },
                }

            };

            var result = formAuthorisation.CanApprove(form);

            Assert.True(result.Authorised);
        }

        private static async Task<string> GetPdfAttachment()
        {
            using (var client = new HttpClient())
            {
                var bytes = await client.GetByteArrayAsync("https://promatiqsswa.blob.core.windows.net/assets/00-PM-GUI-1002_Rev%20A%20-%20Plant%20Access%20Pre-Arrival%20Information%20Sheet.pdf");
                return Convert.ToBase64String(bytes);
            }
        }

        private static string GetCalendarAttachment(PlantEntryRequestForm perForm, string summary, string description)
        {
            DateTime StartTime = DateTime.ParseExact(perForm.Visitors.FirstOrDefault()?.ExpectedTime, "HH:mm", CultureInfo.InvariantCulture);

            DateTime DateStart = perForm.StartDate.Value.AddHours(StartTime.Hour).AddMinutes(StartTime.Minute).ToLocalTime();
            DateTime DateEnd = perForm.EndDate.Value.AddHours(StartTime.Hour + 1).AddMinutes(StartTime.Minute).ToLocalTime();
            string Summary = summary;
            string Location = "159 Taranto Road, Binningup WA 6233";
            string Description = description;

            //create a new stringbuilder instance
            var sb = new StringBuilder();

            //start the calendar item
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("VERSION:2.0");
            sb.AppendLine("PRODID:sswa.azurewebsites.net");
            sb.AppendLine("CALSCALE:GREGORIAN");
            sb.AppendLine("METHOD:PUBLISH");

            //create a time zone to be used in the event itself
            sb.AppendLine("BEGIN:VTIMEZONE");
            sb.AppendLine("TZID:Australia/Perth");
            sb.AppendLine("BEGIN:STANDARD");
            sb.AppendLine("TZOFFSETTO:+0800");
            sb.AppendLine("TZOFFSETFROM:+0800");
            sb.AppendLine("END:STANDARD");
            sb.AppendLine("END:VTIMEZONE");

            //add the event
            sb.AppendLine("BEGIN:VEVENT");

            sb.AppendLine($"UID:{Guid.NewGuid()}");
            sb.AppendLine($"DTSTAMP:{DateTime.Now.ToString("yyyyMMddTHHmm00")}");

            //with time zone specified
            sb.AppendLine("DTSTART;TZID=Australia/Perth:" + DateStart.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("DTEND;TZID=Australia/Perth:" + DateEnd.ToString("yyyyMMddTHHmm00"));

            sb.AppendLine("SUMMARY:" + Summary + "");
            sb.AppendLine("LOCATION:" + Location + "");
            sb.AppendLine("DESCRIPTION:" + Description + "");
            sb.AppendLine("END:VEVENT");

            //end calendar item
            sb.AppendLine("END:VCALENDAR");

            var bytes = Encoding.ASCII.GetBytes(sb.ToString());
            return Convert.ToBase64String(bytes);
        }
    }
}
