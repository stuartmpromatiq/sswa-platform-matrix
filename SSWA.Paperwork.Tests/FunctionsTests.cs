
using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Functions;
using Xunit;

namespace SSWA.Paperwork.Tests
{
    public class FunctionsTests
    {
        private readonly ILogger logger = TestFactory.CreateLogger<string>();
        private readonly ExecutionContext context = TestFactory.CreateExecutionContext();

        public FunctionsTests()
        {
            Environment.SetEnvironmentVariable("CosmosDb:Domain", "https://sswa.documents.azure.com:443/");
            Environment.SetEnvironmentVariable("CosmosDb:ApiKey", "J8CxBXKf7rA6s6IsDkbYtnzK13mZPea4vCe7ofl8f5rJVDIOWi39GWVQEEpH6HjQn34z3Ves6bnlYi5RAnBUmQ==");
            Environment.SetEnvironmentVariable("CosmosDb:DatabaseName", "sswa");
            Environment.SetEnvironmentVariable("CosmosDb:CollectionName", "items");
        }

        [Fact]
        public async Task TestAwaitingApproval()
        {
            await MarkFormsAsComplete.Run(null, logger, context);

            Assert.True(false);
        }

        //[Fact]
        //public async Task TestVisitorDuplicates()
        //{
        //    await CleanUpVisitorDuplicates.Run(null, logger, context);

        //    Assert.True(false);
        //}

        //[Fact]
        //public async Task TestVisitorDuplicates()
        //{
        //    await CleanUpVisitorDuplicates.Run(null, logger, context);

        //    Assert.True(false);
        //}

        //[Fact]
        //public async Task TestPCWExpiry()
        //{
        //    await ExpirePCWInductionsWhereLastPERMoreThanTwoMonthsAgo.Run(null, logger, context);

        //    Assert.True(false);
        //}

        [Fact]
        public async Task TestSearchableDescriptions()
        {
            await AddSearchableDescriptions.Run(null, logger, context);

            Assert.True(false);
        }

        [Fact]
        public async Task TestNextVisit()
        {
            await AddNextSiteVisit.Run(null, logger, context);

            Assert.True(false);
        }

        //[Fact]
        //public async Task TestApprovalReminder()
        //{
        //    await SendReminderForUnapprovedForms.Run(null, logger, context);

        //    Assert.True(false);
        //}
    }
}
