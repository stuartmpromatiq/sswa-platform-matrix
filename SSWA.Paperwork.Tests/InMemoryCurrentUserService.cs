﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Tests
{
    public class InMemoryCurrentUserService : ICurrentUserService
    {
        public InMemoryCurrentUserService(string id, params string[] roles)
        {
            Id = id;
            Roles = roles;
        }

        public string Id { get; }

        public string Name => "Test Test";

        public string Email => "test@test.com";

        public string[] Roles { get; }

        public string[] Permissions => new string[] { };

        public bool UserInRole(string role)
        {
            return Roles.Any(x => x == role);
        }

        public bool UserInRoles(params string[] roles)
        {
            foreach (var role in roles)
            {
                if (Roles.Any(x => x == role))
                {
                    return true;
                }
                else
                {
                    continue;
                }
            }

            return false;
        }

        public bool UserHasPermission(string permission)
        {
            return Permissions.Any(x => x == permission);
        }
    }
}
