﻿using Microsoft.Reporting.WebForms;
using SSWA.Reports.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace SSWA.Reports.Web.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly ReportService _reportService;

        public ReportController()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            _reportService = new ReportService(connectionString);
        }

        public async Task<FileResult> Calibration(int id)
        {
            var calibrationReport = await _reportService.GetCalibrationReportAsync(id);
            var calibrationEquipment = await _reportService.GetCalibrationEquipmentAsync(id);

            var report = new LocalReport
            {
                ReportPath = "bin/CalibrationReport_Outer.rdlc",
            };

            var calibrationReportDataSource = new ReportDataSource("Calibration", new List<Core.CalibrationReport>() { calibrationReport });
            var equipmentReportDataSource = new ReportDataSource("Equipment", calibrationEquipment);

            report.DataSources.Add(calibrationReportDataSource);
            report.DataSources.Add(equipmentReportDataSource);

            report.SubreportProcessing += async (object sender, SubreportProcessingEventArgs e) =>
            {
                var subreportType = calibrationReport.CalibrationType.ToUpperInvariant();
                var valuesReportDataSource = new ReportDataSource();

                switch (subreportType)
                {
                    case "STD":
                        valuesReportDataSource = await GetStandardSubreport(id);
                        break;
                    case "CHLORINE":
                        valuesReportDataSource = await GetChlorineSubreport(id);
                        break;
                    case "FLOW":
                        valuesReportDataSource = await GetFlowSubreport(id);
                        break;
                }

                e.DataSources.Add(valuesReportDataSource);
            };

            var deviceInfo =
                "<DeviceInfo>" +
                " <OutputFormat>EMF</OutputFormat>" +
                " <PageWidth>21cm</PageWidth>" +
                " <PageHeight>29.7cm</PageHeight>" +
                " <MarginTop>1cm</MarginTop>" +
                " <MarginLeft>1cm</MarginLeft>" +
                " <MarginRight>1cm</MarginRight>" +
                " <MarginBottom>1cm</MarginBottom>" +
                "</DeviceInfo>";

            var pdf = report.Render("PDF", deviceInfo);
            var contentType = "application/pdf";
            var fileName = calibrationReport.CalibrationRecordNumber + ".pdf";

            return File(pdf, contentType, fileName);
        }

        private async Task<ReportDataSource> GetFlowSubreport(int id)
        {
            var data = await _reportService.GetFlowReportAsync(id);
            return new ReportDataSource("FlowReport", new List<Core.FlowReport>() { data });
        }

        private async Task<ReportDataSource> GetChlorineSubreport(int id)
        {
            var data = await _reportService.GetChlorineReportAsync(id);
            return new ReportDataSource("ChlorineReport", new List<Core.ChlorineReport>() { data });
        }

        private async Task<ReportDataSource> GetStandardSubreport(int id)
        {
            var data = await _reportService.GetStandardReportAsync(id);
            return new ReportDataSource("ChlorineReport", new List<Core.StandardReport>() { data });
        }
    }
}