﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Core.Models
{
    public class Application
    {
        public string Name { get; set; }

        public Uri URL { get; set; }

        public ICollection<ApplicationRole> Roles { get; set; }
    }
}
