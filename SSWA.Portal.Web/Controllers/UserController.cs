﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SSWA.ExternalUserService;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace SSWA.Portal.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        public IConfiguration Configuration { get; private set; }

        public UserController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<IActionResult> Index()
        {
            //var config
            //var userService = new Auth0UserService(
            //    Configuration["Auth0:ClientId"],
            //    Configuration["Auth0:ClientSecret"], 
            //    Configuration["Auth0:Domain"],
            //    Configuration["Auth0:Connection"],
            //    ""
            //);
            //var users = await userService.GetAllUsersAsync();
            return View(User.Claims);
        }
    }
}