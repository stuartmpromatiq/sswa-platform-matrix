﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallibrationRegister.Models
{
    public class Tag
    {
        public string Name { get; set; }

        public string TagType { get; set; }

        public string Fields { get; set; }
    }
}