﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallibrationRegister.Models
{
    public class Field
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public uint SortOrder { get; set; }
        public string DataType { get; set; }
    }
}