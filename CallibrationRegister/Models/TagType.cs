﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallibrationRegister.Models
{
    public class TagType
    {
        public string Name { get; set; }

        public ICollection<Field> TagFields { get; set; }

        public ICollection<Field> CallibrationFields { get; set; }
    }
}