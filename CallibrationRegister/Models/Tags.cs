﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallibrationRegister.Models
{
    public class Tags
    {
        private static List<Tag> _tags = new List<Tag>();

        public static IEnumerable<Tag> Get()
        {
            return _tags;
        }

        public static Tag GetByName(string name)
        {
            return _tags.SingleOrDefault(x => x.Name == name);
        }
        
        public static void Add(Tag tag)
        {
            _tags.Add(tag);
        }
    }
}