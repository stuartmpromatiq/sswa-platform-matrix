﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallibrationRegister.Models
{
    public class TagTypes
    {
        public static IEnumerable<TagType> Get()
        {
            return new List<TagType>()
            {
                new TagType()
                {
                    Name = "PH",
                    TagFields = GetTagFieldsFromJson(),
                    CallibrationFields = GetCallibrationFieldsFromJson()
                },
                new TagType {
                    Name = "FIT",
                    TagFields = GetTagFieldsFromJson2(),
                    CallibrationFields = GetCallibrationFieldsFromJson2()
                }
            };
        }

        private static ICollection<Field> GetTagFieldsFromJson()
        {
            var json = @"
            [
                {
                    ""Id"" : ""tag-field-param-one"",
                    ""DisplayName"" : ""Tag Parameter One"",
                    ""SortOrder"" : 1,
                    ""DataType"" : ""string""
                },
                {
                    ""Id"" : ""tag-field-param-two"",
                    ""DisplayName"" : ""Tag Parameter Two"",
                    ""SortOrder"" : 2,
                    ""DataType"" : ""number""
                },
                {
                    ""Id"" : ""tag-field-param-three"",
                    ""DisplayName"" : ""Tag Field Three"",
                    ""SortOrder"" : 3,
                    ""DataType"" : ""date""
                }
            ]
            ";

            return JsonConvert.DeserializeObject<ICollection<Field>>(json);
        }

        private static ICollection<Field> GetCallibrationFieldsFromJson()
        {
            var json = @"
            [
                {
                    ""Id"" : ""cal-field-calFieldOne"",
                    ""DisplayName"" : ""Cal Field One"",
                    ""SortOrder"" : 1,
                    ""DataType"" : ""string""
                },
                {
                    ""Id"" : ""cal-field-calFieldTwo"",
                    ""DisplayName"" : ""Cal field Two"",
                    ""SortOrder"" : 2,
                    ""DataType"" : ""number""
                },
                {
                    ""Id"" : ""cal-field-calFieldThree"",
                    ""DisplayName"" : ""Cal Field Three"",
                    ""SortOrder"" : 3,
                    ""DataType"" : ""date""
                }
            ]
            ";

            return JsonConvert.DeserializeObject<ICollection<Field>>(json);
        }

        private static ICollection<Field> GetTagFieldsFromJson2()
        {
            var json = @"
            [
                {
                    ""Id"" : ""tag-field-param-one-fit"",
                    ""DisplayName"" : ""Tag Parameter One FIT"",
                    ""SortOrder"" : 1,
                    ""DataType"" : ""string""
                }
            ]
            ";

            return JsonConvert.DeserializeObject<ICollection<Field>>(json);
        }

        private static ICollection<Field> GetCallibrationFieldsFromJson2()
        {
            var json = @"
            [
                {
                    ""Id"" : ""cal-field-fiel-one-fit"",
                    ""DisplayName"" : ""Cal Field One FIT"",
                    ""SortOrder"" : 1,
                    ""DataType"" : ""string""
                }
            ]
            ";

            return JsonConvert.DeserializeObject<ICollection<Field>>(json);
        }
    }
}