﻿using CallibrationRegister.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallibrationRegister.Controllers
{
    public class TagController : Controller
    {
        // GET: Tag
        public ActionResult Index()
        {
            ViewBag.TagTypes = TagTypes.Get();
            var tags = Tags.Get();
            return View(tags);
        }

        public ActionResult Create(string tagTypeName)
        {
            var tagType = TagTypes.Get().SingleOrDefault(x => x.Name == tagTypeName); 
            
            return View(tagType);
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            var tag = new Tag()
            {
                Name = form["name"],
                TagType = form["tagType"]
            };

            var fields = new Dictionary<string, object>();
            foreach (var key in form.AllKeys.Where(x => x.StartsWith("tag-field-")))
            {
                fields.Add(key, form[key]);
            }

            tag.Fields = JsonConvert.SerializeObject(fields);

            Tags.Add(tag);

            return RedirectToAction("Index");
        }
    }
}