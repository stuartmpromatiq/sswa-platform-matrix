﻿using CallibrationRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallibrationRegister.Controllers
{
    public class TagTypeController : Controller
    {
        // GET: TagType
        public ActionResult Index()
        {
            var tagTypes = TagTypes.Get();
            return View(tagTypes);
        }

        // GET: TagType/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TagType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TagType/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TagType/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: TagType/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TagType/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TagType/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
