﻿using Microsoft.Reporting.WebForms;
using SSWA.Reports.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SSWA.Reports.Deploy.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly ReportService _reportService;

        public ReportController()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            _reportService = new ReportService(connectionString);
        }

        public async Task<FileResult> Calibration(int id)
        {
            var calibrationReport = await _reportService.GetCalibrationReportAsync(id);
            var calibrationEquipment = await _reportService.GetCalibrationEquipmentAsync(id);

            var report = new LocalReport
            {
                ReportPath = "bin/CalibrationReport_Outer.rdlc",
            };

            var calibrationReportDataSource = new ReportDataSource("Calibration", new List<CalibrationReport>() { calibrationReport });
            var equipmentReportDataSource = new ReportDataSource("Equipment", calibrationEquipment);

            var valuesReportDataSource = new ReportDataSource();
            var subreportType = calibrationReport.CalibrationType.ToUpperInvariant();
            switch (subreportType)
            {
                case "STD":
                    valuesReportDataSource = await GetStandardSubreport(id, calibrationReport);
                    report.DataSources.Add(new ReportDataSource("ChlorineReport", new List<ChlorineReport>()));
                    report.DataSources.Add(new ReportDataSource("FlowReport", new List<FlowReport>()));
                    break;
                case "CHLORINE":
                    valuesReportDataSource = await GetChlorineSubreport(id, calibrationReport);
                    report.DataSources.Add(new ReportDataSource("FlowReport", new List<FlowReport>()));
                    report.DataSources.Add(new ReportDataSource("StandardReport", new List<StandardReport>()));
                    break;
                case "FLOW":
                    valuesReportDataSource = await GetFlowSubreport(id, calibrationReport);
                    report.DataSources.Add(new ReportDataSource("ChlorineReport", new List<ChlorineReport>()));
                    report.DataSources.Add(new ReportDataSource("StandardReport", new List<StandardReport>()));
                    break;
            }

            report.DataSources.Add(calibrationReportDataSource);
            report.DataSources.Add(equipmentReportDataSource);
            report.DataSources.Add(valuesReportDataSource);

            //report.SubreportProcessing += async (object sender, SubreportProcessingEventArgs e) =>
            //{
            //    //var subreportType = calibrationReport.CalibrationType.ToUpperInvariant();
            //    var subreportPath = e.ReportPath.ToLowerInvariant();

            //    //if (subreportType == "STD" && !subreportPath.Contains("standard")) return;
            //    //if (subreportType == "CHLORINE" && !subreportPath.Contains("chlorine")) return;
            //    //if (subreportType == "FLOW" && !subreportPath.Contains("flow")) return;

            //    //var valuesReportDataSource = new ReportDataSource();

            //    switch (subreportType)
            //    {
            //        case "STD":
            //            valuesReportDataSource = await GetStandardSubreport(id);
            //            break;
            //        case "CHLORINE":
            //            valuesReportDataSource = await GetChlorineSubreport(id);
            //            break;
            //        case "FLOW":
            //            valuesReportDataSource = await GetFlowSubreport(id);
            //            break;
            //    }

            //    e.DataSources.Add(valuesReportDataSource);
            //};

            var deviceInfo =
                "<DeviceInfo>" +
                " <OutputFormat>EMF</OutputFormat>" +
                " <PageWidth>21cm</PageWidth>" +
                " <PageHeight>29.7cm</PageHeight>" +
                " <MarginTop>1cm</MarginTop>" +
                " <MarginLeft>1cm</MarginLeft>" +
                " <MarginRight>1cm</MarginRight>" +
                " <MarginBottom>1cm</MarginBottom>" +
                "</DeviceInfo>";

            var pdf = report.Render("PDF", deviceInfo);
            var contentType = "application/pdf";
            var fileName = calibrationReport.CalibrationRecordNumber + ".pdf";

            return File(pdf, contentType, fileName);
        }

        private async Task<ReportDataSource> GetFlowSubreport(int id, CalibrationReport report)
        {
            var data = await _reportService.GetFlowReportAsync(id);
            var dataSource = new ReportDataSource("FlowReport", new List<FlowReport>() { data });

            report.CalibrationResult =
                data.PassFailZero &&
                data.PassFailA &&
                data.PassFailB &&
                data.PassFailC &&
                data.PassFailD;

            return dataSource;
        }

        private async Task<ReportDataSource> GetChlorineSubreport(int id, CalibrationReport report)
        {
            var data = await _reportService.GetChlorineReportAsync(id);

            report.CalibrationResult =
                data.PassFailSetPointOne &&
                data.PassFailSetPointTwo &&
                data.PassFailSetPointThree &&
                data.PassFailSetPointFour &&
                data.PassFailSetPointFive &&
                data.PassFailSetPointSix &&
                data.PassFailSetPointSeven &&
                data.PassFailSetPointEight &&
                data.PassFailSetPointNine &&
                data.PassFailSetPointTen &&
                data.PassFailSetPointEleven;

            return new ReportDataSource("ChlorineReport", new List<ChlorineReport>() { data });
        }

        private async Task<ReportDataSource> GetStandardSubreport(int id, CalibrationReport report)
        {
            var data = await _reportService.GetStandardReportAsync(id);

            report.CalibrationResult =
                data.PassFail;

            return new ReportDataSource("StandardReport", new List<StandardReport>() { data });
        }
    }
}