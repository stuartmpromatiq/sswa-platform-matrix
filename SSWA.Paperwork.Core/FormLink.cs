﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class FormLink
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("completed")]
        public bool Completed { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("formId")]
        public string FormId { get; set; }

        public string FormState { get; set; }
    }
}
