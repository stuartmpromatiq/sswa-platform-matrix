﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class FormUserAction
    {
        public FormUserAction()
        {
            User = new User();
        }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("isApproved")]
        public bool? IsApproved { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("actionedAt")]
        public DateTime? ActionedAt { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
