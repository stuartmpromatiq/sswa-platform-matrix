﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public interface IFormAuthorisation
    {
        AuthorisationReponse CanApprove(IForm form);
        AuthorisationReponse CanAuthoriseCancellation(IForm form);
        AuthorisationReponse CanAuthoriseCompletion(IForm form);
        AuthorisationReponse CanAuthoriseRevalidation(IForm form);
        AuthorisationReponse CanAuthoriseSuspension(IForm form);
        AuthorisationReponse CanAuthoriseTestRun(IForm form);
        AuthorisationReponse CanAuthoriseTransfer(IForm form);
        AuthorisationReponse CanCancel(IForm form);
        AuthorisationReponse CanClose(IForm form);
        AuthorisationReponse CanComplete(IForm form);
        AuthorisationReponse CanDeny(IForm form);
        AuthorisationReponse CanEdit(IForm form);
        AuthorisationReponse CanOpen(IForm form);
        AuthorisationReponse CanRequestAuthorisation(IForm form);
        AuthorisationReponse CanRequestCancellation(IForm form);
        AuthorisationReponse CanRequestCompletion(IForm form);
        AuthorisationReponse CanRequestMoreInformation(IForm form);
        AuthorisationReponse CanRequestRevalidation(IForm form);
        AuthorisationReponse CanRequestSuspension(IForm form);
        AuthorisationReponse CanRequestTestRun(IForm form);
        AuthorisationReponse CanRequestTransfer(IForm form);
        AuthorisationReponse CanSubmit(IForm form);
        AuthorisationReponse CanTransfer(IForm form);
    }
}
