using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace SSWA.Paperwork.Core
{
    public interface IForm : IEntity
    {
        [JsonProperty("kind")]
        FormKind Kind { get; }

        [JsonProperty("description")]
        string Description { get; }

        [JsonProperty("canonicalDescription")]
        string CanonicalDescription { get; }

        [JsonProperty("friendlyId")]
        string FriendlyId { get; set; }

        [JsonProperty("revision")]
        int Revision { get; set; }

        [JsonProperty("startDate")]
        DateTime? StartDate { get; set; }

        [JsonProperty("endDate")]
        DateTime? EndDate { get; set; }

        [JsonProperty("status")]
        string Status { get; set; }

        User RequestedBy { get; set; }

        [JsonProperty("approvers")]
        List<FormApproval> Approvers { get; set; }
        List<FormLink> LinkedForms { get; set; }

        [JsonProperty("allowedUsers")]
        List<string> AllowedUsers { get; set; }

        bool CanSee { get; set; }
        bool CanEdit { get; set; }
        bool CanSubmit { get; set; }
        bool CanRequestAuthorisation { get; set; }
        bool CanApprove { get; set; }
        bool CanDeny { get; set; }
        bool CanCancel { get; set; }
        bool CanRequestInfo { get; set; }
        bool CanComplete { get; set; }

        bool CanRequestTransfer { get; set; }
        bool CanAuthoriseTransfer { get; set; }

        bool CanRequestSuspension { get; set; }
        bool CanAuthoriseSuspension { get; set; }

        bool CanRequestRevalidation { get; set; }
        bool CanAuthoriseRevalidation { get; set; }

        bool CanRequestCompletion { get; set; }
        bool CanAuthoriseCompletion { get; set; }

        bool CanRequestTestRun { get; set; }
        bool CanAuthoriseTestRun { get; set; }

        bool CanRequestCancellation { get; set; }
        bool CanAuthoriseCancellation { get; set; }

        bool CanOpen { get; set; }

        bool CanClose { get; set; }

        IEnumerable<User> GetApprovers();
        IEnumerable<User> GetUsersToNotifyOnSubmissionForApproval();
        IEnumerable<User> GetUsersToNotifyOnApproval();
        IEnumerable<User> GetUsersToNotifyOnDenial();
        IEnumerable<User> GetUsersToNotifyOnPartialApproval();
    }

    public interface IFormTemplate : IForm
    {
        [JsonProperty("lookup")]
        TemplateLookup Lookup { get; }
    }


    //private enum DocumentState { New, Draft, AwaitingApproval, PartiallyApproved, Approved, Cancelled, InformationRequired, Completed }
    //private enum DocumentTrigger { Create, Review, SubmitForApproval, Approve, Deny, Cancel, Complete }

    //private StateMachine<DocumentState, DocumentTrigger> _stateMachine;

    //_stateMachine = new StateMachine<DocumentState, DocumentTrigger>(() => State, s => State = s);

    //_stateMachine.Configure(DocumentState.New)
    //             .Permit(DocumentTrigger.Create, DocumentState.Draft);

    //_stateMachine.Configure(DocumentState.Draft)
    //             .PermitIf(DocumentTrigger.SubmitForApproval, DocumentState.AwaitingApproval, () => true)
    //             .Permit(DocumentTrigger.Cancel, DocumentState.Cancelled);

    //_stateMachine.Configure(DocumentState.AwaitingApproval)
    //             .PermitIf(DocumentTrigger.Approve, DocumentState.PartiallyApproved)
    //             .PermitIf(DocumentTrigger.Approve, DocumentState.Approved)
    //             .Permit(DocumentTrigger.Deny, DocumentState.InformationRequired)
    //             .Permit(DocumentTrigger.Review, DocumentState.Draft)
    //             .Permit(DocumentTrigger.Cancel, DocumentState.Cancelled);

    //_stateMachine.Configure(DocumentState.PartiallyApproved)
    //             .PermitReentryIf(DocumentTrigger.Approve, () => true)
    //             .PermitIf(DocumentTrigger.Approve, DocumentState.Approved)
    //             .Permit(DocumentTrigger.Deny, DocumentState.InformationRequired)
    //             .Permit(DocumentTrigger.Review, DocumentState.Draft)
    //             .Permit(DocumentTrigger.Cancel, DocumentState.Cancelled);

    //_stateMachine.Configure(DocumentState.InformationRequired)
    //             .PermitIf(DocumentTrigger.SubmitForApproval, DocumentState.AwaitingApproval, () => true)
    //             .Permit(DocumentTrigger.Review, DocumentState.Draft)
    //             .Permit(DocumentTrigger.Cancel, DocumentState.Cancelled);

    //_stateMachine.Configure(DocumentState.Approved)
    //             .Permit(DocumentTrigger.Complete, DocumentState.Completed)
    //             .Permit(DocumentTrigger.Review, DocumentState.Draft)
    //             .Permit(DocumentTrigger.Cancel, DocumentState.Cancelled);
}


