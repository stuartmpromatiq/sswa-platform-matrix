﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class FormKind
    {
        [JsonConstructor]
        public FormKind(string code, string name)
        {
            Code = code;
            Name = name;
            Slug = name.Trim().ToLowerInvariant().Replace(' ', '-');
        }

        [JsonProperty("code")]
        public string Code { get; private set; }

        [JsonProperty("slug")]
        public string Slug { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }
    }
}
