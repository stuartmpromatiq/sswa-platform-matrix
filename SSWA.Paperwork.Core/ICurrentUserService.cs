﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public interface ICurrentUserService
    {
        string Id { get; }
        string Name { get; }
        string Email { get; }
        string[] Roles { get; }
        string[] Permissions { get; }

        bool UserInRole(string role);

        bool UserInRoles(params string[] roles);
        bool UserHasPermission(string permission);
    }
}
