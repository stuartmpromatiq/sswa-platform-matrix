﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class Roles
    {
        public const string ADMIN = "paperwork_admin";
        public const string EDITOR = "paperwork_editior";
        public const string ALLIANCE_MANAGER = "paperwork_alliance_manager";
        public const string OPERATIONS_MANAGER = "paperwork_operations_manager";
        public const string MAINTENANCE_MANAGER = "paperwork_maintenance_manager";
        public const string PERMIT_AUTHORISER = "paperwork_permit_authoriser";
        public const string PERMIT_ACCEPTOR = "paperwork_permit_acceptor";
    }
}
