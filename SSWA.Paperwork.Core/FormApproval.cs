﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class FormApproval
    {
        [JsonProperty("role")]
        public FormApprovalRole Role { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("isApproved")]
        public bool? IsApproved { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("actionedAt")]
        public DateTime? ActionedAt { get; set; }

        public class FormApprovalRole
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }
        }
    }
}
