﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public static class FormState
    {
        public const string DRAFT = "draft";
        public const string PENDING_APPROVAL = "pending approval";
        public const string PENDING_AUTHORISATION = "pending authorisation";
        public const string PENDING_TRANSFER = "pending transfer";
        public const string PENDING_REVALIDATION = "pending revalidation";
        public const string PENDING_SUSPENSION = "pending suspension";
        public const string PENDING_COMPLETION = "pending completion";
        public const string PENDING_TEST_RUN = "pending test run";
        public const string PENDING_CANCELLATION = "pending cancellation";
        public const string PARTIALLY_APPROVED = "partially approved";
        public const string CANCELLED = "cancelled";
        public const string INFORMATION_REQUIRED = "information required";
        public const string DENIED = "denied";
        public const string REJECTED = "rejected";
        public const string APPROVED = "approved";
        public const string PENDING_OPEN = "pending open";
        public const string OPENED = "opened";
        public const string COMPLETED = "completed";
        public const string SUSPENDED = "suspended";
        public const string TRANSFERRED = "transferred";
        public const string CLOSED = "closed";
        public const string TESTING = "testing";
    }
}
