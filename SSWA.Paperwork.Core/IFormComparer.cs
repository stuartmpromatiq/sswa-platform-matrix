﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class IFormComparer : IEqualityComparer<IForm>
    {
        public bool Equals(IForm x, IForm y)
        {
            return x.Id == y.Id && x.ETag == y.ETag;
        }

        public int GetHashCode(IForm obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
