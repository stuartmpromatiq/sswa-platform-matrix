using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Core
{
    public class AuthorisationReponse
    {
        public bool Authorised { get; }

        public string Message { get; }

        public AuthorisationReponse(bool authorised, string message)
        {
            Authorised = authorised;
            Message = message;
        }
    }

}
