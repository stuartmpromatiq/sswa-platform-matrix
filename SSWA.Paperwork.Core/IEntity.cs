using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SSWA.Paperwork.Core
{
    public interface IEntity
    {
        string Id { get; set; }

        [JsonProperty("type")]
        string Type { get; }

        [JsonProperty("createdAt")]
        DateTime? CreatedAt { get; set; }

        [JsonProperty("createdBy")]
        User CreatedBy { get; set; }

        [JsonProperty("updatedAt")]
        DateTime? UpdatedAt { get; set; }

        [JsonProperty("updatedBy")]
        User UpdatedBy { get; set; }

        [JsonProperty("_etag")]
        string ETag { get; set; }

        [JsonProperty("inactive")]
        bool Inactive { get; set; }
    }
}
