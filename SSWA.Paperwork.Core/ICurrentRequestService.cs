﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public interface ICurrentRequestService
    {
        string Domain { get; }
    }
}
