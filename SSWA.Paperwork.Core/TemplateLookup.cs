﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Core
{
    public class TemplateLookup
    {
        [JsonProperty("level1")]
        public string Level1 { get; set; }

        [JsonProperty("level2")]
        public string Level2 { get; set; }

        [JsonProperty("level3")]
        public string Level3 { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
