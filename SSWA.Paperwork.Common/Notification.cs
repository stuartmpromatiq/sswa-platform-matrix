using Newtonsoft.Json;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Common
{
    public class Notification : BaseEntity, IEntity
    {
        public override string Type => "notification";

        [JsonProperty("recipientId")]
        public string RecipientId { get; set; }

        [JsonProperty("recipient")]
        public User Recipient { get; set; }
        public string Subject { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
        public NotificationAction Action { get; set; }

        [JsonProperty("seen")]
        public bool Seen { get; set; }

        public class NotificationAction
        {
            public string Display { get; set; }
            public string Domain { get; set; }
            public string Path { get; set; }
        }
    }

}
