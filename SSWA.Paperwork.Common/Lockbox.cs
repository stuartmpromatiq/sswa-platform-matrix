﻿using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common
{
    public class Lockbox : BaseEntity, IEntity
    {
        public override string Type => "lockbox";

        public string FormId { get; set; }

        public string FormSlug { get; set; }

        public string FriendlyId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Status { get; set; }

        public string Number { get; set; }
    }
}
