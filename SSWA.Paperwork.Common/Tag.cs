﻿using Newtonsoft.Json;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common
{
    public class Tag : BaseEntity, IEntity
    {
        [JsonProperty("type")]
        public override string Type => "tag";

        [JsonProperty("area")]
        public string Area { get; set; }

        [JsonProperty("pid")]
        public string PID { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("inum")]
        public string Inum { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
    }
}
