﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common
{
    public class PersonTraining
    {
        public string TrainingId { get; set; }
        public string TrainingName { get; set; }
        public string Type { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
