﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common
{
    public class PersonSubcategory : BaseEntity, IEntity
    {
        public override string Type => "person_subcategory";

        public string Name { get; set; }

        [JsonProperty("canonicalName")]
        public string CanonicalName => Name?.ToLowerInvariant()?.Trim();
    }
}
