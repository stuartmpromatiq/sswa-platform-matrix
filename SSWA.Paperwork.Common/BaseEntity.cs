﻿using Newtonsoft.Json;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common
{
    public abstract class BaseEntity : IEntity
    {

        public BaseEntity()
        {
            CreatedBy = new User();
            UpdatedBy = new User();
        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public abstract string Type { get; }

        [JsonProperty("createdAt")]
        public DateTime? CreatedAt { get; set; }

        [JsonProperty("createdBy")]
        public User CreatedBy { get; set; }

        [JsonProperty("updatedAt")]

        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("updatedBy")]

        public User UpdatedBy { get; set; }

        [JsonProperty("_etag")]
        public string ETag { get; set; }

        [JsonProperty("inactive")]
        public bool Inactive { get; set; }
    }
}
