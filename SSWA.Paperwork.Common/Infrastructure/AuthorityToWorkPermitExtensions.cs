﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static SSWA.Paperwork.Common.Forms.AuthorityToWorkForm;

namespace SSWA.Paperwork.Common.Infrastructure
{
    public static class AuthorityToWorkPermitExtensions
    {
        public static string GetLockbox(this IEnumerable<AuthorityToWorkPermit> permits)
        {
            return permits.FirstOrDefault(x => x.Id == "ISO")?.Extras?.FirstOrDefault(x => x.Name == "Lockbox Number")?.Value;
        }
    }
}
