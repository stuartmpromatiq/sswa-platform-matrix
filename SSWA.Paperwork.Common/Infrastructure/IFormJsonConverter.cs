using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Common.Infrastructure
{
    public abstract class JsonCreationConverter<T> : JsonConverter
    {
        protected abstract T Create(Type objectType, JObject jObject);

        public override bool CanConvert(Type objectType)
        {
            return typeof(T) == objectType;
        }

        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            try
            {
                var jObject = JObject.Load(reader);
                var target = Create(objectType, jObject);
                serializer.Populate(jObject.CreateReader(), target);
                return target;
            }
            catch (JsonReaderException)
            {
                return null;
            }
        }

        public override void WriteJson(JsonWriter writer, object value,
            JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    public class FormConverter : JsonCreationConverter<IForm>
    {
        protected override IForm Create(Type objectType, JObject jObject)
        {
            var form = default(IForm);

            if (jObject["type"].ToString() == "form_template")
            {
                switch (jObject["kind"].ToObject<FormKind>().Code)
                {
                    case "ISO":
                        form = new IsolationPermitTemplate();
                        break;
                }
            }
            else if (jObject["type"].ToString() == "form")
            {
                switch (jObject["kind"].ToObject<FormKind>().Code)
                {
                    case "PER":
                        form = new PlantEntryRequestForm();
                        break;
                    case "PWR":
                        form = new PlantWorksRequestForm();
                        break;
                    case "ATW":
                        form = new AuthorityToWorkForm();
                        break;
                    case "ISO":
                        form = new IsolationPermit();
                        break;
                    case "WAH":
                        form = new WorkingAtHeightsPermit();
                        break;
                    case "CSE":
                        form = new ConfinedSpaceEntryPermit();
                        break;
                    case "EXP":
                        form = new ExcavationPermit();
                        break;
                    case "HV":
                        form = new HighVoltagePermit();
                        break;
                }
            }
            

            return form;
        }
    }

    public class FormTemplateConverter : JsonCreationConverter<IFormTemplate>
    {
        protected override IFormTemplate Create(Type objectType, JObject jObject)
        {
            var form = default(IFormTemplate);

            switch (jObject["kind"].ToObject<FormKind>().Code)
            {
                case "ISO":
                    form = new IsolationPermitTemplate();
                    break;
            }


            return form;
        }
    }
}
