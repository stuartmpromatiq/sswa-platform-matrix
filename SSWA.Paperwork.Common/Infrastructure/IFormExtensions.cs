using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Infrastructure.Extensions
{
    public static class IFormExtensions
    {
        public static bool In(this IForm form, params string[] states)
        {
            foreach (var state in states)
            {
                if (form.Status == state)
                    return true;
            }
            return false;
        }

        public static bool NotIn(this IForm form, params string[] states)
        {
            foreach (var state in states)
            {
                if (form.Status == state)
                    return false;
            }
            return true;
        }
    }
}
