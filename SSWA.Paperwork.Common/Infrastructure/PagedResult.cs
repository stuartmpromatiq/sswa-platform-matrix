﻿using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Infrastructure
{
    public class PagedResult<T> where T : IEntity
    {

        public PagedResult(IEnumerable<T> data, string nextPageToken)
        {
            Data = data;
            NextPageToken = nextPageToken;
        }

        public PagedResult(List<IEntity> list, string nextPageToken)
        {
            List<T> data = new List<T>();
            foreach (var item in list)
            {
                data.Add((T)item);
            }
            Data = data;
            NextPageToken = nextPageToken;
        }

        public string NextPageToken { get; private set; }
        public IEnumerable<T> Data { get; private set; }
        public bool HasMoreData => !string.IsNullOrWhiteSpace(NextPageToken);

        public static implicit operator PagedResult<T>(PagedResult<Forms.AuthorityToWorkForm> v)
        {
            var list = new List<IEntity>();

            foreach (var item in v.Data)
            {
                list.Add(item);
            }

            var result = new PagedResult<T>(list, v.NextPageToken);
            return result;
        }

        public static implicit operator PagedResult<T>(PagedResult<Forms.PlantEntryRequestForm> v)
        {
            var list = new List<IEntity>();

            foreach (var item in v.Data)
            {
                list.Add(item);
            }

            var result = new PagedResult<T>(list, v.NextPageToken);
            return result;
        }

        public static implicit operator PagedResult<T>(PagedResult<Forms.PlantWorksRequestForm> v)
        {
            var list = new List<IEntity>();

            foreach (var item in v.Data)
            {
                list.Add(item);
            }

            var result = new PagedResult<T>(list, v.NextPageToken);
            return result;
        }

    }
}
