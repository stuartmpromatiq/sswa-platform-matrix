using MediatR;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Common.Events
{
    public class FormRequiresInformationEvent : INotification
    {
        public IForm Form { get; }
        public FormRequiresInformationEvent(IForm form)
        {
            Form = form;
        }

    }
}
