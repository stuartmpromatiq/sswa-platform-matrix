using MediatR;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Events
{
    public class FormApprovedEvent : INotification
    {
        public IForm Form { get; }
        public FormApprovedEvent(IForm form)
        {
            Form = form;
        }

    }
}
