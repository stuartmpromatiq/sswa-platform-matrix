using MediatR;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Events
{
    public class FormSubmittedEvent : INotification
    {
        public IForm Form { get; }
        public FormSubmittedEvent(IForm form)
        {
            Form = form;
        }

    }
}
