using MediatR;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Events
{
    public class FormPartiallyApprovedEvent : INotification
    {
        public IForm Form { get; }
        public FormPartiallyApprovedEvent(IForm form)
        {
            Form = form;
        }

    }
}

