using MediatR;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Events
{
    public class FormDeniedEvent : INotification
    {
        public IForm Form { get; }
        public FormDeniedEvent(IForm form)
        {
            Form = form;
        }

    }
}
