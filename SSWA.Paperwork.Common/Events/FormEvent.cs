﻿using MediatR;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Events
{
    public class FormEvent : INotification
    {
        public IForm Form { get; }
        public FormEvent(IForm form)
        {
            Form = form;
        }
    }

    public class FormPendingTransferEvent : FormEvent
    {
        public FormPendingTransferEvent(IForm form) : base(form)
        {
        }
    }

    public class FormTransferredEvent : FormEvent
    {
        public FormTransferredEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingSuspensionEvent : FormEvent
    {
        public FormPendingSuspensionEvent(IForm form) : base(form)
        {
        }
    }

    public class FormSuspendedEvent : FormEvent
    {
        public FormSuspendedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingRevalidationEvent : FormEvent
    {
        public FormPendingRevalidationEvent(IForm form) : base(form)
        {
        }
    }

    public class FormRevalidatedEvent : FormEvent
    {
        public FormRevalidatedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingAuthorisationEvent : FormEvent
    {
        public FormPendingAuthorisationEvent(IForm form) : base(form)
        {
        }
    }

    public class FormAuthorisedEvent : FormEvent
    {
        public FormAuthorisedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormOpenedEvent : FormEvent
    {
        public FormOpenedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormClosedEvent : FormEvent
    {
        public FormClosedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingCancellationEvent : FormEvent
    {
        public FormPendingCancellationEvent(IForm form) : base(form)
        {
        }
    }

    public class FormCancelledEvent : FormEvent
    {
        public FormCancelledEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingCompletionEvent : FormEvent
    {
        public FormPendingCompletionEvent(IForm form) : base(form)
        {
        }
    }

    public class FormCompletedEvent : FormEvent
    {
        public FormCompletedEvent(IForm form) : base(form)
        {
        }
    }

    public class FormPendingTestRunEvent : FormEvent
    {
        public FormPendingTestRunEvent(IForm form) : base(form)
        {
        }
    }

    public class FormTestRunningEvent : FormEvent
    {
        public FormTestRunningEvent(IForm form) : base(form)
        {
        }
    }

}
