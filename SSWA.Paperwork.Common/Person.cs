﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common
{
    public class Person : BaseEntity, IEntity
    {
        public Person()
        {
            LatestTraining = new List<PersonTraining>();
            ArchivedTraining = new List<PersonTraining>();
        }

        public override string Type => "person";

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        public string Email { get; set; }

        public string ContactNumber { get; set; }

        public string VehicleRegistration { get; set; }

        public List<PersonTraining> LatestTraining { get; set; }

        public List<PersonTraining> ArchivedTraining { get; set; }

        [JsonProperty("lastVisit")]
        public DateTime? LastVisit { get; set; }

        [JsonProperty("nextVisit")]
        public DateTime? NextVisit { get; set; }

        public string NextPerId { get; set; }

        public string NextPerFriendlyId { get; set; }

        [JsonProperty("canonicalName")]
        public string CanonicalName => $"{Name?.ToLowerInvariant()?.Trim()}";

        [JsonProperty("canonicalCategory")]
        public string CanonicalCategory => Category?.ToLowerInvariant()?.Trim();

        [JsonProperty("canonicalCompany")]
        public string CanonicalCompany => Company?.ToLowerInvariant()?.Trim();


        public void AddTraining(PersonTraining training)
        {
            var existingTraining = this.LatestTraining.FirstOrDefault(x => x.Type == training.Type && x.TrainingName == training.TrainingName);

            if (existingTraining != null)
            {
                this.LatestTraining.Remove(existingTraining);
                this.ArchivedTraining.Add(existingTraining);
            }

            this.LatestTraining.Add(training);
        }

        public bool CheckInduction(PlantEntryRequestForm form)
        {
            if (this.LatestTraining.Any(x =>
               x.Type == "Induction" &&
               x.TrainingName == form.RequestType &&
               x.StartDate.GetValueOrDefault(DateTime.MinValue).Date <= form.StartDate.GetValueOrDefault(DateTime.MaxValue).Date &&
               x.ExpiryDate.GetValueOrDefault(DateTime.MinValue).Date >= form.StartDate.GetValueOrDefault(DateTime.MaxValue).Date))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckAwareness(PlantEntryRequestForm form, string awareness)
        {
            if (this.LatestTraining.Any(x => x.Type == "Awareness" && x.TrainingName == awareness))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
