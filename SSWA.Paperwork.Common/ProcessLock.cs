﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common
{
    public class ProcessLock : BaseEntity, IEntity
    {
        public ProcessLock()
        {
            LockPlacedBy = new FormUserAction();
            LockRemovedBy = new FormUserAction();
        }

        public override string Type => "process_lock";

        [JsonProperty("friendlyId")]
        public string FriendlyId { get; set; }

        [JsonProperty("tagNumber")]
        public string TagNumber { get; set; }

        public string Location { get; set; }

        [JsonProperty("canonicalLocation")]
        public string CanonicalLocation
        {
            get
            {
                var rgx = new Regex("[^a-zA-z0-9]");
                return rgx.Replace(Location ?? "", "")?.ToLowerInvariant();
            }
        }

        public string InfoTagPlaced { get; set; }

        public string Reason { get; set; }

        [JsonProperty("canonicalReason")]
        public string CanonicalReason
        {
            get
            {
                var rgx = new Regex("[^a-zA-z0-9]");
                return rgx.Replace(Reason ?? "", "")?.ToLowerInvariant();
            }
        }

        [JsonProperty("lockPlacedBy")]
        public FormUserAction LockPlacedBy { get; set; }

        [JsonProperty("lockRemovedBy")]
        public FormUserAction LockRemovedBy { get; set; }

        [JsonProperty("uniqueLockNumber")]
        public string UniqueLockNumber { get; set; }

        public string NOE { get; set; }

        public string EquipmentStatus { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        public bool PlaceLock(User user)
        {
            LockPlacedBy.User = user;
            LockPlacedBy.ActionedAt = DateTime.UtcNow;

            return true;
        }

        public bool RemoveLock(User user)
        {
            LockRemovedBy.User = user;
            LockRemovedBy.ActionedAt = DateTime.UtcNow;

            return true;
        }

        public string GetNextId(IEnumerable<string> currentIds)
        {
            var currentMaxId = currentIds.Select(x => x?.PadLeft(4, '0')).Max(); 
            var number = Int32.Parse(currentMaxId.Replace("PL", "")) + 1;

            return "PL" + number.ToString().PadLeft(4, '0');
        }
    }
}
