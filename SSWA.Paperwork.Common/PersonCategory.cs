﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common
{
    public class PersonCategory : BaseEntity, IEntity
    {
        public override string Type => "person_category";

        public string Name { get; set; }

        [JsonProperty("canonicalName")]
        public string CanonicalName => Name?.ToLowerInvariant()?.Trim();
    }
}
