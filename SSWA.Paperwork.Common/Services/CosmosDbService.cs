using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Azure.Documents.SystemFunctions;
using Microsoft.Extensions.Caching.Memory;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Services
{
    public class CosmodDbOptions
    {
        public CosmodDbOptions(string dbName, string collectionName)
        {
            DatabaseName = dbName;
            CollectionName = collectionName;

        }

        public string DatabaseName { get; private set; }
        public string CollectionName { get; private set; }
    }
    public class CosmosDbService<T> where T : IEntity
    {
        private DocumentClient _client;
        private readonly IMemoryCache _cache;
        private readonly string _databaseName = "sswa";
        private readonly string _collectionName = "items";

        public CosmosDbService(DocumentClient client, CosmodDbOptions options, IMemoryCache cache)
        {
            _client = client;
            _cache = cache;
            _databaseName = options.DatabaseName;
            _collectionName = options.CollectionName;
        }

        public async Task<T> GetAsync(String id)
        {
            Uri documentUri = UriFactory.CreateDocumentUri(_databaseName, _collectionName, id);

            return await _client.ReadDocumentAsync<T>(documentUri);
        }

        public async Task<IEnumerable<T>> SqlQueryAsync(string sql, params KeyValuePair<string, object>[] sqlParams)
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions { MaxItemCount = 100, EnableCrossPartitionQuery = false };

            var sqlQuery = new SqlQuerySpec()
            {
                QueryText = sql
            };


            foreach (var sqlParam in sqlParams)
            {
                sqlQuery.Parameters.Add(new SqlParameter("@" + sqlParam.Key, sqlParam.Value));
            }

            var query = _client.CreateDocumentQuery<T>(collectionUri, sqlQuery, options);

            var docQuery = query.AsDocumentQuery();

            // tell server we only want 1 record
            var items = new List<T>();

            while (docQuery.HasMoreResults)
            {
                foreach (var item in await docQuery.ExecuteNextAsync<T>())
                {
                    items.Add(item);
                }
            }

            return items;

        }

        public async Task<IEnumerable<T>> SqlQueryAsync(string type, string sql, params KeyValuePair<string, object>[] sqlParams)
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions { MaxItemCount = 100, EnableCrossPartitionQuery = false };

            var sqlQuery = new SqlQuerySpec()
            {
                QueryText = sql,
                Parameters = new SqlParameterCollection()
                {
                    new SqlParameter("@type", type)
                }
            };


            foreach (var sqlParam in sqlParams)
            {
                sqlQuery.Parameters.Add(new SqlParameter("@" + sqlParam.Key, sqlParam.Value));
            }

            var query = _client.CreateDocumentQuery<T>(collectionUri, sqlQuery, options);

            var docQuery = query.AsDocumentQuery();

            // tell server we only want 1 record
            var items = new List<T>();

            while (docQuery.HasMoreResults)
            {
                foreach (var item in await docQuery.ExecuteNextAsync<T>())
                {
                    items.Add(item);
                }
            }

            return items;

        }

        public async Task<U> SqlQuerySingleAsync<U>(string type, string sql, params KeyValuePair<string, object>[] sqlParams)
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions { MaxItemCount = 100, EnableCrossPartitionQuery = false };

            var sqlQuery = new SqlQuerySpec()
            {
                QueryText = sql,
                Parameters = new SqlParameterCollection()
                {
                    new SqlParameter("@type", type)
                }
            };


            foreach (var sqlParam in sqlParams)
            {
                sqlQuery.Parameters.Add(new SqlParameter("@" + sqlParam.Key, sqlParam.Value));
            }

            var query = _client.CreateDocumentQuery<U>(collectionUri, sqlQuery, options);

            var docQuery = query.AsDocumentQuery();

            // tell server we only want 1 record
            var items = new List<U>();

            while (docQuery.HasMoreResults)
            {
                foreach (var item in await docQuery.ExecuteNextAsync<U>())
                {
                    items.Add(item);
                }
            }

            return items.FirstOrDefault();

        }

        public async Task<IEnumerable<U>> SqlQueryAsync<U>(string type, string sql, params KeyValuePair<string, object>[] sqlParams)
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions { MaxItemCount = 100, EnableCrossPartitionQuery = false };

            var sqlQuery = new SqlQuerySpec()
            {
                QueryText = sql,
                Parameters = new SqlParameterCollection()
                {
                    new SqlParameter("@type", type)
                }
            };


            foreach (var sqlParam in sqlParams)
            {
                sqlQuery.Parameters.Add(new SqlParameter("@" + sqlParam.Key, sqlParam.Value));
            }

            var query = _client.CreateDocumentQuery<U>(collectionUri, sqlQuery, options);

            var docQuery = query.AsDocumentQuery();

            // tell server we only want 1 record
            var items = new List<U>();

            while (docQuery.HasMoreResults)
            {
                try
                {
                    foreach (var item in await docQuery.ExecuteNextAsync<U>())
                    {
                        items.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return items;
        }


        public async Task<PagedResult<T>> SqlQueryPagedAsync(string type, string sql, int pageSize = 50, string continuationToken = null, params KeyValuePair<string, object>[] sqlParams)
        {

            // Look for cache key.
            if (continuationToken != null && !_cache.TryGetValue(continuationToken, out continuationToken))
            {
            }

            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions
            {
                MaxItemCount = pageSize,
                EnableCrossPartitionQuery = false,
                RequestContinuation = continuationToken
            };

            var sqlQuery = new SqlQuerySpec()
            {
                QueryText = sql,
                Parameters = new SqlParameterCollection()
                {
                    new SqlParameter("@type", type)
                }
            };


            foreach (var sqlParam in sqlParams)
            {
                sqlQuery.Parameters.Add(new SqlParameter("@" + sqlParam.Key, sqlParam.Value));
            }

            var query = _client.CreateDocumentQuery<T>(collectionUri, sqlQuery, options);

            var docQuery = query.AsDocumentQuery();

            var result = await docQuery.ExecuteNextAsync<T>();

            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromHours(1));

            var nextPageToken = Guid.NewGuid().ToString();
            _cache.Set(nextPageToken, result.ResponseContinuation, cacheEntryOptions);

            var pagedResult = new PagedResult<T>(result.ToList(), nextPageToken);

            return pagedResult;

        }

        public async Task<IEnumerable<T>> QueryAsync(string type, params Expression<Func<T, bool>>[] predicates)
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions { MaxItemCount = 100, EnableCrossPartitionQuery = false };

            var query = _client.CreateDocumentQuery<T>(collectionUri, options)
                .Where(x => x.Type == type).Where(x => !x.Inactive || !x.Inactive.IsDefined());

            foreach (var predicate in predicates)
            {
                query = query.Where(predicate);
            }

            var docQuery = query.AsDocumentQuery();

            var items = new List<T>();

            while (docQuery.HasMoreResults)
            {
                foreach (var item in await docQuery.ExecuteNextAsync<T>())
                {
                    items.Add(item);
                }
            }

            return items;
        }

        public async Task<PagedResult<T>> QueryPagedAsync(
            string type,
            int pageSize = 50,
            string continuationToken = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> sort = null,
            params Expression<Func<T, bool>>[] predicates)
        {

            // Look for cache key.
            if (continuationToken != null && !_cache.TryGetValue(continuationToken, out continuationToken))
            {
            }

            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            FeedOptions options = new FeedOptions
            {
                MaxItemCount = pageSize,
                EnableCrossPartitionQuery = false,
                RequestContinuation = continuationToken
            };

            var query = _client.CreateDocumentQuery<T>(collectionUri, options)
                .Where(x => x.Type == type).Where(x => !x.Inactive || !x.Inactive.IsDefined());

            foreach (var predicate in predicates)
            {
                query = query.Where(predicate);
            }

            if (sort == null)
            {
                query = query.OrderByDescending(y => y.UpdatedAt);
            }
            else
            {
                query = sort(query);
            }


            var docQuery = query.AsDocumentQuery();

            var result = await docQuery.ExecuteNextAsync<T>();

            // Set cache options.
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                // Keep in cache for this time, reset time if accessed.
                .SetSlidingExpiration(TimeSpan.FromHours(1));

            var nextPageToken = Guid.NewGuid().ToString();
            _cache.Set(nextPageToken, result.ResponseContinuation, cacheEntryOptions);

            var pagedResult = new PagedResult<T>(result.ToList(), nextPageToken);

            return pagedResult;
        }

        public async Task<T> AddAsync(T item)
        {
            // TIP: Use the atomic Upsert for Insert or Replace

            if (item.Id == null)
            {
                item.Id = Guid.NewGuid().ToString();
            }
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_databaseName, _collectionName);
            var result = await _client.UpsertDocumentAsync(collectionUri, item);
            return await this.GetAsync(result.Resource.Id);
        }

        public Task<T> Inactivate(T item)
        {
            item.Inactive = true;

            return UpdateAsync(item);
        }

        public Task<T> Activate(T item)
        {
            item.Inactive = false;

            return UpdateAsync(item);
        }


        public Task RemoveAsync(String id)
        {
            Uri documentUri = UriFactory.CreateDocumentUri(_databaseName, _collectionName, id);

            return _client.DeleteDocumentAsync(documentUri);
        }

        public async Task<T> UpdateAsync(T item)
        {
            Uri documentUri = UriFactory.CreateDocumentUri(_databaseName, _collectionName, item.Id);

            T existingItem = await _client.ReadDocumentAsync<T>(documentUri);

            AccessCondition condition = new AccessCondition { Condition = existingItem.ETag, Type = AccessConditionType.IfMatch };
            var result = await _client.ReplaceDocumentAsync(documentUri, item, new RequestOptions { AccessCondition = condition });
            return await this.GetAsync(item.Id);
        }
    }
}
