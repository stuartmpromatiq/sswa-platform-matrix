﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Infrastructure.Extensions;

namespace SSWA.Paperwork.Common.Services
{
    public class FormAuthorisation : IFormAuthorisation
    {
        public readonly ICurrentUserService _currentUserService;

        public FormAuthorisation(ICurrentUserService currentUserService)
        {
            _currentUserService = currentUserService;
        }

        public AuthorisationReponse CanRequestAuthorisation(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (form.NotIn(FormState.DRAFT, FormState.DENIED, FormState.INFORMATION_REQUIRED))
                return new AuthorisationReponse(false, "This form cannot be submitted while in this state.");

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This form can be submitted.");
        }

        public AuthorisationReponse CanRequestTransfer(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.OPENED, FormState.SUSPENDED))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseTransfer(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_TRANSFER))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanRequestRevalidation(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.TRANSFERRED, FormState.SUSPENDED))
            {
                return new AuthorisationReponse(false, "This form cannot be revalidated while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");


            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseRevalidation(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_REVALIDATION))
            {
                return new AuthorisationReponse(false, "This form cannot be opened while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");


            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanRequestSuspension(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.OPENED))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseSuspension(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_SUSPENSION))
            {
                return new AuthorisationReponse(false, "This form cannot be suspended while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanRequestCompletion(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.OPENED, FormState.TESTING))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseCompletion(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_COMPLETION))
            {
                return new AuthorisationReponse(false, "This form cannot be suspended while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanRequestTestRun(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.OPENED))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseTestRun(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_TEST_RUN))
            {
                return new AuthorisationReponse(false, "This form cannot be suspended while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanRequestCancellation(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.TESTING, FormState.OPENED, FormState.COMPLETED))
            {
                return new AuthorisationReponse(false, "This form cannot be transferred while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_ACCEPTOR, Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanAuthoriseCancellation(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
            {
                return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
            }

            if (atw.NotIn(FormState.PENDING_CANCELLATION))
            {
                return new AuthorisationReponse(false, "This form cannot be suspended while in this state.");
            }

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanOpen(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.PENDING_OPEN))
            {
                return new AuthorisationReponse(false, "This form cannot be opened while in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRoles(Roles.PERMIT_AUTHORISER, Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");


            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanClose(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            if (atw.NotIn(FormState.OPENED, FormState.COMPLETED, FormState.CANCELLED, FormState.TESTING))
            {
                return new AuthorisationReponse(false, "This form cannot be closed while in this state.");
            }

            if (!_currentUserService.UserInRoles(Roles.PERMIT_AUTHORISER, Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanTransfer(IForm form)
        {
            var atw = (form as AuthorityToWorkForm);

            if (atw == null)
                return new AuthorisationReponse(false, "This action can only be done from an Authority To Work form.");

            return new AuthorisationReponse(true, "This action is allowed.");
        }

        public AuthorisationReponse CanApprove(IForm form)
        {
            if (form is AuthorityToWorkForm)
            {
                var atw = (form as AuthorityToWorkForm);

                if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
                {
                    return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
                }

                if (form.Status != FormState.PENDING_AUTHORISATION)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (
                    !_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");
            }
            else if (form is PlantEntryRequestForm)
            {
                if (form.Status != FormState.PENDING_APPROVAL && form.Status != FormState.PARTIALLY_APPROVED)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (!_currentUserService.UserInRole(Roles.ADMIN) &&
                    !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

                //if ((_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) || _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)) && !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) && form.Status == FormState.PARTIALLY_APPROVED)
                //{
                //    return new ValidationResponse(false, "This action cannot be performed by your role when the form is in this state.");
                //}

                //if (_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) && !(_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) || _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)) && form.Status == FormState.PENDING_APPROVAL)
                //{
                //    return new ValidationResponse(false, "This action cannot be performed by your role when the form is in this state.");
                //}

                var approvers = form.Approvers;

                var approvals = approvers.Where(x => x.User?.Id == _currentUserService.Id);
                if (!approvals.Any())
                    return new AuthorisationReponse(false, "You are not a designated approver for this form.");

                if (approvals.All(x => x.IsApproved == true))
                {
                    return new AuthorisationReponse(false, "You have already approved this form.");
                }
            }
            else if (form is PlantWorksRequestForm)
            {
                if (form.Status != FormState.PENDING_APPROVAL && form.Status != FormState.PARTIALLY_APPROVED)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (!_currentUserService.UserInRole(Roles.ADMIN) &&
                    !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

                //if ((_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) || _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)) && !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) && form.Status == FormState.PARTIALLY_APPROVED)
                //{
                //    return new ValidationResponse(false, "This action cannot be performed by your role when the form is in this state.");
                //}

                //if (_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) && !(_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) || _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)) && form.Status == FormState.PENDING_APPROVAL)
                //{
                //    return new ValidationResponse(false, "This action cannot be performed by your role when the form is in this state.");
                //}

                var approvers = form.Approvers;

                var approvals = approvers.Where(x => x.User?.Id == _currentUserService.Id);
                if (!approvals.Any())
                    return new AuthorisationReponse(false, "You are not a designated approver for this form.");

                if (approvals.All(x => x.IsApproved == true))
                {
                    return new AuthorisationReponse(false, "You have already approved this form.");
                }
            }

            return new AuthorisationReponse(true, "This form can be approved.");
        }

        public AuthorisationReponse CanRequestMoreInformation(IForm form)
        {
            if (form is AuthorityToWorkForm)
            {
                var atw = (form as AuthorityToWorkForm);

                if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
                {
                    return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
                }

                if (form.Status != FormState.PENDING_AUTHORISATION)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (
                    !_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");
            }
            else
            {
                if (form.Status != FormState.PENDING_APPROVAL && form.Status != FormState.APPROVED && form.Status != FormState.PARTIALLY_APPROVED)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (!_currentUserService.UserInRole(Roles.ADMIN) &&
                    !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

                var approvers = form.Approvers;

                var approver = approvers.FirstOrDefault(x => x.User?.Id == _currentUserService.Id);
                if (approver == null || (approver.IsApproved.HasValue && !approver.IsApproved.Value))
                    return new AuthorisationReponse(false, "You are not a designated approver for this form.");
            }


            return new AuthorisationReponse(true, "This form can require more info.");
        }

        public AuthorisationReponse CanDeny(IForm form)
        {
            if (form is AuthorityToWorkForm)
            {
                var atw = (form as AuthorityToWorkForm);

                if (_currentUserService.Id == atw.Open.PermitAcceptor.User.Id)
                {
                    return new AuthorisationReponse(false, "You cannot authorise action on a form where you are the Permit Acceptor.");
                }

                if (form.Status != FormState.PENDING_AUTHORISATION)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (
                    !_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");
            }
            else
            {

                if (form.Status != FormState.PENDING_APPROVAL && form.Status != FormState.APPROVED && form.Status != FormState.PARTIALLY_APPROVED)
                    return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");

                if (!_currentUserService.UserInRole(Roles.ADMIN) &&
                    !_currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) &&
                    !_currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER))
                    return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

                var approvers = form.Approvers;

                var approver = approvers.FirstOrDefault(x => x.User?.Id == _currentUserService.Id);
                if (approver == null || (approver.IsApproved.HasValue && !approver.IsApproved.Value))
                    return new AuthorisationReponse(false, "You are not a designated approver for this form.");
            }

            return new AuthorisationReponse(true, "This form can be denied.");
        }

        public AuthorisationReponse CanSubmit(IForm form)
        {
            if (!form.Approvers.Any(x => x.User != null))
                return new AuthorisationReponse(false, "There must be at least one approver set, before this form can be submitted.");

            if (
                    form.NotIn(FormState.DRAFT, FormState.INFORMATION_REQUIRED, FormState.DENIED) ||
                    (form.In(FormState.DENIED) && !_currentUserService.UserInRole(Roles.ADMIN))
                )
                return new AuthorisationReponse(false, "This form cannot be submitted while in this state.");

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRole(Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This form can be submitted.");
        }

        public AuthorisationReponse CanEdit(IForm form)
        {
            if (form is AuthorityToWorkForm || form is IsolationPermit || form is IsolationPermitTemplate)
            {
                if (
                    _currentUserService.UserInRole(Roles.PERMIT_AUTHORISER) || form.CreatedBy.Id == _currentUserService.Id || _currentUserService.UserInRole(Roles.ADMIN))
                    return new AuthorisationReponse(true, "This form can be edited.");
            }
            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRole(Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This form can be edited.");
        }

        public AuthorisationReponse CanCancel(IForm form)
        {
            if (form.Status == FormState.CANCELLED)
            {
                return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRole(Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This form can be cancelled.");
        }


        public AuthorisationReponse CanComplete(IForm form)
        {
            if (form.NotIn(FormState.APPROVED))
            {
                return new AuthorisationReponse(false, "This action is not valid while the form is in this state.");
            }

            if (form.EndDate.Value == null || form.EndDate.Value.Date > DateTime.Now.ToUniversalTime().Date)
            {
                return new AuthorisationReponse(false, "This form can only be completed on or after the finish date.");
            }

            if (form.CreatedBy.Id != _currentUserService.Id && !_currentUserService.UserInRole(Roles.ADMIN))
                return new AuthorisationReponse(false, "You are not the authorised to perform this action.");

            return new AuthorisationReponse(true, "This form can be completed.");
        }
    }
}
