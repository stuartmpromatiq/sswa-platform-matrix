using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Common.Services
{
    public class DocumentService<T> where T : IForm
    {
        private CosmosDbService<T> _client;

        public DocumentService(CosmosDbService<T> client)
        {
            _client = client;
            //_client = new CosmosDbService<T>(docClient);
        }

        public Task<T> InsertAsync(T form)
        {
            form.Id = Guid.NewGuid().ToString();
            form.Revision = 1;
            form.CreatedAt = DateTime.UtcNow;
            form.UpdatedAt = DateTime.UtcNow;

            return _client.AddAsync(form);
        }

        public Task<T> UpdateAsync(T form)
        {
            form.UpdatedAt = DateTime.UtcNow;

            return _client.UpdateAsync(form);
        }

        public Task<IEnumerable<T>> ListAsync(string partitionKey)
        {
            return _client.QueryAsync("form");
        }

        public Task<IEnumerable<T>> QueryAsync(params Expression<Func<T, bool>>[] predicates)
        {
            return _client.QueryAsync("form", predicates);
        }


        public Task<PagedResult<T>> QueryPagedAsync(int pageSize, string token, Func<IQueryable<T>, IOrderedQueryable<T>> sort, params Expression<Func<T, bool>>[] predicates)
        {
            return _client.QueryPagedAsync("form", pageSize, token, sort, predicates);
        }



        public Task<IEnumerable<T>> SqlQueryAsync(string sqlQuery, params KeyValuePair<string, object>[] sqlParams)
        {
            return _client.SqlQueryAsync("form", sqlQuery, sqlParams);
        }

        public Task<U> SqlQueryAsync<U>(string sqlQuery, params KeyValuePair<string, object>[] sqlParams)
        {
            return _client.SqlQuerySingleAsync<U>("form", sqlQuery, sqlParams);
        }

        public Task<PagedResult<T>> SqlQueryPagedAsync(string sqlQuery, int pageSize, string token, params KeyValuePair<string, object>[] sqlParams)
        {
            return _client.SqlQueryPagedAsync("form", sqlQuery, pageSize, token, sqlParams);
        }

        public Task<IEnumerable<T>> QueryTemplatesAsync(params Expression<Func<T, bool>>[] predicates)
        {
            return _client.QueryAsync("form_template", predicates);
        }

        public Task<PagedResult<T>> QueryTemplatesPagedAsync(int pageSize, string token, Func<IQueryable<T>, IOrderedQueryable<T>> sort, params Expression<Func<T, bool>>[] predicates)
        {
            return _client.QueryPagedAsync("form_template", pageSize, token, sort, predicates);
        }

        public Task<IEnumerable<U>> SqlQueryTemplatesAsync<U>(string sqlQuery, params KeyValuePair<string, object>[] sqlParams)
        {
            return _client.SqlQueryAsync<U>("form_template", sqlQuery, sqlParams);
        }

        public Task<PagedResult<T>> SqlQueryTemplatesPagedAsync(string sqlQuery, int pageSize, string token, params KeyValuePair<string, object>[] sqlParams)
        {
            return _client.SqlQueryPagedAsync("form_template", sqlQuery, pageSize, token, sqlParams);
        }

        public Task<T> FindAsync(string id)
        {
            return _client.GetAsync(id);
        }
    }
}
