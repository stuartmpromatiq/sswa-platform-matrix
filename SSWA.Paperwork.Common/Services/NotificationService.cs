using MediatR;
using Microsoft.Extensions.Logging;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common.Events;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static SSWA.Paperwork.Common.Notification;

namespace SSWA.Paperwork.Common.Services
{
    public class NotificationService
        : INotificationHandler<FormSubmittedEvent>,
        INotificationHandler<FormApprovedEvent>,
        INotificationHandler<FormDeniedEvent>,
        INotificationHandler<FormOpenedEvent>,
        INotificationHandler<FormPartiallyApprovedEvent>,
        INotificationHandler<FormRequiresInformationEvent>,
        INotificationHandler<FormPendingAuthorisationEvent>,
        INotificationHandler<FormPendingCancellationEvent>,
        INotificationHandler<FormPendingCompletionEvent>,
        INotificationHandler<FormPendingRevalidationEvent>,
        INotificationHandler<FormPendingSuspensionEvent>,
        INotificationHandler<FormPendingTestRunEvent>,
        INotificationHandler<FormPendingTransferEvent>,
        INotificationHandler<FormRevalidatedEvent>,
        INotificationHandler<FormSuspendedEvent>,
        INotificationHandler<FormTestRunningEvent>,
        INotificationHandler<FormTransferredEvent>,
        INotificationHandler<FormClosedEvent>,
        INotificationHandler<FormCompletedEvent>,
        INotificationHandler<FormAuthorisedEvent>
    {
        private readonly ILogger _logger;
        private readonly CosmosDbService<Notification> _client;

        private readonly EmailService _emailService;

        private readonly ICurrentRequestService _currentRequestService;

        private readonly IExternalUserService _externalUserService;

        public NotificationService(
            ILogger<NotificationService> logger,
             CosmosDbService<Notification> client,
             EmailService emailService,
             ICurrentRequestService currentRequestService,
             IExternalUserService externalUserService)
        {
            _client = client;
            _logger = logger;
            _emailService = emailService;
            _currentRequestService = currentRequestService;
            _externalUserService = externalUserService;
        }

        public async Task Handle(FormSubmittedEvent notification, CancellationToken cancellationToken)
        {

            var form = notification.Form;
            var formType = form.Kind.Code;
            var approverRecipients = form.GetUsersToNotifyOnSubmissionForApproval();
            var allApprovers = form.GetApprovers().Select(x => x.Id);
            var adminIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.ADMIN)?
                .Users;

            var company = "";

            switch (formType)
            {
                case "PER":
                    var per = (PlantEntryRequestForm)form;
                    company = per.Visitors.FirstOrDefault()?.Company;
                    break;
                case "PWR":
                    var pwr = (PlantWorksRequestForm)form;
                    company = pwr.PrincipalContractor;
                    break;
            }

            company = string.IsNullOrWhiteSpace(company) ? "N/A" : company;
            var subject = $"{form.Kind.Code} by {form.CreatedBy.Name} for {company}";

            foreach (var user in approverRecipients.GroupBy(x => x.Id).Select(y => y.First()))
            {
                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = user,
                    Subject = subject,
                    Message = $"{form.CreatedBy.Name} has submitted a {form.Kind.Name} ({form.FriendlyId}) for you to review & approve." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                await _emailService.SendAsync(userNotification);
            }

            foreach (var userId in adminIds.GroupBy(x => x).Select(y => y.First()))
            {
                if (allApprovers.Contains(userId))
                    continue;

                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = subject,
                    Message = $"{form.CreatedBy.Name} has submitted a {form.Kind.Name} ({form.FriendlyId}) for approval." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                await _emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPartiallyApprovedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var approvers = form.GetUsersToNotifyOnPartialApproval();

            var company = "";

            switch (formType)
            {
                case "PER":
                    var per = (PlantEntryRequestForm)form;
                    company = per.Visitors.FirstOrDefault()?.Company;
                    break;
                case "PWR":
                    var pwr = (PlantWorksRequestForm)form;
                    company = pwr.PrincipalContractor;
                    break;
            }

            company = string.IsNullOrWhiteSpace(company) ? "N/A" : company;
            var subject = $"{form.Kind.Code} by {form.CreatedBy.Name} for {company}";

            foreach (var user in approvers.GroupBy(x => x.Id).Select(y => y.First()))
            {
                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = user,
                    Subject = subject,
                    Message = $"{form.CreatedBy.Name} has submitted a {form.Kind.Name} ({form.FriendlyId}) for you to review & approve." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                await _emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormApprovedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;
            var requestor = await _externalUserService.GetUserAsync(form.CreatedBy.Id);

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Approved",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been approved." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            if (form.Kind.Code == "PER")
            {
                var perForm = form as PlantEntryRequestForm;
                var pdfAttachment = await GetPdfAttachment();
                var calendarAttachment = GetCalendarAttachment(perForm, perForm.FriendlyId + " Start", perForm.Description);

                await _emailService.SendAsync(userNotification, calendarAttachment);
                await _client.AddAsync(userNotification);

                calendarAttachment = GetCalendarAttachment(perForm, "Southern Seawater Desalination Plant Visit", "");



                foreach (var visitor in perForm.Visitors)
                {
                    if (string.IsNullOrWhiteSpace(visitor.Email))
                        continue;

                    var awarenessTraining = "";
                    if (perForm.AwarenessTraining.Any())
                    {
                        awarenessTraining = " as well as the following training: <i>";
                        awarenessTraining += string.Join(" / ", perForm.AwarenessTraining);
                        awarenessTraining = "</i>";
                    }

                    var inductionTraining = "Visitor";
                    switch (perForm.RequestType)
                    {
                        case "STW":
                            inductionTraining = "Short Term Works";
                            break;
                        case "PCW":
                            inductionTraining = "Period Contract Works";
                            break;
                        case "MW":
                            inductionTraining = "Marine Works";
                            break;
                    }

                    var description = $@"
                        Dear {visitor.Name},
                        <br /><br />
                        {requestor.Name} from Southern Seawater Desalination Plant has received management approval for your entry to the Plant as per your agreed time. 
                        <br /><br />
                        This calendar entry can be used to help with scheduling of your visit to the Plant. 
                        <br /><br />
                        Please see the attached pre-arrival information in preparation for your entry to the Plant. Safety is at the forefront of our minds in everything we do at the Plant so take careful note of this information. 
                        <br /><br />
                        <b>Our motto is �No job is so urgent or minor that it cannot be done safely�. Any person at the Plant is authorised to stop any job at any time to review and ensure correct safe job planning. 
                        <br /><br />
                        Please assist us in our commitment to safety, in the first instance by planning and communicating well any jobs you are involved with, personally ensuring you have all the necessary and appropriate resources.</b>
                        <br /><br />
                        You are required to complete a <i>{inductionTraining}</i> induction{awarenessTraining}. This will be done on site.
                        <br /><br />
                        We look forward to seeing you at the Plant. Please feel free to check with {requestor.Name} (Email: <a href=""mailto:{requestor.Email}"">{requestor.Email}</a>, Phone: <a href=""tel:{requestor.Phone}"">{requestor.Phone}</a>) if any clarification is required.
                        <br /><br />
                        Regards,
                        <br /><br />
                        The Southern Seawater Desalination Plant team 
                        ";


                    await _emailService.SendAdhocAsync(visitor.Email, "Upcoming SSWA Plant Visit", description, calendarAttachment, pdfAttachment);
                }

            }
            else
            {

                await _client.AddAsync(userNotification);
                await _emailService.SendAsync(userNotification);
            }

        }

        public async Task Handle(FormDeniedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Denied",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been denied. Please see the form for more details." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormRequiresInformationEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Requires More Information",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been marked as requiring additional information. Please see the form for more details." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormPendingAuthorisationEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} by {form.CreatedBy.Name} submitted for authorisation",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for authorisation." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPendingCancellationEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} by {form.CreatedBy.Name} submitted for cancellation",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for cancellation." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormOpenedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Open",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) is now marked as Open." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };


            await _emailService.SendAsync(userNotification);
            await _client.AddAsync(userNotification);

        }

        private static async Task<string> GetPdfAttachment()
        {
            using (var client = new HttpClient())
            {
                var bytes = await client.GetByteArrayAsync("https://promatiqsswa.blob.core.windows.net/assets/00-PM-GUI-1002_Rev%20A%20-%20Plant%20Access%20Pre-Arrival%20Information%20Sheet.pdf");
                return Convert.ToBase64String(bytes);
            }
        }

        private static string GetCalendarAttachment(PlantEntryRequestForm perForm, string summary, string description)
        {
            DateTime StartTime = DateTime.ParseExact(perForm.Visitors.FirstOrDefault()?.ExpectedTime, "HH:mm", CultureInfo.InvariantCulture);

            DateTime DateStart = perForm.StartDate.Value.AddHours(StartTime.Hour).AddMinutes(StartTime.Minute).ToLocalTime();
            DateTime DateEnd = perForm.EndDate.Value.AddHours(StartTime.Hour + 1).AddMinutes(StartTime.Minute).ToLocalTime();
            string Summary = summary;
            string Location = "159 Taranto Road, Binningup WA 6233";
            string Description = description;

            //create a new stringbuilder instance
            var sb = new StringBuilder();

            //start the calendar item
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("VERSION:2.0");
            sb.AppendLine("PRODID:sswa.azurewebsites.net");
            sb.AppendLine("CALSCALE:GREGORIAN");
            sb.AppendLine("METHOD:PUBLISH");

            //create a time zone to be used in the event itself
            sb.AppendLine("BEGIN:VTIMEZONE");
            sb.AppendLine("TZID:Australia/Perth");
            sb.AppendLine("BEGIN:STANDARD");
            sb.AppendLine("TZOFFSETTO:+0800");
            sb.AppendLine("TZOFFSETFROM:+0800");
            sb.AppendLine("END:STANDARD");
            sb.AppendLine("END:VTIMEZONE");

            //add the event
            sb.AppendLine("BEGIN:VEVENT");

            sb.AppendLine($"UID:{Guid.NewGuid()}");
            sb.AppendLine($"DTSTAMP:{DateTime.Now.ToString("yyyyMMddTHHmm00")}");

            //with time zone specified
            sb.AppendLine("DTSTART;TZID=Australia/Perth:" + DateStart.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("DTEND;TZID=Australia/Perth:" + DateEnd.ToString("yyyyMMddTHHmm00"));

            sb.AppendLine("SUMMARY:" + Summary + "");
            sb.AppendLine("LOCATION:" + Location + "");
            sb.AppendLine("DESCRIPTION:" + Description + "");
            sb.AppendLine("END:VEVENT");

            //end calendar item
            sb.AppendLine("END:VCALENDAR");

            var bytes = Encoding.ASCII.GetBytes(sb.ToString());
            return Convert.ToBase64String(bytes);
        }

        public async Task Handle(FormPendingSuspensionEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} by {form.CreatedBy.Name} submitted for suspension",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for suspension." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPendingRevalidationEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} {form.CreatedBy.Name} submitted for revalidation",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for revalidation." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPendingCompletionEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} {form.CreatedBy.Name} submitted for completion",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for completion." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPendingTestRunEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} {form.CreatedBy.Name} submitted for testing",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for testing." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormPendingTransferEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var formType = form.Kind.Code;
            var authoriserIds = (await _externalUserService.GetAllRolesAsync())
                .FirstOrDefault(x => x.Name == Roles.PERMIT_AUTHORISER)?
                .Users;

            foreach (var userId in authoriserIds.GroupBy(x => x).Select(y => y.First()))
            {
                ExternalUser user = null;
                try
                {
                    user = await _externalUserService.GetUserAsync(userId);
                }
                catch
                {
                    continue;
                }

                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = user.Id,
                    Recipient = new User()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name
                    },
                    Subject = $"{form.Kind.Code} {form.CreatedBy.Name} submitted for transfer",
                    Message = $"{form.CreatedBy.Name} has submitted an {form.Kind.Name} ({form.FriendlyId}) for transfer." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                    Action = new NotificationAction()
                    {
                        Domain = _currentRequestService.Domain,
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await _client.AddAsync(userNotification);
                //_emailService.SendAsync(userNotification);
            }
        }

        public async Task Handle(FormRevalidatedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Revalidation",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been revalidated and is now marked as Open." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormSuspendedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Suspended",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been suspended" +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormTestRunningEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} in Testing",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been marked as Testing" +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormTransferredEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Transferred",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been transferred to you." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormClosedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Closed",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been marked as Closed." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormCompletedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Completed",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been marked as Completed." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public async Task Handle(FormAuthorisedEvent notification, CancellationToken cancellationToken)
        {
            var form = notification.Form;
            var recipient = form.CreatedBy;

            var userNotification = new Notification()
            {
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                RecipientId = recipient.Id,
                Recipient = recipient,
                Subject = $"{form.FriendlyId} Authorised",
                Message = $"Your {form.Kind.Name} ({form.FriendlyId}) has been authorised and can now be Opened. Please review comments added by Permit Authoriser before opening the ATW." +
                          $"<br /><br />Request: <em>{form.Description}</em>",
                Action = new NotificationAction()
                {
                    Domain = _currentRequestService.Domain,
                    Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                    Display = "View Form"
                }
            };

            await _client.AddAsync(userNotification);
            await _emailService.SendAsync(userNotification);
        }

        public Task<IEnumerable<Notification>> ListAsync(string userId) => _client.QueryAsync("notification", x => x.RecipientId == userId && !x.Seen);

        public Task<PagedResult<Notification>> PagedListAsync(int pageSize, string token, string userId) => _client.QueryPagedAsync("notification", pageSize, token, null, x => x.RecipientId == userId && !x.Seen);
    }
}
