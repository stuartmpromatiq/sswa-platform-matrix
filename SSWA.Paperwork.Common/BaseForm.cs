﻿using Newtonsoft.Json;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SSWA.Paperwork.Common
{
    public abstract class BaseForm : BaseEntity, IForm
    {
        public BaseForm()
        {
            Approvers = new List<FormApproval>()
            {
                //new FormApproval() {
                //    Role = new FormApproval.FormApprovalRole() {
                //      Id =  "paperwork_operations_manager",
                //      Name = "Operations Manager"
                //    },
                //    User =  null
                //},
                //new FormApproval() {
                //    Role = new FormApproval.FormApprovalRole() {
                //      Id =  "paperwork_maintenance_manager",
                //      Name = "Maintenance Manager"
                //    },
                //    User =  null
                //},
                //new FormApproval() {
                //    Role = new FormApproval.FormApprovalRole() {
                //      Id =  "paperwork_alliance_manager",
                //      Name = "Alliance Manager"
                //    },
                //    User =  null
                //}
            };
            LinkedForms = new List<FormLink>();
        }

        [JsonProperty("type")]
        public override string Type => "form";

        [JsonProperty("kind")]
        public FormKind Kind { get; set; }

        [JsonProperty("description")]
        public abstract string Description { get; }

        [JsonProperty("canonicalDescription")]
        public string CanonicalDescription
        {
            get
            {
                var rgx = new Regex("[^a-zA-z0-9]");
                return rgx.Replace(Description ?? "", "")?.ToLowerInvariant();
            }
        }

        [JsonProperty("friendlyId")]
        public string FriendlyId { get; set; }

        [JsonProperty("revision")]
        public int Revision { get; set; }

        [JsonProperty("startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime? EndDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        public User RequestedBy { get; set; }

        [JsonProperty("approvers")]
        public List<FormApproval> Approvers { get; set; }

        [JsonProperty("linkedForms", NullValueHandling = NullValueHandling.Ignore)]
        public List<FormLink> LinkedForms { get; set; }

        [JsonProperty("allowedUsers", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> AllowedUsers { get; set; }

        public bool CanSee { get; set; }
        public bool CanEdit { get; set; }
        public bool CanSubmit { get; set; }
        public bool CanRequestAuthorisation { get; set; }
        public bool CanApprove { get; set; }
        public bool CanDeny { get; set; }
        public bool CanCancel { get; set; }
        public bool CanRequestInfo { get; set; }
        public bool CanComplete { get; set; }

        public bool CanRequestTransfer { get; set; }
        public bool CanAuthoriseTransfer { get; set; }

        public bool CanRequestSuspension { get; set; }
        public bool CanAuthoriseSuspension { get; set; }

        public bool CanRequestRevalidation { get; set; }
        public bool CanAuthoriseRevalidation { get; set; }

        public bool CanOpen { get; set; }

        public bool CanClose { get; set; }
        public bool CanRequestCompletion { get; set; }
        public bool CanAuthoriseCompletion { get; set; }
        public bool CanRequestTestRun { get; set; }
        public bool CanAuthoriseTestRun { get; set; }
        public bool CanRequestCancellation { get; set; }
        public bool CanAuthoriseCancellation { get; set; }

        public virtual IEnumerable<User> GetApprovers()
        {
            return this.Approvers.Where(x => x.User?.Id != null).Select(x => x.User);
        }

        public virtual IEnumerable<User> GetUsersToNotifyOnApproval()
        {
            return new User[] { this.CreatedBy };
        }

        public virtual IEnumerable<User> GetUsersToNotifyOnDenial()
        {
            return new User[] { this.CreatedBy };
        }

        public virtual IEnumerable<User> GetUsersToNotifyOnSubmissionForApproval()
        {
            return this.Approvers
                        .Where(x => x.User?.Id != null)
                        .Where(x => x.User.Id != this.CreatedBy.Id)
                        .Where(x => x.Role.Id != Roles.ALLIANCE_MANAGER)
                        .Select(x => x.User);
        }

        public virtual IEnumerable<User> GetUsersToNotifyOnPartialApproval()
        {
            return this.Approvers
                        .Where(x => x.User?.Id != null)
                        .Where(x => x.User.Id != this.CreatedBy.Id)
                        .Where(x => x.Role.Id == Roles.ALLIANCE_MANAGER)
                        .Select(x => x.User);
        }

        public virtual string GenerateFriendlyId()
        {
            return $"{Kind.Code}-" +
                $"{(CreatedAt ?? DateTime.UtcNow).ToString("yyMMdd")}." +
                $"{(CreatedAt ?? DateTime.UtcNow).ToString("hhmmss")}";
        }

    }

    public abstract class BaseFormTemplate : BaseForm, IFormTemplate
    {
        [JsonProperty("lookup")]
        public TemplateLookup Lookup => new TemplateLookup();
    }
}
