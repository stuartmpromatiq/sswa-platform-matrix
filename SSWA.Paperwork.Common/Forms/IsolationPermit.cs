﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Forms
{
    public class IsolationPermit : BaseForm
    {
        public IsolationPermit()
        {
            Kind = new FormKind("ISO", "Isolation Permit");
        }

        public override string Description => WorksDescription;

        public string SubContractor { get; set; }

        public string SwitchingProgramNumber { get; set; }

        public string WorksDescription { get; set; }

        public string PhysicalLocation { get; set; }

        public string Stage { get; set; }

        [JsonProperty("equipmentNumber")]
        public string EquipmentNumber { get; set; }

        [JsonProperty("lockbox")]
        public string Lockbox { get; set; }

        public List<IsolationPermitMethodStep> IsolationMethod { get; set; } = new List<IsolationPermitMethodStep>();

        public List<IsolationPermitAttachment> Attachments { get; set; } = new List<IsolationPermitAttachment>();


        public class IsolationPermitLookupHierarchy
        {
            [JsonProperty("level1")]
            public string Level1 { get; set; }

            [JsonProperty("level2")]
            public string Level2 { get; set; }

            [JsonProperty("level3")]
            public string Level3 { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public class IsolationPermitMethodStep
        {
            public int Step { get; set; }

            public string EquipmentDescription { get; set; }

            public string Tag { get; set; }

            public string EquipmentType { get; set; }

            public string Area { get; set; }

            public List<string> Notes { get; set; } = new List<string>();

            public string EnergyType { get; set; }

            public string AsFoundState { get; set; }

            public string IsolationState { get; set; }

            public string Lockbox { get; set; }

            public string Lock { get; set; }
        }

        public class IsolationPermitAttachment
        {
            public int Step { get; set; }
            public string Name { get; set; }

            public string Type { get; set; }

            public string FileLocation { get; set; }
        }

    }


}
