﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class FormYesNoAnswer
    {
        public string Section { get; set; }

        public string Question { get; set; }
        public bool? Answer { get; set; }

        public string Notes { get; set; }
    }

    public class FormYesNoAnswerWithDetails : FormYesNoAnswer
    {
        public string Details { get; set; }
    }

    public class FormYesNoAnswerWthPerson: FormYesNoAnswer
    {
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string FinishTime { get; set; }
        public byte[] Signature { get; set; }
    }

}
