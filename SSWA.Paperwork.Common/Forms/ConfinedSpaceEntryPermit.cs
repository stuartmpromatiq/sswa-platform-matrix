﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class ConfinedSpaceEntryPermit : BaseForm
    {
        public ConfinedSpaceEntryPermit()
        {
            Kind = new Core.FormKind("CSE", "Confined Space Entry Permit");
            PreConditionAssessments = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Is there potential for other works/ machinery to affect the safe O2 levels or introduce contaminants?",
                    Notes = "Stop!"
                },
                new FormYesNoAnswer()
                {
                    Question = "Given the work schedule, can activities be completed in accordance with the permit conditions?",
                    Notes = "Stop!"
                },
                new FormYesNoAnswer()
                {
                    Question = "Risk Assessment completed?",
                    Notes = "Stop!"
                },
                new FormYesNoAnswer()
                {
                    Question = "Additional Pre-conditions (as determined by risk assessment)",
                },
                new FormYesNoAnswer()
                {
                    Question = "All Person appropriately trained",
                    Notes = "Stop!"
                }
            };

            WorkControls1 = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "JSEA"
                },
                new FormYesNoAnswer()
                {
                    Question = "Pre-start checklist"
                },
                new FormYesNoAnswer()
                {
                    Question = "Emergency response plan in place"
                },
                new FormYesNoAnswer()
                {
                    Question = "Satellite Lock Box /Work Pack"
                },
                new FormYesNoAnswer()
                {
                    Question = "Entry / Exit log (See Part F overleaf)"
                },
            };

            WorkControls2 = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Initial atmospheric test conducted"
                },
                new FormYesNoAnswer()
                {
                    Question = "Fitness and competency of standby & entry personnel"
                }
            };

            WorkControls3 = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Equipment flushed and purged"
                },
                new FormYesNoAnswer()
                {
                    Question = "Equipment depressurized / drained"
                },
                new FormYesNoAnswer()
                {
                    Question = "Exclusion zone set up"
                },
                 new FormYesNoAnswer()
                {
                    Question = "Warning signs / barricades"
                },
                  new FormYesNoAnswer()
                {
                    Question = "Atmospheric testing "
                },
                   new FormYesNoAnswer()
                {
                    Question = "Atmospheric monitoring"
                },
                    new FormYesNoAnswer()
                {
                    Question = "Breathing apparatus required"
                }
            };

            WorkControls4 = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Alternate entry points secured"
                },
                new FormYesNoAnswer()
                {
                    Question = "Alternate entry points secured"
                },
                new FormYesNoAnswer()
                {
                    Question = "Temperature monitoring"
                },
                 new FormYesNoAnswer()
                {
                    Question = "Lighting"
                },
                  new FormYesNoAnswer()
                {
                    Question = "Fire extinguisher"
                },
                   new FormYesNoAnswer()
                {
                    Question = "Standby person"
                },
                    new FormYesNoAnswer()
                {
                    Question = "Scaffolding (if required)"
                }
            };

            EmergencyResponseRequirements = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Full body harness",
                },
                new FormYesNoAnswer()
                {
                    Question = "Rescue – mechanical hoist / winch ",
                },
                new FormYesNoAnswer()
                {
                    Question = "Lifeline",
                },
                new FormYesNoAnswer()
                {
                    Question = "Bullhorn",
                },
                new FormYesNoAnswer()
                {
                    Question = "Respiratory Protection Type",
                    Notes = ""
                }
            };

            CommunicationMethods = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Visible / audible contact",
                },
                new FormYesNoAnswer()
                {
                    Question = "Portable radio",
                },
                new FormYesNoAnswer()
                {
                    Question = "Lifeline signals",
                }
            };

            AddtionalPPERequirements = new List<FormYesNoAnswer>()
            {
                 new FormYesNoAnswer()
                {
                    Question = "Gloves",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Rubber boots",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Chemical gloves",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Mono goggles",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Other",
                    Notes = ""
                },
                 new FormYesNoAnswer()
                {
                    Question = "Hearing protection",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Fall arrest system",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Chemical suit",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Face shield",
                },
                 new FormYesNoAnswer()
                {
                    Question = "Torch",
                },
            };


            InternalAtmosphericTests = new List<AtmosphericTestResult>();
            ExternalAtmosphericTests = new List<AtmosphericTestResult>();

            for (var i = 0; i <= 6; i++)
            {
                InternalAtmosphericTests.Add(new AtmosphericTestResult());
                ExternalAtmosphericTests.Add(new AtmosphericTestResult());
            }

            AtmosphericTestingRequirements = new List<FormYesNoAnswer>()
            {
                new FormYesNoAnswer()
                {
                    Question = "Methodology consistent with the JSEA"
                },
                new FormYesNoAnswer()
                {
                    Question = "Allowable O2 limit is N/A if SCBA is worn"

                },
                new FormYesNoAnswer()
                {
                    Question = "Check workplace for absence of combustibles"
                }
            };

            AtmosphericTestingMethods = new List<AtmoshpericTestingMethod>()
            {
                new AtmoshpericTestingMethod()
                {
                    Method = "Area check only",
                },
                new AtmoshpericTestingMethod()
                {
                    Method = "Continuous monitoring",
                },
                new AtmoshpericTestingMethod()
                {
                    Method = "Periodic testing",
                }
            };
        }
        public override string Description => WorksDescription;

        public string MainTasks { get; set; }

        public string EquipmentDescription { get; set; }

        public string ToolsDescription { get; set; }

        public string RadioChannel { get; set; }

        public string Company { get; set; }

        public string WorksDescription { get; set; }

        public List<FormYesNoAnswer> PreConditionAssessments { get; set; }

        public List<FormYesNoAnswer> WorkControls1 { get; set; }

        public List<FormYesNoAnswer> WorkControls2 { get; set; }

        public List<FormYesNoAnswer> WorkControls3 { get; set; }

        public List<FormYesNoAnswer> WorkControls4 { get; set; }

        public List<FormYesNoAnswer> EmergencyResponseRequirements { get; set; }

        public List<FormYesNoAnswer> CommunicationMethods { get; set; }

        public List<FormYesNoAnswer> AddtionalPPERequirements { get; set; }

        public List<AtmosphericTestResult> ExternalAtmosphericTests { get; set; }

        public List<AtmosphericTestResult> InternalAtmosphericTests { get; set; }

        public List<FormYesNoAnswer> AtmosphericTestingRequirements { get; set; }

        public List<AtmoshpericTestingMethod> AtmosphericTestingMethods { get; set; }

        public class AtmosphericTestResult
        {
            public DateTime? RecordedAt { get; set; }
            public string DetectorNumber { get; set; }
            public string TesterName { get; set; }
            public decimal? LowerExplosiveLimitLevel { get; set; }
            public decimal? OxygenLevel { get; set; }
            public string Other { get; set; }
        }

        public class AtmoshpericTestingMethod
        {
            public string Method { get; set; }
            public bool? Used { get; set; }
            public int? TestingFrequency { get; set; }
            public decimal? LowerExplosiveLimit { get; set; }
            public decimal? OxygenLimit { get; set; }
            public string Other { get; set; }

        }
    }


}
