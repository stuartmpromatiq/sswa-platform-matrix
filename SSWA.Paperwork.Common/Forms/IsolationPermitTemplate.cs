﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common.Forms
{
    public class IsolationPermitTemplate : IsolationPermit, IFormTemplate
    {
        public IsolationPermitTemplate() : base()
        {
        }

        [JsonProperty("type")]
        public override string Type => "form_template";

        [JsonProperty("lookup")]
        public TemplateLookup Lookup { get; set; } = new TemplateLookup();
    }


}
