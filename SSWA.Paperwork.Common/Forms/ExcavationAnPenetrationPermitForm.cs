﻿using System;
using System.Collections.Generic;
using System.Text;
using static SSWA.Paperwork.Common.Forms.WorkingAtHeightsPermit;

namespace SSWA.Paperwork.Common.Forms
{
    public class ExcavationPermit : BaseForm
    {
        public ExcavationPermit()
        {
            Kind = new Core.FormKind("EXP", "Excavation Permit");

            ExcavationAreaItems = new List<ExcavationAreaItem>()
            {
                new ExcavationAreaItem("Water / Fire Mains", 1),
                new ExcavationAreaItem("Air", 2),
                new ExcavationAreaItem("Electrical - High Voltage", 3),
                new ExcavationAreaItem("Communications (Phone / Internet)", 4),
                new ExcavationAreaItem("Sewerage / Drainage", 5),
                new ExcavationAreaItem("Fuel", 6),
                new ExcavationAreaItem("Other Buried Objects", 7),
            };

            var controlGroup = new FormFieldGroup("Additional", 1);
            var hazardGroup = new FormFieldGroup("Hazards", 1);

            AdditionalControls = new List<FormField>()
            {
                new FormField(controlGroup, "ACCESS AND EGRESS TO EXCAVATION", 1),
                new FormField(controlGroup, "ULTRASONIC LOCATION OF SERVICES", 2),
                new FormField(controlGroup, "HAND DIG WHEN WITHIN 1M OF SERVICES AND STRUCTURES", 3),
                new FormField(controlGroup, "BATTERING / BENCHING / SHORING (REQUIRED FOR EXCAVATIONS OF DEPTH ≥1.5M)", 4),
                new FormField(controlGroup, "BARRICADING AND BARRIERS TO BE ERECTED:", 5),
                new FormField(controlGroup, "EXCLUSION ZONE", 6),
                new FormField(controlGroup, "SERVICES MARKING", 7),
                new FormField(controlGroup, "FAUNA ACCESS AND EGRESS POINTS", 8)
            };

            HazardIdentifications = new List<FormField>()
            {
                new FormField(hazardGroup, "Surface Encumbrances Identified/Secured", "", 1),
                new FormField(hazardGroup, "Underground utilities identified/located", "", 1),
                new FormField(hazardGroup, "Utilities Protected", "", 1),
                new FormField(hazardGroup, "System Lockout/Tagout   (Tag I.D. No. _____)", "", 1),
                new FormField(hazardGroup, "Access and Egress", "", 1),
                new FormField(hazardGroup, "Protection from Vehicular Traffic", "", 1),
                new FormField(hazardGroup, "Equipment/Tool grinding", "", 1),
                new FormField(hazardGroup, "Manual Digging", "", 1),
                new FormField(hazardGroup, "Protection from Water Accumulation", "", 1),
                new FormField(hazardGroup, "Warning systems for Mobile Equipment", "", 1),
                new FormField(hazardGroup, "Stability of Adjacent Structures", "", 1),
                new FormField(hazardGroup, "Site Inspections", "", 1),
                new FormField(hazardGroup, "Confined Space", "", 1),
                new FormField(hazardGroup, "Atmospheric Testing", "", 1),
                new FormField(hazardGroup, "Temperature extremes", "Hot or Cold", 1),
                new FormField(hazardGroup, "Radiation", "Microwave – Infra red - Atomic", 1),
                new FormField(hazardGroup, "Flooding", "", 1),
                new FormField(hazardGroup, "Weather Conditions", "Sun – Rain – Wind -Lightning", 1),
                new FormField(hazardGroup, "Slips & Falls", "", 1),
                new FormField(hazardGroup, "Falling Objects", "", 1),
                new FormField(hazardGroup, "Structural Weakness", "", 1),
                new FormField(hazardGroup, "Moving Equipment", "", 1),
                new FormField(hazardGroup, "Access(Entry / Exit), Blocked Exit", "", 1),
                new FormField(hazardGroup, "Rescue", "Would rescue be difficult?", 1),
                new FormField(hazardGroup, "Engulfment", "Covered by or sinking into materials", 1),
                new FormField(hazardGroup, "Electricity", "Supply or electrical tools", 1),
                new FormField(hazardGroup, "Lighting", "Is lighting required?", 1),
                new FormField(hazardGroup, "Fire", "Is there a chance of fire?", 1),
                new FormField(hazardGroup, "Mobile Equipment / Plant", "", 1),
                new FormField(hazardGroup, "Substances in Area", "Hazardous substances?", 1),
                new FormField(hazardGroup, "Chemical Reactions", "", 1),
                new FormField(hazardGroup, "Work being done nearby", "", 1),
                new FormField(hazardGroup, "Disturbing Sludge", "Will sludge affect working area?", 1),
                new FormField(hazardGroup, "Effects on adjacent areas", "", 1),
                new FormField(hazardGroup, "Unauthorized entry", "", 1),
                new FormField(hazardGroup, "Signage", "Is signage required?", 1),
                new FormField(hazardGroup, "Communication", "Special communication equipment required?", 1),
                new FormField(hazardGroup, "Personnel – Special Requirements", "", 1),
                new FormField(hazardGroup, "Procedures", "", 1),
                new FormField(hazardGroup, "Isolation", "", 1),
                new FormField(hazardGroup, "Manual Handling", "", 1),
                new FormField(hazardGroup, "Noise", "", 1),
                new FormField(hazardGroup, "Special PPE Requirements ?", "", 1),
                new FormField(hazardGroup, "Entry Frequency", "", 1)
            };
    }
        public override string Description => Reason;

        public string Reason { get; set; }
        public string Company { get; set; }
        public string ExcavationLocation { get; set; }
        public decimal? ExcavationDepth { get; set; }
        public decimal? ExcavationWidth { get; set; }
        public decimal? ExcavationLength { get; set; }

        public FormYesNoAnswerWithDetails DrawingProvided = new FormYesNoAnswerWithDetails()
        {
            Section = "Header",
            Question = "Drawing of the works provided?",
            Answer = null
        };

        public FormYesNoAnswerWithDetails SimultaneousOperations = new FormYesNoAnswerWithDetails()
        {
            Section = "Permits",
            Question = "Are there any simultaneous operations?",
            Answer = null
        };

        public FormYesNoAnswerWithDetails DewateringRequired = new FormYesNoAnswerWithDetails()
        {
            Section = "Permits",
            Question = "IS DEWATERING REQUIRED?",
            Answer = null
        };

        public FormYesNoAnswerWthPerson SpotterRequired = new FormYesNoAnswerWthPerson()
        {
            Section = "Permits",
            Question = "IS A SPOTTER REQUIRED?",
            Answer = null
        };

        public IEnumerable<ExcavationAreaItem> ExcavationAreaItems { get; set; }

        public IEnumerable<FormField> AdditionalControls { get; set; }

        public IEnumerable<FormField> HazardIdentifications { get; set; }

       

        public class ExcavationAreaItem
        {
            public ExcavationAreaItem()
            {

            }
            public ExcavationAreaItem(string item, int order)
            {
                Item = item;
                Order = order;
            }

            public string Item { get; set; }
            public bool? Answer { get; set; }

            public string Location { get; set; }

            public decimal? Depth { get; set; }

            public bool? IsolationNeeded { get; set; }

            public int Order { get; set; }
        }

    }
}
