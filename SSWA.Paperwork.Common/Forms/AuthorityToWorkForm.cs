﻿using Newtonsoft.Json;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class AuthorityToWorkForm : BaseForm
    {

        public AuthorityToWorkForm()
        {
            SignOnsAndOffs = new List<AuthorityToWorkSignOnAndOff>();
            Transfers = new List<AuthorityToWorkFormTransfer>();
            Suspensions = new List<AuthorityToWorkFormSuspension>();
            Revalidations = new List<AuthorityToWorkFormRevalidation>();
            Completions = new List<AuthorityToWorkFormCompletion>();
            Open = new AuthorityToWorkFormOpen()
            {
                PermitAcceptor = new FormUserAction(),
                PermitAuthoriser = new FormUserAction()
            };
            Close = new AuthorityToWorkFormClose()
            {
                PermitAcceptor = new FormUserAction(),
                PermitAuthoriser = new FormUserAction()
            };
            Isolation = new AuthorityToWorkIsolation()
            {
                PermitAcceptor = new FormUserAction(),
                PermitAuthoriser = new FormUserAction(),
            };
            Deisolation = new AuthorityToWorkIsolation()
            {
                PermitAcceptor = new FormUserAction(),
                PermitAuthoriser = new FormUserAction(),
            };
            IsolationMarkup = new AuthorityToWorkIsolation()
            {
                PermitAcceptor = new FormUserAction(),
                PermitAuthoriser = new FormUserAction(),
            };
            Kind = new FormKind("ATW", "Authority To Work");
            PrincipalContractor = new AuthorityToWorkPerson();

            PermitAcceptor = new AuthorityToWorkPerson();
            Permits = new List<AuthorityToWorkPermit>();
            OnBehalfOf = new AuthorityToWorkPerson();
            PrincipalContractor = new AuthorityToWorkPerson();
        }

        public override string Description => WorksDescription;

        [JsonProperty("worksDescription")]
        public string WorksDescription { get; set; }

        [JsonProperty("physicalLocation")]
        public string PhysicalLocation { get; set; }

        [JsonProperty("stage")]
        public string Stage { get; set; }

        [JsonProperty("equipmentNumber")]
        public string EquipmentNumber { get; set; }

        [JsonProperty("lockbox")]
        public string Lockbox { get; set; }

        public string WorkOrderNumber { get; set; }

        public bool ShutdownRequired { get; set; }

        public List<DateTime> WorkDuration { get; set; }

        public AuthorityToWorkPerson OnBehalfOf { get; set; }

        public AuthorityToWorkPerson PrincipalContractor { get; set; }

        public AuthorityToWorkPerson PermitAcceptor { get; set; }

        public List<AuthorityToWorkPermit> Permits { get; set; }

        [JsonProperty("open")]
        public AuthorityToWorkFormOpen Open { get; set; }

        [JsonProperty("close")]
        public AuthorityToWorkFormClose Close { get; set; }

        [JsonProperty("isolation")]
        public AuthorityToWorkIsolation Isolation { get; set; }

        [JsonProperty("deisolation")]
        public AuthorityToWorkIsolation Deisolation { get; set; }

        [JsonProperty("isolationMarkup")]
        public AuthorityToWorkIsolation IsolationMarkup { get; set; }

        public List<AuthorityToWorkSignOnAndOff> SignOnsAndOffs { get; set; }

        public List<AuthorityToWorkFormTransfer> Transfers { get; set; }

        public List<AuthorityToWorkFormSuspension> Suspensions { get; set; }

        public List<AuthorityToWorkFormRevalidation> Revalidations { get; set; }

        public List<AuthorityToWorkFormCompletion> Completions { get; set; }

        public string GetNextId(string currentMaxId)
        {
            var number = Int32.Parse(currentMaxId.Replace("ATW", "")) + 1;

            return "ATW" + number.ToString().PadLeft(4, '0');
        }

        public class AuthorityToWorkPerson
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("number")]
            public string Number { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }
        }

        public class AuthorityToWorkPermit
        {
            public AuthorityToWorkPermit()
            {
                Extras = new List<AuthorityToWorkPermitExtra>();

            }

            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("permitId")]
            public string PermitId { get; set; }
            public bool Required { get; set; }
            public bool Generate { get; set; }
            public bool Removeable { get; set; }

            public List<AuthorityToWorkPermitExtra> Extras { get; set; }

            public class AuthorityToWorkPermitExtra
            {
                public string Name { get; set; }
                public string Value { get; set; }
            }
        }


        public class AuthorityToWorkSignOnAndOff
        {
            public DateTime? SignInDate { get; set; }
            public DateTime? SignOutDate { get; set; }
            public User PermitUser { get; set; }
        }

        public class AuthorityToWorkFormTransfer
        {
            public AuthorityToWorkFormTransfer()
            {
                OriginalPermitAcceptor = new FormUserAction();
                NewPermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            public FormUserAction OriginalPermitAcceptor { get; set; }
            public FormUserAction NewPermitAcceptor { get; set; }
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkFormSuspension
        {
            public AuthorityToWorkFormSuspension()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkFormRevalidation
        {
            public AuthorityToWorkFormRevalidation()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkFormOpen
        {
            public AuthorityToWorkFormOpen()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkIsolation
        {
            public AuthorityToWorkIsolation()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkFormClose
        {
            public AuthorityToWorkFormClose()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }
        }

        public class AuthorityToWorkFormCompletion
        {
            public AuthorityToWorkFormCompletion()
            {
                PermitAcceptor = new FormUserAction();
                PermitAuthoriser = new FormUserAction();
            }

            [JsonProperty("permitAcceptor")]
            public FormUserAction PermitAcceptor { get; set; }

            [JsonProperty("permitAuthoriser")]
            public FormUserAction PermitAuthoriser { get; set; }

            public string Status { get; set; }
        }

    }
}
