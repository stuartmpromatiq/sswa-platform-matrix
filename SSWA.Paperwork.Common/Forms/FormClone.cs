﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class FormClone
    {
        public string Id { get; set; }

        public List<DateTime> WorkDuration { get; set; }

        public List<FormCloneLink> LinkedForms { get; set; }

        public class FormCloneLink
        {
            public string Id { get; set; }
            public string FormId { get; set; }
            public bool Clone { get; set; }
        }

    }
}
