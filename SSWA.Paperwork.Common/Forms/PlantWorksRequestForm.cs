using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;

namespace SSWA.Paperwork.Common.Forms
{
    public class PlantWorksRequestForm : BaseForm
    {
        public PlantWorksRequestForm()
        {
            Kind = new FormKind("PWR", "Plant Works Request");
            ShiftInformation = new List<PlantWorksRequestFormShift>();
            AdditionalRequirements = new List<PlantWorksRequestFormAdditionalRequirement>();
            WorkDuration = new List<DateTime>();
        }

        public override string Description => WorksDescription;

        public string WorksDescription { get; set; }

        public string PhysicalLocation { get; set; }

        public string PrincipalContractor { get; set; }

        public List<DateTime> WorkDuration { get; set; }
        public string RequestReason { get; set; }

        public List<PlantWorksRequestFormShift> ShiftInformation { get; set; }

        public List<PlantWorksRequestFormAdditionalRequirement> AdditionalRequirements { get; set; }


        public class PlantWorksRequestFormShift
        {
            public string Label { get; set; }
            public string StartTime { get; set; }
            public string FinishTime { get; set; }
        }
        public class PlantWorksRequestFormAdditionalRequirement
        {
            public string Label { get; set; }
            public string Notes { get; set; }
        }

    }
}
