using Newtonsoft.Json;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;

namespace SSWA.Paperwork.Common.Forms
{
    public class PlantEntryRequestForm : BaseForm, IForm
    {
        public PlantEntryRequestForm()
        {

            Visitors = new List<PlantEntryRequestFormVisitor>();
            AwarenessTraining = new List<string>();
            Kind = new FormKind("PER", "Plant Entry Request");
            SwipeCardAccess = new PlantEntryRequestFormAccess();
            PlantAccess = new PlantEntryRequestFormAccess();
            VehicleAccess = new PlantEntryRequestFormVehicleDetails();
            Host = new PlantEntryRequestFormPlantHost();
        }

        public override string Description => RequestReason;

        public string RequestType { get; set; }
        public string RequestReason { get; set; }
        public PlantEntryRequestFormPlantHost Host { get; set; }

        [JsonProperty("visitors")]
        public List<PlantEntryRequestFormVisitor> Visitors { get; set; }
        public PlantEntryRequestFormVehicleDetails VehicleAccess { get; set; }
        public PlantEntryRequestFormAccess SwipeCardAccess { get; set; }
        public PlantEntryRequestFormAccess PlantAccess { get; set; }
        public List<string> AwarenessTraining { get; set; }

        public class PlantEntryRequestFormVisitor
        {
            public PlantEntryRequestFormVisitor()
            {
                VisitDuration = new List<DateTime>();
            }

            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("category")]
            public string Category { get; set; }

            [JsonProperty("company")]
            public string Company { get; set; }

            [JsonProperty("contactNumber")]
            public string ContactNumber { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }
            public string AccessCardNo { get; set; }
            public string ExpectedTime { get; set; }
            public List<DateTime> VisitDuration { get; set; }
        }

        public class PlantEntryRequestFormPlantHost
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string ContactNumber { get; set; }

            public string Email { get; set; }
        }

        public class PlantEntryRequestFormVehicleDetails
        {
            public string RegistrationNumber { get; set; }
            public string CardAuthorisationNumber { get; set; }
        }

        public class PlantEntryRequestFormAccess
        {
            public PlantEntryRequestFormAccess()
            {
                Areas = new List<string>();
            }

            public List<string> Areas { get; set; }
            public string Comments { get; set; }

        }
    }
}


