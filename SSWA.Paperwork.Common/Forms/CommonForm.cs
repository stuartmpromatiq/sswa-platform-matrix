using SSWA.Paperwork.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Common.Forms
{
    public class CommonForm : BaseForm
    {
        public CommonForm(string description)
        {
            Description = description;
        }
        public override string Description { get; }
    }
}
