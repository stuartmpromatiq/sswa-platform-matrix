﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class HighVoltagePermit : BaseForm
    {
        public HighVoltagePermit()
        {
            Kind = new Core.FormKind("HV", "High Voltage Permit");

        }
        public override string Description => WorksDescription;

        public string SwitchingProgamNumber { get; set; }

        public string Contractor { get; set; }

        public string ReferenceDrawingNumbers { get; set; }

        public string WorkLocation { get; set; }

        public string WorksDescription { get; set; }

        public string PermitAccess { get; set; }

        public string PermitPurpose { get; set; }

        public string IsolationPoints { get; set; }

        public string EquipmentEarthedAt { get; set; }
    }
}
