﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Common.Forms
{
    public class WorkingAtHeightsPermit : BaseForm
    {
        public WorkingAtHeightsPermit()
        {
            Kind = new Core.FormKind("WAH", "Working At Heights Permit");

            var ewpGroup = new FormFieldGroup("EWP", 1);
            var ladders = new FormFieldGroup("Ladders", 2);
            var general = new FormFieldGroup("General", 3);
            var fpe = new FormFieldGroup("Fall Prevention Equipment", 4);
            var workBox = new FormFieldGroup("Workbox/Mancage", 5);
            var gridmesh = new FormFieldGroup("Gridmesh/Flooring Removal", 6);

            Controls = new List<FormField>()
            {
                new FormField(ewpGroup, "Equipment is to be operated by a person competent to do so", 1),
                new FormField(ewpGroup, "Pre-start of the equipment has been completed and recorded", 1),
                new FormField(ewpGroup, "Ground conditions are suitable for the range of activities", 1),
                new FormField(ewpGroup, "Are there any overhead hazards that may affect operation", 1),
                new FormField(ladders, "Ladder is suitable (preferably a platform) and in good working order", 2),
                new FormField(ladders, "Work is being conducted within 2m of handrail, open edge or mobile equipment (if yes, fall prevention required)", 2),
                new FormField(general, "Is a spotter required", 3),
                new FormField(general, "Barricading/signage erected/posted", 3),
                new FormField(general, "Communications required and tested", 3),
                new FormField(general, "Personnel are trained and competent", 3),
                new FormField(fpe, "Equipment is rated to at least 15kn", 4),
                new FormField(fpe, "Anchor point is rated for 15kn loading", 4),
                new FormField(fpe, "Full body harness to be used has been inspected and is suitable for job tasks", 4),
                new FormField(fpe, "Shock absorbing lanyard required", 4),
                new FormField(fpe, "Dual lanyards required", 4),
                new FormField(workBox, "Other working platforms, eg, scaffold / ewp, have been considered as an alternative", 5),
                new FormField(workBox, "Workbox / mancage shall not be suspended over personnel", 5),
                new FormField(workBox, "An effective means of communication between the workbox and crane operator is provided", 5),
                new FormField(gridmesh, "Gridmesh / flooring removal permit attached", 6)
            };

            RescuePlan = new EmergencyRescuePlan();
        }

        public override string Description => WorksDescription;

        public string WorksDescription { get; set; }

        public string PhysicalLocation { get; set; }

        public string SubContractor { get; set; }

        public List<FormField> Controls { get; set; }

        public EmergencyRescuePlan RescuePlan { get; set; }

        
        public class EmergencyRescuePlan
        {
            public string RadioChannel { get; set; }

            public string PhoneNumber { get; set; }

            public string PersonnelRecoveryPlan { get; set; }

            public bool? RescueEquipmentAtJobSite { get; set; }
        }

        public class FormFieldGroup
        {
            public FormFieldGroup()
            {

            }
            public FormFieldGroup(string name, int order)
            {
                Name = name;
                Order = order;
            }
            public string Name { get; set; }
            public int Order { get; set; }
        }

        public class FormField
        {
            public FormField()
            {

            }
            public FormField(FormFieldGroup group, string control, int order)
            {
                Group = group;
                Control = control;
                Order = order;
            }

            public FormField(FormFieldGroup group, string control, string explanation, int order)
            {
                Group = group;
                Control = control;
                Explanation = explanation;
                Order = order;
            }

            public FormFieldGroup Group { get; set; }

            public int Order { get; set; }

            public string Control { get; set; }

            [JsonProperty( NullValueHandling = NullValueHandling.Ignore)]
            public string Explanation { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public bool? Answer { get; set; }
        }
    }
}
