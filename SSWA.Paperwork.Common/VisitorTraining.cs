﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Common
{
    public class VisitorTraining : BaseEntity, IEntity
    {
        public override string Type => "visitor_training";

        [JsonProperty("trainingType")]
        public string TrainingType { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public int? Expiry { get; set; }

        public string ExpiryUnit { get; set; }


        public string CanonicalTrainingType => TrainingType?.ToLowerInvariant()?.Trim();
        public string CanonicalName => Name?.ToLowerInvariant()?.Trim();

    }
}
