﻿using Microsoft.Reporting.WebForms;
using SSWA.Reports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSWA.Reports
{
    /// <summary>
    /// Summary description for Reports
    /// </summary>
    public class Reports : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            LocalReport myReport = new LocalReport
            {
                //ReportEmbeddedResource = "SSWA.Reports.Models.CalibrationReport_Outer.rdlc";
                ReportPath = "bin/CalibrationReport_Outer.rdlc",
            };

            var data = new CalibrationReport()
            {
                TagName = "AT00001",
                TagDescription = "TRANSMITTER, FLOW, AIR BUILDING CC05101 INSTRUMENT AIR",
                TagManufacturer = "MAKE MANU",
                TagModel = "MODEL MODEL",
                TagDataSheet = "DS-00001-A01",
                TagProcedure = "PR-046172-01-A",
                TagReferenceDrawing = "DR-AB678-126",
                TagSerialNumber = "SN-00000000001",
                CalibrationComments = "COMMENTS ABOUT CALIBRATION 12345",
                CalibrationRecordNumber = "C-0101223-1122",
                CalibrationTestedAt = DateTime.Parse("2017-01-01"),
                CalibrationTestedBy = "Liam Wallace",
                CalibrationResult = true,
                CalibrationWorkOrderNumber = "WO-1233123-123123",
                DocumentIssuedAt = DateTime.Parse("2017-01-01"),
                DocumentNumber = "DOC-01230123-123123",
                DocumentRevision = "A"
            };

            var equipment = new List<CalibrationEquipment>()
            {
                new CalibrationEquipment()
                    {
                    MakeModel = "MAKE MANU MODEL",
                    SerialNumber = "SN-00000001",
                    TestFrequency = "MONTHLY",
                    Type = "CALIB EQUIP TYPE"
                    },
                new CalibrationEquipment()
                {
                    MakeModel = "MAKE MANU MODEL",
                    SerialNumber = "SN-00000002",
                    TestFrequency = "DAILY",
                    Type = "AJJDA AJDIA ADNADN"
                },
                    new CalibrationEquipment()
                {
                    MakeModel = "MAKE MANU MODEL",
                    SerialNumber = "SN-00000003",
                    TestFrequency = "WEEKLY",
                    Type = "APFAI PFNKA LFHANA"
                }
            };

            var reportDataSource = new ReportDataSource("Calibration", new List<CalibrationReport>() { data });
            var reportDataSource2 = new ReportDataSource("Equipment", equipment);

            myReport.DataSources.Add(reportDataSource);
            myReport.DataSources.Add(reportDataSource2);

            myReport.SubreportProcessing += MyReport_SubreportProcessing;
            var deviceInfo = 
                "<DeviceInfo>" + 
                " <OutputFormat>EMF</OutputFormat>" +
                " <PageWidth>21cm</PageWidth>" + 
                " <PageHeight>29.7cm</PageHeight>" + 
                " <MarginTop>1cm</MarginTop>" + 
                " <MarginLeft>1cm</MarginLeft>" + 
                " <MarginRight>1cm</MarginRight>" + 
                " <MarginBottom>1cm</MarginBottom>" + 
                "</DeviceInfo>";
            var pdf = myReport.Render("PDF", deviceInfo);

            context.Response.Clear();
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Disposition", "attachment;filename=\"File.pdf\"");
            context.Response.BinaryWrite(pdf);
            context.Response.Flush();
            context.Response.End();

            context.ApplicationInstance.CompleteRequest();
        }

        private void MyReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            if (e.ReportPath.Contains("chlorine"))
            {
                var chlorineData = new ChlorineReport()
                {
                    CalibrationAsFound = 1,
                    CalibrationAsLeft = 1,
                    CalibrationObservedSetPointEight = 8,
                    CalibrationObservedSetPointEleven = 11,
                    CalibrationObservedSetPointFive = 5,
                    CalibrationObservedSetPointFour = 4,
                    CalibrationObservedSetPointNine = 9,
                    CalibrationObservedSetPointOne = 1,
                    CalibrationObservedSetPointSeven = 7,
                    CalibrationObservedSetPointSix = 6,
                    CalibrationObservedSetPointTen = 10,
                    CalibrationObservedSetPointThree = 3,
                    CalibrationObservedSetPointTwo = 2,
                    TagAccuracy = 0.5m,
                    TagAccuracyUom = "chl",
                    TagMaxRange = 12,
                    TagMinRange = 4,
                    TagSetPointEight = 80,
                    TagSetPointEleven = 110,
                    TagSetPointFive = 50,
                    TagSetPointFour = 40,
                    TagSetPointNine = 90,
                    TagSetPointOne = 10,
                    TagSetPointSeven = 70,
                    TagSetPointSix = 60,
                    TagSetPointTen = 100,
                    TagSetPointThree = 30,
                    TagSetPointTwo = 20,
                    TagUom = "chl2"
                };

                var reportDataSource3 = new ReportDataSource("ChlorineReport", new List<ChlorineReport>() { chlorineData });
                e.DataSources.Add(reportDataSource3);
            }

            if (e.ReportPath.Contains("flow"))
            {
                var data = new FlowReport();
                var reportDataSource3 = new ReportDataSource("FlowReport", new List<FlowReport>() { data });
                e.DataSources.Add(reportDataSource3);
            }

            if (e.ReportPath.Contains("standard"))
            {
                var data = new StandardReport();
                var reportDataSource3 = new ReportDataSource("StandardReport", new List<StandardReport>() { data });
                e.DataSources.Add(reportDataSource3);
            }
          
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}