﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSWA.Reports.Models
{
    public class StdTag
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Passed { get; set; }
    }

    public class StdTagDataSource : IEnumerable<StdTag>
    {
        public IEnumerator<StdTag> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}