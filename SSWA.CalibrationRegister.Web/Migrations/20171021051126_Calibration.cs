﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SSWA.CalibrationRegister.Web.Migrations
{
    public partial class Calibration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Calibration_Tag_TagID",
                table: "Calibration");

            migrationBuilder.DropIndex(
                name: "IX_Calibration_TagID",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "TagID",
                table: "Calibration");

            migrationBuilder.AddColumn<int>(
                name: "CalibrationTagID",
                table: "Calibration",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CalibrationValues",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Comments",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentNumber",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "IssuedAt",
                table: "Calibration",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "RecordNumber",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResultCalculations",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Revision",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TestedAt",
                table: "Calibration",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "TestedBy",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkOrderNumber",
                table: "Calibration",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CalibrationEquipment",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CalibrationID = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Inactive = table.Column<bool>(type: "bit", nullable: false),
                    MakeModel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SerialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TestFrequency = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalibrationEquipment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CalibrationEquipment_Calibration_CalibrationID",
                        column: x => x.CalibrationID,
                        principalTable: "Calibration",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CalibrationTag",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataSheet = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fields = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Inactive = table.Column<bool>(type: "bit", nullable: false),
                    Make = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Model = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Procedure = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReferenceDrawing = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SerialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TagID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalibrationTag", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CalibrationTag_Tag_TagID",
                        column: x => x.TagID,
                        principalTable: "Tag",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Calibration_CalibrationTagID",
                table: "Calibration",
                column: "CalibrationTagID");

            migrationBuilder.CreateIndex(
                name: "IX_CalibrationEquipment_CalibrationID",
                table: "CalibrationEquipment",
                column: "CalibrationID");

            migrationBuilder.CreateIndex(
                name: "IX_CalibrationTag_TagID",
                table: "CalibrationTag",
                column: "TagID");

            migrationBuilder.AddForeignKey(
                name: "FK_Calibration_CalibrationTag_CalibrationTagID",
                table: "Calibration",
                column: "CalibrationTagID",
                principalTable: "CalibrationTag",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Calibration_CalibrationTag_CalibrationTagID",
                table: "Calibration");

            migrationBuilder.DropTable(
                name: "CalibrationEquipment");

            migrationBuilder.DropTable(
                name: "CalibrationTag");

            migrationBuilder.DropIndex(
                name: "IX_Calibration_CalibrationTagID",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "CalibrationTagID",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "CalibrationValues",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "Comments",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "DocumentNumber",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "IssuedAt",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "RecordNumber",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "ResultCalculations",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "Revision",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "TestedAt",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "TestedBy",
                table: "Calibration");

            migrationBuilder.DropColumn(
                name: "WorkOrderNumber",
                table: "Calibration");

            migrationBuilder.AddColumn<int>(
                name: "TagID",
                table: "Calibration",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Calibration_TagID",
                table: "Calibration",
                column: "TagID");

            migrationBuilder.AddForeignKey(
                name: "FK_Calibration_Tag_TagID",
                table: "Calibration",
                column: "TagID",
                principalTable: "Tag",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
