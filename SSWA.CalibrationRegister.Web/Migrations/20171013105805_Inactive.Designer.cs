﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using SSWA.CalibrationRegister.Web.Database;
using System;

namespace SSWA.CalibrationRegister.Web.Migrations
{
    [DbContext(typeof(CalibrationDbContext))]
    [Migration("20171013105805_Inactive")]
    partial class Inactive
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.Calibration", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("Inactive");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<int>("TagID");

                    b.HasKey("ID");

                    b.HasIndex("TagID");

                    b.ToTable("Calibration");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.Equipment", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("Inactive");

                    b.Property<string>("MakeModel");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("SerialNumber");

                    b.Property<string>("TestFrequency");

                    b.Property<string>("Type");

                    b.HasKey("ID");

                    b.ToTable("Equipment");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.ReportingTagType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("Inactive");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("ReportingTagType");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.Tag", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("DataSheet");

                    b.Property<string>("Description");

                    b.Property<string>("Fields");

                    b.Property<bool>("Inactive");

                    b.Property<string>("Make");

                    b.Property<string>("Model");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("Name");

                    b.Property<string>("Procedure");

                    b.Property<string>("ReferenceDrawing");

                    b.Property<string>("SerialNumber");

                    b.Property<int>("TagTypeID");

                    b.HasKey("ID");

                    b.HasIndex("TagTypeID");

                    b.ToTable("Tag");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.TagType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CalibrationFields");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<bool>("Inactive");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("Name");

                    b.Property<int>("ReportingTagTypeID");

                    b.Property<string>("ResultCalculations");

                    b.Property<string>("TagFields");

                    b.Property<string>("UnitOfMeasure");

                    b.HasKey("ID");

                    b.HasIndex("ReportingTagTypeID");

                    b.ToTable("TagType");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AuthId")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<bool>("Inactive");

                    b.Property<DateTime>("ModifiedAt");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("ID");

                    b.ToTable("User");
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.Calibration", b =>
                {
                    b.HasOne("SSWA.CalibrationRegister.Web.Database.Tag", "Tag")
                        .WithMany()
                        .HasForeignKey("TagID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.Tag", b =>
                {
                    b.HasOne("SSWA.CalibrationRegister.Web.Database.TagType", "TagType")
                        .WithMany()
                        .HasForeignKey("TagTypeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SSWA.CalibrationRegister.Web.Database.TagType", b =>
                {
                    b.HasOne("SSWA.CalibrationRegister.Web.Database.ReportingTagType", "ReportingTagType")
                        .WithMany()
                        .HasForeignKey("ReportingTagTypeID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
