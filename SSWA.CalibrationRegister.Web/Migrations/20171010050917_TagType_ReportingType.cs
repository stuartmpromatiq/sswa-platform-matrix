﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SSWA.CalibrationRegister.Web.Migrations
{
    public partial class TagType_ReportingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReportingTagTypeID",
                table: "TagType",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TagType_ReportingTagTypeID",
                table: "TagType",
                column: "ReportingTagTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_TagType_ReportingTagType_ReportingTagTypeID",
                table: "TagType",
                column: "ReportingTagTypeID",
                principalTable: "ReportingTagType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagType_ReportingTagType_ReportingTagTypeID",
                table: "TagType");

            migrationBuilder.DropIndex(
                name: "IX_TagType_ReportingTagTypeID",
                table: "TagType");

            migrationBuilder.DropColumn(
                name: "ReportingTagTypeID",
                table: "TagType");
        }
    }
}
