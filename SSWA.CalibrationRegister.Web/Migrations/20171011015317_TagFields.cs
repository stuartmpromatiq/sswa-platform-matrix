﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SSWA.CalibrationRegister.Web.Migrations
{
    public partial class TagFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferenceNumber",
                table: "Tag");

            migrationBuilder.AddColumn<string>(
                name: "DataSheet",
                table: "Tag",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Procedure",
                table: "Tag",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferenceDrawing",
                table: "Tag",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataSheet",
                table: "Tag");

            migrationBuilder.DropColumn(
                name: "Procedure",
                table: "Tag");

            migrationBuilder.DropColumn(
                name: "ReferenceDrawing",
                table: "Tag");

            migrationBuilder.AddColumn<string>(
                name: "ReferenceNumber",
                table: "Tag",
                nullable: true);
        }
    }
}
