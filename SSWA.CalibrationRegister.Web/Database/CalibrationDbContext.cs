﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SSWA.CalibrationRegister.Web.Models;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class CalibrationDbContext : DbContext
    {
        public CalibrationDbContext(DbContextOptions<CalibrationDbContext> options) : base(options) { }

        public DbSet<Calibration> Calibrations { get; set; }
        public DbSet<CalibrationTag> CalibrationTags { get; set; }
        public DbSet<CalibrationEquipment> CalibrationEquipment { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagType> TagTypes { get; set; }
        public DbSet<ReportingTagType> ReportingTagTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Equipment> Equipment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Calibration>().ToTable("Calibration");
            modelBuilder.Entity<CalibrationTag>().ToTable("CalibrationTag");
            modelBuilder.Entity<CalibrationEquipment>().ToTable("CalibrationEquipment");
            modelBuilder.Entity<Tag>().ToTable("Tag");
            modelBuilder.Entity<TagType>().ToTable("TagType");
            modelBuilder.Entity<ReportingTagType>().ToTable("ReportingTagType");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Equipment>().ToTable("Equipment");

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            AddAuitInfo();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddAuitInfo();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void AddAuitInfo()
        {
            var entries = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    ((BaseEntity)entry.Entity).CreatedAt = DateTime.UtcNow;
                }
            ((BaseEntity)entry.Entity).ModifiedAt = DateTime.UtcNow;
            }
        }
    }
}
