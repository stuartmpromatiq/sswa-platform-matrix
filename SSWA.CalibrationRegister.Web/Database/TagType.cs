﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class TagType : BaseEntity
    {
        public string Name { get; set; }
        public string UnitOfMeasure { get; set; }
        public string TagFields { get; set; }
        public string CalibrationFields { get; set; }
        public string ResultCalculations { get; set; }
        public int ReportingTagTypeID { get; set; }

        public virtual ReportingTagType ReportingTagType { get; set; }
    }
}

