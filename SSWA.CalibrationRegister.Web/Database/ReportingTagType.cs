﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class ReportingTagType : BaseEntity
    {
        public string Name { get; set; }
    }
}
