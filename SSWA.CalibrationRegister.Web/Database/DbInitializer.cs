﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public static class DbInitializer
    {
        public static void Initialize(CalibrationDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has een seeded
            }

            var users = new User[]
            {
                new User{ Email = "liamw@promatiq.com", FirstName = "Liam", LastName = "Wallace", AuthId = "123" }
            };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();       
        }
    }
}
