﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class Tag : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string ReferenceDrawing { get; set; }
        public string Procedure { get; set; }
        public string DataSheet { get; set; }
        public int TagTypeID { get; set; }
        public string Fields { get; set; }

        [NotMapped]
        public string NiceFields { get {
                if (!string.IsNullOrWhiteSpace(Fields)) {
                    var fields = JsonConvert.DeserializeObject<Dictionary<string, string>>(Fields);
                    var stringFields = fields
                        .Where(x => !string.IsNullOrWhiteSpace(x.Value))
                        .Select(x =>
                        {
                            return x.Key + " = " + x.Value;
                        });

                    return string.Join(", ", stringFields);
                }
                else
                {
                    return "";
                }
            }
        }

        public virtual TagType TagType { get; set; }
    }
}
