﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class Equipment : BaseEntity
    {
        [DisplayName("Make / Model")]
        public string MakeModel { get; set; }

        [DisplayName("Equipment Type")]
        public string Type { get; set; }

        [DisplayName("Serial Number")]
        public string SerialNumber { get; set; }

        [DisplayName("Test Frequency")]
        public string TestFrequency { get; set; }
    }
}
