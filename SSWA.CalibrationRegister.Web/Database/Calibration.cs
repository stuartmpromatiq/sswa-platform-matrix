﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Database
{
    public class Calibration : BaseEntity
    {
        public string RecordNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string Revision { get; set; }
        public DateTime IssuedAt { get; set; }
        public DateTime TestedAt { get; set; }
        public string TestedBy { get; set; }
        public string Comments { get; set; }
        public string WorkOrderNumber { get; set; }
        public int CalibrationTagID { get; set; }
        public string CalibrationValues { get; set; }
        public string ResultCalculations { get; set; }

        public bool? VerificationOnly { get; set; }

        public virtual CalibrationTag CalibrationTag { get; set; }
        public virtual ICollection<CalibrationEquipment> CalibrationEquipment { get; set; }
    }

    public class CalibrationTag : BaseEntity
    {
        public int TagID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string ReferenceDrawing { get; set; }
        public string Procedure { get; set; }
        public string DataSheet { get; set; }
        public string Fields { get; set; }

        public virtual Tag Tag { get; set; }
    }

    public class CalibrationEquipment : BaseEntity
    {
        public string MakeModel { get; set; }
        public string Type { get; set; }
        public string SerialNumber { get; set; }
        public string TestFrequency { get; set; }
        public int CalibrationID { get; set; }

        public virtual Calibration Calibration { get; set; }
    }
}
