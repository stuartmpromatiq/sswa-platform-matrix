﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SSWA.CalibrationRegister.Web.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class EditTagTypeViewModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }

        [DisplayName("Name"), Required]
        public string Name { get; set; }

        [DisplayName("Unit Of Measure")]
        public string UnitOfMeasure { get; set; }

        public IList<Field> TagFields { get; set; }

        public IList<Field> CalibrationFields { get; set; }

        public IList<Field> ResultCalculations { get; set; }

        [DisplayName("Reporting Type")]
        public int ReportingTagTypeID { get; set; }

        public SelectList ReportingTagTypes { get; set; }

        public SelectList DataTypes { get; set; }
    }
}
