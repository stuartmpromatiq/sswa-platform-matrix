﻿using SSWA.CalibrationRegister.Web.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class TagTypeListViewModel
    {
        [DisplayName("ID")]
        public int ID { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Reporting Type")]
        public string ReportingType { get; set; }

        [DisplayName("Unit Of Measure")]
        public string UnitOfMeasure { get; set; }

        [DisplayName("Tag Fields")]
        public string TagFields { get; set; }

        [DisplayName("Calibration Fields")]
        public string CalibrationFields { get; set; }

        [DisplayName("Result Calculations")]
        public string ResultCalculations { get; set; }

        [DisplayName("Created")]
        public string CreatedAt { get; set; }

        [DisplayName("Updated")]
        public string ModifiedAt { get; set; }

    }
}
