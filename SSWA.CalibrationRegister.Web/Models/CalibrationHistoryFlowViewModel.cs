﻿using SSWA.Reports.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class CalibrationHistoryFlowViewModel : ICalibrationHistoryViewModel
    {
        public CalibrationHistoryViewModel Details { get; set; }
        public IEnumerable<FlowHistoryReport> Calibrations { get; set; }


        public string ChartData { get; set; }
        public string ChartValues { get; set; }
        public string ChartNames { get; set; }
    }
}
