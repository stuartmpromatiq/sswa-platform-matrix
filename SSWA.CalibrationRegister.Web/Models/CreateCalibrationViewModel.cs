﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SSWA.CalibrationRegister.Web.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class CreateCalibrationViewModel
    {
        public CreateCalibrationViewModel()
        {
            Equipment = new List<Equipment>();
            TagFields = new List<Field>();
            CalibrationFields = new List<Field>();
        }

        [DisplayName("Record Number")]
        public string RecordNumber { get; set; }

        [Required]
        [DisplayName("Document Number")]
        public string DocumentNumber { get; set; }

        [Required]
        [DisplayName("Revision")]
        public string Revision { get; set; }

        [Required]
        [DisplayName("Issued At")]
        [DataType(DataType.Date)]
        public DateTime IssuedAt { get; set; }

        [Required]
        [DisplayName("Tested At")]
        [DataType(DataType.Date)]
        public DateTime TestedAt { get; set; }

        [Required]
        [DisplayName("Tested By")]
        public string TestedBy { get; set; }

        [DisplayName("Comments")]
        public string Comments { get; set; }

        [Required]
        [DisplayName("Work Order Number")]
        public string WorkOrderNumber { get; set; }

        [Required]
        public int TagID { get; set; }

        [DisplayName("Name")]
        public string TagName { get; set; }

        [DisplayName("Description")]
        public string TagDescription { get; set; }

        [DisplayName("Make")]
        public string TagMake { get; set; }

        [DisplayName("Model")]
        public string TagModel { get; set; }

        [DisplayName("Serial Number")]
        public string TagSerialNumber { get; set; }

        [DisplayName("Reference Drawing")]
        public string TagReferenceDrawing { get; set; }

        [DisplayName("Procedure")]
        public string TagProcedure { get; set; }

        [DisplayName("Data Sheet")]
        public string TagDataSheet { get; set; }

        public IList<Field> TagFields { get; set; }

        public IList<Field> CalibrationFields { get; set; }

        public IList<Equipment> Equipment { get; set; }

        public SelectList EquipmentSelectList { get; set; }

        [DisplayName("Verifcation Only")]
        public bool VerificationOnly { get; set; }
    }
}
