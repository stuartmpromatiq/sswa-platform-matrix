﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class RoleListViewModel
    {
        public bool IsInRole { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RoleId { get; set; }

        [HiddenInput(DisplayValue = true)]
        public string RoleName { get; set; }
    }
}
