﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SSWA.CalibrationRegister.Web.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class CalibrationListViewModel
    {
        [HiddenInput]
        public int ID { get; set; }

        [DisplayName("Calibration Record Number")]
        public string RecordNumber { get; set; }

        [DisplayName("Tag Name")]
        public string TagName { get; set; }

        [DisplayName("Tag Description.")]
        public string TagDescription { get; set; }

        [DisplayName("Tested Date")]
        public DateTime TestedAt { get; set; }

        [DisplayName("Tested By")]
        public string TestedBy { get; set; }

        [DisplayName("Pass / Fail")]
        public bool PassFail { get; set; }

        [DisplayName("Verification Only")]
        public bool VerificationOnly { get; set; }

        public int TagID { get; set; }
    }
}
