﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class EditTagViewModel
    {
        public int ID { get; set; }

        [DisplayName("Name"), Required]
        public string Name { get; set; }

        [DisplayName("Description"), Required]
        public string Description { get; set; }

        [DisplayName("Make")]
        public string Make { get; set; }

        [DisplayName("Model")]
        public string Model { get; set; }

        [DisplayName("Serial Number")]
        public string SerialNumber { get; set; }

        [DisplayName("Reference Drawing")]
        public string ReferenceDrawing { get; set; }

        [DisplayName("Procedure")]
        public string Procedure { get; set; }

        [DisplayName("Data Sheet")]
        public string DataSheet { get; set; }

        [DisplayName("Tag Type")]
        public string TagTypeName { get; set; }

        public IEnumerable<Field> Fields { get; set; }
    }
}
