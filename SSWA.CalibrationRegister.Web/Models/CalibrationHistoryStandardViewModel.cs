﻿using SSWA.Reports.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class CalibrationHistoryStandardViewModel : ICalibrationHistoryViewModel
    {
        public CalibrationHistoryViewModel Details { get; set; }
        public IEnumerable<StandardHistoryReport> Calibrations { get; set; }

        public string ChartData { get; set; }
        public string ChartValues  { get; set; }
        public string ChartNames { get; set; }
    }
}
