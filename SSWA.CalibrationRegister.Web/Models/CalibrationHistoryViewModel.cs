﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class CalibrationHistoryViewModel
    {
        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public int TagID { get; set; }
    }
}
