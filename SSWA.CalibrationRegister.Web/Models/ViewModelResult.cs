﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class ViewModelResult<T> where T : class
    {
        public T ViewModel { get; private set; }

        public bool IsValid { get; private set; }

        public ICollection<string> Messages { get; private set; }

        public ViewModelResult(T viewModel)
        {
            ViewModel = viewModel;
            Messages = new List<string>();
            IsValid = true;
        }

        public void AddError(string message)
        {
            IsValid = false;
            Messages.Add(message);
        }

    }
}
