﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public static class Extensions
    {
        public static IList<Field> MapToFields(this string templateFields)
        {
            return JsonConvert.DeserializeObject<IList<Field>>(templateFields);
        }

        public static Dictionary<string, string> MapToDictionary(this string fields)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(fields);
        }

        public static IList<Field> MapToFields(this string templateFields, string valueFields)
        {
            var fields = JsonConvert.DeserializeObject<IList<Field>>(templateFields);

            var values = new Dictionary<string, string>();
            try
            {
                values = JsonConvert.DeserializeObject<Dictionary<string, string>>(valueFields);
            }
            catch
            {
            }


            foreach (var value in values)
            {
                var field = fields.SingleOrDefault(x => x.Id == value.Key);

                if (field == null) continue;

                field.Value = value.Value;
            }

            return fields;
        }

        public static string GetFirstName(this ClaimsPrincipal claimsPrincipal)
        {
            var firstName = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName);
            return firstName?.Value;
        }

        public static string GetLastName(this ClaimsPrincipal claimsPrincipal)
        {
            var firstName = claimsPrincipal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname);
            return firstName?.Value;
        }

        public static bool HasRole(this ClaimsPrincipal claimsPrincipal, string role)
        {
            role = role?.ToLowerInvariant();
            return claimsPrincipal.Claims
                .Any(x => x.Type == "http://api.sswa/claims/roles" && x.Value.ToLowerInvariant() == role);
        }

        public static void AddModelError<TModel, TProperty>(
            this ModelStateDictionary modelState,
            Expression<Func<TModel, TProperty>> ex,
            string message
        )
        {
            var key = ExpressionHelper.GetExpressionText(ex);
            modelState.AddModelError(key, message);
        }
    }
}
