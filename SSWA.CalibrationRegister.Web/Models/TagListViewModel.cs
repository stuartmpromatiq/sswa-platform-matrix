﻿using Microsoft.AspNetCore.Mvc.Rendering;
using SSWA.CalibrationRegister.Web.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class TagListViewModel
    {
        public PaginatedList<Tag> Tags { get; set; }
        public IEnumerable<TagType> TagTypes { get; set; }
        public int? TagTypeId { get; set; }
        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public SelectList TagTypeList { get; set; }
    }
}
