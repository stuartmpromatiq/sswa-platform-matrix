﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Models
{
    public class Field
    {
        [DisplayName("ID"), Required]
        public string Id { get; set; }

        [DisplayName("Name"), Required]
        public string DisplayName { get; set; }

        [DisplayName("Sort Order"), Required]
        public int SortOrder { get; set; }

        [DisplayName("Data Type"), Required]
        public string DataType { get; set; }

        [DisplayName("Default Value")]
        public string DefaultValue { get; set; }

        public string Value { get; set; }

        public bool Inactive { get; set; }

        [DisplayName("Show On Chart")]
        public bool Chart { get; set; }
    }
    
}
