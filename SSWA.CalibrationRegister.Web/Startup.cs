﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.EntityFrameworkCore;
using SSWA.CalibrationRegister.Web.Database;
using SSWA.CalibrationRegister.Web.Services;
using SSWA.ExternalUserService;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using SSWA.Reports.Core;

namespace CalRegConcept
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<UserResolverService>();
            services.AddScoped<TagService>();
            services.AddScoped<UserService>();
            services.AddScoped<CalibrationService>();
            services.AddTransient<IExternalUserService, Auth0UserService>(
                s => new Auth0UserService(
                    new Auth0UserServiceConfiguration()
                    {
                        AppId = Configuration["Auth0:ClientId"],
                        ClientId = Configuration["Auth0:ClientId"],
                        ClientSecret = Configuration["Auth0:ClientSecret"],
                        Domain = Configuration["Auth0:Domain"],
                        Connection = Configuration["Auth0:Connection"],
                        SigningKey = Configuration["Auth0:SigningKey"],
                        SendgridApiKey = Configuration["SendGrid:ApiKey"],
                        AppName = "Calibration Register"
                    }));
            services.AddScoped(s => new ReportService(Configuration.GetConnectionString("DefaultConnection")));

            // Add entity framework core
            services.AddDbContextPool<CalibrationDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), opt => opt.UseRowNumberForPaging())
            );

            // Add authentication services
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
                options.LogoutPath = new PathString("/Account/Logout");
            })
            .AddOpenIdConnect("Auth0", options => {

                options.SaveTokens = true;

                // Set the authority to your Auth0 domain
                options.Authority = $"https://{Configuration["Auth0:Domain"]}";

                // Configure the Auth0 Client ID and Client Secret
                options.ClientId = Configuration["Auth0:ClientId"];
                options.ClientSecret = Configuration["Auth0:ClientSecret"];

                // Set response type to code
                options.ResponseType = OpenIdConnectResponseType.Code;
                options.AuthenticationMethod = OpenIdConnectRedirectBehavior.RedirectGet;

                // Configure the scope
                options.Scope.Clear();
                options.Scope.Add("openid");
                options.Scope.Add("profile");

                // Set the callback path, so Auth0 will call back to http://localhost:5000/signin 
                // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard 
                options.CallbackPath = new PathString("/signin");

                // Configure the Claims Issuer to be Auth0
                options.ClaimsIssuer = "Auth0";


                options.Events = new OpenIdConnectEvents
                {
                    // handle the logout redirection 
                    OnRedirectToIdentityProviderForSignOut = (context) =>
                    {
                        var logoutUri = $"https://{Configuration["Auth0:Domain"]}/v2/logout?client_id={Configuration["Auth0:ClientId"]}";

                        var postLogoutUri = context.Properties.RedirectUri;
                        if (!string.IsNullOrEmpty(postLogoutUri))
                        {
                            if (postLogoutUri.StartsWith("/"))
                            {
                                // transform to absolute
                                var request = context.Request;
                                postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase + postLogoutUri;
                            }
                            logoutUri += $"&returnTo={ Uri.EscapeDataString(postLogoutUri)}";
                        }

                        context.Response.Redirect(logoutUri);
                        context.HandleResponse();

                        return Task.CompletedTask;
                    },
                    OnRedirectToIdentityProvider = (context) =>
                    {
                        context.ProtocolMessage.SetParameter("connection", Configuration["Auth0:Connection"]);
                        return Task.CompletedTask;
                    },
                    OnUserInformationReceived = notification =>
                    {
                        Console.WriteLine("Token validation received");
                        return Task.FromResult(true);
                    },
                    OnTokenValidated = notification =>
                    {
                        var claimsIdentity = notification.Principal.Identity as ClaimsIdentity;
                        Console.WriteLine($"{claimsIdentity?.Name} authenticated using bearer authentication.");

                        return Task.FromResult(true);
                    }
                };
            });

            services.AddMvc();

            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});

            services.AddAuthorization(options =>
            {
                options.AddPolicy("create:tags", policy => policy.RequireClaim("http://api.sswa/claims/permissions", "create:tags"));
                options.AddPolicy("edit:tags", policy => policy.RequireClaim("http://api.sswa/claims/permissions", "edit:tags"));
                options.AddPolicy("delete:tags", policy => policy.RequireClaim("http://api.sswa/claims/permissions", "delete:tags"));

                options.AddPolicy("create:calibrations", policy => policy.RequireClaim("http://api.sswa/claims/permissions", "create:calibrations"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            InitializeDatabase(app);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Calibration}/{action=Index}/{id?}");
            });

            //var options = new RewriteOptions()
            //    .AddRedirectToHttps();

            //app.UseRewriter(options);
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var database = scope.ServiceProvider.GetRequiredService<CalibrationDbContext>().Database;
                foreach (var line in database.GetPendingMigrations())
                {
                    database.Migrate();
                }

            }
        }
    }
}
