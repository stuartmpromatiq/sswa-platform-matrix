﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SSWA.CalibrationRegister.Web.Database;
using SSWA.CalibrationRegister.Web.Models;
using SSWA.Reports.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Services
{
    public class CalibrationService
    {
        private CalibrationDbContext _context;
        private TagService _tagService;
        private UserService _userService;
        private ReportService _reportService;

        public CalibrationService(CalibrationDbContext context, TagService tagService, UserService userService, ReportService reportService)
        {
            _context = context;
            _tagService = tagService;
            _userService = userService;
            _reportService = reportService;
        }

        public async Task<ICalibrationHistoryViewModel> GetCalibrationHistoryViewModelAsync(int tagId)
        {
            var tag = await _tagService.GetTagAsync(tagId);
            var tagType = await _tagService.GetTagTypeAsync(tag.TagTypeID);

            var calibrationFields = JsonConvert.DeserializeObject<IList<Field>>(tagType.CalibrationFields, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            var tagFields = JsonConvert.DeserializeObject<IList<Field>>(tagType.TagFields, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            var chartFields = calibrationFields.Where(x => x.DataType == "number" && x.Chart).Union(tagFields.Where(y => y.DataType == "number" && y.Chart));
   
            var chartValues = JsonConvert.SerializeObject(chartFields.Select(x => x.Id));
            var chartNames = JsonConvert.SerializeObject(chartFields.ToDictionary(x => x.Id, y => y.DisplayName));

            var history = await _reportService.GetCalibrationHistoryRecordsAsync(tagId);
            var header = new CalibrationHistoryViewModel();

            header.TagName = tag.Name;
            header.TagDescription = tag.Description;
            header.TagID = tag.ID;

            ICalibrationHistoryViewModel viewModel = default(ICalibrationHistoryViewModel);
            switch (tagType.ReportingTagType.Name.ToUpperInvariant())
            {
                case "CHLORINE":

                    var chlorineViewModel = new CalibrationHistoryChlorineViewModel();
                    var chlorineHistory = _reportService.GetChlorineHistoryReport(history);

                    chlorineViewModel.Details = header;
                    chlorineViewModel.Calibrations = chlorineHistory;
                    chlorineViewModel.ChartData = JsonConvert.SerializeObject(chlorineHistory);
                    chlorineViewModel.ChartValues = chartValues;
                    chlorineViewModel.ChartNames = chartNames;

                    return chlorineViewModel;
 
                case "FLOW":

                    var flowViewModel = new CalibrationHistoryFlowViewModel();
                    var flowHistory = _reportService.GetFlowHistoryReport(history);

                    flowViewModel.Details = header;
                    flowViewModel.Calibrations = flowHistory;
                    flowViewModel.ChartData = JsonConvert.SerializeObject(flowHistory);
                    flowViewModel.ChartValues = chartValues;
                    flowViewModel.ChartNames = chartNames;

                    return flowViewModel;

                case "STD":

                    var standardviewModel = new CalibrationHistoryStandardViewModel();
                    var standardHistory = _reportService.GetStandardHistoryReport(history);

                    standardviewModel.Details = header;
                    standardviewModel.Calibrations = standardHistory;
                    standardviewModel.ChartData = JsonConvert.SerializeObject(standardHistory);
                    standardviewModel.ChartValues = chartValues;
                    standardviewModel.ChartNames = chartNames;

                    return standardviewModel;
            }

            return viewModel;
        }

        public async Task<ViewModelResult<CreateCalibrationViewModel>> GetCreateCalibrationViewModelAsync(int tagId)
        {
            if (tagId <= 0)
                throw new ArgumentOutOfRangeException(nameof(tagId));

            var tag = await _tagService.GetTagAsync(tagId);

            if (tag == null)
                throw new ArgumentOutOfRangeException(nameof(tag));

            var tagType = await _tagService.GetTagTypeAsync(tag.TagTypeID);

            if (tagType == null)
                throw new ArgumentOutOfRangeException(nameof(tagType));

            var equipment = await _context.Equipment
                                          .Where(x => !x.Inactive)
                                          .OrderBy(x => x.Type)
                                          .ThenBy(x => x.MakeModel)
                                          .ThenBy(x => x.SerialNumber)
                                          .Select(x => new { Type = x.Type, ID = x.ID, MakeModel = x.MakeModel + " / SN: " + x.SerialNumber })
                                          .ToListAsync();



            var user = await _userService.GetCurrentUserAsync();

            var viewModel = new CreateCalibrationViewModel
            {
                TagID = tagId,
                TagName = tag.Name,
                TagDescription = tag.Description,
                TagMake = tag.Make,
                TagModel = tag.Model,
                TagSerialNumber = tag.SerialNumber,            
                TagReferenceDrawing = tag.ReferenceDrawing,
                TagProcedure = tag.Procedure,
                TagDataSheet = tag.DataSheet,
                TagFields = tagType.TagFields.MapToFields(tag.Fields).Where(x => !x.Inactive).OrderBy(x => x.SortOrder).ToList(),
                CalibrationFields = tagType.CalibrationFields.MapToFields().Where(x => !x.Inactive).OrderBy(x => x.SortOrder).ToList(),
                DocumentNumber = "00-QA-TMP-1007",
                Revision = "C",
                IssuedAt = DateTime.Now,
                RecordNumber = "CAL." + GetHashCode(),
                TestedBy = user?.FullName,
                TestedAt = DateTime.Now,
                EquipmentSelectList = new SelectList(equipment, "ID", "MakeModel", null, "Type")
            };

            var viewModelResult = new ViewModelResult<CreateCalibrationViewModel>(viewModel);
            foreach (var field in viewModel.TagFields)
            {
                if (!string.IsNullOrWhiteSpace(field.Value) && field.DataType.ToLowerInvariant() == "number")
                {
                    if (!decimal.TryParse(field.Value, out decimal decimalValue))
                    {
                        viewModelResult.AddError($"{field.DisplayName} should be a number only. Please remove any text -- for example: Units Of Measure. Current value is \"{ field.Value}\".");
                    }
                }
            }

            return viewModelResult;
        }

        public async Task<CreateCalibrationViewModel> GetCreateCalibrationViewModelAsync(int tagId, CreateCalibrationViewModel existingViewModel, IFormCollection formCollection)
        {
            var viewModelResult = await GetCreateCalibrationViewModelAsync(tagId);
            var viewModel = viewModelResult.ViewModel;

            viewModel.Comments = existingViewModel.Comments;
            viewModel.DocumentNumber = existingViewModel.DocumentNumber;
            viewModel.Equipment = existingViewModel.Equipment;
            viewModel.IssuedAt = existingViewModel.IssuedAt;
            viewModel.RecordNumber = existingViewModel.RecordNumber;
            viewModel.Revision = existingViewModel.Revision;
            viewModel.TagID = existingViewModel.TagID;
            viewModel.TestedAt = existingViewModel.TestedAt;
            viewModel.TestedBy = existingViewModel.TestedBy;
            viewModel.WorkOrderNumber = existingViewModel.WorkOrderNumber;

            foreach (var calibrationField in viewModel.CalibrationFields)
            {
                calibrationField.Value = formCollection[calibrationField.Id];
            }

            return viewModel;
        }

        public async Task<IEnumerable<CalibrationListViewModel>> GetCalibrationListViewModelAsync()
        {
            //var calibrations = await _context.Calibrations
            //        .Include(x => x.CalibrationTag)
            //        .OrderByDescending(x => x.ModifiedAt)
            //        .ToListAsync();

            var calibrations = await _reportService.GetRecentCalibrationsAsync();

            var viewModel = calibrations.Select(x => new CalibrationListViewModel()
            {
                ID = x.ID,
                RecordNumber = x.CalibrationRecordNumber,
                TagName = x.TagName,
                TagDescription = x.TagDescription,
                TestedAt = x.CalibrationTestedAt.GetValueOrDefault(DateTime.MinValue),
                TestedBy = x.CalibrationTestedBy,
                PassFail = _reportService.GetPassFail(x),
                TagID = x.TagID,
                VerificationOnly = x.VerificationOnly.GetValueOrDefault(false)
            });

            return viewModel;
        }

        public async Task CreateCalibrationAsync(CreateCalibrationViewModel viewModel, Dictionary<string, string> fields)
        {
            var calibration = new Calibration();

            calibration.DocumentNumber = viewModel.DocumentNumber;
            calibration.VerificationOnly = viewModel.VerificationOnly;
            calibration.Comments = viewModel.Comments;
            calibration.IssuedAt = viewModel.IssuedAt;
            calibration.RecordNumber = viewModel.RecordNumber;
            calibration.Revision = viewModel.Revision;
            calibration.TestedAt = viewModel.TestedAt;
            calibration.TestedBy = viewModel.TestedBy;
            calibration.WorkOrderNumber = viewModel.WorkOrderNumber;
            calibration.CalibrationValues = JsonConvert.SerializeObject(fields);

            var tag = await _tagService.GetTagAsync(viewModel.TagID);

            var calibrationTag = new CalibrationTag();
            calibrationTag.TagID = viewModel.TagID;
            calibrationTag.Make = tag.Make;
            calibrationTag.Model = tag.Model;
            calibrationTag.Name = tag.Name;
            calibrationTag.Procedure = tag.Procedure;
            calibrationTag.SerialNumber = tag.SerialNumber;
            calibrationTag.ReferenceDrawing = tag.ReferenceDrawing;
            calibrationTag.Description = tag.Description;
            calibrationTag.DataSheet = tag.DataSheet;
            calibrationTag.Fields = tag.Fields;

            var equipmentIds = viewModel.Equipment?.Select(x => x.ID);

            if (equipmentIds != null && equipmentIds.Any())
            {
                calibration.CalibrationEquipment = new List<Database.CalibrationEquipment>();
                var equipment = await _context.Equipment.Where(x => equipmentIds.Contains(x.ID)).ToListAsync();

                foreach (var item in equipment)
                {
                    var calibrationEquipment = new Database.CalibrationEquipment()
                    {
                        MakeModel = item.MakeModel,
                        SerialNumber = item.SerialNumber, 
                        TestFrequency = item.TestFrequency,
                        Type = item.Type
                    };

                    calibration.CalibrationEquipment.Add(calibrationEquipment);
                }
            }

            calibration.CalibrationTag = calibrationTag;

            await _context.Calibrations.AddAsync(calibration);
            await _context.SaveChangesAsync();
        }
    }
}
