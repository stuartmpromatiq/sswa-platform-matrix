﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SSWA.CalibrationRegister.Web.Database;
using SSWA.CalibrationRegister.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Services
{
    public class TagService
    {
        private CalibrationDbContext _context;

        public TagService(CalibrationDbContext context)
        {
            _context = context;
        }

        public async Task<Tag> GetTagAsync(int tagId)
        {
            var tag = await _context.Tags.FindAsync(tagId);
            return tag;
        }

        public async Task<PaginatedList<Tag>> GetAllTagsAsync(int? page = 1, int? tagTypeId = null, string tagName = null, string tagDescription = null)
        {
            var tags = _context.Tags
                .AsNoTracking();

            if (tagTypeId != null)
            {
                tags = tags.Where(x => x.TagTypeID == tagTypeId);
            }

            if (!string.IsNullOrEmpty(tagName))
            {
                tags = tags.Where(x => EF.Functions.Like(x.Name, $"%{tagName}%"));
            }

            if (!string.IsNullOrEmpty(tagDescription))
            {
                tags = tags.Where(x => EF.Functions.Like(x.Description, $"%{tagDescription}%"));
            }

            tags = tags.OrderBy(x => x.TagTypeID).ThenBy(x => x.Name);

            var result = await PaginatedList<Tag>.CreateAsync(tags.Include(x => x.TagType), page ?? 1, 50);
            return result;
        }

        public async Task<TagType> GetTagTypeAsync(int tagTypeId)
        {
            var tagType = await _context.TagTypes
                .AsNoTracking()
                .Include(x => x.ReportingTagType)
                .SingleOrDefaultAsync(x => x.ID == tagTypeId);
            return tagType;

        }

        public async Task<IEnumerable<TagType>> GetAllTagTypesAsync()
        {
            var tagTypes = await _context
                .TagTypes
                .AsNoTracking()
                .Include(x => x.ReportingTagType)
                .ToListAsync();
            return tagTypes;
        }

        public async Task<IEnumerable<ReportingTagType>> GetAllReportingTagTypesAsync()
        {
            var reportingTagTypes = await _context
                            .ReportingTagTypes
                            .AsNoTracking()
                            .ToListAsync();

            return reportingTagTypes;
        }

        public async Task<TagListViewModel> GetTagListViewModelAsync(int? page = 1, int? tagTypeId = null, string tagName = null, string tagDescription = null)
        {
            var tags = await this.GetAllTagsAsync(page, tagTypeId, tagName, tagDescription);
            var tagTypes = await this.GetAllTagTypesAsync();
            var tagTypeList = tagTypes
                              .Where(x => !x.Inactive)
                              .OrderBy(x => x.ReportingTagType.Name)
                              .ThenBy(x => x.Name)
                              .Select(x => new { Type = x.ReportingTagType.Name, ID = x.ID, Name = x.Name })
                              .ToList();

            var viewModel = new TagListViewModel
            {
                Tags = tags,
                TagTypes = tagTypes,
                TagTypeId = tagTypeId,
                TagName = tagName,
                TagDescription = tagDescription,
                TagTypeList = new SelectList(tagTypeList, "ID", "Name", null, "Type")
            };

            return viewModel;
        }

        public async Task<CreateTagViewModel> GetCreateTagViewModelAsync(int tagTypeId)
        {
            if (tagTypeId <= 0)
                throw new ArgumentOutOfRangeException(nameof(tagTypeId));

            var tagType = await GetTagTypeAsync(tagTypeId);

            if (tagType == null)
                throw new ArgumentOutOfRangeException(nameof(tagType));

            var viewModel = new CreateTagViewModel
            {
                Fields = tagType.TagFields.MapToFields().Where(x => !x.Inactive),
                TagTypeID = tagTypeId,
                TagTypeName = tagType.Name
            };

            return viewModel;
        }

        public async Task<EditTagViewModel> GetEditTagViewModelAsync(int tagId)
        {
            if (tagId <= 0)
                throw new ArgumentOutOfRangeException(nameof(tagId));

            var tag = await this.GetTagAsync(tagId);

            if (tag == null)
                throw new ArgumentOutOfRangeException(nameof(tag));

            var tagType = await GetTagTypeAsync(tag.TagTypeID);

            if (tagType == null)
                throw new ArgumentException(nameof(tagType));

            var viewModel = new EditTagViewModel()
            {
                ID = tag.ID,
                Name = tag.Name,
                Description = tag.Description,
                Make = tag.Make,
                Model = tag.Model,
                SerialNumber = tag.SerialNumber,
                ReferenceDrawing = tag.ReferenceDrawing,
                Procedure = tag.Procedure,
                DataSheet = tag.DataSheet,
                TagTypeName = tagType.Name,
                Fields = tagType.TagFields.MapToFields(tag.Fields)
            };

            return viewModel;
        }

        public async Task<EditTagTypeViewModel> GetEditTagTypeViewModelAsync(int tagTypeId)
        {
            if (tagTypeId <= 0)
                throw new ArgumentOutOfRangeException(nameof(tagTypeId));

            var tagType = await GetTagTypeAsync(tagTypeId);

            if (tagType == null)
                throw new ArgumentException(nameof(tagType));

            var reportingTypes = await GetAllReportingTagTypesAsync();

            var viewModel = new EditTagTypeViewModel()
            {
                ID = tagType.ID,
                Name = tagType.Name,
                ReportingTagTypeID = tagType.ReportingTagTypeID,
                UnitOfMeasure = tagType.UnitOfMeasure,
                CalibrationFields = JsonConvert.DeserializeObject<IList<Field>>(tagType.CalibrationFields, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
                TagFields = JsonConvert.DeserializeObject<IList<Field>>(tagType.TagFields, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
                ResultCalculations = JsonConvert.DeserializeObject<IList<Field>>(tagType.ResultCalculations, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore }),
                DataTypes = new SelectList(new[] { "text", "number", "date", "checkbox" }),
                ReportingTagTypes = new SelectList(reportingTypes, "ID", "Name")
            };

            return viewModel;
        }

        public async Task<IEnumerable<TagTypeListViewModel>> GetTagTypeListViewModelAsync()
        {
            var tagTypes = await this.GetAllTagTypesAsync();

            var viewModel = tagTypes
                .OrderBy(x => x.ReportingTagType.Name)
                .ThenBy(x => x.Name)
                .Select(x => new TagTypeListViewModel()
                {
                    ID = x.ID,
                    Name = x.Name,
                    UnitOfMeasure = x.UnitOfMeasure,
                    ReportingType = x.ReportingTagType?.Name,
                    TagFields = JsonConvert.DeserializeObject<IEnumerable<Field>>(x.TagFields)?
                        .Where(y => !y.Inactive)
                        .OrderBy(y => y.SortOrder)
                        .Select(y => y.DisplayName + ":\t" + y.DataType)
                        .Aggregate("", (a, b) => a + b + "\r\n"),
                    CalibrationFields = JsonConvert.DeserializeObject<IEnumerable<Field>>(x.CalibrationFields)?
                        .Where(y => !y.Inactive)
                        .OrderBy(y => y.SortOrder)
                        .Select(y => y.DisplayName + ":\t" + y.DataType)
                        .Aggregate("", (a, b) => a + b + "\r\n"),
                    ResultCalculations = JsonConvert.DeserializeObject<IEnumerable<Field>>(x.ResultCalculations)?
                        .Where(y => !y.Inactive)
                        .OrderBy(y => y.SortOrder)
                        .Select(y => y.DisplayName)
                        .Aggregate("", (a, b) => a + b + "\r\n"),
                    CreatedAt = x.CreatedAt.ToShortDateString(),
                    ModifiedAt = x.ModifiedAt.ToShortDateString()
                });

            return viewModel;
        }

        public async Task CreateTagAsync(CreateTagViewModel viewModel, Dictionary<string, string> fields)
        {
            var tag = new Tag();

            tag.TagTypeID = viewModel.TagTypeID;
            tag.Name = viewModel.Name;
            tag.Description = viewModel.Description;
            tag.Make = viewModel.Make;
            tag.Model = viewModel.Model;
            tag.SerialNumber = viewModel.SerialNumber;
            tag.ReferenceDrawing = viewModel.ReferenceDrawing;
            tag.Procedure = viewModel.Procedure;
            tag.DataSheet = viewModel.DataSheet;
            tag.Fields = JsonConvert.SerializeObject(fields);

            await _context.Tags.AddAsync(tag);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateTagAsync(int tagId, EditTagViewModel viewModel, Dictionary<string, string> fields)
        {
            var tag = await _context.Tags.FindAsync(tagId);

            tag.Name = viewModel.Name;
            tag.Description = viewModel.Description;
            tag.Make = viewModel.Make;
            tag.Model = viewModel.Model;
            tag.SerialNumber = viewModel.SerialNumber;
            tag.ReferenceDrawing = viewModel.ReferenceDrawing;
            tag.Procedure = viewModel.Procedure;
            tag.DataSheet = viewModel.DataSheet;
            tag.Fields = JsonConvert.SerializeObject(fields);

            _context.Tags.Update(tag);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateTagTypeAsync(int tagTypeId, EditTagTypeViewModel viewModel)
        {
            var tagType = await _context.TagTypes.FindAsync(tagTypeId);

            tagType.Name = viewModel.Name;
            tagType.UnitOfMeasure = viewModel.UnitOfMeasure;
            tagType.ReportingTagTypeID = viewModel.ReportingTagTypeID;
            tagType.TagFields = JsonConvert.SerializeObject(viewModel.TagFields);
            tagType.CalibrationFields = JsonConvert.SerializeObject(viewModel.CalibrationFields);
            tagType.ResultCalculations = JsonConvert.SerializeObject(viewModel.ResultCalculations);

            _context.TagTypes.Update(tagType);
            await _context.SaveChangesAsync();
        }

        //public async Task UpdateAsync(UpdateTagViewModel viewModel)
        //{

        //}
    }
}
