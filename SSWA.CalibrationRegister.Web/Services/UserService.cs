﻿using Microsoft.EntityFrameworkCore;
using SSWA.CalibrationRegister.Web.Database;
using SSWA.ExternalUserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.CalibrationRegister.Web.Services
{
    public class UserService
    {
        private readonly CalibrationDbContext _context;
        private readonly IExternalUserService _externalUserService;
        private readonly UserResolverService _userResolverService;

        public UserService(CalibrationDbContext context, IExternalUserService externalUserService, UserResolverService userResolverService)
        {
            _context = context;
            _externalUserService = externalUserService;
            _userResolverService = userResolverService;
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            var users = await _context.Users.ToListAsync();
            return users;
        }

        public async Task<User> GetUserAsync(int id)
        {
            var user = await _context.Users.FindAsync(id);
            return user;
        }

        public async Task<User> GetCurrentUserAsync()
        {
            var authId = _userResolverService.GetAuthId();
            var user = await _context.Users.FirstOrDefaultAsync(x => x.AuthId == authId);
            return user;
        }

        public async Task CreateUpdateUserAsync(User user)
        {
            var existingUser = await _context.Users
                .SingleOrDefaultAsync(x => x.AuthId == user.AuthId);

            if (existingUser == null)
            {
                await CreateUserAsync(user);
            }
            else
            {
                user.ID = existingUser.ID;
                await UpdateUserAsync(user);
            }
        }

        public async Task CreateUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(User user)
        {
            var existingUser = await _context.Users.FindAsync(user.ID);

            existingUser.Inactive = user.Inactive;
            existingUser.FirstName = user.FirstName;
            existingUser.LastName = user.LastName;

            _context.Users.Update(existingUser);
            await _context.SaveChangesAsync();
            if (existingUser.AuthId != null)
            {
                await _externalUserService.UpdateUserAsync(new ExternalUser(existingUser.AuthId, existingUser.Email, "", existingUser.FirstName, existingUser.LastName, new string[0]));
            }
        }
    }
}
