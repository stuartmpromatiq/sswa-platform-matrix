﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSWA.CalibrationRegister.Web.Services;
using SSWA.CalibrationRegister.Web.Models;
using SSWA.CalibrationRegister.Web.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace SSWA.CalibrationRegister.Web.Controllers
{
    [Authorize]
    public class TagController : Controller
    {
        private TagService _tagService;
        private CalibrationDbContext _context;

        public TagController(TagService tagService, CalibrationDbContext context)
        {
            _tagService = tagService;
            _context = context;
        }

        // GET: Tag
        public async Task<ActionResult> Index(int? page = 1, int? tagTypeId = null, string tagName = null, string tagDescription = null)
        {
            var viewModel = await _tagService.GetTagListViewModelAsync(page, tagTypeId, tagName, tagDescription);
            return View(viewModel);
        }

        // GET: Tag/Create
        [Authorize(Policy = "create:tags")]
        public async Task<ActionResult> Create(int tagTypeId)
        {
            var viewModel = await _tagService.GetCreateTagViewModelAsync(tagTypeId);
            return View(viewModel);
        }

        // POST: Tag/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "create:tags")]
        public async Task<ActionResult> Create(CreateTagViewModel viewModel, IFormCollection formCollection)
        {
            try
            {
                var fields = new Dictionary<string, string>();
                foreach (var key in formCollection.Keys.Where(x => x.StartsWith("tag_")))
                {
                    var value = formCollection[key].ToString();
                    fields.Add(key, value);
                }

                await _tagService.CreateTagAsync(viewModel, fields);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Tag/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var viewModel = await _tagService.GetEditTagViewModelAsync(id);
            return View(viewModel);
        }

        // POST: Tag/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, EditTagViewModel viewModel, IFormCollection formCollection)
        {
            try
            {
                var fields = new Dictionary<string, string>();
                foreach (var key in formCollection.Keys.Where(x => x.StartsWith("tag_")))
                {
                    var value = formCollection[key].ToString();
                    fields.Add(key, value);
                }

                await _tagService.UpdateTagAsync(id, viewModel, fields);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Tag/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Tag/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}