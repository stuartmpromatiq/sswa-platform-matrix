﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SSWA.CalibrationRegister.Web.Models;
using Microsoft.AspNetCore.Authorization;
using SSWA.CalibrationRegister.Web.Database;

namespace SSWA.CalibrationRegister.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly CalibrationDbContext _context;
        public HomeController(CalibrationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> About()
        {
            ViewData["Message"] = "Your application description page. 1312312313132. abcdcdasds";

            return View(User.Claims);
        }

        [Authorize]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
