﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SSWA.CalibrationRegister.Web.Services;
using Microsoft.AspNetCore.Authorization;
using SSWA.CalibrationRegister.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using System.Linq.Expressions;
using Newtonsoft.Json;

namespace SSWA.CalibrationRegister.Web.Controllers
{
    [Authorize]
    public class CalibrationController : Controller
    {
        private readonly CalibrationService _calibrationService;
        private readonly TagService _tagService;

        public CalibrationController(CalibrationService calibrationService, TagService tagService)
        {
            _calibrationService = calibrationService;
            _tagService = tagService;
        }

        public async Task<IActionResult> Index()
        {
            var viewModel = await _calibrationService.GetCalibrationListViewModelAsync();
            return View(viewModel);
        }

        // GET: Calibration/Tag
        public async Task<ActionResult> Tag(int? page = 1, int? tagTypeId = null, string tagName = null, string tagDescription = null)
        {
            var viewModel = await _tagService.GetTagListViewModelAsync(page, tagTypeId, tagName, tagDescription);
            return View(viewModel);
        }

        // GET: Calibration/History
        public async Task<ActionResult> History(int tagId)
        {
            var viewModel = await _calibrationService.GetCalibrationHistoryViewModelAsync(tagId);
            if (viewModel.GetType() == typeof(CalibrationHistoryStandardViewModel))
            {
                var stdViewModel = (viewModel as CalibrationHistoryStandardViewModel);
                return View("HistoryStandard", viewModel);
            }

            if (viewModel.GetType() == typeof(CalibrationHistoryChlorineViewModel))
            {
                var stdViewModel = (viewModel as CalibrationHistoryChlorineViewModel);
                return View("HistoryChlorine", viewModel);
            }

            if (viewModel.GetType() == typeof(CalibrationHistoryFlowViewModel))
            {
                var stdViewModel = (viewModel as CalibrationHistoryFlowViewModel);
                return View("HistoryFlow", viewModel);
            }

            return View();
        }

        // GET: Calibration/Create
        [Authorize(Policy = "create:calibrations")]
        public async Task<ActionResult> Create(int tagId)
        {
            var viewModelResult = await _calibrationService.GetCreateCalibrationViewModelAsync(tagId);

            if (viewModelResult.IsValid)
            {
                return View(viewModelResult.ViewModel);
            }
            else
            {
                return View("CreateValidationError", viewModelResult);
            }

        }

        // POST: Calibration/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "create:calibrations")]
        public async Task<ActionResult> Create(int tagId, CreateCalibrationViewModel viewModel, IFormCollection formCollection)
        {
            try
            {
                if (viewModel.VerificationOnly)
                {
                    viewModel.RecordNumber = "VER." + GetHashCode();
                }
                else
                {
                    viewModel.RecordNumber = "CAL." + GetHashCode();
                }
                
                var fields = new Dictionary<string, string>();
                for (int i = 0; i < viewModel.CalibrationFields.Count; i++)
                {
                    var value = viewModel.CalibrationFields[i].Value;
                    var key = viewModel.CalibrationFields[i].Id;
                    var type = viewModel.CalibrationFields[i].DataType;

                    if (!string.IsNullOrWhiteSpace(value) && type == "number" && !decimal.TryParse(value, out decimal decimalValue))
                    {
                        ModelState.AddModelError<CreateCalibrationViewModel, string>(x => x.CalibrationFields[i].Value, "This field must be a number");
                        continue;
                    }

                    if (!string.IsNullOrWhiteSpace(value) && type == "checkbox" && !bool.TryParse(value, out bool boollValue))
                    {
                        ModelState.AddModelError<CreateCalibrationViewModel, string>(x => x.CalibrationFields[i].Value, "This field must be checked or unchecked");
                        continue;
                    }

                    fields.Add(key, value);
                }

                if (ModelState.IsValid)
                {
                    await _calibrationService.CreateCalibrationAsync(viewModel, fields);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    var newViewModel = await _calibrationService.GetCreateCalibrationViewModelAsync(viewModel.TagID, viewModel, formCollection);
                    return View(newViewModel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Could not add calibration");
                var newViewModel = await _calibrationService.GetCreateCalibrationViewModelAsync(viewModel.TagID, viewModel, formCollection);
                return View(newViewModel);
            }
        }

        public void Report()
        {
            //LocalReport report = new LocalReport();
        }
    }
}