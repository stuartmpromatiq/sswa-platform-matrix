﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSWA.CalibrationRegister.Web.Services;
using SSWA.CalibrationRegister.Web.Models;

namespace SSWA.CalibrationRegister.Web.Controllers
{
    public class TagTypeController : Controller
    {
        private TagService _tagService;

        public TagTypeController(TagService tagService)
        {
            _tagService = tagService;
        }

        // GET: TagType
        public async Task<ActionResult> Index()
        {
            var viewModel = await _tagService.GetTagTypeListViewModelAsync();
            return View(viewModel);
        }

        // GET: TagType/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TagType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TagType/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TagType/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var viewModel = await _tagService.GetEditTagTypeViewModelAsync(id);
            return View(viewModel);
        }

        // POST: TagType/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, EditTagTypeViewModel viewModel, IFormCollection formCollection)
        {
            try
            {
                // TODO: Add update logic here
                await _tagService.UpdateTagTypeAsync(id, viewModel);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: TagType/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TagType/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}