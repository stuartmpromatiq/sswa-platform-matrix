﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSWA.ExternalUserService;
using SSWA.CalibrationRegister.Web.Database;
using JWT;
using JWT.Serializers;
using Microsoft.Extensions.Configuration;
using SSWA.CalibrationRegister.Web.Models;
using Microsoft.AspNetCore.Authorization;
using SSWA.CalibrationRegister.Web.Services;

namespace SSWA.CalibrationRegister.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private IExternalUserService _externalUserService;
        private UserService _userService;
        private IConfiguration _configuration;

        public UserController(
            IExternalUserService externalUserService, 
            IConfiguration configuration, 
            UserService userService)
        {
            _configuration = configuration;
            _externalUserService = externalUserService;
            _userService = userService;
        }

        // GET: User
        public async Task<ActionResult> Index()
        {
            var localUsers = await _userService.GetAllUsersAsync();
            var externalUsers = await _externalUserService.GetAllUsersAsync();

            var viewModel = new List<UserListViewModel>();

            foreach (var externaUser in externalUsers)
            {
                var localUser = localUsers.SingleOrDefault(x => x.AuthId == externaUser.Identifier);
                var userItem = new UserListViewModel();
                if (localUser != null)
                {
                    userItem.ID = localUser.ID;
                    userItem.Name = localUser.FirstName + " " + localUser.LastName;
                    userItem.Email = localUser.Email;
                }
                else
                {
                    userItem.Name = externaUser.FirstName + " " + externaUser.LastName;
                    userItem.Email = externaUser.Email;

                }
                userItem.AccountActivated = externaUser.IsActivated;
                userItem.LastLogin = externaUser.LastLogin;
                userItem.LoginCount = externaUser.LoginsCount;

                viewModel.Add(userItem);
            }

            return View(viewModel);
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(User user)
        {
            var externalUser = new ExternalUser("", user.Email, "", user.FirstName, user.LastName, new string[0]);
            var activationCallback = Url.Action("Activate", "Account", new { }, Request.Scheme);

            var invitedUser = await _externalUserService.InviteUserAsync(externalUser, activationCallback);
            user.AuthId = invitedUser.Identifier;
            await _userService.CreateUserAsync(user);

            return RedirectToAction(nameof(Index));
        }

        // GET: User/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var user = await _userService.GetUserAsync(id);
            var roles = await _externalUserService.GetAllRolesAsync();
            var userRoles = await _externalUserService.GetUserRolesAsync(user.AuthId);

            var viewModel = new EditUserViewModel()
            {
                ID = user.ID,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Roles = roles.Select(x => new RoleListViewModel()
                {
                    RoleId = x.Id,
                    RoleName = x.Description,
                    IsInRole = userRoles.Any(y => y.Id == x.Id)
                }).ToList(),
            };

            return View(viewModel);
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, EditUserViewModel viewModel)
        {
            try
            {
                var existingUser = await _userService.GetUserAsync(id);

                existingUser.FirstName = viewModel.FirstName;
                existingUser.LastName = viewModel.LastName;

                var addRoles = viewModel.Roles.Where(x => x.IsInRole).Select(x => new ExternalRole()
                {
                    Id = x.RoleId
                });

                var deleteRoles = viewModel.Roles.Where(x => !x.IsInRole).Select(x => new ExternalRole()
                {
                    Id = x.RoleId
                });

                await _userService.UpdateUserAsync(existingUser);
                await _externalUserService.UpdateUserRolesAsync(existingUser.AuthId, addRoles, deleteRoles);

                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel { Message = "Could not update user." });
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}