﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Importer
{
    public class PermitModel
    {
        public int? PermitId { get; set; }
        public string Description { get; set; }
        public string ATWNumber { get; set; }
        public string JSANumber { get; set; }
        public UserModel OpenedBy { get; set; }
        public UserModel AcceptedBy { get; set; }
        public UserModel ClosedBy { get; set; }
        public UserModel IsolatedBy { get; set; }
        public UserModel DeisolatedBy { get; set; }
        public UserModel IsolationMarkupCompletedBy { get; set; }
        public DateTime? DateOpened { get; set; }
        public DateTime? DateClosed { get; set; }
        public DateTime? DateTestOpened { get; set; }
        public DateTime? DateTestClosed { get; set; }
        public bool RequireShutdown { get; set; }
        public string Stage { get; set; }
        public string LockBox { get; set; }
        public string TagNumber { get; set; }
        public string PWRNumber { get; set; }
        public string ISONumber { get; set; }
        public string HVNumber { get; set; }
        public string HWNumber { get; set; }
        public string CSENumber { get; set; }
        public string WAHNumber { get; set; }
        public string EXPNumber { get; set; }
        public string GMRNumber { get; set; }
        public string JHANumber { get { return null; } }
        public Status Status { get; set; }
    }
}
