﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Importer
{
    public class Status
    {
        public char? StatusId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
