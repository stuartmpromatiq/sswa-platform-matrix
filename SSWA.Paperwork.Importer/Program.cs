﻿using CsvHelper;
using Dapper;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Importer
{
    class Program
    {
        private const string docUrl = "http://ssjvbinsql02/premarked-isolations/";
        static readonly string _cosmosDomain = "https://sswa.documents.azure.com:443/";
        static readonly string _cosmosKey = "J8CxBXKf7rA6s6IsDkbYtnzK13mZPea4vCe7ofl8f5rJVDIOWi39GWVQEEpH6HjQn34z3Ves6bnlYi5RAnBUmQ==";
        static readonly string _cosmosDatabase = "sswa";
        static readonly string _cosmosCollection = "items";

        static async Task Main(string[] args)
        {
            //await MigrateATWForms();
            //await MigrateProcessLocks();
            // await ImportIsoTemplates();
            await UpdateIsoTemplatesAndPermits();
        }

        private static async Task UpdateIsoTemplatesAndPermits()
        {
            using (var client = new DocumentClient(
                 new Uri(_cosmosDomain),
                 _cosmosKey,
                 serializerSettings: new JsonSerializerSettings()
                 {
                     ContractResolver = new CamelCasePropertyNamesContractResolver(),
                     DateTimeZoneHandling = DateTimeZoneHandling.Utc
                 }
              ))
            {
                Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_cosmosDatabase, _cosmosCollection);

                var result = client.CreateDocumentQuery<IsolationPermitTemplate>(collectionUri, sqlExpression: @"SELECT
                    *
                FROM c
                Where c.type = 'form_template' and c.kind.code = 'ISO'");

                foreach (var record in result)
                {
                    foreach (var attachment in record.Attachments)
                    {
                        string location = attachment.FileLocation;

                        if (location.Contains("revl", StringComparison.CurrentCultureIgnoreCase) && location.Contains(".docx", StringComparison.InvariantCultureIgnoreCase))
                        {
                            location = location.Replace(".docx", "", StringComparison.InvariantCultureIgnoreCase);
                            location = location.Replace("revl", "", StringComparison.InvariantCultureIgnoreCase);

                            location = location.Trim() + ".docx";
                        }

                        //location = location.Replace(@"\\SSJVBINFS02\Isolation Permits - Pre marked isolations\", "");
                        //location = location.Replace("G:\\30 O&M Users Drive\\27.0 Permit system\\27.1.1 ISOLATION\\Isolation Permits - Pre marked isolations\\", "");
                        //location = location.Replace("file://\\\\SSJVBINFS02\\Isolation Permits - Pre marked isolations\\", "");
                        //location = location.Replace("file://", "");
                        //location = location.Replace("http://ssjvbinsql02/premarked-isolations/", "");
                        //location = location.Replace("\\\\SSJVBINFS02\\Groups\\30 O&M Users Drive\\27.0 Permit system\\27.1.1 ISOLATION\\Isolation Permits - Pre marked isolations\\", "");

                        attachment.FileLocation = location;
                    }

                    Uri documentUri = UriFactory.CreateDocumentUri(_cosmosDatabase, _cosmosCollection, record.Id);

                    IsolationPermitTemplate existingItem = await client.ReadDocumentAsync<IsolationPermitTemplate>(documentUri);

                    var correctType = (IsolationPermitTemplate) record;

                    AccessCondition condition = new AccessCondition { Condition = existingItem.ETag, Type = AccessConditionType.IfMatch };
                    var updateResult = await client.ReplaceDocumentAsync(documentUri, correctType, new RequestOptions { AccessCondition = condition });
                }

            }

            Console.ReadKey();
        }

        private static async Task ImportIsoTemplates()
        {
            var file = "ISO_ImportTemplate.csv";
            var csvReaderConfig = new CsvHelper.Configuration.Configuration() { HasHeaderRecord = false, IgnoreBlankLines = true };

            var templates = new List<IsolationPermitTemplate>();
            using (var reader = new StreamReader(file))
            using (var csv = new CsvReader(reader, csvReaderConfig))
            {
                var records = csv.GetRecords<dynamic>();

                var section = 1;
                var template = new IsolationPermitTemplate();
                var count = 1;
                foreach (var record in records)
                {
                    if (record.Field1 == "Grouping Level 1")
                    {
                        section = 1;
                        continue;
                    }

                    if (record.Field1 == "Sub-contractor" || record.Field1 == "(optional)")
                    {
                        section = 2;
                        continue;
                    }

                    if (record.Field1 == "Equipment")
                    {
                        section = 3;
                        continue;
                    }

                    if (record.Field1 == "Network Path to original .docx file")
                    {
                        section = 4;
                        continue;
                    }

                    if (record.Field1 == "")
                    {
                        section = 5;
                        continue;
                    }

                    if (section == 1)
                    {
                        if (!string.IsNullOrWhiteSpace(template.Id))
                        {
                            templates.Add(template);
                            count = 1;
                        }

                        template = new IsolationPermitTemplate();

                        Console.WriteLine("==========================================");
                        template.Id = Guid.NewGuid().ToString();
                        template.Lookup.Level1 = record.Field1?.ToString()?.Trim()?.ToUpperInvariant();
                        template.Lookup.Level2 = record.Field2?.ToString()?.Trim()?.ToUpperInvariant();
                        template.Lookup.Level3 = record.Field3?.ToString()?.Trim()?.ToUpperInvariant();
                        template.Lookup.Name = record.Field4?.ToString()?.Trim()?.ToUpperInvariant();
                        template.EquipmentNumber = record.Field5?.ToString()?.Trim()?.ToUpperInvariant()?.Replace("(", "").Replace(")","");

                        Console.WriteLine($"TAG {template.EquipmentNumber}: {template.Lookup.Level1} => {template.Lookup.Level2} => {template.Lookup.Level3} => {template.Lookup.Name}");
                    }

                    if (section == 3)
                    {
                        var equipment = ((string) record.Field1?.ToString()?.Trim()?.ToUpperInvariant()).Replace(";", ":").Split(":");

                        if (equipment.Length == 1)
                        {
                            equipment = ((string)record.Field1?.ToString()?.Trim()?.ToUpperInvariant()).Replace(";", ":").Split(" ");
                        }

                        if (equipment.Length < 2)
                        {
                            throw new Exception();
                        }

                        var notes = new List<String>();

                        var tag = equipment[0];
                        var description = string.Join(" ", equipment.Skip(1)).Trim();

                        if (tag.Contains("#"))
                        {
                            notes.Add("#");
                            tag = tag.Replace("#", "").Trim();
                        }

                        var method = new IsolationPermit.IsolationPermitMethodStep()
                        {
                            Step = count,
                            Tag = tag,
                            EquipmentDescription = description,
                            Notes = notes,
                            EnergyType = record.Field2?.ToString()?.Trim()?.ToUpperInvariant(),
                            AsFoundState = record.Field3?.ToString()?.Trim()?.ToUpperInvariant(),
                            IsolationState = record.Field4?.ToString()?.Trim()?.ToUpperInvariant()
                        };

                        Console.WriteLine($"{tag} => {description}");

                        template.IsolationMethod.Add(method);
                        count++;
                    }

                    if (section == 4)
                    {
                        template.Attachments.Add(
                            new IsolationPermit.IsolationPermitAttachment() {
                                FileLocation = "file://" + record.Field1?.ToString()?.Trim()
                            });
                    }
                }

                templates.Add(template);

                using (var client = new DocumentClient(
                   new Uri(_cosmosDomain),
                   _cosmosKey,
                   serializerSettings: new JsonSerializerSettings()
                   {
                       ContractResolver = new CamelCasePropertyNamesContractResolver(),
                       DateTimeZoneHandling = DateTimeZoneHandling.Utc
                   }
                ))
                {
                    Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_cosmosDatabase, _cosmosCollection);
                        
                    foreach (var record in templates)
                    {
                        var result = await client.UpsertDocumentAsync(collectionUri, record);

                    }
                }
            }
        }

        private static async Task MigrateProcessLocks()
        {
            IEnumerable<ProcessLockModel> processLockModels = new List<ProcessLockModel>();

            using (var connection = new SqlConnection("Server=promatiq-test;Database=ATW_Register;User ID=sa;Password=Promatiq#1"))
            using (var client = new DocumentClient(
               new Uri(_cosmosDomain),
               _cosmosKey,
               serializerSettings: new JsonSerializerSettings()
               {
                   ContractResolver = new CamelCasePropertyNamesContractResolver(),
                   DateTimeZoneHandling = DateTimeZoneHandling.Utc
               }
           ))
            {
                processLockModels = await connection
                   .QueryAsync
                   (
                       "ProcessLock_GetAll",
                       new[]
                       {
                            typeof(ProcessLockModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(Status)
                       },
                       objects =>
                       {
                           var processLockModel = objects[0] as ProcessLockModel;
                           //if (permit == null) return null;

                           processLockModel.LockPlacedBy = objects[1] as UserModel;
                           processLockModel.LockRemovedBy = objects[2] as UserModel;
                           processLockModel.Status = objects[3] as Status;

                           return processLockModel;
                       },
                       splitOn: "PlacedById, RemovedById, StatusId",
                       commandType: CommandType.StoredProcedure
                   );

                foreach (var processLockModel in processLockModels)
                {
                    var processLock = new ProcessLock()
                    {
                        Id = processLockModel.PLNumber,
                        FriendlyId = processLockModel.PLNumber,
                        TagNumber = processLockModel.TagNumber ?? "",
                        Reason = processLockModel.Reason,
                        InfoTagPlaced = processLockModel.InfoTagPlaced,
                        NOE = processLockModel.NOE,
                        EquipmentStatus = processLockModel.EquipmentStatus,
                        Location = processLockModel.Location,
                        UniqueLockNumber = processLockModel.UniqueLockNumber,
                    };

                    switch (processLockModel.Status.Name)
                    {
                        case "Open":
                            processLock.Status = FormState.OPENED;
                            break;
                        case "Closed":
                            processLock.Status = FormState.CLOSED;
                            break;
                        case "Pending":
                            processLock.Status = FormState.PENDING_OPEN;
                            break;
                        case "Cancelled":
                            processLock.Status = FormState.CANCELLED;
                            break;
                        default:
                            processLock.Status = FormState.CLOSED;
                            break;
                    }

                    processLock.LockPlacedBy.User.Name = processLockModel.LockPlacedBy.FullName;
                    processLock.LockPlacedBy.User.Email = processLockModel.LockPlacedBy.Email;
                    processLock.LockPlacedBy.ActionedAt = processLockModel.DatePlaced;

                    processLock.LockRemovedBy.User.Name = processLockModel.LockRemovedBy.FullName;
                    processLock.LockRemovedBy.User.Email = processLockModel.LockRemovedBy.Email;
                    processLock.LockRemovedBy.ActionedAt = processLockModel.DateRemoved;

                    processLock.CreatedAt = processLockModel.DatePlaced;
                    processLock.UpdatedAt = processLockModel.DatePlaced;
                    processLock.CreatedBy = new User()
                    {
                        Name = processLockModel.LockPlacedBy.FullName,
                        Email = processLockModel.LockPlacedBy.Email,
                    };

                    processLock.UpdatedBy = new User()
                    {
                        Name = processLockModel.LockPlacedBy.FullName,
                        Email = processLockModel.LockPlacedBy.Email,
                    };

                    Console.WriteLine(processLock);

                    Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_cosmosDatabase, _cosmosCollection);

                    var result = await client.UpsertDocumentAsync(collectionUri, processLock);
                }
            }
        }


        private static async Task MigrateATWForms()
        {
            IEnumerable<PermitModel> permits = new List<PermitModel>();

            using (var connection = new SqlConnection("Server=promatiq-test;Database=ATW_Register;User ID=sa;Password=Promatiq#1"))
            using (var client = new DocumentClient(
               new Uri(_cosmosDomain),
               _cosmosKey,
               serializerSettings: new JsonSerializerSettings()
               {
                   ContractResolver = new CamelCasePropertyNamesContractResolver(),
                   DateTimeZoneHandling = DateTimeZoneHandling.Utc
               }
           ))
            {
                permits = await connection
                   .QueryAsync
                   (
                       "PermitRegister_GetAll",
                       new[]
                       {
                            typeof(PermitModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(UserModel),
                            typeof(Status)
                       },
                       objects =>
                       {
                           var permit = objects[0] as PermitModel;
                           //if (permit == null) return null;

                           permit.OpenedBy = objects[1] as UserModel;
                           permit.AcceptedBy = objects[2] as UserModel;
                           permit.ClosedBy = objects[3] as UserModel;
                           permit.IsolatedBy = objects[4] as UserModel;
                           permit.DeisolatedBy = objects[5] as UserModel;
                           permit.IsolationMarkupCompletedBy = objects[6] as UserModel;
                           permit.Status = objects[7] as Status;

                           return permit;
                       },
                       splitOn: "OpenedById, AcceptedById, ClosedById, IsolatedById, DeisolatedById, IsolationMarkupCompletedById, StatusId",
                       commandType: CommandType.StoredProcedure
                   );

                foreach (var permit in permits)
                {
                    var atwForm = new AuthorityToWorkForm()
                    {
                        Id = permit.ATWNumber,
                        FriendlyId = permit.ATWNumber,
                        EquipmentNumber = permit.TagNumber,
                        WorksDescription = permit.Description,
                        ShutdownRequired = permit.RequireShutdown,
                        Stage = permit.Stage,
                        StartDate = permit.DateOpened,
                        EndDate = permit.DateClosed
                    };

                    atwForm.WorkDuration = new List<DateTime>();

                    if (permit.DateOpened != null)
                    {
                        atwForm.WorkDuration.Add(permit.DateOpened.Value);
                    }
                    else
                    {
                        atwForm.WorkDuration.Add(DateTime.MinValue);
                    }

                    if (permit.DateClosed != null)
                    {
                        atwForm.WorkDuration.Add(permit.DateClosed.Value);
                    }
                    else
                    {
                        atwForm.WorkDuration.Add(DateTime.MaxValue);
                    }

                    switch (permit.Status.Name)
                    {
                        case "Open":
                            atwForm.Status = FormState.OPENED;
                            break;
                        case "Closed":
                            atwForm.Status = FormState.CLOSED;
                            break;
                        case "Pending":
                            atwForm.Status = FormState.PENDING_OPEN;
                            break;
                        case "Cancelled":
                            atwForm.Status = FormState.CANCELLED;
                            break;
                        default:
                            atwForm.Status = FormState.CLOSED;
                            break;
                    }

                    atwForm.PermitAcceptor.Name = permit.AcceptedBy.FullName;
                    atwForm.PermitAcceptor.Email = permit.AcceptedBy.Email;
                    atwForm.PermitAcceptor.Number = permit.AcceptedBy.ContactNumber;

                    atwForm.Isolation.PermitAcceptor.User.Name = permit.IsolatedBy.FullName;
                    atwForm.Isolation.PermitAcceptor.User.Email = permit.IsolatedBy.Email;

                    atwForm.Deisolation.PermitAcceptor.User.Name = permit.DeisolatedBy.FullName;
                    atwForm.Deisolation.PermitAcceptor.User.Email = permit.DeisolatedBy.Email;

                    atwForm.IsolationMarkup.PermitAcceptor.User.Name = permit.IsolationMarkupCompletedBy.FullName;
                    atwForm.IsolationMarkup.PermitAcceptor.User.Email = permit.IsolationMarkupCompletedBy.Email;

                    atwForm.CreatedAt = permit.DateOpened;
                    atwForm.UpdatedAt = permit.DateOpened;
                    atwForm.CreatedBy = new User()
                    {
                        Name = permit.AcceptedBy.FullName,
                        Email = permit.AcceptedBy.Email,
                    };

                    atwForm.UpdatedBy = new User()
                    {
                        Name = permit.OpenedBy.FullName,
                        Email = permit.OpenedBy.Email,
                    };

                    atwForm.Open.PermitAcceptor.ActionedAt = permit.DateOpened;
                    atwForm.Open.PermitAcceptor.User.Name = permit.AcceptedBy.FullName;
                    atwForm.Open.PermitAcceptor.User.Email = permit.AcceptedBy.Email;
                    atwForm.Open.PermitAuthoriser.ActionedAt = permit.DateOpened;
                    atwForm.Open.PermitAuthoriser.User.Name = permit.OpenedBy.FullName;
                    atwForm.Open.PermitAuthoriser.User.Email = permit.OpenedBy.Email;


                    atwForm.Close.PermitAcceptor.ActionedAt = permit.DateClosed;
                    atwForm.Close.PermitAcceptor.User.Name = permit.AcceptedBy.FullName;
                    atwForm.Close.PermitAcceptor.User.Email = permit.AcceptedBy.Email;
                    atwForm.Close.PermitAuthoriser.ActionedAt = permit.DateClosed;
                    atwForm.Close.PermitAuthoriser.User.Name = permit.ClosedBy.FullName;
                    atwForm.Close.PermitAuthoriser.User.Email = permit.ClosedBy.Email;

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Job Safety & Environmental Analysis",
                        Id = "JSEA",
                        PermitId = permit.JSANumber ?? "",
                        Required = true,
                        Generate = true
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Hot Work",
                        Id = "HW",
                        PermitId = permit.HWNumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Confined Space Entry",
                        Id = "CSE",
                        PermitId = permit.CSENumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Working At Height",
                        Id = "WAH",
                        PermitId = permit.WAHNumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Grid Mesh Removal",
                        Id = "GMR",
                        PermitId = permit.GMRNumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Excavation & Penetration",
                        Id = "EXP",
                        PermitId = permit.EXPNumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "High Voltage Electrical",
                        Id = "HV",
                        PermitId = permit.HVNumber ?? "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Low Voltage Electrical",
                        Id = "LV",
                        PermitId = "",
                    });

                    atwForm.Permits.Add(new AuthorityToWorkForm.AuthorityToWorkPermit()
                    {
                        Name = "Isolations",
                        Id = "ISO",
                        PermitId = permit.ISONumber ?? "",
                        Extras = new List<AuthorityToWorkForm.AuthorityToWorkPermit.AuthorityToWorkPermitExtra>()
                    {
                        new AuthorityToWorkForm.AuthorityToWorkPermit.AuthorityToWorkPermitExtra()
                        {
                            Name = "Lockbox Number",
                            Value = permit.LockBox ?? ""
                        }
                    }
                    });


                    Console.WriteLine(atwForm);

                    Uri collectionUri = UriFactory.CreateDocumentCollectionUri(_cosmosDatabase, _cosmosCollection);

                    var result = await client.UpsertDocumentAsync(collectionUri, atwForm);

                }
            }
        }
    }
}
