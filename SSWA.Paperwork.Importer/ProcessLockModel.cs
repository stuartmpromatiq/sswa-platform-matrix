﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Importer
{
    public class ProcessLockModel
    {
        public ProcessLockModel()
        {
            LockPlacedBy = new UserModel();
            LockRemovedBy = new UserModel();
            Status = new Status();
        }

        public int? ProcessLockId { get; set; }
        public string PLNumber { get; set; }
        public string TagNumber { get; set; }
        public string Location { get; set; }
        public UserModel LockPlacedBy { get; set; }
        public UserModel LockRemovedBy { get; set; }
        public string InfoTagPlaced { get; set; }
        public string Reason { get; set; }
        public string UniqueLockNumber { get; set; }
        public DateTime? DatePlaced { get; set; }
        public DateTime? DateRemoved { get; set; }
        public string NOE { get; set; }
        public string EquipmentStatus { get; set; }
        public Status Status { get; set; }
    }
}
