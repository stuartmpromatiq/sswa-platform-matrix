﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Paperwork.Importer
{
    public class UserModel
    {
        public int? UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string PersonalLock { get; set; }
        public bool IsPermitUser { get; set; }
        public bool IsPermitAcceptor { get; set; }
        public bool IsPermitAuthoriser { get; set; }
        public bool IsSystemAdministrator { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }
    }
}
