﻿using System;

namespace SSWA.Reports.Core
{
    public static class CalibrationQueries
    {
        public const string CalibrationOuter =
        @"
            SELECT
                ID =                         c.ID,
	            TagName =                    ct.[Name],
                TagDescription =             ct.[Description],
                TagManufacturer =            ct.Make,
                TagModel =                   ct.Model,
                TagDataSheet =               ct.DataSheet,
                TagProcedure =               ct.[Procedure],
                TagReferenceDrawing =        ct.ReferenceDrawing,
                TagSerialNumber =            ct.SerialNumber,
                CalibrationComments =        c.Comments,
                CalibrationRecordNumber =    c.RecordNumber,
                CalibrationTestedAt =        c.TestedAt,
                CalibrationTestedBy =        c.TestedBy,
                CalibrationResult =          1,
                CalibrationWorkOrderNumber = c.WorkOrderNumber,
                DocumentIssuedAt =           c.IssuedAt,
                DocumentNumber =             c.DocumentNumber,
                DocumentRevision =           c.Revision,
                CalibrationType =            rtt.Name,
                TagValues =                  ct.Fields,
                VerificationOnly =           c.VerificationOnly
            FROM Calibration c
            LEFT JOIN CalibrationTag ct ON ct.ID = c.CalibrationTagID
            LEFT JOIN Tag t On t.ID = ct.TagID
            LEFT JOIN TagType tt On tt.ID = t.TagTypeID
            LEFT JOIN ReportingTagType rtt ON rtt.ID = tt.ReportingTagTypeID
            WHERE c.ID = @CalibrationID
        ";

        public const string CalibrationHistoryByTag =
        @"
            SELECT
                ID =                         c.ID,
	            TagName =                    ct.[Name],
                TagDescription =             ct.[Description],
                TagManufacturer =            ct.Make,
                TagModel =                   ct.Model,
                TagDataSheet =               ct.DataSheet,
                TagProcedure =               ct.[Procedure],
                TagReferenceDrawing =        ct.ReferenceDrawing,
                TagSerialNumber =            ct.SerialNumber,
                CalibrationComments =        c.Comments,
                CalibrationRecordNumber =    c.RecordNumber,
                CalibrationTestedAt =        c.TestedAt,
                CalibrationTestedBy =        c.TestedBy,
                CalibrationResult =          1,
                CalibrationWorkOrderNumber = c.WorkOrderNumber,
                DocumentIssuedAt =           c.IssuedAt,
                DocumentNumber =             c.DocumentNumber,
                DocumentRevision =           c.Revision,
                CalibrationType =            rtt.Name,
                TagValues =                  ct.Fields,
                CalibrationValues =          c.CalibrationValues,
                VerificationOnly =           c.VerificationOnly
            FROM Calibration c
            LEFT JOIN CalibrationTag ct ON ct.ID = c.CalibrationTagID
            LEFT JOIN Tag t On t.ID = ct.TagID
            LEFT JOIN TagType tt On tt.ID = t.TagTypeID
            LEFT JOIN ReportingTagType rtt ON rtt.ID = tt.ReportingTagTypeID
            WHERE ct.TagID = @TagID
            ORDER BY c.IssuedAt DESC
        ";

        public const string CalibrationRecent =
        @"
            SELECT TOP 100
                ID =                         c.ID,
                TagID =                      ct.TagID,
	            TagName =                    ct.[Name],
                TagDescription =             ct.[Description],
                TagManufacturer =            ct.Make,
                TagModel =                   ct.Model,
                TagDataSheet =               ct.DataSheet,
                TagProcedure =               ct.[Procedure],
                TagReferenceDrawing =        ct.ReferenceDrawing,
                TagSerialNumber =            ct.SerialNumber,
                CalibrationComments =        c.Comments,
                CalibrationRecordNumber =    c.RecordNumber,
                CalibrationTestedAt =        c.TestedAt,
                CalibrationTestedBy =        c.TestedBy,
                CalibrationResult =          1,
                CalibrationWorkOrderNumber = c.WorkOrderNumber,
                DocumentIssuedAt =           c.IssuedAt,
                DocumentNumber =             c.DocumentNumber,
                DocumentRevision =           c.Revision,
                CalibrationType =            rtt.Name,
                TagValues =                  ct.Fields,
                CalibrationValues =          c.CalibrationValues,
                VerificationOnly =           c.VerificationOnly,
                TagTypeName =                tt.Name,
                ReportingTagTypeName =       rtt.Name
            FROM Calibration c
            LEFT JOIN CalibrationTag ct ON ct.ID = c.CalibrationTagID
            LEFT JOIN Tag t On t.ID = ct.TagID
            LEFT JOIN TagType tt On tt.ID = t.TagTypeID
            LEFT JOIN ReportingTagType rtt ON rtt.ID = tt.ReportingTagTypeID
            ORDER BY c.ModifiedAt DESC
        ";

        public const string CalibrationEquipment = 
        @"
            SELECT
	            MakeModel =     ce.MakeModel,
	            SerialNumber =  ce.SerialNumber,
	            TestFrequency = ce.TestFrequency,
	            Type =          ce.Type
            FROM CalibrationEquipment ce
            WHERE ce.CalibrationID = @CalibrationId
            ORDER BY ce.Type, ce.MakeModel, ce.SerialNumber
        ";

        public const string CalibrationInner = 
        @"
            SELECT 
	            TagValues =         ct.Fields,
	            CalibrationValues = c.CalibrationValues
            FROM Calibration c
            LEFT JOIN CalibrationTag ct ON ct.ID = c.CalibrationTagID
            WHERE c.ID = @CalibrationId
        ";

        public const string CalibrationFields =
        @"
            SELECT
                TagFields,
                CalibrationFields
            FROM Calibration c
            LEFT JOIN CalibrationTag ct ON ct.ID = c.CalibrationTagID
            LEFT JOIN Tag t On t.ID = ct.TagID
            LEFT JOIN TagType tt On tt.ID = t.TagTypeID
            WHERE c.ID = @CalibrationId
        ";
    }
}
