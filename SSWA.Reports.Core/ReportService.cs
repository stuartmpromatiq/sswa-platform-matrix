﻿using Dapper;
using Newtonsoft.Json;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace SSWA.Reports.Core
{
    public class ReportService
    {
        private readonly string _connectionString;
        private readonly JsonSerializerSettings _serializerSettings;

        public ReportService(string connectionString)
        {
            _connectionString = connectionString;
            _serializerSettings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Error = (sender, e) =>
                {
                    e.ErrorContext.Handled = true;
                }
            };
        }

        public async Task<CalibrationReport> GetCalibrationReportAsync(int id)
        {

            using (var connection = new SqlConnection(_connectionString))
            {
                var calibrationReport = await connection.QueryFirstAsync<CalibrationReport>(CalibrationQueries.CalibrationOuter, new { CalibrationId = id });

                var values = await GetCalibrationSheetValuesAsync(id);
                var tagValues = JsonConvert.DeserializeObject<CalibrationReport>(values.TagValues, _serializerSettings);

                calibrationReport.InjectFrom<NoNullsInjection>(tagValues);

                return calibrationReport;
            }
        }

        public async Task<ChlorineReport> GetChlorineReportAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var report = new ChlorineReport();

                var values = await GetCalibrationSheetValuesAsync(id);
                var tagValues = JsonConvert.DeserializeObject<ChlorineReport>(values.TagValues, _serializerSettings);
                var calibrationValues = JsonConvert.DeserializeObject<ChlorineReport>(values.CalibrationValues, _serializerSettings);

                report.InjectFrom<NoNullsInjection>(tagValues)
                      .InjectFrom<NoNullsInjection>(calibrationValues);

                return report;
            }
        }

        public async Task<FlowReport> GetFlowReportAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var report = new FlowReport();

                var values = await GetCalibrationSheetValuesAsync(id);
                var tagValues = JsonConvert.DeserializeObject<FlowReport>(values.TagValues, _serializerSettings);
                var calibrationValues = JsonConvert.DeserializeObject<FlowReport>(values.CalibrationValues, _serializerSettings);

                report.InjectFrom<NoNullsInjection>(tagValues)
                      .InjectFrom<NoNullsInjection>(calibrationValues);

                return report;
            }
        }

        public async Task<StandardReport> GetStandardReportAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var report = new StandardReport();

                var values = await GetCalibrationSheetValuesAsync(id);
                var fields = await GetCalibrationFieldValuesAsync(id);

                var tagValues = JsonConvert.DeserializeObject<StandardReport>(values.TagValues, _serializerSettings);
                var calibrationValues = JsonConvert.DeserializeObject<StandardReport>(values.CalibrationValues, _serializerSettings);
                var fieldValues = JsonConvert.DeserializeObject<StandardReport>(fields, _serializerSettings);

                report.InjectFrom<NoNullsInjection>(tagValues)
                      .InjectFrom<NoNullsInjection>(calibrationValues)
                      .InjectFrom<NoNullsInjection>(fieldValues);

                return report;
            }
        }

        public async Task<IEnumerable<CalibrationEquipment>> GetCalibrationEquipmentAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var calibrationEquipment = await connection.QueryAsync<CalibrationEquipment>(CalibrationQueries.CalibrationEquipment, new { CalibrationId = id });
                return calibrationEquipment;
            }
        }

        public async Task<CalibrationSheetValues> GetCalibrationSheetValuesAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var values = await connection.QueryFirstAsync<CalibrationSheetValues>(CalibrationQueries.CalibrationInner, new { CalibrationId = id });

                return values;
            }
        }

        public async Task<string> GetCalibrationFieldValuesAsync(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var fields = await connection.QueryFirstAsync<CalibrationFieldValues>(CalibrationQueries.CalibrationFields, new { CalibrationId = id });
                var tagFields = JsonConvert.DeserializeObject<IEnumerable<CalibrationField>>(fields.TagFields, _serializerSettings);
                var calibrationFields = JsonConvert.DeserializeObject<IEnumerable<CalibrationField>>(fields.CalibrationFields, _serializerSettings);

                var keyValues = new Dictionary<string, string>();
                foreach (var field in calibrationFields)
                {
                    keyValues[field.FieldId] = field.DisplayName;
                }

                foreach (var field in tagFields)
                {
                    keyValues[field.FieldId] = field.DisplayName;
                }

                return JsonConvert.SerializeObject(keyValues);
            }
        }

        public async Task<IEnumerable<CalibrationHistoryRecord>> GetCalibrationHistoryRecordsAsync(int tagId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var history = await connection.QueryAsync<CalibrationHistoryRecord>(CalibrationQueries.CalibrationHistoryByTag, new { TagId = tagId });
                return history;
            }
        }

        public async Task<IEnumerable<CalibrationHistoryRecord>> GetRecentCalibrationsAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var history = await connection.QueryAsync<CalibrationHistoryRecord>(CalibrationQueries.CalibrationRecent);
                return history;
            }
        }

        public bool GetPassFail(CalibrationHistoryRecord record)
        {
            switch (record.ReportingTagTypeName.ToUpperInvariant())
            {
                case "CHLORINE":
                    return GetChlorineHistoryReport(record).CalibrationResult.GetValueOrDefault(false);
                case "FLOW":
                    return GetFlowHistoryReport(record).CalibrationResult.GetValueOrDefault(false);
                case "STD":
                    return GetStandardHistoryReport(record).CalibrationResult.GetValueOrDefault(false);
                default:
                    return false;
            }
        }

        public IEnumerable<StandardHistoryReport> GetStandardHistoryReport(IEnumerable<CalibrationHistoryRecord> records)
        {
            foreach (var record in records)
            {
                StandardHistoryReport report = GetStandardHistoryReport(record);

                yield return report;
            }
        }

        public StandardHistoryReport GetStandardHistoryReport(CalibrationHistoryRecord record)
        {
            var report = new StandardHistoryReport();
            report.DocumentRevision = record.DocumentRevision;
            report.DocumentNumber = record.DocumentNumber;
            report.DocumentIssuedAt = record.DocumentIssuedAt.GetValueOrDefault(new DateTime());
            report.TagName = record.TagName;
            report.TagDescription = record.TagDescription;
            report.CalibrationRecordNumber = record.CalibrationRecordNumber;
            report.CalibrationTestedAt = record.CalibrationTestedAt.GetValueOrDefault(new DateTime());
            report.CalibrationTestedBy = record.CalibrationTestedBy;

            var tagValues = JsonConvert.DeserializeObject<StandardHistoryReport>(record.TagValues, _serializerSettings);
            var calibrationValues = JsonConvert.DeserializeObject<StandardHistoryReport>(record.CalibrationValues, _serializerSettings);

            report.InjectFrom<NoNullsInjection>(tagValues)
                 .InjectFrom<NoNullsInjection>(calibrationValues);

            report.ID = record.ID;
            report.CalibrationResult = report.PassFail;
            report.VerificationOnly = record.VerificationOnly.GetValueOrDefault(false);
            return report;
        }

        public IEnumerable<FlowHistoryReport> GetFlowHistoryReport(IEnumerable<CalibrationHistoryRecord> records)
        {
            foreach (var record in records)
            {
                FlowHistoryReport report = GetFlowHistoryReport(record);

                yield return report;
            }
        }

        public FlowHistoryReport GetFlowHistoryReport(CalibrationHistoryRecord record)
        {
            var report = new FlowHistoryReport();
            report.DocumentRevision = record.DocumentRevision;
            report.DocumentNumber = record.DocumentNumber;
            report.DocumentIssuedAt = record.DocumentIssuedAt.GetValueOrDefault(new DateTime());
            report.TagName = record.TagName;
            report.TagDescription = record.TagDescription;
            report.CalibrationRecordNumber = record.CalibrationRecordNumber;
            report.CalibrationTestedAt = record.CalibrationTestedAt.GetValueOrDefault(new DateTime());
            report.CalibrationTestedBy = record.CalibrationTestedBy;


            var tagValues = JsonConvert.DeserializeObject<FlowHistoryReport>(record.TagValues, _serializerSettings);
            var calibrationValues = JsonConvert.DeserializeObject<FlowHistoryReport>(record.CalibrationValues, _serializerSettings);

            report.InjectFrom<NoNullsInjection>(tagValues)
                 .InjectFrom<NoNullsInjection>(calibrationValues);

            report.ID = record.ID;
            report.CalibrationResult =
                report.PassFailZero &&
                report.PassFailA &&
                report.PassFailB &&
                report.PassFailC &&
                report.PassFailD;
            report.VerificationOnly = record.VerificationOnly.GetValueOrDefault(false);
            return report;
        }

        public IEnumerable<ChlorineHistoryReport> GetChlorineHistoryReport(IEnumerable<CalibrationHistoryRecord> records)
        {
            foreach (var record in records)
            {
                ChlorineHistoryReport report = GetChlorineHistoryReport(record);

                yield return report;
            }
        }

        public ChlorineHistoryReport GetChlorineHistoryReport(CalibrationHistoryRecord record)
        {
            var report = new ChlorineHistoryReport();
            report.DocumentRevision = record.DocumentRevision;
            report.DocumentNumber = record.DocumentNumber;
            report.DocumentIssuedAt = record.DocumentIssuedAt.GetValueOrDefault(new DateTime());
            report.TagName = record.TagName;
            report.TagDescription = record.TagDescription;
            report.CalibrationRecordNumber = record.CalibrationRecordNumber;
            report.CalibrationTestedAt = record.CalibrationTestedAt.GetValueOrDefault(new DateTime());
            report.CalibrationTestedBy = record.CalibrationTestedBy;


            var tagValues = JsonConvert.DeserializeObject<ChlorineHistoryReport>(record.TagValues, _serializerSettings);
            var calibrationValues = JsonConvert.DeserializeObject<ChlorineHistoryReport>(record.CalibrationValues, _serializerSettings);

            report.InjectFrom<NoNullsInjection>(tagValues)
                 .InjectFrom<NoNullsInjection>(calibrationValues);

            report.ID = record.ID;
            report.CalibrationResult =
                report.PassFailSetPointOne &&
                report.PassFailSetPointTwo &&
                report.PassFailSetPointThree &&
                report.PassFailSetPointFour &&
                report.PassFailSetPointFive &&
                report.PassFailSetPointSix &&
                report.PassFailSetPointSeven &&
                report.PassFailSetPointEight &&
                report.PassFailSetPointNine &&
                report.PassFailSetPointTen &&
                report.PassFailSetPointEleven;
            report.VerificationOnly = record.VerificationOnly.GetValueOrDefault(false);
            return report;
        }
    }
}
