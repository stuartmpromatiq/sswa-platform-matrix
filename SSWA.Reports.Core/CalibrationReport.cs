﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Reports.Core
{
    public class CalibrationReport
    {
        public int ID { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentRevision { get; set; }
        public DateTime? DocumentIssuedAt { get; set; }

        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public string TagManufacturer { get; set; }
        public string TagModel { get; set; }
        public string TagSerialNumber { get; set; }
        public string TagReferenceDrawing { get; set; }
        public string TagDataSheet { get; set; }
        public string TagProcedure { get; set; }
        public string TagRange { get { return TagMinRange + " - " + TagMaxRange + " " + TagUom; } }
        public string TagAccuracyRange { get { return "+/- " + TagAccuracy + (string.IsNullOrWhiteSpace(TagAccuracyUom) ? "%" : TagAccuracyUom); } }

        [JsonProperty("tag_uom")]
        public string TagUom { get; set; }
        [JsonProperty("tag_min_range")]
        public decimal? TagMinRange { get; set; }
        [JsonProperty("tag_max_range")]
        public decimal? TagMaxRange { get; set; }
        [JsonProperty("tag_accuracy")]
        public decimal? TagAccuracy { get; set; }
        [JsonProperty("tag_accuracy_unit")]
        public string TagAccuracyUom { get; set; }

        public string CalibrationRecordNumber { get; set; }

        public DateTime? CalibrationTestedAt { get; set; }

        [JsonProperty("calibration_tested_at")]
        public string CalibrationTestedAtString { get { return CalibrationTestedAt.GetValueOrDefault(new DateTime()).ToString("yyyy-MM-dd"); } }
        public string CalibrationTestedBy { get; set; }
        public string CalibrationComments { get; set; }
        public bool? CalibrationResult { get; set; }
        public string CalibrationWorkOrderNumber { get; set; }
        public string CalibrationType { get; set; }

        public bool? VerificationOnly { get; set; }
    }

    public class CalibrationEquipment
    {
        public string Type { get; set; }
        public string MakeModel { get; set; }
        public string SerialNumber { get; set; }
        public string TestFrequency { get; set; }
    }

    public class StandardReport
    {
        [JsonProperty("tag_uom")]
        public string TagUom { get; set; }
        [JsonProperty("tag_accuracy_unit")]
        public string TagAccuracyUom { get; set; }
        [JsonProperty("tag_offset_unit")]
        public string TagOffsetUom { get; set; }
        [JsonProperty("tag_slope_uom")]
        public string TagSlopeUom { get; set; }



        [JsonProperty("tag_min_range")]
        public decimal? TagMinRange { get; set; }
        [JsonProperty("tag_max_range")]
        public decimal? TagMaxRange { get; set; }
        [JsonProperty("tag_accuracy")]
        public decimal? TagAccuracy { get; set; }
        [JsonProperty("tag_standard_one")]
        public decimal? TagStandardOne { get; set; }
        [JsonProperty("tag_standard_two")]
        public decimal? TagStandardTwo { get; set; }
        [JsonProperty("tag_reference")]
        public decimal? TagReference { get; set; }
        [JsonProperty("tag_offset")]
        public decimal? TagOffset { get; set; }
        [JsonProperty("tag_slope")]
        public decimal? TagSlope { get; set; }

        [JsonProperty("calibration_single_point")]
        public bool? CalibrationSinglePoint { get; set; }


        [JsonProperty("calibration_as_found")]
        public decimal? CalibrationAsFound { get; set; }
        [JsonProperty("calibration_lab_value")]
        public decimal? CalibrationLabValue { get; set; }
        [JsonProperty("calibration_reference_reading")]
        public decimal? CalibrationReferenceReading { get; set; }
        [JsonProperty("calibration_as_left")]
        public decimal? CalibrationAsLeft { get; set; }


        [JsonProperty("calibration_cell_constant_as_found")]
        public decimal? CalibrationCellConstantAsFound { get; set; }
        [JsonProperty("calibration_cell_constant_as_left")]
        public decimal? CalibrationCellConstantAsLeft { get; set; }
        [JsonProperty("calibration_temperature")]
        public decimal? CalibrationTemperature { get; set; }
        [JsonProperty("calibration_slope")]
        public decimal? CalibrationSlope { get; set; }
        [JsonProperty("calibration_offset")]
        public decimal? CalibrationOffset { get; set; }
        [JsonProperty("calibration_gain_factor")]
        public decimal? CalibrationGainFactor { get; set; }


        [JsonProperty("calibration_as_found_display_name")]
        public string CalibrationAsFoundDisplayName { get; set; }
        [JsonProperty("calibration_lab_value_display_name")]
        public string CalibrationLabValueDisplayName { get; set; }
        [JsonProperty("calibration_reference_reading_display_name")]
        public string CalibrationReferenceReadingDisplayName { get; set; }
        [JsonProperty("calibration_as_left_display_name")]
        public string CalibrationAsLeftDisplayName { get; set; }


        [JsonProperty("calibration_cell_constant_as_found_display_name")]
        public string CalibrationCellConstantAsFoundDisplayName { get; set; }
        [JsonProperty("calibration_cell_constant_as_left_display_name")]
        public string CalibrationCellConstantAsLeftDisplayName { get; set; }
        [JsonProperty("calibration_temperature_display_name")]
        public string CalibrationTemperatureDisplayName { get; set; }
        [JsonProperty("calibration_slope_display_name")]
        public string CalibrationSlopeDisplayName { get; set; }
        [JsonProperty("calibration_offset_display_name")]
        public string CalibrationOffsetDisplayName { get; set; }
        [JsonProperty("calibration_gain_factor_display_name")]
        public string CalibrationGainFactorDisplayName { get; set; }

        [JsonProperty("calibration_setpoint_one")]
        public decimal? CalibrationSetpointOne { get; set; }
        [JsonProperty("calibration_readback_one")]
        public decimal? CalibrationReadbackOne { get; set; }
        [JsonProperty("calibration_setpoint_two")]
        public decimal? CalibrationSetpointTwo { get; set; }
        [JsonProperty("calibration_readback_two")]
        public decimal? CalibrationReadbackTwo { get; set; }

        public decimal? Deviation
        {
            get
            {
                if (CalibrationSetpointOne != null || CalibrationSetpointTwo != null)
                {
                    return null;
                }
                else if (CalibrationSetpointOne != null)
                {
                    return null;
                }
                else if (CalibrationSetpointTwo != null)
                {
                    return null;
                }
                else if (CalibrationSinglePoint == null || CalibrationSinglePoint.GetValueOrDefault(false))
                    //return CalibrationLabValue == 0 ? null : ((CalibrationAsLeft - CalibrationLabValue) / CalibrationLabValue) * 100m;

                    return CalibrationAsLeft + CalibrationLabValue == 0 ? 0 : (Math.Abs((CalibrationAsLeft - CalibrationLabValue) ?? 0) / ((CalibrationAsLeft + CalibrationLabValue) / 2)) * 100m;
                else
                    //return TagReference == 0 ? null : ((CalibrationReferenceReading - TagReference) / TagReference) * 100m;

                    return CalibrationReferenceReading + TagReference == 0 ? 0 : (Math.Abs((CalibrationReferenceReading - TagReference) ?? 0) / ((CalibrationReferenceReading + TagReference) / 2)) * 100m;

            }
        }

        public bool? PassFail
        {
            get
            {
                if (Deviation == null)
                {
                    if (CalibrationSetpointOne != null &&  CalibrationSetpointTwo != null)
                    {
                        var calOne = CalibrationReadbackOne + CalibrationSetpointOne == 0 ? 0 : (Math.Abs((CalibrationReadbackOne - CalibrationSetpointOne) ?? 0) / ((CalibrationReadbackOne + CalibrationSetpointOne) / 2)) * 100m;
                        var calTwo = CalibrationReadbackTwo + CalibrationSetpointTwo == 0 ? 0 : (Math.Abs((CalibrationReadbackTwo - CalibrationSetpointTwo) ?? 0) / ((CalibrationReadbackTwo + CalibrationSetpointTwo) / 2)) * 100m;

                        return calOne <= TagAccuracy && calOne >= TagAccuracy * -1m && calTwo <= TagAccuracy && calTwo >= TagAccuracy * -1m;
                    }
                    else if (CalibrationSetpointOne != null)
                    {
                        var calOne = CalibrationReadbackOne + CalibrationSetpointOne == 0 ? 0 : (Math.Abs((CalibrationReadbackOne - CalibrationSetpointOne) ?? 0) / ((CalibrationReadbackOne + CalibrationSetpointOne) / 2)) * 100m;
                        return calOne <= TagAccuracy && calOne >= TagAccuracy * -1m;
                    }
                    else if (CalibrationSetpointTwo != null)
                    {
                        var calTwo = CalibrationReadbackTwo + CalibrationSetpointTwo == 0 ? 0 : (Math.Abs((CalibrationReadbackTwo - CalibrationSetpointTwo) ?? 0) / ((CalibrationReadbackTwo + CalibrationSetpointTwo) / 2)) * 100m;
                        return calTwo <= TagAccuracy && calTwo >= TagAccuracy * -1m;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return Deviation <= TagAccuracy && Deviation >= TagAccuracy * -1m;
                }
            }
        }

        public bool? VerificationOnly { get; set; }
    }

    public class FlowReport
    {
        [JsonProperty("tag_uom")]
        public string TagUom { get; set; }
        [JsonProperty("tag_accuracy_unit")]
        public string TagAccuracyUom { get; set; }

        [JsonProperty("tag_min_range")]
        public decimal? TagMinRange { get; set; }
        [JsonProperty("tag_max_range")]
        public decimal? TagMaxRange { get; set; }
        [JsonProperty("tag_accuracy")]
        public decimal? TagAccuracy { get; set; }

        [JsonProperty("tag_expected_range_resistance_pin_seven_eight")]
        public string TagExpectedRangeResistancePinSevenEight { get; set; }
        [JsonProperty("tag_expected_range_resistance_pin_seven_nine")]
        public string TagExpectedRangeResistancePinSevenNine { get; set; }
        [JsonProperty("tag_expected_range_resistance_pin_eight_nine")]
        public string TagExpectedRangeResistancePinEightNine { get; set; }

        [JsonProperty("tag_expected_range_insulation_pin_seven_one")]
        public string TagExpectedRangeInsulationPinSevenOne { get; set; }
        [JsonProperty("tag_expected_range_insulation_pin_eight_one")]
        public string TagExpectedRangeInsulationePinEightOne { get; set; }

        [JsonProperty("tag_expected_range_empty_insulation_pin_one_two")]
        public string TagExpectedRangeEmptyInsulationPinOneTwo { get; set; }
        [JsonProperty("tag_expected_range_empty_insulation_pin_one_three")]
        public string TagExpectedRangeEmptyInsulationePinEightOneThree { get; set; }

        [JsonProperty("tag_expected_range_full_insulation_pin_one_two")]
        public string TagExpectedRangeFullInsulationPinOneTwo { get; set; }
        [JsonProperty("tag_expected_range_full_insulation_pin_one_three")]
        public string TagExpectedRangeFullInsulationePinEightOneThree { get; set; }

        [JsonProperty("tag_flow_element_dn")]
        public decimal? TagFlowElementDn { get; set; }
        [JsonProperty("tag_flow_element_gk")]
        public decimal? TagFlowElementGk { get; set; }
        [JsonProperty("tag_flow_element_gkl")]
        public decimal? TagFlowElementGkl { get; set; }



        [JsonProperty("calibration_resistance_pin_seven_eight")]
        public decimal? CalibrationResistancePinSevenEight { get; set; }
        [JsonProperty("calibration_resistance_pin_seven_nine")]
        public decimal? CalibrationResistancePinSevenNine { get; set; }
        [JsonProperty("calibration_resistance_pin_eight_nine")]
        public decimal? CalibrationResistancePinEightNine { get; set; }

        [JsonProperty("calibration_insulation_pin_seven_one")]
        public decimal? CalibrationInsulationPinSevenOne { get; set; }
        [JsonProperty("calibration_insulation_pin_eight_one")]
        public decimal? CalibrationInsulationePinEightOne { get; set; }

        [JsonProperty("calibration_empty_insulation_pin_one_two")]
        public decimal? CalibrationEmptyInsulationPinOneTwo { get; set; }
        [JsonProperty("calibration_empty_insulation_pin_one_three")]
        public decimal? CalibrationEmptyInsulationePinEightOneThree { get; set; }


        [JsonProperty("calibration_full_insulation_pin_one_two")]
        public decimal? CalibrationFullInsulationPinOneTwo { get; set; }
        [JsonProperty("calibration_full_insulation_pin_one_three")]
        public decimal? CalibrationFullInsulationePinEightOneThree { get; set; }


        [JsonProperty("calibration_current_output_ma_zero")]
        public decimal? CalibrationCurrentOutputMaZero { get; set; }
        [JsonProperty("calibration_current_output_ma_a")]
        public decimal? CalibrationCurrentOutputMaA { get; set; }
        [JsonProperty("calibration_current_output_ma_b")]
        public decimal? CalibrationCurrentOutputMaB { get; set; }
        [JsonProperty("calibration_current_output_ma_c")]
        public decimal? CalibrationCurrentOutputMaC { get; set; }
        [JsonProperty("calibration_current_output_ma_d")]
        public decimal? CalibrationCurrentOutputMaD { get; set; }

        [JsonProperty("calibration_calculated_flow_rate_zero")]
        public decimal? CalibrationCalculatedFlowRateZero { get; set; }
        [JsonProperty("calibration_calculated_flow_rate_a")]
        public decimal? CalibrationCalculatedFlowRateA { get; set; }
        [JsonProperty("calibration_calculated_flow_rate_b")]
        public decimal? CalibrationCalculatedFlowRateB { get; set; }
        [JsonProperty("calibration_calculated_flow_rate_c")]
        public decimal? CalibrationCalculatedFlowRateC { get; set; }
        [JsonProperty("calibration_calculated_flow_rate_d")]
        public decimal? CalibrationCalculatedFlowRateD { get; set; }

        [JsonProperty("calibration_observed_flow_rate_zero")]
        public decimal? CalibrationObservedFlowRateZero { get; set; }
        [JsonProperty("calibration_observed_flow_rate_a")]
        public decimal? CalibrationObservedFlowRateA { get; set; }
        [JsonProperty("calibration_observed_flow_rate_b")]
        public decimal? CalibrationObservedFlowRateB { get; set; }
        [JsonProperty("calibration_observed_flow_rate_c")]
        public decimal? CalibrationObservedFlowRateC { get; set; }
        [JsonProperty("calibration_observed_flow_rate_d")]
        public decimal? CalibrationObservedFlowRateD { get; set; }

        [JsonProperty("calibration_calculated_deviation_zero")]
        public decimal? CalibrationCalculatedDeviationZero { get; set; }
        [JsonProperty("calibration_calculated_deviation_a")]
        public decimal? CalibrationCalculatedDeviationA { get; set; }
        [JsonProperty("calibration_calculated_deviation_b")]
        public decimal? CalibrationCalculatedDeviationB { get; set; }
        [JsonProperty("calibration_calculated_deviation_c")]
        public decimal? CalibrationCalculatedDeviationC { get; set; }
        [JsonProperty("calibration_calculated_deviation_d")]
        public decimal? CalibrationCalculatedDeviationD { get; set; }

        public bool PassFailZero { get { return CalibrationCalculatedDeviationZero <= TagAccuracy && CalibrationCalculatedDeviationZero >= TagAccuracy * -1m; } }
        public bool PassFailA { get { return CalibrationCalculatedDeviationA <= TagAccuracy && CalibrationCalculatedDeviationA >= TagAccuracy * -1m; } }
        public bool PassFailB { get { return CalibrationCalculatedDeviationB <= TagAccuracy && CalibrationCalculatedDeviationB >= TagAccuracy * -1m; } }
        public bool PassFailC { get { return CalibrationCalculatedDeviationC <= TagAccuracy && CalibrationCalculatedDeviationC >= TagAccuracy * -1m; } }
        public bool PassFailD { get { return CalibrationCalculatedDeviationD <= TagAccuracy && CalibrationCalculatedDeviationD >= TagAccuracy * -1m; } }

        public bool? VerificationOnly { get; set; }
    }

    public class ChlorineReport
    {
        [JsonProperty("tag_uom")]
        public string TagUom { get; set; }
        [JsonProperty("tag_accuracy_unit")]
        public string TagAccuracyUom { get; set; }

        [JsonProperty("tag_min_range")]
        public decimal? TagMinRange { get; set; }
        [JsonProperty("tag_max_range")]
        public decimal? TagMaxRange { get; set; }
        [JsonProperty("tag_accuracy")]
        public decimal? TagAccuracy { get; set; }

        public decimal? TagSetPointPercentageOne { get { return 0; } }
        public decimal? TagSetPointPercentageTwo { get { return 10; } }
        public decimal? TagSetPointPercentageThree { get { return 20; } }
        public decimal? TagSetPointPercentageFour { get { return 30; } }
        public decimal? TagSetPointPercentageFive { get { return 40; } }
        public decimal? TagSetPointPercentageSix { get { return 50; } }
        public decimal? TagSetPointPercentageSeven { get { return 60; } }
        public decimal? TagSetPointPercentageEight { get { return 70; } }
        public decimal? TagSetPointPercentageNine { get { return 80; } }
        public decimal? TagSetPointPercentageTen { get { return 90; } }
        public decimal? TagSetPointPercentageEleven { get { return 100; } }

        [JsonProperty("tag_chlorinator_rotameter_set_point_one")]
        public decimal? TagSetPointOne { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_two")]
        public decimal? TagSetPointTwo { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_three")]
        public decimal? TagSetPointThree { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_four")]
        public decimal? TagSetPointFour { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_five")]
        public decimal? TagSetPointFive { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_six")]
        public decimal? TagSetPointSix { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_seven")]
        public decimal? TagSetPointSeven { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_eight")]
        public decimal? TagSetPointEight { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_nine")]
        public decimal? TagSetPointNine { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_ten")]
        public decimal? TagSetPointTen { get; set; }
        [JsonProperty("tag_chlorinator_rotameter_set_point_eleven")]
        public decimal? TagSetPointEleven { get; set; }

        [JsonProperty("calibration_as_found")]
        public decimal? CalibrationAsFound { get; set; }
        [JsonProperty("calibration_as_left")]
        public decimal? CalibrationAsLeft { get; set; }

        [JsonProperty("calibration_sfc_observed_set_point_one")]
        public decimal? CalibrationObservedSetPointOne { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_two")]
        public decimal? CalibrationObservedSetPointTwo { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_three")]
        public decimal? CalibrationObservedSetPointThree { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_four")]
        public decimal? CalibrationObservedSetPointFour { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_five")]
        public decimal? CalibrationObservedSetPointFive { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_six")]
        public decimal? CalibrationObservedSetPointSix { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_seven")]
        public decimal? CalibrationObservedSetPointSeven { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_eight")]
        public decimal? CalibrationObservedSetPointEight { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_nine")]
        public decimal? CalibrationObservedSetPointNine { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_ten")]
        public decimal? CalibrationObservedSetPointTen { get; set; }
        [JsonProperty("calibration_sfc_observed_set_point_eleven")]
        public decimal? CalibrationObservedSetPointEleven { get; set; }

      

        public decimal? DeviationSetPointOne { get { return TagSetPointOne + CalibrationObservedSetPointOne == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointOne - TagSetPointOne) ?? 0) / ((CalibrationObservedSetPointOne + TagSetPointOne) / 2)) * 100m;  } }
        public decimal? DeviationSetPointTwo { get { return TagSetPointTwo + CalibrationObservedSetPointTwo == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointTwo - TagSetPointTwo) ?? 0) / ((CalibrationObservedSetPointTwo + TagSetPointTwo) / 2)) * 100m; } }
        public decimal? DeviationSetPointThree { get { return TagSetPointThree + CalibrationObservedSetPointThree == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointThree - TagSetPointThree) ?? 0) / ((CalibrationObservedSetPointThree + TagSetPointThree) / 2)) * 100m; } }
        public decimal? DeviationSetPointFour { get { return TagSetPointFour + CalibrationObservedSetPointFour == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointFour - TagSetPointFour) ?? 0) / ((CalibrationObservedSetPointFour + TagSetPointFour) / 2)) * 100m; } }
        public decimal? DeviationSetPointFive { get { return TagSetPointFive + CalibrationObservedSetPointFive == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointFive - TagSetPointFive) ?? 0) / ((CalibrationObservedSetPointFive + TagSetPointFive) / 2)) * 100m; } }
        public decimal? DeviationSetPointSix { get { return TagSetPointSix + CalibrationObservedSetPointSix == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointSix - TagSetPointSix) ?? 0) / ((CalibrationObservedSetPointSix + TagSetPointSix) / 2)) * 100m; } }
        public decimal? DeviationSetPointSeven { get { return TagSetPointSeven + CalibrationObservedSetPointSeven == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointSeven - TagSetPointSeven) ?? 0) / ((CalibrationObservedSetPointSeven + TagSetPointSeven) / 2)) * 100m; } }
        public decimal? DeviationSetPointEight { get { return TagSetPointEight + CalibrationObservedSetPointEight == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointEight - TagSetPointEight) ?? 0) / ((CalibrationObservedSetPointEight + TagSetPointEight) / 2)) * 100m; } }
        public decimal? DeviationSetPointNine { get { return TagSetPointNine + CalibrationObservedSetPointNine == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointNine - TagSetPointNine) ?? 0) / ((CalibrationObservedSetPointNine + TagSetPointNine) / 2)) * 100m; } }
        public decimal? DeviationSetPointTen { get { return TagSetPointTen + CalibrationObservedSetPointTen == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointTen - TagSetPointTen) ?? 0) / ((CalibrationObservedSetPointTen + TagSetPointTen) / 2)) * 100m; } }
        public decimal? DeviationSetPointEleven { get { return TagSetPointEleven + CalibrationObservedSetPointEleven == 0 ? 0 : (Math.Abs((CalibrationObservedSetPointEleven - TagSetPointEleven) ?? 0) / ((CalibrationObservedSetPointEleven + TagSetPointEleven) / 2)) * 100m; } }

        public bool PassFailSetPointOne { get { return DeviationSetPointOne <= TagAccuracy && DeviationSetPointOne >= TagAccuracy * -1m; } }
        public bool PassFailSetPointTwo { get { return DeviationSetPointTwo <= TagAccuracy && DeviationSetPointTwo >= TagAccuracy * -1m; } }
        public bool PassFailSetPointThree { get { return DeviationSetPointThree <= TagAccuracy && DeviationSetPointThree >= TagAccuracy * -1m; } }
        public bool PassFailSetPointFour { get { return DeviationSetPointFour <= TagAccuracy && DeviationSetPointFour >= TagAccuracy * -1m; } }
        public bool PassFailSetPointFive { get { return DeviationSetPointFive <= TagAccuracy && DeviationSetPointFive >= TagAccuracy * -1m; } }
        public bool PassFailSetPointSix { get { return DeviationSetPointSix <= TagAccuracy && DeviationSetPointSix >= TagAccuracy * -1m; } }
        public bool PassFailSetPointSeven { get { return DeviationSetPointSeven <= TagAccuracy && DeviationSetPointSeven >= TagAccuracy * -1m; } }
        public bool PassFailSetPointEight { get { return DeviationSetPointEight <= TagAccuracy && DeviationSetPointEight >= TagAccuracy * -1m; } }
        public bool PassFailSetPointNine { get { return DeviationSetPointNine <= TagAccuracy && DeviationSetPointNine >= TagAccuracy * -1m; } }
        public bool PassFailSetPointTen { get { return DeviationSetPointTen <= TagAccuracy && DeviationSetPointTen >= TagAccuracy * -1m; } }
        public bool PassFailSetPointEleven { get { return DeviationSetPointEleven <= TagAccuracy && DeviationSetPointEleven >= TagAccuracy * -1m; } }

        public bool? VerificationOnly { get; set; }

    }

    public class StandardHistoryReport : StandardReport
    {
        public int ID { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentRevision { get; set; }
        public DateTime DocumentIssuedAt { get; set; }

        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public string TagManufacturer { get; set; }
        public string TagModel { get; set; }
        public string TagSerialNumber { get; set; }
        public string TagReferenceDrawing { get; set; }
        public string TagDataSheet { get; set; }
        public string TagProcedure { get; set; }
        public string TagRange { get { return TagMinRange + " - " + TagMaxRange + " " + TagUom; } }
        public string TagAccuracyRange { get { return "+/- " + TagAccuracy + TagAccuracyUom; } }

        public string CalibrationRecordNumber { get; set; }
        public DateTime? CalibrationTestedAt { get; set; }
        [JsonProperty("calibration_tested_at")]
        public string CalibrationTestedAtString { get { return CalibrationTestedAt.GetValueOrDefault(new DateTime()).ToString("yyyy-MM-dd"); } }
        public string CalibrationTestedBy { get; set; }
        public string CalibrationComments { get; set; }
        public bool? CalibrationResult { get; set; }
        public string CalibrationWorkOrderNumber { get; set; }
        public string CalibrationType { get; set; }

    }

    public class ChlorineHistoryReport : ChlorineReport
    {
        public int ID { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentRevision { get; set; }
        public DateTime DocumentIssuedAt { get; set; }

        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public string TagManufacturer { get; set; }
        public string TagModel { get; set; }
        public string TagSerialNumber { get; set; }
        public string TagReferenceDrawing { get; set; }
        public string TagDataSheet { get; set; }
        public string TagProcedure { get; set; }
        public string TagRange { get { return TagMinRange + " - " + TagMaxRange + " " + TagUom; } }
        public string TagAccuracyRange { get { return "+/- " + TagAccuracy + TagAccuracyUom; } }

        public string CalibrationRecordNumber { get; set; }
        public DateTime? CalibrationTestedAt { get; set; }
        [JsonProperty("calibration_tested_at")]
        public string CalibrationTestedAtString { get { return CalibrationTestedAt.GetValueOrDefault(new DateTime()).ToString("yyyy-MM-dd"); } }
        public string CalibrationTestedBy { get; set; }
        public string CalibrationComments { get; set; }
        public bool? CalibrationResult { get; set; }
        public string CalibrationWorkOrderNumber { get; set; }
        public string CalibrationType { get; set; }

    }

    public class FlowHistoryReport : FlowReport
    {
        public int ID { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentRevision { get; set; }
        public DateTime DocumentIssuedAt { get; set; }

        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public string TagManufacturer { get; set; }
        public string TagModel { get; set; }
        public string TagSerialNumber { get; set; }
        public string TagReferenceDrawing { get; set; }
        public string TagDataSheet { get; set; }
        public string TagProcedure { get; set; }
        public string TagRange { get { return TagMinRange + " - " + TagMaxRange + " " + TagUom; } }
        public string TagAccuracyRange { get { return "+/- " + TagAccuracy + TagAccuracyUom; } }

        public string CalibrationRecordNumber { get; set; }
        public DateTime? CalibrationTestedAt { get; set; }
        [JsonProperty("calibration_tested_at")]
        public string CalibrationTestedAtString { get { return CalibrationTestedAt.GetValueOrDefault(new DateTime()).ToString("yyyy-MM-dd"); } }
        public string CalibrationTestedBy { get; set; }
        public string CalibrationComments { get; set; }
        public bool? CalibrationResult { get; set; }
        public string CalibrationWorkOrderNumber { get; set; }
        public string CalibrationType { get; set; }
    }

    public class CalibrationSheetValues
    {
        public string CalibrationValues { get; set; }
        public string TagValues { get; set; }
    }

    public class CalibrationFieldValues
    {
        public string CalibrationFields { get; set; }
        public string TagFields { get; set; }
    }

    public class CalibrationField
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string FieldId => Id + "_display_name";
    }
}
