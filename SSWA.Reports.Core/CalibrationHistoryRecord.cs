﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Reports.Core
{
    public class CalibrationHistoryRecord
    {
        public int ID { get; set; }

        public string DocumentNumber { get; set; }
        public string DocumentRevision { get; set; }
        public DateTime? DocumentIssuedAt { get; set; }

        public int TagID { get; set; }
        public string TagName { get; set; }
        public string TagDescription { get; set; }
        public string TagManufacturer { get; set; }
        public string TagModel { get; set; }
        public string TagSerialNumber { get; set; }
        public string TagReferenceDrawing { get; set; }
        public string TagDataSheet { get; set; }
        public string TagProcedure { get; set; }
        public string TagValues { get; set; }       
        public string CalibrationValues { get; set; }
        public string CalibrationRecordNumber { get; set; }
        public DateTime? CalibrationTestedAt { get; set; }
        public string CalibrationTestedBy { get; set; }
        public string CalibrationComments { get; set; }
        public bool? CalibrationResult { get; set; }
        public string CalibrationWorkOrderNumber { get; set; }
        public string CalibrationType { get; set; }

        public bool? VerificationOnly { get; set; }

        public string TagTypeName { get; set; }

        public string ReportingTagTypeName { get; set; }
    }
}
