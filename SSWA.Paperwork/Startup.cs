using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
//using Serilog;
using Serilog.Events;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Infrastructure;
using SSWA.Paperwork.Services;

namespace SSWA.Admin.App
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();

            services.AddTransient<IExternalUserService, Auth0UserService>(
               s => new Auth0UserService(
                   new Auth0UserServiceConfiguration()
                   {
                       AppId = Configuration["Auth0:AppId"],
                       ClientId = Configuration["Auth0:ClientId"],
                       ClientSecret = Configuration["Auth0:ClientSecret"],
                       Domain = Configuration["Auth0:Domain"],
                       Connection = Configuration["Auth0:Connection"],
                       SigningKey = Configuration["Auth0:SigningKey"],
                       SendgridApiKey = Configuration["SendGrid:ApiKey"],
                       AppName = "SSWA Platform"
                   }));
            services.AddTransient<DocumentClient>(s =>
                new DocumentClient(
                    new Uri(Configuration["CosmosDb:Domain"]),
                    Configuration["CosmosDb:ApiKey"],
                    serializerSettings: new JsonSerializerSettings()
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                        Converters = new List<JsonConverter>() { new FormConverter(), new FormTemplateConverter() },
                        ObjectCreationHandling = ObjectCreationHandling.Replace
                        
                    }
                )
            );

            services.AddTransient<CosmodDbOptions>(p => new CosmodDbOptions(Configuration["CosmosDb:DatabaseName"], Configuration["CosmosDb:CollectionName"]));
            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddScoped<ICurrentRequestService, CurrentRequestService>();
            services.AddTransient<IFormAuthorisation, FormAuthorisation>();

            services.AddTransient(typeof(CosmosDbService<>));
            services.AddTransient(typeof(DocumentService<>));
            services.AddTransient(typeof(NotificationService));
            services.AddTransient<EmailService>(p => new EmailService(Configuration["SendGrid:ApiKey"], p.GetService<ILogger<EmailService>>()));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IPrincipal>(
                provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);

            services.AddMediatR();

            services.AddResponseCaching();
            services.AddResponseCompression();

            services.AddMemoryCache();

            // Add authentication services.
            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Authority = "https://sswa.au.auth0.com/";
                options.Audience = "https://api.sswa";
                options.SaveToken = true;

                options.Events = new JwtBearerEvents()
                {
                    OnTokenValidated = notification =>
                    {
                        var claimsIdentity = notification.Principal.Identity as ClaimsIdentity;
                        Console.WriteLine($"{claimsIdentity?.Name} authenticated using bearer authentication.");

                        var claims = new List<Claim>();
                        claims.Add(new Claim(ClaimTypes.Role, "test"));
                        var appIdentity = new ClaimsIdentity(claims);
                        notification.Principal.AddIdentity(appIdentity);


                        return Task.FromResult(true);
                    }
                };
            })
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
                options.LogoutPath = new PathString("/Account/Logout");
            })
            .AddOpenIdConnect("Auth0", options =>
            {

                options.SaveTokens = true;

                // Set the authority to your Auth0 domain
                options.Authority = $"https://{Configuration["Auth0:Domain"]}";

                // Configure the Auth0 Client ID and Client Secret
                options.ClientId = Configuration["Auth0:ClientId"];
                options.ClientSecret = Configuration["Auth0:ClientSecret"];

                // Set response type to code
                options.ResponseType = OpenIdConnectResponseType.Code;
                options.AuthenticationMethod = OpenIdConnectRedirectBehavior.RedirectGet;

                // Configure the scope
                options.Scope.Clear();
                options.Scope.Add("openid");
                options.Scope.Add("profile");

                // Set the callback path, so Auth0 will call back to http://localhost:5000/signin
                // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard
                options.CallbackPath = new PathString("/signin");

                // Configure the Claims Issuer to be Auth0
                options.ClaimsIssuer = "Auth0";


                options.Events = new OpenIdConnectEvents
                {
                    // handle the logout redirection
                    OnRedirectToIdentityProviderForSignOut = (context) =>
                    {
                        var logoutUri = $"https://{Configuration["Auth0:Domain"]}/v2/logout?client_id={Configuration["Auth0:ClientId"]}";

                        var postLogoutUri = context.Properties.RedirectUri;
                        if (!string.IsNullOrEmpty(postLogoutUri))
                        {
                            if (postLogoutUri.StartsWith("/"))
                            {
                                // transform to absolute
                                var request = context.Request;
                                postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase + postLogoutUri;
                            }
                            logoutUri += $"&returnTo={ Uri.EscapeDataString(postLogoutUri)}";
                        }https://i.imgur.com/sDAczL3.png

                        context.Response.Redirect(logoutUri);
                        context.HandleResponse();

                        return Task.CompletedTask;
                    },
                    OnRedirectToIdentityProvider = (context) =>
                    {
                        context.ProtocolMessage.SetParameter("connection", Configuration["Auth0:Connection"]);
                        return Task.CompletedTask;
                    },
                    OnUserInformationReceived = notification =>
                    {
                        Console.WriteLine("Token validation received");
                        return Task.FromResult(true);
                    },
                    OnTokenValidated = notification =>
                    {
                        var claimsIdentity = notification.Principal.Identity as ClaimsIdentity;
                        Console.WriteLine($"{claimsIdentity?.Name} authenticated using bearer authentication.");

                        var claims = new List<Claim>();
                        claims.Add(new Claim(ClaimTypes.Role, "test"));
                        var appIdentity = new ClaimsIdentity(claims);
                        notification.Principal.AddIdentity(appIdentity);


                        return Task.FromResult(true);
                    }
                };
            });

            // Add framework services.
            services.AddMvc();


            services.AddAuthorization(options =>
            {

                options.AddPolicy("create:forms", policy => policy.RequireClaim("http://api.sswa/claims/permissions", "create:forms"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            CosmosDbService<PersonCategory> personCategoryService,
            CosmosDbService<PersonSubcategory> personSubcategoryService,
            CosmosDbService<VisitorTraining> visitorTrainingService)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseResponseCaching();
            app.UseResponseCompression();



            var task = Task.Run(async () =>
            {
                var categories = await personCategoryService.QueryAsync("person_category");

                if (categories.Count() == 0)
                {
                    var seedCategories = new List<PersonCategory>()
                    {
                        new PersonCategory(){ Name = "SSWA" },
                        new PersonCategory(){ Name = "Contractor" },
                        new PersonCategory(){ Name = "Visitor" },
                    };

                    foreach (var seedCategory in seedCategories)
                    {
                        await personCategoryService.AddAsync(seedCategory);
                    }
                }

                var subcategories = await personSubcategoryService.QueryAsync("person_subcategory");
                if (subcategories.Count() == 0)
                {
                    var seedSubcategories = new List<PersonSubcategory>()
                    {
                        new PersonSubcategory(){ Name = "Water Corporation" },
                        new PersonSubcategory(){ Name = "Valoriza Group" },
                        new PersonSubcategory(){ Name = "Tecnicas Reunidas Group" },
                    };

                    foreach (var seedSubcategory in seedSubcategories)
                    {
                        await personSubcategoryService.AddAsync(seedSubcategory);
                    }
                }

                var training = await visitorTrainingService.QueryAsync("visitor_training");
                if (training.Count() == 0)
                {
                    var seedTrainings = new List<VisitorTraining>()
                    {
                        new VisitorTraining() {TrainingType = "Induction", Name = "PCW", Expiry = 12, ExpiryUnit = "month"},
                        new VisitorTraining() {TrainingType = "Induction", Name = "EDD", Expiry = 12, ExpiryUnit = "hour"},
                        new VisitorTraining() {TrainingType = "Induction", Name = "DWRD", Expiry = 12, ExpiryUnit = "month"},
                        new VisitorTraining() {TrainingType = "Induction", Name = "STW", Expiry = 1, ExpiryUnit = "month"},
                        new VisitorTraining() {TrainingType = "Induction", Name = "MW", Expiry = 3, ExpiryUnit = "month"},
                        new VisitorTraining() {TrainingType = "Induction", Name = "V", Expiry = 12, ExpiryUnit = "hour"},
                        new VisitorTraining() {TrainingType = "Awareness", Name = "Chemicals", Expiry = null, ExpiryUnit = null},
                        new VisitorTraining() {TrainingType = "Awareness", Name = "Chlorine", Expiry = null, ExpiryUnit = null},
                        new VisitorTraining() {TrainingType = "Awareness", Name = "High Voltage", Expiry = null, ExpiryUnit = null},
                        new VisitorTraining() {TrainingType = "Awareness", Name = "High Pressure", Expiry = null, ExpiryUnit = null},
                    };

                    foreach (var seedTraining in seedTrainings)
                    {
                        await visitorTrainingService.AddAsync(seedTraining);
                    }
                }

            });

            Task.WaitAll(task);


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default2",
                    template: "{controller=Home}/{action=Index}");


                // routes.MapRoute(
                //    name: "default",
                //    template: "{controller=User}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
