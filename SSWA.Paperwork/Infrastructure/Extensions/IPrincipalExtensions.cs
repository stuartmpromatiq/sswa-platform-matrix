using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace SSWA.Admin.App.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        const string BASE_CLAIM_NAME = "http://api.sswa/claims/";
        const string NAME_CLAIM_NAME = BASE_CLAIM_NAME + "name";
        const string EMAIL_CLAIM_NAME = BASE_CLAIM_NAME + "email";
        const string PHONE_CLAIM_NAME = BASE_CLAIM_NAME + "phone";
        const string ROLE_CLAIM_NAME = BASE_CLAIM_NAME + "roles";
        const string PERMISSION_CLAIM_NAME = BASE_CLAIM_NAME + "permissions";

        public static string GetId(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?
                            .Value;
        }

        public static string GetName(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .FirstOrDefault(c => c.Type == NAME_CLAIM_NAME)?
                            .Value;
        }

        public static string GetEmail(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .FirstOrDefault(c => c.Type == EMAIL_CLAIM_NAME)?
                            .Value;
        }

        public static string GetPhone(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .FirstOrDefault(c => c.Type == PHONE_CLAIM_NAME)?
                            .Value;
        }

        public static string[] GetRoles(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .Where(c => c.Type == ROLE_CLAIM_NAME)
                            .Select(x => x.Value)
                            .ToArray();
        }

        public static string[] GetPermissions(this ClaimsPrincipal principal)
        {
            return principal.Claims
                            .Where(c => c.Type == PERMISSION_CLAIM_NAME)
                            .Select(x => x.Value)
                            .ToArray();
        }

        public static bool HasRole(this ClaimsPrincipal principal, string role)
        {
            return principal.HasClaim(ROLE_CLAIM_NAME, role);
        }

        public static bool HasPermission(this ClaimsPrincipal principal, string permission)
        {
            return principal.HasClaim(PERMISSION_CLAIM_NAME, permission);
        }

    }
}
