using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SSWA.Admin.App.Extensions
{

    public static class TypeExtensions
    {
        public static PropertyInfo GetPublicProperty(this Type type, string property)
        {
            if (type.IsInterface)
            {
                var propertyInfos = new List<PropertyInfo>();

                var considered = new List<Type>();
                var queue = new Queue<Type>();
                considered.Add(type);
                queue.Enqueue(type);
                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (considered.Contains(subInterface)) continue;

                        considered.Add(subInterface);
                        queue.Enqueue(subInterface);
                    }

                    var propertyInfo = subType.GetProperty(property,
                        BindingFlags.IgnoreCase
                        | BindingFlags.FlattenHierarchy
                        | BindingFlags.Public
                        | BindingFlags.Instance);

                    if (propertyInfo != null)
                    {
                        return propertyInfo;
                    }
                }

                return null;
            }

            return type.GetProperty(property,
                BindingFlags.IgnoreCase
                | BindingFlags.FlattenHierarchy
                | BindingFlags.Public
                | BindingFlags.Instance);

        }
    }

}
