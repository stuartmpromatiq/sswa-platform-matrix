using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Admin.App.Extensions;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Controllers
{
    [Produces("application/json")]
    public class ProcessLockController : Controller
    {
        private readonly CosmosDbService<ProcessLock> _processLockService;
        private readonly ICurrentUserService _currentUserService;

        public ProcessLockController(
            CosmosDbService<ProcessLock> processLockService,
            ICurrentUserService currentUserService
            )
        {
            _processLockService = processLockService;
            _currentUserService = currentUserService;
        }

        [HttpGet("/api/process-locks/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<PagedResult<ProcessLock>> Get(
          [FromQuery] string friendlyId,
          [FromQuery] string tagNumber,
          [FromQuery] string description,
          [FromQuery] int pageSize = 20,
          [FromQuery] string token = null,
          [FromQuery] string sortBy = "friendlyId",
          [FromQuery] string orderBy = "descending"
      )
        {
            var predicates = new List<Expression<Func<ProcessLock, bool>>>();
            var sort = GetOrderBy<ProcessLock>(sortBy, orderBy);

            if (!string.IsNullOrWhiteSpace(friendlyId))
            {
                predicates.Add(x =>
                    x.FriendlyId == friendlyId.ToUpperInvariant().Trim() ||
                    x.FriendlyId.Contains(friendlyId.ToUpperInvariant().Trim()) ||
                    x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant().Trim()));
            }

            if (description != null)
            {
                var rgx = new Regex("[^a-zA-z0-9]");
                var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                predicates.Add(x =>
                    x.CanonicalReason.Contains(searchableDescription));
            }

            if (!string.IsNullOrWhiteSpace(tagNumber))
            {
                predicates.Add(x =>
                    x.TagNumber == tagNumber.ToUpperInvariant().Trim() ||
                    x.TagNumber.Contains(tagNumber.ToUpperInvariant().Trim()) ||
                    x.TagNumber.StartsWith(tagNumber.ToUpperInvariant().Trim()));
            }

            //if (!string.IsNullOrWhiteSpace(category))
            //{
            //    predicates.Add(x => x.CanonicalCategory == category);
            //}

            //if (!string.IsNullOrWhiteSpace(company))
            //{
            //    predicates.Add(x => x.CanonicalCompany == company);
            //}

            return await _processLockService.QueryPagedAsync("process_lock", pageSize, token, sort, predicates.ToArray());
        }

        [HttpGet("/api/process-locks/download")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<ProcessLock>> Download(
          [FromQuery] string friendlyId,
          [FromQuery] string tagNumber,
          [FromQuery] string description,
          [FromQuery] string sortBy = "friendlyId",
          [FromQuery] string orderBy = "descending"
      )
        {
            var predicates = new List<Expression<Func<ProcessLock, bool>>>();
            var sort = GetOrderBy<ProcessLock>(sortBy, orderBy);

            if (!string.IsNullOrWhiteSpace(friendlyId))
            {
                predicates.Add(x =>
                    x.FriendlyId == friendlyId.ToUpperInvariant().Trim() ||
                    x.FriendlyId.Contains(friendlyId.ToUpperInvariant().Trim()) ||
                    x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant().Trim()));
            }

            if (description != null)
            {
                var rgx = new Regex("[^a-zA-z0-9]");
                var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                predicates.Add(x =>
                    x.CanonicalReason.Contains(searchableDescription));
            }

            if (!string.IsNullOrWhiteSpace(tagNumber))
            {
                predicates.Add(x =>
                    x.TagNumber == tagNumber.ToUpperInvariant().Trim() ||
                    x.TagNumber.Contains(tagNumber.ToUpperInvariant().Trim()) ||
                    x.TagNumber.StartsWith(tagNumber.ToUpperInvariant().Trim()));
            }

            //if (!string.IsNullOrWhiteSpace(category))
            //{
            //    predicates.Add(x => x.CanonicalCategory == category);
            //}

            //if (!string.IsNullOrWhiteSpace(company))
            //{
            //    predicates.Add(x => x.CanonicalCompany == company);
            //}

            return await _processLockService.QueryAsync("process_lock", predicates.ToArray());
        }

        [HttpGet("/api/process-locks/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<ProcessLock> Get(string id)
        {

            return await _processLockService.GetAsync(id);
        }

        [HttpPost("/api/process-locks/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Post([FromBody] ProcessLock processLock)
        {
            try
            {
                var sqlQuery = $@"
                    SELECT VALUE(udf.TO_INT(replace(c.friendlyId, 'PL', '')))
                    FROM c
                    WHERE c.type = @type
                ";

                processLock.CreatedAt = DateTime.UtcNow;
                processLock.UpdatedAt = DateTime.UtcNow;
                processLock.FriendlyId = processLock.GetNextId(await _processLockService.SqlQueryAsync<string>("process_lock", sqlQuery));
                return Ok(await _processLockService.AddAsync(processLock));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("/api/process-locks/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(string id, [FromBody] ProcessLock processLock)
        {
            var existingProcessLock = await _processLockService.GetAsync(id);

            if (existingProcessLock == null)
            {
                return NotFound();
            }

            try
            {
                existingProcessLock.EquipmentStatus = processLock.EquipmentStatus;
                existingProcessLock.Location = processLock.Location;
                existingProcessLock.LockPlacedBy = processLock.LockPlacedBy;
                existingProcessLock.LockRemovedBy = processLock.LockRemovedBy;
                existingProcessLock.NOE = processLock.NOE;
                existingProcessLock.Reason = processLock.Reason;
                existingProcessLock.TagNumber = processLock.TagNumber;
                existingProcessLock.UniqueLockNumber = processLock.UniqueLockNumber;
                existingProcessLock.UpdatedAt = DateTime.UtcNow;

                return Ok(await _processLockService.UpdateAsync(existingProcessLock));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost("/api/process-locks/{id}/hide")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Hide(string id)
        {
            var existingProcessLock = await _processLockService.GetAsync(id);

            if (existingProcessLock == null)
            {
                return NotFound();
            }

            try
            {
                existingProcessLock.Inactive = true;
                await _processLockService.UpdateAsync(existingProcessLock);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private Func<IQueryable<T>, IOrderedQueryable<T>> GetOrderBy<T>(string orderColumn, string orderType) where T : ProcessLock
        {
            Type typeQueryable = typeof(IQueryable<T>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);
            string[] props = orderColumn.Split('.');
            IQueryable<T> query = new List<T>().AsQueryable<T>();
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");

            Expression expr = arg;
            foreach (string prop in props)
            {
                PropertyInfo pi = type.GetPublicProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            LambdaExpression lambda = Expression.Lambda(expr, arg);
            string methodName = orderType == "ascending" ? "OrderBy" : "OrderByDescending";

            MethodCallExpression resultExp =
                Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(T), type }, outerExpression.Body, Expression.Quote(lambda));
            var finalLambda = Expression.Lambda(resultExp, argQueryable);
            return (Func<IQueryable<T>, IOrderedQueryable<T>>)finalLambda.Compile();
        }
    }
}
