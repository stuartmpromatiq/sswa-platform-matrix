using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Paperwork.Models;
using JWT;
using JWT.Serializers;
using SSWA.ExternalUserService;
using Microsoft.Extensions.Configuration;

namespace SSWA.Admin.App.Controllers
{
    public class AccountController : Controller
    {
        private IExternalUserService _externalUserService;
        private IConfiguration _configuration;

        public AccountController(IExternalUserService externalUserService, IConfiguration configuration)
        {
            _configuration = configuration;
            _externalUserService = externalUserService;
        }

        [AllowAnonymous]
        public async Task Login(string returnUrl = "/")
        {
            await HttpContext.ChallengeAsync("Auth0", new AuthenticationProperties() { RedirectUri = returnUrl });
        }

        [AllowAnonymous]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Auth0", new AuthenticationProperties
            {
                // Indicate here where Auth0 should redirect the user after a logout.
                // Note that the resulting absolute Uri must be whitelisted in the
                // **Allowed Logout URLs** settings for the client.
                RedirectUri = Url.Action("Index", "Home")
            });
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Activate(string userToken)
        {
            dynamic metadata = null;
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                var signingKey = _configuration["Auth0:SigningKey"];

                metadata = decoder.DecodeToObject(userToken, signingKey, verify: true);
            }
            catch (TokenExpiredException)
            {
                return View("Error", new ErrorViewModel() { Message = "Token has expired" });
            }
            catch (SignatureVerificationException)
            {
                return View("Error", new ErrorViewModel() { Message = "Token has invalid signature" });
            }

            string id = metadata["id"].ToString();
            var user = await _externalUserService.GetUserAsync(id);

            if (user.AppMetadata["activation_pending"] != null && !((bool)user.AppMetadata["activation_pending"]))
                return View(new ActivateUserViewModel { Email = user.Email, UserToken = userToken, PendingActivation = false });

            if (user != null)
                return View(new ActivateUserViewModel { Email = user.Email, UserToken = userToken, PendingActivation = true });

            return View("Error", new ErrorViewModel() { Message = "Error activating user, could not find an exact match for this email address." });
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Activate(ActivateUserViewModel model, [FromQuery] string userToken)
        {
            dynamic metadata = null;
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                metadata = decoder.DecodeToObject(model.UserToken, _configuration["Auth0:SigningKey"], verify: true);
            }
            catch (TokenExpiredException)
            {
                return View("Error", new ErrorViewModel() { Message = "Token has expired" });
            }
            catch (SignatureVerificationException)
            {
                return View("Error", new ErrorViewModel() { Message = "Token has invalid signature" });
            }

            if (metadata == null)
            {
                return View("Error",
                    new ErrorViewModel() { Message = "Unable to find the token." });
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            string id = metadata["id"];
            var user = await _externalUserService.GetUserAsync(id);
            if (user != null)
            {
                if (user.AppMetadata["activation_pending"] != null && !((bool)user.AppMetadata["activation_pending"]))
                    return View("Error", new ErrorViewModel() { Message = "Error activating user, the user is already active." });

                await _externalUserService.ActivateUserAsync(user.Identifier, model.Password);
                await _externalUserService.UpdateMetadataAsync(user.Identifier, new { activation_pending = false });

                return View("Activated");
            }

            return View("Error",
                new ErrorViewModel() { Message = "Error activating user, could not find an exact match for this email address." });
        }

        [AllowAnonymous]
        public IActionResult AccessDenied(string returnUrl)
        {
            return View("Error", new ErrorViewModel() { Message = "You do not have access to this page." });
        }
    }
}
