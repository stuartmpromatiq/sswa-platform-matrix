using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Controllers
{
    [Produces("application/json")]
    public class TagController : Controller
    {
        private readonly CosmosDbService<Tag> _tagService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMediator _mediator;

        public TagController(
            IMediator mediator,
            CosmosDbService<Tag> fetchService,
            ICurrentUserService currentUserService)
        {
            _tagService = fetchService;
            _currentUserService = currentUserService;

            _mediator = mediator;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/tags/{searchTerm}")]
        [ResponseCache(Duration = 3600)]
        public async Task<IEnumerable<Tag>> Get([FromRoute] string searchTerm)
        {
            var predicates = new List<Expression<Func<Tag, bool>>>();

            if (!(string.IsNullOrWhiteSpace(searchTerm)))
            {
                predicates.Add(x => x.Id.StartsWith(searchTerm.ToUpperInvariant()) || x.Description.Contains(searchTerm));
            }

            var result = await _tagService.QueryPagedAsync("tag", 25, null, null, predicates.ToArray());

            return result.Data.OrderBy(x => x.Id);
        }
    }
}
