using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paperwork.Models;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Models;

namespace SSWA.Admin.App.Controllers
{
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly IExternalUserService _externalUserService;
        private readonly NotificationService _notificationService;

        private readonly ICurrentUserService _currentUserService;

        private readonly DocumentService<IForm> _fetchService;

        private readonly CosmosDbService<UserRolePriority> _rolePriorityService;

        public UserController(
            IExternalUserService externalUserService,
            NotificationService notificationService,
            ICurrentUserService currentUserService,
            DocumentService<IForm> fetchService,
            CosmosDbService<UserRolePriority> rolePriorityService)
        {
            _externalUserService = externalUserService;
            _notificationService = notificationService;
            _currentUserService = currentUserService;
            _fetchService = fetchService;
            _rolePriorityService = rolePriorityService;
        }

        [HttpGet("/api/users")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<IEnumerable<ExternalUser>> Get()
        {
            var externalUsers = await _externalUserService.GetAllUsersAsync();

            return externalUsers;
        }

        [HttpGet("/api/users/role/{role}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<IEnumerable<ExternalUser>> GetByRole(string role)
        {
            var query = $"app_metadata.authorization.roles:\"{role}\"";
            var externalUsers = await _externalUserService.GetUsersByQueryAsync(query);
            externalUsers = externalUsers.ToList();

            var existingUsers = await _rolePriorityService.QueryAsync("user_role_priority", x => x.RoleId == role);

            var users = externalUsers.ToList();

            foreach (var user in users)
            {
                var existingUser = existingUsers.FirstOrDefault(x => x.UserId == user.Id);
                user.Priority = existingUser?.Priority ?? 3;
            }

            return users.OrderBy(x => x.Priority).ThenBy(x => x.Name);
        }

        [HttpGet("/api/users/{userId}/forms")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<IEnumerable<IForm>> GetUserForms(string userId)
        {
            var forms = await _fetchService.ListAsync(userId);

            return forms.OrderByDescending(x => x.UpdatedAt);
        }

        [HttpGet("/api/user/forms")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<IForm>> GetMyForms([FromQuery] int pageSize = 20, string token = null)
        {
            var id = _currentUserService.Id;

            var result = await _fetchService.QueryPagedAsync(pageSize, token, null, x => x.CreatedBy.Id == id, x => x.Status != FormState.CANCELLED);

            return result;
        }

        [HttpGet("/api/user/forms/{formType}/{searchTerm?}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<IEnumerable<IForm>> SearchMyForms(string formType, string searchTerm)
        {
            var id = _currentUserService.Id;

            var predicates = new List<Expression<Func<IForm, bool>>>();

            //if (!_currentUserService.UserInRole(Roles.ADMIN))
            //{
            //    predicates.Add(x => x.CreatedBy.Id == id);
            //}
            predicates.Add(x => x.Status != FormState.CANCELLED);
            predicates.Add(x => x.Kind.Slug == formType);

            if (!(string.IsNullOrWhiteSpace(searchTerm)))
            {
                predicates.Add(x =>
                    x.FriendlyId.StartsWith(searchTerm.ToUpperInvariant()) ||
                    x.FriendlyId.Contains(searchTerm.ToUpperInvariant()) ||
                    x.Description.Contains(searchTerm));
            }

            var result = await _fetchService.QueryAsync(predicates.ToArray());

            return result.OrderByDescending(x => x.UpdatedAt)
                         .Take(10)
                         .Select<IForm, IForm>(x => new CommonForm(x.Description) { Id = x.Id, FriendlyId = x.FriendlyId });
        }

        [HttpGet("/api/user/approvals")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<IForm>> GetMyApprovals([FromQuery] int pageSize = 20, string token = null)
        {
            var status = _currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) ? FormState.PARTIALLY_APPROVED : FormState.PENDING_APPROVAL;
            var sqlQuery = $@"select distinct value c from c join a in c.approvers where c.type = @type and a.user.id = @id and a.isApproved = null and c.status = '{status}' order by c.startDate";

            var items = await _fetchService.SqlQueryPagedAsync(sqlQuery, pageSize, token, new KeyValuePair<string, object>("id", _currentUserService.Id));

            return items;
        }

        [HttpGet("/api/user/awaiting-completion")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<IForm>> GetAwaitingCompletion([FromQuery] int pageSize = 20, string token = null)
        {
            var predicates = new List<Expression<Func<IForm, bool>>>();

            predicates.Add(x => x.Status == FormState.APPROVED);
            predicates.Add(x => x.EndDate <= DateTime.Now.ToUniversalTime().Date);

            if (!_currentUserService.UserInRoles(Roles.ADMIN, Roles.PERMIT_AUTHORISER))
            {
                predicates.Add(x => x.CreatedBy.Id == _currentUserService.Id);
            }

            var forms = await _fetchService.QueryPagedAsync(pageSize, token, null, predicates.ToArray());

            return forms;
        }

        [HttpGet("/api/user/awaiting-authorisation")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<IForm>> GetAwaitingAuthorisation([FromQuery] int pageSize = 20, string token = null)
        {
            var predicates = new List<Expression<Func<IForm, bool>>>();

            predicates.Add(x =>
                x.Status == FormState.PENDING_AUTHORISATION ||
                x.Status == FormState.PENDING_CANCELLATION ||
                x.Status == FormState.PENDING_COMPLETION ||
                x.Status == FormState.PENDING_REVALIDATION ||
                x.Status == FormState.PENDING_SUSPENSION ||
                x.Status == FormState.PENDING_TEST_RUN ||
                x.Status == FormState.PENDING_TRANSFER
            );
            predicates.Add(x => x.Kind.Slug == "authority-to-work");

            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
            {
                predicates.Add(x => x.CreatedBy.Id == _currentUserService.Id);
            }

            var forms = await _fetchService.QueryPagedAsync(pageSize, token, null, predicates.ToArray());

            return forms;
        }

        [HttpGet("/api/user/awaiting-open")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<IForm>> GetAwaitingOpen([FromQuery] int pageSize = 20, string token = null)
        {
            var predicates = new List<Expression<Func<IForm, bool>>>();

            predicates.Add(x => x.Status == FormState.APPROVED);
            predicates.Add(x => x.Kind.Slug == "authority-to-work");
            if (!_currentUserService.UserInRole(Roles.PERMIT_AUTHORISER))
            {
                predicates.Add(x => x.CreatedBy.Id == _currentUserService.Id);
            }

            var forms = await _fetchService.QueryPagedAsync(pageSize, token, null, predicates.ToArray());

            return forms;
        }

        [HttpGet("/api/user/notifications")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<PagedResult<Notification>> GetMyNotifications([FromQuery] int pageSize = 20, string token = null)
        {
            var result = await _notificationService.PagedListAsync(pageSize, token, _currentUserService.Id);
            return result;
        }

        // GET: User
        [HttpGet("/admin/users")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 600)]
        public async Task<ActionResult> Index()
        {
            var externalUsers = await _externalUserService.GetAllUsersAsync();

            var viewModel = new List<UserListViewModel>();

            foreach (var externaUser in externalUsers)
            {
                var userItem = new UserListViewModel();
                userItem.ID = externaUser.Id;
                userItem.Name = externaUser.FirstName + " " + externaUser.LastName;
                userItem.Email = externaUser.Email;
                userItem.Phone = externaUser.Phone;
                userItem.Blocked = externaUser.Blocked;

                userItem.AccountActivated = externaUser.IsActivated;
                userItem.LastLogin = externaUser.LastLogin;
                userItem.LoginCount = externaUser.LoginsCount;

                viewModel.Add(userItem);
            }

            return View(viewModel);
        }


        // GET: User/Create
        [HttpGet("/admin/users/create")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost("/admin/users/create")]
        [ValidateAntiForgeryToken]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Create(UserViewModel user)
        {
            var externalUser = new ExternalUser("", user.Email.Trim(), user.Phone, user.FirstName.Trim(), user.LastName.Trim(), new string[0]);
            var activationCallback = Url.Action("Activate", "Account", new { }, Request.Scheme);

            var invitedUser = await _externalUserService.InviteUserAsync(externalUser, activationCallback);
            user.AuthId = invitedUser.Identifier;

            return RedirectToAction(nameof(Index));
        }

        // GET: User/Edit/5
        [HttpGet("/admin/users/edit/{id}")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Edit(string id)
        {
            var user = await _externalUserService.GetUserAsync(id);
            var roles = await _externalUserService.GetAllRolesAsync();
            var userRoles = await _externalUserService.GetUserRolesAsync(id);

            var viewModel = new EditUserViewModel()
            {
                ID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Phone = user.Phone,
                Roles = roles.Select(x => new RoleListViewModel()
                {
                    RoleId = x.Id,
                    RoleDescription = x.Description,
                    RoleName = x.Name,
                    IsInRole = userRoles.Any(y => y.Id == x.Id)
                }).ToList(),
            };


            return View(viewModel);
        }

        // POST: User/Edit/5
        [HttpPost("/admin/users/edit/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Edit(string id, EditUserViewModel viewModel)
        {
            try
            {
                var existingUser = await _externalUserService.GetUserAsync(id);

                existingUser.FirstName = viewModel.FirstName;
                existingUser.LastName = viewModel.LastName;
                existingUser.Phone = viewModel.Phone;

                var addRoles = viewModel.Roles.Where(x => x.IsInRole).Select(x => new ExternalRole()
                {
                    Id = x.RoleId,
                    Name = x.RoleName,
                    Description = x.RoleDescription
                });

                var deleteRoles = viewModel.Roles.Where(x => !x.IsInRole).Select(x => new ExternalRole()
                {
                    Id = x.RoleId,
                    Name = x.RoleName,
                    Description = x.RoleDescription
                });

                if (existingUser.Id != null)
                {
                    existingUser.AppRoles = addRoles;
                    await _externalUserService.UpdateUserAsync(existingUser);
                }

                await _externalUserService.UpdateUserRolesAsync(existingUser.Id, addRoles, deleteRoles);

                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel { Message = "Could not update user." });
            }
        }

        // POST: User/Block/5
        [HttpPost("/admin/users/block/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Block(string id)
        {
            try
            {
                var existingUser = await _externalUserService.BlockUserAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel { Message = "Could not make user inactive." });
            }
        }

        // POST: User/Block/5
        [HttpPost("/admin/users/unblock/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Unblock(string id)
        {
            try
            {
                var existingUser = await _externalUserService.UnblockUserAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel { Message = "Could not make user active." });
            }
        }

        // GET: User/Edit/5
        [HttpGet("/admin/users/approvers/{id}")]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Approvers(string id)
        {
            var viewModel = new EditRolePriorityViewModel();

            var roles = await _externalUserService.GetAllRolesAsync();
            var role = roles.First(x => x.Name == id);

            var existingUsers = await _rolePriorityService.QueryAsync("user_role_priority", x => x.RoleId == id);

            viewModel.RoleId = role.Id;
            viewModel.RoleName = role.Name;
            viewModel.RoleDisplayName = role.Description;

            foreach (var userId in role.Users)
            {
                try
                {
                    var user = await _externalUserService.GetUserAsync(userId);
                    var userViewmodel = new UserRolePriority()
                    {
                        Id = existingUsers.FirstOrDefault(x => x.UserId == userId)?.Id,
                        Priority = existingUsers.FirstOrDefault(x => x.UserId == userId)?.Priority ?? 3,
                        UserId = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    };

                    viewModel.Users.Add(userViewmodel);

                }
                catch
                {
                    continue;
                }
            }

            viewModel.Users =
                viewModel.Users
                         .OrderBy(x => x.Priority)
                         .ThenBy(x => x.LastName)
                         .ThenBy(x => x.FirstName)
                         .ToList();

            ViewBag.Priorities = new List<EditRolePriorityViewModel.RolePriorityViewModel>()
            {
                new EditRolePriorityViewModel.RolePriorityViewModel(){Id = 1, Name = "Primary Approver"},
                new EditRolePriorityViewModel.RolePriorityViewModel(){Id = 2, Name = "Secondary Approver"},
                new EditRolePriorityViewModel.RolePriorityViewModel(){Id = 3, Name = "Tertiary Approver"}
            };

            return View(viewModel);
        }

        // POST: User/Edit/5
        [HttpPost("/admin/users/approvers/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        public async Task<ActionResult> Approvers(string id, EditRolePriorityViewModel viewModel)
        {
            try
            {
                foreach (var user in viewModel.Users)
                {
                    user.RoleId = viewModel.RoleName;
                    await _rolePriorityService.AddAsync(user);
                }

                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel { Message = "Could not update priorities." });
            }
        }
    }
}
