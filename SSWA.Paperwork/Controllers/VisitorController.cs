using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paperwork.Models;
using SSWA.Admin.App.Extensions;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Models;

namespace SSWA.Paperwork.Controllers
{
    [Produces("application/json")]
    public class VisitorController : Controller
    {
        private readonly IExternalUserService _externalUserService;
        private readonly NotificationService _notificationService;
        private readonly ICurrentUserService _currentUserService;
        private readonly CosmosDbService<Person> _personService;
        private readonly CosmosDbService<PersonCategory> _personCategoryService;
        private readonly CosmosDbService<PersonSubcategory> _personSubcategoryService;
        private readonly CosmosDbService<VisitorTraining> _trainingService;
        private readonly DocumentService<PlantEntryRequestForm> _perService;
        private readonly DocumentService<IForm> _fetchService;

        public VisitorController(
            IExternalUserService externalUserService,
            NotificationService notificationService,
            ICurrentUserService currentUserService,
            CosmosDbService<Person> personService,
            CosmosDbService<PersonCategory> personCategoryService,
            CosmosDbService<PersonSubcategory> personSubcategoryService,
            CosmosDbService<VisitorTraining> trainingService,
            DocumentService<PlantEntryRequestForm> perService,
            DocumentService<IForm> fetchService
)
        {
            _externalUserService = externalUserService;
            _notificationService = notificationService;
            _currentUserService = currentUserService;
            _personService = personService;
            _personCategoryService = personCategoryService;
            _personSubcategoryService = personSubcategoryService;
            _trainingService = trainingService;
            _perService = perService;
            _fetchService = fetchService;
        }

        [HttpGet("/api/visitors/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<PagedResult<Person>> Get(
            [FromQuery] string name,
            [FromQuery] string category,
            [FromQuery] string company,
            [FromQuery] int pageSize = 20,
            [FromQuery] string token = null,
            [FromQuery] string sortBy = "name",
            [FromQuery] string orderBy = "ascending"
        )
        {
            var predicates = new List<Expression<Func<Person, bool>>>();
            var sort = GetOrderBy<Person>(sortBy, orderBy);

            if (!string.IsNullOrWhiteSpace(name))
            {
                predicates.Add(x =>
                    x.CanonicalName == name.ToLowerInvariant().Trim() ||
                    x.CanonicalName.Contains(name.ToLowerInvariant().Trim()) ||
                    x.CanonicalName.StartsWith(name.ToLowerInvariant().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(category))
            {
                predicates.Add(x => x.CanonicalCategory == category);
            }

            if (!string.IsNullOrWhiteSpace(company))
            {
                predicates.Add(x => x.CanonicalCompany == company);
            }

            var people = await _personService.QueryPagedAsync("person", pageSize, token, sort, predicates.ToArray());

            return people;
        }

        [HttpGet("/api/visitors/download")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<Person>> Download(
           [FromQuery] string name,
           [FromQuery] string category,
           [FromQuery] string company,
           [FromQuery] string sortBy = "name",
           [FromQuery] string orderBy = "ascending"
       )
        {
            var predicates = new List<Expression<Func<Person, bool>>>();
            var sort = GetOrderBy<Person>(sortBy, orderBy);

            if (!string.IsNullOrWhiteSpace(name))
            {
                predicates.Add(x =>
                    x.CanonicalName == name.ToLowerInvariant().Trim() ||
                    x.CanonicalName.Contains(name.ToLowerInvariant().Trim()) ||
                    x.CanonicalName.StartsWith(name.ToLowerInvariant().Trim()));
            }

            if (!string.IsNullOrWhiteSpace(category))
            {
                predicates.Add(x => x.CanonicalCategory == category);
            }

            if (!string.IsNullOrWhiteSpace(company))
            {
                predicates.Add(x => x.CanonicalCompany == company);
            }

            var people = await _personService.QueryAsync("person", predicates.ToArray());

            return people;
        }


        [HttpGet("/api/visitors/categories")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<PersonCategory>> GetCategories()
        {
            var categories = await _personCategoryService.QueryAsync("person_category");

            return categories.OrderBy(x => x.CanonicalName);
        }

        [HttpGet("/api/visitors/subcategories")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<PersonSubcategory>> GetSubcategories()
        {
            var subcategories = await _personSubcategoryService.QueryAsync("person_subcategory");

            return subcategories.OrderBy(x => x.CanonicalName);
        }

        [HttpGet("/api/visitors/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Person> Get(string id)
        {

            var people = await _personService.GetAsync(id);

            return people;
        }

        [HttpGet("/api/visitors/search/{query}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<Person>> Search(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return new List<Person>();
            }

            var people = await _personService.QueryAsync("person", x => x.CanonicalName.StartsWith(query.ToLowerInvariant().Trim()));

            return people.OrderBy(x => x.CanonicalName);
        }

        [HttpGet("/api/visitors/subcategories/search/{query}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<PersonSubcategory>> SearchSubcategories(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return new List<PersonSubcategory>();
            }

            var people = await _personSubcategoryService.QueryAsync("person_subcategory", x => x.CanonicalName.StartsWith(query.ToLowerInvariant().Trim()));

            return people.OrderBy(x => x.CanonicalName);
        }

        [HttpGet("/api/visitors/search/{query}/training/{formId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> SearchTraining(string query, string formId)
        {
            var result = new Dictionary<string, string>();
            result["Induction"] = "N/A";
            result["Awareness"] = "N/A";

            if (string.IsNullOrWhiteSpace(query))
            {
                return Ok(result);
            }

            var people = await _personService.QueryAsync("person", x => x.CanonicalName.StartsWith(query.ToLowerInvariant().Trim()));
            if (!people.Any())
            {
                return Ok(result);
            }

            var person = people.First();
            var form = await _perService.FindAsync(formId);

            if (form == null)
            {
                return Ok(result);
            }

            CheckInduction(result, person, form);

            CheckAwareness(result, person, form);

            return Ok(result);
        }

        private static void CheckAwareness(Dictionary<string, string> result, Person person, PlantEntryRequestForm form)
        {
            // Awareness
            result["Awareness"] = "";
            foreach (var awareness in form.AwarenessTraining)
            {
                if (!person.LatestTraining.Any(x => x.Type == "Awareness" && x.TrainingName == awareness))
                {
                    result["Awareness"] += $"{awareness} - NO, ";
                }
                else
                {
                    result["Awareness"] += $"{awareness} - YES, ";
                }
            }
            if (result["Awareness"] == "")
            {
                result["Awareness"] = "N/A";
            }
        }

        private static void CheckInduction(Dictionary<string, string> result, Person person, PlantEntryRequestForm form)
        {
            // Induction
            if (person.LatestTraining.Any(x =>
                x.Type == "Induction" &&
                (
                    (x.TrainingName == "EDD" && (form.RequestType == "EDD")) ||
                    (x.TrainingName == "DWRD" && (form.RequestType == "DWRD")) ||
                    (x.TrainingName == "PCW" && (form.RequestType == "PCW" || form.RequestType == "STW" || form.RequestType == "V")) ||
                    (x.TrainingName == "STW" && (form.RequestType == "STW" || form.RequestType == "V")) ||
                    (x.TrainingName == "V" && (form.RequestType == "V"))
                ) &&
                x.StartDate.GetValueOrDefault(DateTime.MinValue) <= form.StartDate.GetValueOrDefault(DateTime.MaxValue) &&
                x.ExpiryDate.GetValueOrDefault(DateTime.MinValue) >= form.StartDate.GetValueOrDefault(DateTime.MaxValue)))
            {
                result["Induction"] = "YES";
            }
            else
            {
                result["Induction"] = "NO";
            }
        }

        [HttpPost("/api/visitors/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Post(Person person)
        {
            try
            {
                return Ok(await _personService.AddAsync(person));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("/api/visitors/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(string id, [FromBody] Person person)
        {
            var existingPerson = await _personService.GetAsync(id);

            if (existingPerson == null)
            {
                return NotFound();
            }

            try
            {
                existingPerson.Name = person.Name;
                existingPerson.Category = person.Category;
                existingPerson.Company = person.Company;
                existingPerson.ContactNumber = person.ContactNumber;
                existingPerson.Email = person.Email;
                existingPerson.VehicleRegistration = person.VehicleRegistration;

                return Ok(await _personService.UpdateAsync(existingPerson));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("/api/visitors/{id}/hide")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Hide(string id)
        {
            var existingPerson = await _personService.GetAsync(id);

            if (existingPerson == null)
            {
                return NotFound();
            }

            try
            {
                existingPerson.Inactive = true;
                await _personService.UpdateAsync(existingPerson);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("/api/visitors/{id}/training/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Put(string id, [FromBody] PersonTraining training)
        {
            var existingPerson = await _personService.GetAsync(id);


            if (existingPerson == null)
            {
                return NotFound();
            }


            var existingTraining = existingPerson.LatestTraining.FirstOrDefault(x => x.Type == training.Type && x.TrainingName == training.TrainingName);

            if (existingTraining != null)
            {
                existingPerson.LatestTraining.Remove(existingTraining);
                existingPerson.ArchivedTraining.Add(existingTraining);
            }

            var personTraining = new PersonTraining()
            {
                Type = training.Type,
                TrainingName = training.TrainingName,
                StartDate = training.StartDate.GetValueOrDefault(DateTime.Today),
            };

            var trainingTemplate = (await _trainingService.QueryAsync("visitor_training", x => x.TrainingType == training.Type && x.Name == training.TrainingName)).FirstOrDefault();

            if (trainingTemplate == null)
            {
                return NotFound();
            }

            SetExpiry(personTraining, trainingTemplate);

            existingPerson.LatestTraining.Add(personTraining);

            try
            {
                return Ok(await _personService.UpdateAsync(existingPerson));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("/api/visitors/{id}/forms")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IEnumerable<IForm>> GetVisitorForms(string id)
        {

                var forms = await _fetchService.SqlQueryAsync("SELECT c.id, c.type, c.requestReason, c.description, c.kind, c.friendlyId, c.startDate, c.endDate, c.status FROM c JOIN a IN c.visitors WHERE a.id = @visitorId Order By c.startDate Desc", new KeyValuePair<string, object>("visitorId", id));
                return forms;
        }

        [HttpPost("/api/visitors/{id}/merge/{correctId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Merge(string id, string correctId)
        {
            var incorrectVisitor = await _personService.GetAsync(id);
            var correctVisitor = await _personService.GetAsync(correctId);

            if (incorrectVisitor == null || correctVisitor == null)
            {
                return NotFound();
            }

            try
            {
                var forms = await _perService.SqlQueryAsync("SELECT VALUE c FROM c JOIN a IN c.visitors WHERE a.id = @visitorId", new KeyValuePair<string, object>("visitorId", id));

                foreach (var form in forms)
                {
                    foreach (var visitor in form.Visitors.Where(x => x.Id == id))
                    {

                        visitor.Id = correctVisitor.Id;
                        visitor.Name = correctVisitor.Name;
                        visitor.Email = correctVisitor.Email;
                        visitor.Category = correctVisitor.Category;
                        visitor.Company = correctVisitor.Company;
                        visitor.ContactNumber = correctVisitor.ContactNumber;
                    }

                    try
                    {
                        await _perService.UpdateAsync(form);
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
            }
            catch (Exception ex1)
            {
                return BadRequest(ex1.Message);

            }


            try
            {
                incorrectVisitor.Inactive = true;
                return Ok(await _personService.UpdateAsync(incorrectVisitor));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private static void SetExpiry(PersonTraining personTraining, VisitorTraining trainingTemplate)
        {
            if (trainingTemplate.Expiry != null && trainingTemplate.ExpiryUnit != null)
            {
                if (string.Equals(trainingTemplate.ExpiryUnit, "hour", StringComparison.InvariantCultureIgnoreCase))
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddHours(trainingTemplate.Expiry.Value);
                }
                else if (string.Equals(trainingTemplate.ExpiryUnit, "day", StringComparison.InvariantCultureIgnoreCase))
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddDays(trainingTemplate.Expiry.Value);
                }
                else if (string.Equals(trainingTemplate.ExpiryUnit, "month", StringComparison.InvariantCultureIgnoreCase))
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddMonths(trainingTemplate.Expiry.Value);
                }
                else if (string.Equals(trainingTemplate.ExpiryUnit, "year", StringComparison.InvariantCultureIgnoreCase))
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddYears(trainingTemplate.Expiry.Value);
                }
            }
        }

        private Func<IQueryable<T>, IOrderedQueryable<T>> GetOrderBy<T>(string orderColumn, string orderType) where T : Person
        {
            Type typeQueryable = typeof(IQueryable<T>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);
            string[] props = orderColumn.Split('.');
            IQueryable<T> query = new List<T>().AsQueryable<T>();
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");

            Expression expr = arg;
            foreach (string prop in props)
            {
                PropertyInfo pi = type.GetPublicProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            LambdaExpression lambda = Expression.Lambda(expr, arg);
            string methodName = orderType == "ascending" ? "OrderBy" : "OrderByDescending";

            MethodCallExpression resultExp =
                Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(T), type }, outerExpression.Body, Expression.Quote(lambda));
            var finalLambda = Expression.Lambda(resultExp, argQueryable);
            return (Func<IQueryable<T>, IOrderedQueryable<T>>)finalLambda.Compile();
        }
    }
}
