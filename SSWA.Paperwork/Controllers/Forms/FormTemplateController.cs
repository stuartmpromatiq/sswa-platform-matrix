using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Models.ViewModels;

namespace SSWA.Paperwork.Controllers.Forms
{
    public class FormTemplateController : FormControllerBase
    {
        private readonly DocumentService<IFormTemplate> _fetchService;

        public FormTemplateController(ICurrentUserService currentUserService,
                                      IFormAuthorisation formAuthorisation,
            DocumentService<IFormTemplate> fetchService) : base(currentUserService, formAuthorisation)
        {
            _fetchService = fetchService;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/form-templates")]
        public async Task<PagedResult<IFormTemplate>> GetFormTemplates([FromQuery] int pageSize = 20,
                                                          [FromQuery] string token = null,
                                                          [FromQuery] string name = null,
                                                          [FromQuery] string level1 = null,
                                                          [FromQuery] string level2 = null,
                                                          [FromQuery] string level3 = null,
                                                          [FromQuery] string sortBy = "createdAt",
                                                          [FromQuery] string orderBy = "descending")
        {


            var sort = GetOrderBy<IFormTemplate>(sortBy, orderBy);
            var filters = new List<Expression<Func<IFormTemplate, bool>>>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                filters.Add(x =>
                x.Lookup.Name == name ||
                x.Lookup.Name.Contains(name));
            }

            if (!string.IsNullOrWhiteSpace(level1))
            {
                filters.Add(x => x.Lookup.Level1 == level1);
            }

            if (!string.IsNullOrWhiteSpace(level2))
            {
                filters.Add(x => x.Lookup.Level2 == level2);
            }

            if (!string.IsNullOrWhiteSpace(level3))
            {
                filters.Add(x => x.Lookup.Level3 == level3);
            }

            var forms = await _fetchService.QueryTemplatesPagedAsync(pageSize, token, sort, filters.ToArray());
            return forms;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/form-templates/download")]
        public async Task<IEnumerable<IFormTemplate>> GetFormTemplateDownloads(
                                                          [FromQuery] string name = null,
                                                          [FromQuery] string level1 = null,
                                                          [FromQuery] string level2 = null,
                                                          [FromQuery] string level3 = null,
                                                          [FromQuery] string sortBy = "createdAt",
                                                          [FromQuery] string orderBy = "descending")
        {


            var sort = GetOrderBy<IFormTemplate>(sortBy, orderBy);
            var filters = new List<Expression<Func<IFormTemplate, bool>>>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                filters.Add(x =>
                x.Lookup.Name == name ||
                x.Lookup.Name.Contains(name));
            }

            if (!string.IsNullOrWhiteSpace(level1))
            {
                filters.Add(x => x.Lookup.Level1 == level1);
            }

            if (!string.IsNullOrWhiteSpace(level2))
            {
                filters.Add(x => x.Lookup.Level2 == level2);
            }

            if (!string.IsNullOrWhiteSpace(level3))
            {
                filters.Add(x => x.Lookup.Level3 == level3);
            }

            var forms = await _fetchService.QueryTemplatesAsync(filters.ToArray());
            return forms;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/form-templates/lookup/{formType}")]
        public async Task<IEnumerable<FormTemplateLookup>> GetFormTemplatesLookup(
          [FromRoute] string formType
        )
        {

            var sql = @"
                select
                    c.id, c.kind.slug as type, c.lookup.level1, c.lookup.level2, c.lookup.level3, c.lookup.name
                from c
                where c.type = @type and c.kind.slug = @formType
            ";

            var forms = await _fetchService.SqlQueryTemplatesAsync<FormTemplateLookup>(sql, new KeyValuePair<string, object>("formType", formType));

            return forms;

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/form-templates/{formType}/{templateId}")]
        public async Task<IFormTemplate> GetFormTemplate(
          [FromRoute] string formType,
          [FromRoute] string templateId
        )
        {

            var form = await _fetchService.FindAsync(templateId);

            return form;

        }
    }
}
