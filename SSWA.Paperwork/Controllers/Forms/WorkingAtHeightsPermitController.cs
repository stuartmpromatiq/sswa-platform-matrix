using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SSWA.Paperwork.Common.Forms.WorkingAtHeightsPermit;

namespace SSWA.Paperwork.Controllers
{
    public class WorkingAtHeightsPermitController : FormControllerBase
    {
        private readonly DocumentService<WorkingAtHeightsPermit> _wahService;

        public WorkingAtHeightsPermitController(DocumentService<WorkingAtHeightsPermit> wahService,
                                         ICurrentUserService currentUserService,
                                         IFormAuthorisation formAuthorisation)
            : base(currentUserService, formAuthorisation)
        {
            _wahService = wahService;
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[HttpGet("/api/forms/working-at-heights-permit/")]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        return Ok(new WorkingAtHeightsPermit());
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest();
        //    }
        //}

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/working-at-heights-permit/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] WorkingAtHeightsPermit form)
        {
            var existingForm = await _wahService.FindAsync(id);
            var canEdit = FormAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            existingForm.UpdatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };

            existingForm.Revision += 1;

            existingForm.WorksDescription = form.WorksDescription;
            existingForm.PhysicalLocation = form.PhysicalLocation;
            existingForm.SubContractor = form.SubContractor;

            if (existingForm.RescuePlan == null)
                existingForm.RescuePlan = new WorkingAtHeightsPermit.EmergencyRescuePlan();
            existingForm.RescuePlan.PersonnelRecoveryPlan = form?.RescuePlan?.PersonnelRecoveryPlan;
            existingForm.RescuePlan.PhoneNumber = form?.RescuePlan?.PhoneNumber;
            existingForm.RescuePlan.RadioChannel = form?.RescuePlan?.RadioChannel;
            existingForm.RescuePlan.RescueEquipmentAtJobSite = form?.RescuePlan?.RescueEquipmentAtJobSite;

            existingForm.Controls = new List<FormField>();

            try
            {
                await _wahService.UpdateAsync(existingForm);

                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }
    }
}
