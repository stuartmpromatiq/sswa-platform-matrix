using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SSWA.Paperwork.Common.Forms.ConfinedSpaceEntryPermit;

namespace SSWA.Paperwork.Controllers
{
    public class ConfinedSpaceEntryPermitController : FormControllerBase
    {
        private readonly DocumentService<ConfinedSpaceEntryPermit> _permitService;

        public ConfinedSpaceEntryPermitController(DocumentService<ConfinedSpaceEntryPermit> wahService,
                                         ICurrentUserService currentUserService,
                                         IFormAuthorisation formAuthorisation)
            : base(currentUserService, formAuthorisation)
        {
            _permitService = wahService;
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[HttpGet("/api/forms/confined-space-entry-permit/")]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        return Ok(new ConfinedSpaceEntryPermit());
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest();
        //    }
        //}

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/confined-space-entry-permit/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] ConfinedSpaceEntryPermit form)
        {
            var existingForm = await _permitService.FindAsync(id);
            var canEdit = FormAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            existingForm.UpdatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };

            existingForm.Revision += 1;

            existingForm.WorksDescription = form.WorksDescription;
            existingForm.StartDate = form.StartDate;
            existingForm.EndDate = form.EndDate;
            existingForm.EquipmentDescription = form.EquipmentDescription;
            existingForm.ToolsDescription = form.ToolsDescription;
            existingForm.MainTasks = form.MainTasks;
            existingForm.RadioChannel = form.RadioChannel;
            existingForm.RequestedBy = form.RequestedBy;
            existingForm.Company = form.Company;

            try
            {
                await _permitService.UpdateAsync(existingForm);

                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }
    }
}
