using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Admin.App.Extensions;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Events;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Infrastructure.Extensions;
using SSWA.Paperwork.Models;
using SSWA.Paperwork.Models.ViewModels;
using static SSWA.Paperwork.Common.Forms.WorkingAtHeightsPermit;

namespace SSWA.Admin.App.Controllers
{
    [Produces("application/json")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class FormController : Controller
    {
        private readonly DocumentService<PlantEntryRequestForm> _plantEntryRequestService;
        private readonly DocumentService<PlantWorksRequestForm> _plantWorksRequestService;

        private readonly DocumentService<AuthorityToWorkForm> _authorityToWorkService;
        private readonly DocumentService<IsolationPermit> _isoService;
        private readonly DocumentService<WorkingAtHeightsPermit> _wahService;
        private readonly DocumentService<ConfinedSpaceEntryPermit> _cseService;
        private readonly DocumentService<HighVoltagePermit> _hvService;
        private readonly DocumentService<ExcavationPermit> _expService;
        private readonly DocumentService<IsolationPermitTemplate> _isoTemplateService;
        private readonly DocumentService<IForm> _fetchService;
        private readonly CosmosDbService<Lockbox> _lockboxService;
        private readonly CosmosDbService<Person> _personService;
        private readonly CosmosDbService<PersonSubcategory> _personSubcategoryService;
        private readonly CosmosDbService<VisitorTraining> _trainingService;
        private readonly ICurrentUserService _currentUserService;
        private readonly IMediator _mediator;
        private readonly IFormAuthorisation _formAuthorisation;

        public FormController(
            IMediator mediator,
            DocumentService<PlantEntryRequestForm> service,
            DocumentService<PlantWorksRequestForm> worksService,
            DocumentService<AuthorityToWorkForm> atwService,
            DocumentService<IsolationPermit> isoService,
            DocumentService<WorkingAtHeightsPermit> wahService,
            DocumentService<ConfinedSpaceEntryPermit> cseService,
            DocumentService<HighVoltagePermit> hvService,
            DocumentService<ExcavationPermit> expService,
            DocumentService<IsolationPermitTemplate> isoTemplateService,
            DocumentService<IForm> fetchService,
            CosmosDbService<Lockbox> lockboxService,
            CosmosDbService<Person> personService,
            CosmosDbService<PersonSubcategory> personSubcategoryService,
            CosmosDbService<VisitorTraining> trainingService,
            ICurrentUserService currentUserService,
            IFormAuthorisation formAuthorisation)
        {
            _plantEntryRequestService = service;
            _plantWorksRequestService = worksService;
            _authorityToWorkService = atwService;
            _isoService = isoService;
            _wahService = wahService;
            _cseService = cseService;
            _hvService = hvService;
            _expService = expService;
            _isoTemplateService = isoTemplateService;
            _fetchService = fetchService;
            _lockboxService = lockboxService;
            _personService = personService;
            _personSubcategoryService = personSubcategoryService;
            _trainingService = trainingService;
            _currentUserService = currentUserService;

            _mediator = mediator;
            _formAuthorisation = formAuthorisation;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/forms")]
        public async Task<PagedResult<IForm>> Get([FromQuery] int pageSize = 20, string token = null)
        {
            var predicates = new List<Expression<Func<IForm, bool>>>();

            var forms = await _fetchService.QueryPagedAsync(pageSize, token, null, predicates.ToArray());

            return forms;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/forms/find-by-friendly-id/{friendlyId}")]
        public async Task<IActionResult> FindFormByFriendlyId(string friendlyId)
        {
            var form = (await _fetchService.QueryAsync(x => x.FriendlyId == friendlyId)).FirstOrDefault();

            if (form == null)
            {
                return NotFound();
            }

            return Ok(form);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/forms/{formType}")]
        public async Task<PagedResult<IForm>> Get(
            [FromRoute] string formType,
            [FromQuery] int pageSize = 20,
            [FromQuery] string token = null,
            [FromQuery] string status = "any",
            [FromQuery] string friendlyId = null,
            [FromQuery(Name = "startDate[]")] DateTime?[] startDate = null,
            [FromQuery(Name = "endDate[]")] DateTime?[] endDate = null,
            [FromQuery] string permitAcceptor = "any",
            [FromQuery] string permitAuthoriser = "any",
            [FromQuery] string contractor = "any",
            [FromQuery] string tagNumber = null,
            [FromQuery] string lockbox = null,
            [FromQuery] string description = null,
            [FromQuery] string visitor = null,
            [FromQuery] string company = null,
            [FromQuery] string sortBy = "createdAt",
            [FromQuery] string orderBy = "descending")
        {


            if (formType == "authority-to-work")
            {
                var atwSort = GetOrderBy<AuthorityToWorkForm>(sortBy, orderBy);
                var atwPredicates = new List<Expression<Func<AuthorityToWorkForm, bool>>>();

                atwPredicates.Add(x => x.Kind.Slug == "authority-to-work");

                if (status != "any")
                {
                    atwPredicates.Add(x => x.Status == status);
                }
                else
                {
                    atwPredicates.Add(x => x.Status != FormState.CANCELLED);
                }

                if (startDate != null && startDate.Length == 2)
                {
                    atwPredicates.Add(x => x.StartDate >= startDate[0].Value.Date.ToUniversalTime() && x.StartDate < startDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (endDate != null && endDate.Length == 2)
                {
                    atwPredicates.Add(x => x.EndDate >= endDate[0].Value.Date.ToUniversalTime() && x.EndDate < endDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (friendlyId != null)
                {
                    atwPredicates.Add(x =>
                        x.FriendlyId == friendlyId.ToUpperInvariant() ||
                        x.FriendlyId.Contains(friendlyId.ToUpperInvariant()) ||
                        x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant()));
                }

                if (tagNumber != null)
                {
                    atwPredicates.Add(x =>
                        x.EquipmentNumber == tagNumber.ToUpperInvariant() ||
                        x.EquipmentNumber.Contains(tagNumber.ToUpperInvariant()) ||
                        x.EquipmentNumber.StartsWith(tagNumber.ToUpperInvariant()));
                }

                if (lockbox != null)
                {
                    atwPredicates.Add(x =>
                        x.Lockbox == lockbox.ToUpperInvariant() ||
                        x.Lockbox.Contains(lockbox.ToUpperInvariant()) ||
                        x.Lockbox.StartsWith(lockbox.ToUpperInvariant()));
                }

                if (description != null)
                {
                    var rgx = new Regex("[^a-zA-z0-9]");
                    var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                    atwPredicates.Add(x =>
                        x.CanonicalDescription.Contains(searchableDescription));
                }

                if (permitAcceptor != "any")
                {
                    atwPredicates.Add(x => x.CreatedBy.Id == permitAcceptor || x.Open.PermitAcceptor.User.Id == permitAcceptor || x.Close.PermitAcceptor.User.Id == permitAcceptor);
                }

                if (permitAuthoriser != "any")
                {
                    atwPredicates.Add(x => x.Open.PermitAuthoriser.User.Id == permitAuthoriser || x.Close.PermitAuthoriser.User.Id == permitAuthoriser);
                }

                var forms = await _authorityToWorkService.QueryPagedAsync(pageSize, token, atwSort, atwPredicates.ToArray());
                return forms;
            }
            else
            {
                var predicates = new List<Expression<Func<IForm, bool>>>();
                var sort = GetOrderBy<IForm>(sortBy, orderBy);

                if (visitor != null)
                {
                    visitor = visitor.Trim().ToLowerInvariant();
                    predicates.Add(x => ((PlantEntryRequestForm) x).Visitors.Any(y => y.Name.ToLower().Contains(visitor)));
                }

                if (company != null)
                {
                    company = company.Trim().ToLowerInvariant();
                    predicates.Add(x => ((PlantEntryRequestForm)x).Visitors.Any(y => y.Company.ToLower().Contains(company)));
                }

                if (!
                    (_currentUserService.UserInRole(Roles.ADMIN) ||
                    _currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) ||
                    _currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) ||
                    _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)
                    )
                )
                {
                    predicates.Add(x => x.CreatedBy.Id == _currentUserService.Id || x.AllowedUsers.Contains(_currentUserService.Id));
                }

                if (formType != "any")
                {
                    predicates.Add(x => x.Kind.Slug == formType);
                }
                else
                {
                    predicates.Add(x => x.Kind.Slug == "plant-works-request" || x.Kind.Slug == "plant-entry-request");
                }

                if (status != "any")
                {
                    predicates.Add(x => x.Status == status);
                }
                else
                {
                    predicates.Add(x => x.Status != FormState.CANCELLED && x.Status != FormState.COMPLETED);
                }

                if (description != null)
                {
                    var rgx = new Regex("[^a-zA-z0-9]");
                    var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                    predicates.Add(x =>
                        x.CanonicalDescription.Contains(searchableDescription));
                }

                if (startDate != null && startDate.Length == 2)
                {
                    var dateTime = startDate[0].Value;
                    var date = dateTime.Date;
                    var local = date.ToLocalTime();
                    var universal = date.ToUniversalTime();

                    predicates.Add(x => x.StartDate >= startDate[0].Value.Date.ToUniversalTime() && x.StartDate < startDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (endDate != null && endDate.Length == 2)
                {
                    predicates.Add(x => x.EndDate >= endDate[0].Value.Date.ToUniversalTime() && x.EndDate < endDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (friendlyId != null)
                {
                    predicates.Add(x =>
                        x.FriendlyId == friendlyId.ToUpperInvariant() ||
                        x.FriendlyId.Contains(friendlyId.ToUpperInvariant()) ||
                        x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant()));
                }

                var forms = await _fetchService.QueryPagedAsync(pageSize, token, sort, predicates.ToArray());

                return forms;

            }
        }

       

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/forms/{formType}/download")]
        [ResponseCache(Duration = 6000)]
        public async Task<IEnumerable<IForm>> Download(
            [FromRoute] string formType,
            [FromQuery] string status = "any",
            [FromQuery] string friendlyId = null,
            [FromQuery(Name = "startDate[]")] DateTime?[] startDate = null,
            [FromQuery(Name = "endDate[]")] DateTime?[] endDate = null,
            [FromQuery] string permitAcceptor = "any",
            [FromQuery] string permitAuthoriser = "any",
            [FromQuery] string contractor = "any",
            [FromQuery] string tagNumber = null,
            [FromQuery] string lockbox = null,
            [FromQuery] string description = null,
            [FromQuery] string visitor = null,
            [FromQuery] string company = null,
            [FromQuery] string sortBy = "createdAt",
            [FromQuery] string orderBy = "descending")
        {


            if (formType == "authority-to-work")
            {
                var atwSort = GetOrderBy<AuthorityToWorkForm>(sortBy, orderBy);
                var atwPredicates = new List<Expression<Func<AuthorityToWorkForm, bool>>>();

                atwPredicates.Add(x => x.Kind.Slug == "authority-to-work");

                if (status != "any")
                {
                    atwPredicates.Add(x => x.Status == status);
                }
                else
                {
                    atwPredicates.Add(x => x.Status != FormState.CANCELLED);
                }

                if (startDate != null && startDate.Length == 2)
                {
                    atwPredicates.Add(x => x.StartDate >= startDate[0].Value.Date.ToUniversalTime() && x.StartDate < startDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (endDate != null && endDate.Length == 2)
                {
                    atwPredicates.Add(x => x.EndDate >= endDate[0].Value.Date.ToUniversalTime() && x.EndDate < endDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (friendlyId != null)
                {
                    atwPredicates.Add(x =>
                        x.FriendlyId == friendlyId.ToUpperInvariant() ||
                        x.FriendlyId.Contains(friendlyId.ToUpperInvariant()) ||
                        x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant()));
                }

                if (tagNumber != null)
                {
                    atwPredicates.Add(x =>
                        x.EquipmentNumber == tagNumber.ToUpperInvariant() ||
                        x.EquipmentNumber.Contains(tagNumber.ToUpperInvariant()) ||
                        x.EquipmentNumber.StartsWith(tagNumber.ToUpperInvariant()));
                }

                if (lockbox != null)
                {
                    atwPredicates.Add(x =>
                        x.Lockbox == lockbox.ToUpperInvariant() ||
                        x.Lockbox.Contains(lockbox.ToUpperInvariant()) ||
                        x.Lockbox.StartsWith(lockbox.ToUpperInvariant()));
                }

                if (description != null)
                {
                    var rgx = new Regex("[^a-zA-z0-9]");
                    var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                    atwPredicates.Add(x =>
                        x.CanonicalDescription.Contains(searchableDescription));
                }

                if (permitAcceptor != "any")
                {
                    atwPredicates.Add(x => x.CreatedBy.Id == permitAcceptor || x.Open.PermitAcceptor.User.Id == permitAcceptor || x.Close.PermitAcceptor.User.Id == permitAcceptor);
                }

                if (permitAuthoriser != "any")
                {
                    atwPredicates.Add(x => x.Open.PermitAuthoriser.User.Id == permitAuthoriser || x.Close.PermitAuthoriser.User.Id == permitAuthoriser);
                }

                var forms = await _authorityToWorkService.QueryAsync(atwPredicates.ToArray());
                return forms;
            }
            else
            {

                var predicates = new List<Expression<Func<IForm, bool>>>();
                var sort = GetOrderBy<IForm>(sortBy, orderBy);

                if (!
                    (_currentUserService.UserInRole(Roles.ADMIN) ||
                    _currentUserService.UserInRole(Roles.ALLIANCE_MANAGER) ||
                    _currentUserService.UserInRole(Roles.OPERATIONS_MANAGER) ||
                    _currentUserService.UserInRole(Roles.MAINTENANCE_MANAGER)
                    )
                )
                {
                    predicates.Add(x => x.CreatedBy.Id == _currentUserService.Id);
                }

                if (formType != "any")
                {
                    predicates.Add(x => x.Kind.Slug == formType);
                }
                else
                {
                    predicates.Add(x => x.Kind.Slug == "plant-works-request" || x.Kind.Slug == "plant-entry-request");
                }

                if (status != "any")
                {
                    predicates.Add(x => x.Status == status);
                }
                else
                {
                    predicates.Add(x => x.Status != FormState.CANCELLED && x.Status != FormState.COMPLETED);
                }

                if (description != null)
                {
                    var rgx = new Regex("[^a-zA-z0-9]");
                    var searchableDescription = rgx.Replace(description, "")?.ToLowerInvariant();

                    predicates.Add(x =>
                        x.CanonicalDescription.Contains(searchableDescription));
                }

                if (startDate != null && startDate.Length == 2)
                {
                    var dateTime = startDate[0].Value;
                    var date = dateTime.Date;
                    var local = date.ToLocalTime();
                    var universal = date.ToUniversalTime();

                    predicates.Add(x => x.StartDate >= startDate[0].Value.Date.ToUniversalTime() && x.StartDate < startDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (endDate != null && endDate.Length == 2)
                {
                    predicates.Add(x => x.EndDate >= endDate[0].Value.Date.ToUniversalTime() && x.EndDate < endDate[1].Value.Date.ToUniversalTime().AddDays(1));
                }

                if (friendlyId != null)
                {
                    predicates.Add(x =>
                        x.FriendlyId == friendlyId.ToUpperInvariant() ||
                        x.FriendlyId.Contains(friendlyId.ToUpperInvariant()) ||
                        x.FriendlyId.StartsWith(friendlyId.ToUpperInvariant()));
                }

                var forms = await _fetchService.QueryAsync(predicates.ToArray());

                return forms;

            }
        }

        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        [HttpGet("/admin/forms/bulk-update/form-links")]
        public async Task<IActionResult> BulkUpdate()
        {

            var forms = await _fetchService.QueryAsync();
            foreach (var form in forms)
            {
                await AddReferenceIntoLinkedForms(form);
            }

            return Ok();
        }

        [Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
        [HttpGet("/admin/forms/bulk-update/lockboxes")]
        public async Task<IActionResult> BulkUpdateLockboxes()
        {

            var forms = await _authorityToWorkService.QueryAsync(x => x.Kind.Code == "ATW");
            foreach (var form in forms)
            {
                if (form.Lockbox != null)
                    continue;
                form.Lockbox = form.Permits.GetLockbox();
                await _authorityToWorkService.UpdateAsync(form);
                await Task.Delay(10);
            }

            return Ok();
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/admin/forms/lockboxes")]
        [ResponseCache(Duration = 60)]
        public async Task<IEnumerable<Lockbox>> Lockboxes()
        {
            var sql2 = $@"  SELECT
                                 c.friendlyId,
                                 c.id as formId,
                                 c.kind.slug as formSlug,
                                 c.lockbox as number,
                                 c.startDate,
                                 c.endDate,
                                 c['status']
                            FROM c
                            WHERE
                                c.type = 'form' and
                                c.status NOT IN('closed', 'cancelled') and
                                c.kind.code = 'ATW' and
                                c.lockbox != null and c.lockbox != ''
                            order by c.friendlyId";

            var sql = $@"
                            SELECT c.lockbox as number
                            FROM c
                            WHERE
                                c.type = 'form' and
                                c.status = 'opened' and
                                c.kind.code = 'ATW' and
                                c.lockbox != null and c.lockbox != ''
                        ";

            var lockboxes = await _lockboxService.SqlQueryAsync(sql2);

            return lockboxes;
        }



        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("/api/forms/{formType}/{formId}")]
        public async Task<IForm> GetForm(string formType, string formId)
        {
            IForm form = null;

            form = await _fetchService.FindAsync(formId);

            form.CanSubmit = _formAuthorisation.CanSubmit(form).Authorised;
            form.CanRequestAuthorisation = _formAuthorisation.CanRequestAuthorisation(form).Authorised;
            form.CanApprove = _formAuthorisation.CanApprove(form).Authorised;
            form.CanDeny = _formAuthorisation.CanDeny(form).Authorised;
            form.CanEdit = _formAuthorisation.CanEdit(form).Authorised;
            form.CanCancel = _formAuthorisation.CanCancel(form).Authorised;
            form.CanRequestInfo = _formAuthorisation.CanRequestMoreInformation(form).Authorised;
            form.CanComplete = _formAuthorisation.CanComplete(form).Authorised;

            form.CanOpen = _formAuthorisation.CanOpen(form).Authorised;
            form.CanClose = _formAuthorisation.CanClose(form).Authorised;

            form.CanRequestTransfer = _formAuthorisation.CanRequestTransfer(form).Authorised;
            form.CanAuthoriseTransfer = _formAuthorisation.CanAuthoriseTransfer(form).Authorised;

            form.CanRequestSuspension = _formAuthorisation.CanRequestSuspension(form).Authorised;
            form.CanAuthoriseSuspension = _formAuthorisation.CanAuthoriseSuspension(form).Authorised;

            form.CanRequestRevalidation = _formAuthorisation.CanRequestRevalidation(form).Authorised;
            form.CanAuthoriseRevalidation = _formAuthorisation.CanAuthoriseRevalidation(form).Authorised;

            form.CanRequestCompletion = _formAuthorisation.CanRequestCompletion(form).Authorised;
            form.CanAuthoriseCompletion = false;

            form.CanRequestTestRun = _formAuthorisation.CanRequestTestRun(form).Authorised;
            form.CanAuthoriseTestRun = false;

            form.CanRequestCancellation = _formAuthorisation.CanRequestCancellation(form).Authorised;
            form.CanAuthoriseCancellation = false;

            return form;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("/api/forms/plant-entry-request")]
        public async Task<IActionResult> Post([FromBody] PlantEntryRequestForm form)
        {
            PopulatePlantEntryRequestForm(form);
            await AddReferenceIntoLinkedForms(form);

            foreach (var visitor in form.Visitors)
            {
                if (String.IsNullOrWhiteSpace(visitor.Id))
                {
                    var existingPeople = await _personService.QueryAsync("visitor", x => x.CanonicalName.StartsWith(visitor.Name.ToLowerInvariant().Trim()));
                    if (existingPeople.Count() > 0)
                    {
                        var existingPerson = existingPeople.First();
                        visitor.Id = existingPerson.Id;
                    }
                    else
                    {
                        var person = new Person()
                        {
                            Name = visitor.Name,
                            Company = visitor.Company,
                            Category = visitor.Category,
                            ContactNumber = visitor.ContactNumber,
                            Email = visitor.Email
                        };

                        var newPerson = await _personService.AddAsync(person);
                        visitor.Id = newPerson.Id;
                    }
                }

                if (!string.IsNullOrWhiteSpace(visitor.Company))
                {
                    var existingCompany = await _personSubcategoryService.QueryAsync("person_subcategory", x => x.CanonicalName.StartsWith(visitor.Company.ToLowerInvariant().Trim()));
                    if (existingCompany.Count() == 0)
                    {
                        var subcategory = new PersonSubcategory()
                        {
                            Name = visitor.Company
                        };
                        await _personSubcategoryService.AddAsync(subcategory);
                    }

                };
            }

            try
            {
                await _plantEntryRequestService.InsertAsync(form);
                return Ok(form);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }

        private void PopulatePlantEntryRequestForm(PlantEntryRequestForm form)
        {
            form.CreatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };
            form.UpdatedBy = form.CreatedBy;
            form.Status = FormState.DRAFT;
            form.FriendlyId = form.GenerateFriendlyId();
            form.StartDate = form.Visitors.FirstOrDefault()?.VisitDuration.FirstOrDefault();
            form.EndDate = form.Visitors.FirstOrDefault()?.VisitDuration.LastOrDefault();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/plant-entry-request/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] PlantEntryRequestForm form)
        {
            var existingForm = await _plantEntryRequestService.FindAsync(id);
            var canEdit = _formAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            form.UpdatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };

            form.Revision += 1;
            form.StartDate = form.Visitors.FirstOrDefault()?.VisitDuration.FirstOrDefault();
            form.EndDate = form.Visitors.FirstOrDefault()?.VisitDuration.LastOrDefault();

            if (existingForm.In(FormState.APPROVED, FormState.PARTIALLY_APPROVED))
            {
                form.Status = FormState.DRAFT;
                foreach (var approver in form.Approvers)
                {
                    approver.IsApproved = null;
                    approver.ActionedAt = null;
                }
            }

            await AddReferenceIntoLinkedForms(form);

            foreach (var visitor in form.Visitors)
            {
                if (String.IsNullOrWhiteSpace(visitor.Id))
                {
                    var existingPeople = await _personService.QueryAsync("visitor", x => x.CanonicalName.StartsWith(visitor.Name.ToLowerInvariant().Trim()));
                    if (existingPeople.Count() > 0)
                    {
                        var existingPerson = existingPeople.First();
                        visitor.Id = existingPerson.Id;
                    }
                    else
                    {
                        var person = new Person()
                        {
                            Name = visitor.Name,
                            Company = visitor.Company,
                            Category = visitor.Category,
                            ContactNumber = visitor.ContactNumber,
                            Email = visitor.Email
                        };

                        var newPerson = await _personService.AddAsync(person);
                        visitor.Id = newPerson.Id;
                    }
                }

                if (!string.IsNullOrWhiteSpace(visitor.Company))
                {
                    var existingCompany = await _personSubcategoryService.QueryAsync("person_subcategory", x => x.CanonicalName.StartsWith(visitor.Company.ToLowerInvariant().Trim()));
                    if (existingCompany.Count() == 0)
                    {
                        var subcategory = new PersonSubcategory()
                        {
                            Name = visitor.Company
                        };
                        await _personSubcategoryService.AddAsync(subcategory);
                    }

                };

                var trainingTemplates = await _trainingService.QueryAsync("visitor_training");
                var training = new PersonTraining();

                if (!string.IsNullOrWhiteSpace(visitor.AccessCardNo))
                {
                    var person = await _personService.GetAsync(visitor.Id);

                    if (!person.CheckInduction(form))
                    {
                        var template = trainingTemplates.Single(x => x.TrainingType == "Induction" && x.Name == form.RequestType);

                        training = new PersonTraining()
                        {
                            Type = "Induction",
                            TrainingName = form.RequestType,
                            StartDate = form.StartDate.GetValueOrDefault(DateTime.Today),
                        };

                        SetExpiry(training, template);

                        person.AddTraining(training);
                    }

                    foreach (var awareness in form.AwarenessTraining)
                    {
                        if (!person.CheckAwareness(form, awareness))
                        {
                            var template = trainingTemplates.Single(x => x.TrainingType == "Awareness" && x.Name == awareness);

                            training = new PersonTraining()
                            {
                                Type = "Awareness",
                                TrainingName = awareness,
                                StartDate = form.StartDate.GetValueOrDefault(DateTime.Today),
                            };

                            person.AddTraining(training);
                        }
                    }

                    await _personService.UpdateAsync(person);
                }
            }

            await _plantEntryRequestService.UpdateAsync(form);

            return Ok(form);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("/api/forms/plant-works-request")]
        public async Task<IActionResult> Post([FromBody] PlantWorksRequestForm form)
        {
            PopulatePlantWorksRequestForm(form);

            await AddReferenceIntoLinkedForms(form);

            try
            {
                await _plantWorksRequestService.InsertAsync(form);
                return Ok(form);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }

        private void PopulatePlantWorksRequestForm(PlantWorksRequestForm form)
        {
            form.CreatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };
            form.UpdatedBy = form.CreatedBy;
            form.Status = FormState.DRAFT;
            form.FriendlyId = form.GenerateFriendlyId();
            form.StartDate = form.WorkDuration.FirstOrDefault();
            form.EndDate = form.WorkDuration.LastOrDefault();
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/plant-works-request/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] PlantWorksRequestForm form)
        {
            var existingForm = await _plantWorksRequestService.FindAsync(id);
            var canEdit = _formAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            form.UpdatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };

            form.Revision += 1;
            form.StartDate = form.WorkDuration.FirstOrDefault();
            form.EndDate = form.WorkDuration.LastOrDefault();

            if (existingForm.In(FormState.APPROVED, FormState.PARTIALLY_APPROVED))
            {
                form.Status = FormState.DRAFT;
                foreach (var approver in form.Approvers)
                {
                    approver.IsApproved = null;
                    approver.ActionedAt = null;
                }
            }

            await AddReferenceIntoLinkedForms(form);

            await _plantWorksRequestService.UpdateAsync(form);

            return Ok(form);
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("/api/forms/authority-to-work")]
        public async Task<IActionResult> Post([FromBody] AuthorityToWorkForm form)
        {
            await PopulateAuthorityToWorkForm(form);
            await AddReferenceIntoLinkedForms(form);

            try
            {
                await _fetchService.InsertAsync(form);
                await CreateOrUpdateIsolationPermit(form);
                await CreateOrUpdateWorkingAtHeightsPermit(form);
                await CreateOrUpdateConfinedSpaceEntryPermit(form);
                await CreateOrUpdateExcavationPermit(form);
                await CreateOrUpdateHighVoltagePermit(form);


                return Ok(form);

            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }

        private async Task CreateOrUpdateIsolationPermit(AuthorityToWorkForm form)
        {
            // Create ISO if necessary
            if (!form.Permits.Any(x => x.Id == "ISO" && x.Required))
            {
                return;
            }

            var permitId = form.Permits.Single(x => x.Id == "ISO").PermitId;
            var permit = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as IsolationPermit;

            if (permit == null)
            {
                var isoTemplate = (await _isoTemplateService.QueryTemplatesAsync(x => x.EquipmentNumber == form.EquipmentNumber)).FirstOrDefault();

                permit = new IsolationPermit()
                {
                    FriendlyId = permitId,
                    Status = form.Status,
                    CreatedBy = form.CreatedBy,
                    UpdatedBy = form.UpdatedBy,
                    WorksDescription = form.WorksDescription,
                    PhysicalLocation = form.PhysicalLocation,
                    Lockbox = form.Lockbox,
                    IsolationMethod = isoTemplate?.IsolationMethod ?? new List<IsolationPermit.IsolationPermitMethodStep>(),
                    Attachments = isoTemplate?.Attachments ?? new List<IsolationPermit.IsolationPermitAttachment>(),
                    AllowedUsers = form.AllowedUsers,
                    LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = form.FriendlyId,
                            }
                        },
                };

                await _isoService.InsertAsync(permit);
            }
            else
            {
                permit.Status = form.Status;
                permit.UpdatedBy = form.UpdatedBy;
                permit.WorksDescription = form.WorksDescription;
                permit.PhysicalLocation = form.PhysicalLocation;
                permit.Lockbox = form.Lockbox;
                permit.AllowedUsers = form.AllowedUsers;

                await _isoService.UpdateAsync(permit);
            }


        }

        private async Task CreateOrUpdateWorkingAtHeightsPermit(AuthorityToWorkForm form)
        {
            // Create WAH if necessary
            if (!form.Permits.Any(x => x.Id == "WAH" && x.Required))
            {
                return;
            }

            var permitId = form.Permits.Single(x => x.Id == "WAH").PermitId;
            var permit = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as WorkingAtHeightsPermit;

            if (permit == null)
            {

                permit = new WorkingAtHeightsPermit()
                {
                    FriendlyId = permitId,
                    Status = form.Status,
                    CreatedBy = form.CreatedBy,
                    UpdatedBy = form.UpdatedBy,
                    WorksDescription = form.WorksDescription,
                    PhysicalLocation = form.PhysicalLocation,
                    SubContractor = form.PrincipalContractor?.Name,
                    AllowedUsers = form.AllowedUsers,
                    LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = form.FriendlyId,
                            }
                        },
                };

                await _wahService.InsertAsync(permit);
            }
            else
            {
                permit.Status = form.Status;
                permit.UpdatedBy = form.UpdatedBy;
                permit.WorksDescription = form.WorksDescription;
                permit.PhysicalLocation = form.PhysicalLocation;
                permit.SubContractor = form.PrincipalContractor?.Name;
                permit.AllowedUsers = form.AllowedUsers;


                await _wahService.UpdateAsync(permit);
            }
        }

        private async Task CreateOrUpdateConfinedSpaceEntryPermit(AuthorityToWorkForm form)
        {
            // Create WAH if necessary
            if (!form.Permits.Any(x => x.Id == "CSE" && x.Required))
            {
                return;
            }

            var permitId = form.Permits.Single(x => x.Id == "CSE").PermitId;
            var permit = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as ConfinedSpaceEntryPermit;

            if (permit == null)
            {

                permit = new ConfinedSpaceEntryPermit()
                {
                    FriendlyId = permitId,
                    Status = form.Status,
                    CreatedBy = form.CreatedBy,
                    UpdatedBy = form.UpdatedBy,
                    StartDate = form.StartDate,
                    EndDate = form.EndDate,
                    WorksDescription = form.WorksDescription + " " + form.PhysicalLocation,
                    Company = form.PrincipalContractor?.Name,
                    RequestedBy = form.CreatedBy,
                    AllowedUsers = form.AllowedUsers,
                    LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = form.FriendlyId,
                            }
                        },
                };

                await _cseService.InsertAsync(permit);
            }
            else
            {
                permit.Status = form.Status;
                permit.UpdatedBy = form.UpdatedBy;
                permit.StartDate = form.StartDate;
                permit.EndDate = form.EndDate;
                permit.WorksDescription = form.WorksDescription + form + form.PhysicalLocation;
                permit.Company = form.PrincipalContractor?.Name;
                permit.AllowedUsers = form.AllowedUsers;


                await _cseService.UpdateAsync(permit);
            }
        }

        private async Task CreateOrUpdateExcavationPermit(AuthorityToWorkForm form)
        {
            // Create WAH if necessary
            if (!form.Permits.Any(x => x.Id == "EXP" && x.Required))
            {
                return;
            }

            var permitId = form.Permits.Single(x => x.Id == "EXP").PermitId;
            var permit = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as ExcavationPermit;

            if (permit == null)
            {

                permit = new ExcavationPermit()
                {
                    FriendlyId = permitId,
                    Status = form.Status,
                    CreatedBy = form.CreatedBy,
                    UpdatedBy = form.UpdatedBy,
                    StartDate = form.StartDate,
                    EndDate = form.EndDate,
                    RequestedBy = form.CreatedBy,
                    AllowedUsers = form.AllowedUsers,
                    LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = form.FriendlyId,
                            }
                        },
                };

                await _expService.InsertAsync(permit);
            }
            else
            {
                permit.Status = form.Status;
                permit.UpdatedBy = form.UpdatedBy;
                permit.StartDate = form.StartDate;
                permit.EndDate = form.EndDate;
                permit.AllowedUsers = form.AllowedUsers;


                await _expService.UpdateAsync(permit);
            }
        }

        private async Task CreateOrUpdateHighVoltagePermit(AuthorityToWorkForm form)
        {
            // Create WAH if necessary
            if (!form.Permits.Any(x => x.Id == "HV" && x.Required))
            {
                return;
            }

            var permitId = form.Permits.Single(x => x.Id == "HV").PermitId;
            var permit = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as HighVoltagePermit;

            if (permit == null)
            {

                permit = new HighVoltagePermit()
                {
                    FriendlyId = permitId,
                    Status = form.Status,
                    CreatedBy = form.CreatedBy,
                    UpdatedBy = form.UpdatedBy,
                    StartDate = form.StartDate,
                    EndDate = form.EndDate,
                    RequestedBy = form.CreatedBy,
                    WorksDescription = form.WorksDescription,
                    WorkLocation = form.PhysicalLocation,
                    AllowedUsers = form.AllowedUsers,
                    LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = form.FriendlyId,
                            }
                        },
                };

                await _hvService.InsertAsync(permit);
            }
            else
            {
                permit.Status = form.Status;
                permit.UpdatedBy = form.UpdatedBy;
                permit.StartDate = form.StartDate;
                permit.EndDate = form.EndDate;
                permit.WorksDescription = form.WorksDescription;
                permit.WorkLocation = form.PhysicalLocation;
                permit.AllowedUsers = form.AllowedUsers;


                await _hvService.UpdateAsync(permit);
            }
        }


        private async Task PopulateAuthorityToWorkForm(AuthorityToWorkForm form)
        {
            form.CreatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };
            form.UpdatedBy = form.CreatedBy;
            form.Status = FormState.DRAFT;

            //if (form.OnBehalfOf != null && !string.IsNullOrEmpty(form.OnBehalfOf.Name))
            //{
            //    form.PermitAcceptor = form.OnBehalfOf;
            //}

            var sqlQuery = $@"
                SELECT TOP 1 VALUE(udf.TO_INT(replace(c.friendlyId, 'ATW', '')))
                FROM c
                WHERE c.type = 'form'
                AND c.kind.slug = 'authority-to-work'
                AND c.createdAt >= '2018-01-01T00:00:00.0000Z' and c.createdAt < '2100-01-01:00:00:00.0000Z'
                ORDER BY c.friendlyId DESC
            ";

            var maxId = await _fetchService.SqlQueryAsync<string>(sqlQuery) ?? "ATW0000";
            form.FriendlyId = form.GetNextId(maxId);
            form.StartDate = form.WorkDuration.FirstOrDefault();
            form.EndDate = form.WorkDuration.LastOrDefault();

            form.Lockbox = form.Permits.GetLockbox();

            form.CreatedBy = new User()
            {
                Id = form.PermitAcceptor.Id,
                Name = form.PermitAcceptor.Name,
                Email = form.PermitAcceptor.Email,
            };

            form.Open.PermitAcceptor.User = form.CreatedBy;
            if (form.Open.PermitAuthoriser.User == null)
                form.Open.PermitAuthoriser.User = new User();

            if (form.Close.PermitAcceptor.User == null)
                form.Close.PermitAcceptor.User = new User();
            if (form.Close.PermitAuthoriser.User == null)
                form.Close.PermitAuthoriser.User = new User();

            if (form.IsolationMarkup.PermitAcceptor.User == null)
                form.IsolationMarkup.PermitAcceptor.User = new User();
            if (form.IsolationMarkup.PermitAuthoriser.User == null)
                form.IsolationMarkup.PermitAuthoriser.User = new User();

            if (form.Isolation.PermitAcceptor.User == null)
                form.Isolation.PermitAcceptor.User = new User();
            if (form.Isolation.PermitAuthoriser.User == null)
                form.Isolation.PermitAuthoriser.User = new User();

            if (form.Deisolation.PermitAcceptor.User == null)
                form.Deisolation.PermitAcceptor.User = new User();
            if (form.Deisolation.PermitAuthoriser.User == null)
                form.Deisolation.PermitAuthoriser.User = new User();

            if (form.Completions.Any())
            {
                form.Completions[0].PermitAcceptor.User = new User();
            }
            else
            {
                form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion()
                {
                    PermitAcceptor = new FormUserAction()
                    {
                        User = new Paperwork.Core.User(),
                    },
                    PermitAuthoriser = new FormUserAction()
                    {
                        User = new User(),
                    }
                });
            }

            var number = (Int32.Parse(maxId.Replace("ATW", "")) + 1).ToString().PadLeft(4, '0');

            foreach (var permit in form.Permits.Where(x => x.Generate))
            {
                permit.PermitId = $"{permit.Id}{number}";
                permit.Required = true;
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/authority-to-work/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] AuthorityToWorkForm form)
        {
            var existingForm = await _fetchService.FindAsync(id);
            var canEdit = _formAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            form.CreatedBy = new User()
            {
                Id = form.PermitAcceptor.Id,
                Name = form.PermitAcceptor.Name,
                Email = form.PermitAcceptor.Email,
            };


            form.UpdatedBy = new User()
            {
                Id = _currentUserService.Id,
                Name = _currentUserService.Name,
                Email = _currentUserService.Email
            };

            form.Revision++;
            form.StartDate = form.WorkDuration.FirstOrDefault();
            form.EndDate = form.WorkDuration.LastOrDefault();

            form.Lockbox = form.Permits.GetLockbox();

            form.Open.PermitAcceptor.User = form.CreatedBy;
            if (form.Open.PermitAuthoriser.User == null)
                form.Open.PermitAuthoriser.User = new User();

            //form.Close.PermitAcceptor.User = form.CreatedBy;
            if (form.Close.PermitAuthoriser.User == null)
                form.Close.PermitAuthoriser.User = new User();

            if (form.IsolationMarkup.PermitAcceptor.User == null)
                form.IsolationMarkup.PermitAcceptor.User = new User();
            if (form.IsolationMarkup.PermitAuthoriser.User == null)
                form.IsolationMarkup.PermitAuthoriser.User = new User();

            if (form.Isolation.PermitAcceptor.User == null)
                form.Isolation.PermitAcceptor.User = new User();
            if (form.Isolation.PermitAuthoriser.User == null)
                form.Isolation.PermitAuthoriser.User = new User();

            if (form.Deisolation.PermitAcceptor.User == null)
                form.Deisolation.PermitAcceptor.User = new User();
            if (form.Deisolation.PermitAuthoriser.User == null)
                form.Deisolation.PermitAuthoriser.User = new User();


            //if (form.Completions.Any())
            //{
            //    form.Completions[0].PermitAcceptor.User = form.CreatedBy;
            //}
            //else
            //{
            //    form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion()
            //    {
            //        PermitAcceptor = new FormUserAction()
            //        {
            //            User = form.CreatedBy,
            //        },
            //        PermitAuthoriser = new FormUserAction()
            //        {
            //            User = new User(),
            //        }
            //    });
            //}

            var number = (Int32.Parse(form.FriendlyId.Replace("ATW", ""))).ToString().PadLeft(4, '0');

            foreach (var permit in form.Permits.Where(x => !x.Generate && x.Required))
            {
                if (permit.Id == "JSEA") continue;

                if (permit.Id == "ISO")
                {
                    var permitId = permit.PermitId;
                    var isoForm = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as IsolationPermit;

                    isoForm.FriendlyId = null;
                    isoForm.LinkedForms = new List<FormLink>();
                    isoForm.Inactive = true;
                    isoForm.Status = FormState.CANCELLED;

                    await _isoService.UpdateAsync(isoForm);
                }

                if (permit.Id == "WAH")
                {
                    var permitId = permit.PermitId;
                    var wahForm = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as WorkingAtHeightsPermit;

                    wahForm.FriendlyId = null;
                    wahForm.LinkedForms = new List<FormLink>();
                    wahForm.Inactive = true;
                    wahForm.Status = FormState.CANCELLED;

                    await _wahService.UpdateAsync(wahForm);
                }

                if (permit.Id == "CSE")
                {
                    var permitId = permit.PermitId;
                    var wahForm = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as ConfinedSpaceEntryPermit;

                    wahForm.FriendlyId = null;
                    wahForm.LinkedForms = new List<FormLink>();
                    wahForm.Inactive = true;
                    wahForm.Status = FormState.CANCELLED;

                    await _cseService.UpdateAsync(wahForm);
                }

                if (permit.Id == "EXP")
                {
                    var permitId = permit.PermitId;
                    var wahForm = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as ExcavationPermit;

                    wahForm.FriendlyId = null;
                    wahForm.LinkedForms = new List<FormLink>();
                    wahForm.Inactive = true;
                    wahForm.Status = FormState.CANCELLED;

                    await _expService.UpdateAsync(wahForm);
                }

                if (permit.Id == "HV")
                {
                    var permitId = permit.PermitId;
                    var wahForm = (await _fetchService.QueryAsync(x => x.FriendlyId == permitId)).FirstOrDefault() as HighVoltagePermit;

                    wahForm.FriendlyId = null;
                    wahForm.LinkedForms = new List<FormLink>();
                    wahForm.Inactive = true;
                    wahForm.Status = FormState.CANCELLED;

                    await _hvService.UpdateAsync(wahForm);
                }

                permit.Required = false;
                permit.PermitId = null;
            }

            foreach (var permit in form.Permits.Where(x => x.Generate && string.IsNullOrWhiteSpace(x.PermitId)))
            {
                permit.PermitId = $"{permit.Id}{number}";
                permit.Required = true;
            }

            await AddReferenceIntoLinkedForms(form);
            try
            {
                await _fetchService.UpdateAsync(form);

                await CreateOrUpdateIsolationPermit(form);
                await CreateOrUpdateWorkingAtHeightsPermit(form);
                await CreateOrUpdateConfinedSpaceEntryPermit(form);
                await CreateOrUpdateExcavationPermit(form);
                await CreateOrUpdateHighVoltagePermit(form);

                return Ok(form);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }



        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("/api/forms/authority-to-work/{id}/clone")]
        public async Task<IActionResult> Clone(string id, [FromBody] FormClone formClone)
        {

            var existingForm = await _authorityToWorkService.FindAsync(id);
            existingForm.Id = null;
            existingForm.CreatedAt = DateTime.UtcNow;
            existingForm.UpdatedAt = DateTime.UtcNow;
            existingForm.WorkDuration = formClone.WorkDuration;
            existingForm.Open.PermitAuthoriser = new FormUserAction();
            existingForm.Open.PermitAcceptor.ActionedAt = null;
            existingForm.Open.PermitAcceptor.IsApproved = false;
            existingForm.Open.PermitAcceptor.User.Id = _currentUserService.Id;
            existingForm.Open.PermitAcceptor.User.Email = _currentUserService.Email;
            existingForm.Open.PermitAcceptor.User.Name = _currentUserService.Name;
            existingForm.PermitAcceptor.Email = _currentUserService.Email;
            existingForm.PermitAcceptor.Id = _currentUserService.Id;
            existingForm.PermitAcceptor.Name = _currentUserService.Name;
            existingForm.PermitAcceptor.Number = "";
            existingForm.Isolation = new AuthorityToWorkForm.AuthorityToWorkIsolation();
            existingForm.IsolationMarkup = new AuthorityToWorkForm.AuthorityToWorkIsolation();
            existingForm.Deisolation = new AuthorityToWorkForm.AuthorityToWorkIsolation();
            existingForm.Close = new AuthorityToWorkForm.AuthorityToWorkFormClose();
            existingForm.Completions.Clear();
            existingForm.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion()
            {
                PermitAcceptor = new FormUserAction()
                {
                    User = new Paperwork.Core.User(),
                },
                PermitAuthoriser = new FormUserAction()
                {
                    User = new User(),
                }
            });
            existingForm.Lockbox = null;
            var lockbox = existingForm.Permits?.FirstOrDefault(x => x.Id == "ISO")?.Extras?.FirstOrDefault(x => x.Name == "Lockbox Number");
            lockbox.Value = null;

            var perId = "";
            var pwrId = "";
            var existingIsoId = existingForm.Permits?.FirstOrDefault(x => x.Id == "ISO")?.PermitId;
            var existingWahId = existingForm.Permits?.FirstOrDefault(x => x.Id == "WAH")?.PermitId;


            try
            {
                if (formClone.LinkedForms != null && formClone.LinkedForms.Any())
                {
                    foreach (var linkedForm in formClone.LinkedForms?.Where(x => x.Clone))
                    {
                        switch (linkedForm.Id)
                        {
                            case "PER" when !string.IsNullOrEmpty(linkedForm.FormId):
                                {
                                    var existingLinkedForm = (await _plantEntryRequestService.QueryAsync(x => x.FriendlyId == linkedForm.FormId)).FirstOrDefault();
                                    existingLinkedForm.Id = null;
                                    existingLinkedForm.CreatedAt = DateTime.UtcNow;
                                    existingLinkedForm.UpdatedAt = DateTime.UtcNow;

                                    foreach (var visitor in existingLinkedForm.Visitors)
                                    {
                                        visitor.AccessCardNo = null;
                                        visitor.VisitDuration = formClone.WorkDuration;
                                    }
                                    foreach (var link in existingLinkedForm.LinkedForms)
                                    {
                                        link.FormId = "";
                                    }

                                    existingLinkedForm.Approvers.ForEach(x =>
                                    {
                                        x.IsApproved = null;
                                    });
                                    PopulatePlantEntryRequestForm(existingLinkedForm);

                                    try
                                    {
                                        var newPer = await _fetchService.InsertAsync(existingLinkedForm);
                                        perId = newPer.FriendlyId;
                                    }
                                    catch (Exception ex)
                                    {
                                        return BadRequest(existingForm);
                                    }

                                    break;
                                }

                            case "PER":
                                {
                                    var newForm = new PlantEntryRequestForm();
                                    newForm.CreatedBy = existingForm.CreatedBy;
                                    newForm.UpdatedBy = existingForm.UpdatedBy;
                                    newForm.RequestReason = existingForm.WorksDescription;
                                    newForm.CreatedAt = DateTime.UtcNow;
                                    newForm.UpdatedAt = DateTime.UtcNow;

                                    newForm.Visitors.Add(new PlantEntryRequestForm.PlantEntryRequestFormVisitor()
                                    {
                                        VisitDuration = existingForm.WorkDuration
                                    });

                                    newForm.Approvers = new List<FormApproval>()
                            {
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_operations_manager",
                                      Name = "Operations Manager"
                                    },
                                    User =  null
                                },
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_maintenance_manager",
                                      Name = "Maintenance Manager"
                                    },
                                    User =  null
                                },
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_alliance_manager",
                                      Name = "Alliance Manager"
                                    },
                                    User =  null
                                }
                            };

                                    PopulatePlantEntryRequestForm(newForm);

                                    try
                                    {
                                        var newPer = await _fetchService.InsertAsync(newForm);
                                        perId = newPer.FriendlyId;
                                    }
                                    catch (Exception ex)
                                    {
                                        return BadRequest(existingForm);
                                    }

                                    break;
                                }
   
                            case "PWR" when !string.IsNullOrEmpty(linkedForm.FormId):
                                {
                                    var existingLinkedForm = (await _plantWorksRequestService.QueryAsync(x => x.FriendlyId == linkedForm.FormId)).FirstOrDefault();

                                    existingLinkedForm.Id = null;
                                    existingLinkedForm.CreatedAt = DateTime.UtcNow;
                                    existingLinkedForm.UpdatedAt = DateTime.UtcNow;
                                    existingLinkedForm.WorkDuration = formClone.WorkDuration;

                                    foreach (var link in existingLinkedForm.LinkedForms)
                                    {
                                        link.FormId = "";
                                    }

                                    existingLinkedForm.Approvers.ForEach(x =>
                                    {
                                        x.IsApproved = null;
                                    });

                                    PopulatePlantWorksRequestForm(existingLinkedForm);

                                    try
                                    {
                                        var newPwr = await _fetchService.InsertAsync(existingLinkedForm);
                                        pwrId = newPwr.FriendlyId;
                                    }
                                    catch (Exception ex)
                                    {
                                        return BadRequest(existingForm);
                                    }

                                    break;
                                }

                            case "PWR":
                                {
                                    var newForm = new PlantWorksRequestForm();
                                    newForm.CreatedBy = existingForm.CreatedBy;
                                    newForm.UpdatedBy = existingForm.UpdatedBy;
                                    newForm.RequestReason = existingForm.WorksDescription;
                                    newForm.WorksDescription = existingForm.WorksDescription;
                                    newForm.WorkDuration = existingForm.WorkDuration;
                                    newForm.CreatedAt = DateTime.UtcNow;
                                    newForm.UpdatedAt = DateTime.UtcNow;

                                    newForm.Approvers = new List<FormApproval>()
                            {
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_operations_manager",
                                      Name = "Operations Manager"
                                    },
                                    User =  null
                                },
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_maintenance_manager",
                                      Name = "Maintenance Manager"
                                    },
                                    User =  null
                                },
                                new FormApproval() {
                                    Role = new FormApproval.FormApprovalRole() {
                                      Id =  "paperwork_alliance_manager",
                                      Name = "Alliance Manager"
                                    },
                                    User =  null
                                }
                            };

                                    PopulatePlantWorksRequestForm(newForm);

                                    try
                                    {
                                        var newPwr = await _fetchService.InsertAsync(newForm);
                                        pwrId = newPwr.FriendlyId;
                                    }
                                    catch (Exception ex)
                                    {
                                        return BadRequest(existingForm);
                                    }

                                    break;
                                }
                        }
                    }

                }

                if (existingForm.LinkedForms != null)
                {
                    foreach (var link in existingForm.LinkedForms)
                    {
                        if (link.Id == "PER")
                        {
                            link.FormId = perId;
                        }

                        if (link.Id == "PWR")
                        {
                            link.FormId = pwrId;
                        }
                    }
                }

                await PopulateAuthorityToWorkForm(existingForm);
                await AddReferenceIntoLinkedForms(existingForm);
                await _fetchService.InsertAsync(existingForm);

                if (!string.IsNullOrWhiteSpace(existingIsoId))
                {
                    var isoForm = (await _fetchService.QueryAsync(x => x.FriendlyId == existingIsoId)).FirstOrDefault() as IsolationPermit;
                    var newPermitId = existingForm.Permits.FirstOrDefault(x => x.Id == "ISO")?.PermitId;
                    if (isoForm == null)
                    {
                        isoForm = new IsolationPermit()
                        {
                            FriendlyId = newPermitId,
                            Status = existingForm.Status,
                            CreatedBy = existingForm.CreatedBy,
                            UpdatedBy = existingForm.UpdatedBy,
                            WorksDescription = existingForm.WorksDescription,
                            PhysicalLocation = existingForm.PhysicalLocation,
                            Lockbox = existingForm.Lockbox,
                            LinkedForms = new List<FormLink>()
                            {
                                new FormLink()
                                {
                                    Id = "ATW",
                                    FormId = existingForm.FriendlyId,
                                }
                            },
                        };
                    }
                    else
                    {
                        isoForm.FriendlyId = newPermitId;
                        isoForm.Status = existingForm.Status;
                        isoForm.CreatedBy = existingForm.CreatedBy;
                        isoForm.UpdatedBy = existingForm.UpdatedBy;
                        isoForm.WorksDescription = existingForm.WorksDescription;
                        isoForm.PhysicalLocation = existingForm.PhysicalLocation;
                        isoForm.Lockbox = existingForm.Lockbox;
                        isoForm.LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = existingForm.FriendlyId,
                            }
                        };
                    }

                    await _isoService.InsertAsync(isoForm);
                }

                if (!string.IsNullOrWhiteSpace(existingWahId))
                {
                    var wahForm = (await _fetchService.QueryAsync(x => x.FriendlyId == existingIsoId)).FirstOrDefault() as WorkingAtHeightsPermit;
                    var newPermitId = existingForm.Permits.FirstOrDefault(x => x.Id == "WAH")?.PermitId;
                    if (wahForm == null)
                    {
                        wahForm = new WorkingAtHeightsPermit()
                        {
                            FriendlyId = newPermitId,
                            Status = existingForm.Status,
                            CreatedBy = existingForm.CreatedBy,
                            UpdatedBy = existingForm.UpdatedBy,
                            WorksDescription = existingForm.WorksDescription,
                            PhysicalLocation = existingForm.PhysicalLocation,
                            LinkedForms = new List<FormLink>()
                            {
                                new FormLink()
                                {
                                    Id = "ATW",
                                    FormId = existingForm.FriendlyId,
                                }
                            },
                        };
                    }
                    else
                    {
                        wahForm.FriendlyId = newPermitId;
                        wahForm.Status = existingForm.Status;
                        wahForm.CreatedBy = existingForm.CreatedBy;
                        wahForm.UpdatedBy = existingForm.UpdatedBy;
                        wahForm.WorksDescription = existingForm.WorksDescription;
                        wahForm.PhysicalLocation = existingForm.PhysicalLocation;
                        wahForm.LinkedForms = new List<FormLink>()
                        {
                            new FormLink()
                            {
                                Id = "ATW",
                                FormId = existingForm.FriendlyId,
                            }
                        };
                    }

                    await _wahService.InsertAsync(wahForm);
                }


                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(existingForm);
            }

        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/{formType}/{formId}/status/{status}")]
        public async Task<IActionResult> UpdateStatus(string formType, string formId, string status, [FromBody] dynamic message)
        {
            if (formType == "plant-entry-request")
            {
                var form = await _plantEntryRequestService.FindAsync(formId);

                if (status == "submit")
                {
                    var validation = _formAuthorisation.CanSubmit(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers)
                        {
                            approver.IsApproved = null;
                            approver.ActionedAt = null;
                            approver.Message = null;
                        }

                        form.Status = FormState.PENDING_APPROVAL;
                        try
                        {
                            await _plantEntryRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormSubmittedEvent(form));
                            return Ok(new { Message = "Form submitted for approval." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "approve")
                {
                    var validation = _formAuthorisation.CanApprove(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = true;
                            approver.ActionedAt = DateTime.UtcNow;
                        }


                        var mgrApproval = form.Approvers
                                                   .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER)
                                                   .All(x => x.IsApproved.GetValueOrDefault(false));
                        var opsOrMntApproval = form.Approvers
                                                   .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                                   .Any(x => x.IsApproved.GetValueOrDefault(false));

                        if (opsOrMntApproval && form.Status == FormState.PENDING_APPROVAL)
                        {
                            form.Status = FormState.PARTIALLY_APPROVED;
                            await _plantEntryRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormPartiallyApprovedEvent(form));
                        }

                        if (mgrApproval && form.Status == FormState.PARTIALLY_APPROVED)
                        {
                            form.Status = FormState.APPROVED;
                            await _plantEntryRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormApprovedEvent(form));
                        }
                        else if (mgrApproval && opsOrMntApproval)
                        {
                            form.Status = FormState.APPROVED;
                            await _plantEntryRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormApprovedEvent(form));
                        }

                        return Ok(new { Message = "Form approved." });
                    }
                }

                if (status == "deny")
                {
                    var validation = _formAuthorisation.CanDeny(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = false;
                            approver.ActionedAt = DateTime.UtcNow;
                            approver.Message = message.message;
                        }

                        form.Status = FormState.DENIED;
                        await _plantEntryRequestService.UpdateAsync(form);
                        await _mediator.Publish(new FormDeniedEvent(form));
                        return Ok(new { Message = "Form denied." });
                    }
                }


                if (status == "more information")
                {
                    var validation = _formAuthorisation.CanRequestMoreInformation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = false;
                            approver.ActionedAt = DateTime.UtcNow;
                            approver.Message = message.message;
                        }

                        form.Status = FormState.INFORMATION_REQUIRED;
                        await _plantEntryRequestService.UpdateAsync(form);
                        await _mediator.Publish(new FormRequiresInformationEvent(form));
                        return Ok(new { Message = "Form requested for more information." });
                    }
                }


                if (status == "cancel")
                {
                    var validation = _formAuthorisation.CanCancel(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers)
                        {
                            approver.IsApproved = null;
                            approver.ActionedAt = null;
                            approver.Message = null;
                        }

                        form.Status = FormState.CANCELLED;
                        await _plantEntryRequestService.UpdateAsync(form);
                        //await _mediator.Publish(new FormDeniedEvent(form));
                        return Ok(new { Message = "Form cancelled." });
                    }
                }

                if (status == "complete")
                {
                    var validation = _formAuthorisation.CanComplete(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {

                        form.Status = FormState.COMPLETED;
                        await _plantEntryRequestService.UpdateAsync(form);
                        return Ok(new { Message = "Form closed off." });
                    }
                }

                return NotFound();
            }
            else if (formType == "plant-works-request")
            {
                var form = await _plantWorksRequestService.FindAsync(formId);

                if (status == "submit")
                {
                    var validation = _formAuthorisation.CanSubmit(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers)
                        {
                            approver.IsApproved = null;
                            approver.ActionedAt = null;
                            approver.Message = null;
                        }

                        form.Status = FormState.PENDING_APPROVAL;
                        try
                        {
                            await _plantWorksRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormSubmittedEvent(form));
                            return Ok(new { Message = "Form submitted for approval." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "approve")
                {
                    var validation = _formAuthorisation.CanApprove(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = true;
                            approver.ActionedAt = DateTime.UtcNow;
                        }

                        var mgrApproval = form.Approvers
                                                  .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER)
                                                  .All(x => x.IsApproved.GetValueOrDefault(false));
                        var opsAndMntApproval = form.Approvers
                                                   .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                                   .All(x => x.IsApproved.GetValueOrDefault(false));

                        if (opsAndMntApproval && form.Status == FormState.PENDING_APPROVAL)
                        {
                            form.Status = FormState.PARTIALLY_APPROVED;
                            await _plantWorksRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormPartiallyApprovedEvent(form));
                        }

                        if (mgrApproval && form.Status == FormState.PARTIALLY_APPROVED)
                        {
                            form.Status = FormState.APPROVED;
                            await _plantWorksRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormApprovedEvent(form));
                        }
                        else if (mgrApproval && opsAndMntApproval)
                        {
                            form.Status = FormState.APPROVED;
                            await _plantWorksRequestService.UpdateAsync(form);
                            await _mediator.Publish(new FormApprovedEvent(form));
                        }
                        else
                        {
                            await _plantWorksRequestService.UpdateAsync(form);
                        }

                        return Ok(new { Message = "Form approved." });
                    }
                }

                if (status == "deny")
                {
                    var validation = _formAuthorisation.CanDeny(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = false;
                            approver.ActionedAt = DateTime.UtcNow;
                            approver.Message = message.message;
                        }

                        form.Status = FormState.DENIED;
                        await _plantWorksRequestService.UpdateAsync(form);
                        await _mediator.Publish(new FormDeniedEvent(form));
                        return Ok(new { Message = "Form denied." });
                    }
                }

                if (status == "more information")
                {
                    var validation = _formAuthorisation.CanRequestMoreInformation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers
                            .Where(x => x.User?.Id == _currentUserService.Id))
                        {
                            approver.IsApproved = false;
                            approver.ActionedAt = DateTime.UtcNow;
                            approver.Message = message.message;
                        }

                        form.Status = FormState.INFORMATION_REQUIRED;
                        await _plantWorksRequestService.UpdateAsync(form);
                        await _mediator.Publish(new FormRequiresInformationEvent(form));
                        return Ok(new { Message = "Form requested for more information." });
                    }
                }


                if (status == "cancel")
                {
                    var validation = _formAuthorisation.CanCancel(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers)
                        {
                            approver.IsApproved = null;
                            approver.ActionedAt = null;
                            approver.Message = null;
                        }

                        form.Status = FormState.CANCELLED;
                        await _plantWorksRequestService.UpdateAsync(form);
                        //await _mediator.Publish(new FormDeniedEvent(form));
                        return Ok(new { Message = "Form cancelled." });
                    }
                }

                if (status == "complete")
                {
                    var validation = _formAuthorisation.CanComplete(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {

                        form.Status = FormState.COMPLETED;
                        await _plantWorksRequestService.UpdateAsync(form);
                        return Ok(new { Message = "Form closed off." });
                    }
                }

                return NotFound();
            }
            else if (formType == "authority-to-work")
            {
                var form = await _authorityToWorkService.FindAsync(formId);

                if (status == "request authorisation")
                {
                    var validation = _formAuthorisation.CanRequestAuthorisation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        foreach (var approver in form.Approvers)
                        {
                            approver.IsApproved = null;
                            approver.ActionedAt = null;
                            approver.Message = null;
                        }

                        form.Status = FormState.PENDING_AUTHORISATION;
                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormPendingAuthorisationEvent(form));
                            return Ok(new { Message = "Form submitted for authorisation." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "approve")
                {
                    var validation = _formAuthorisation.CanApprove(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.OPENED;

                        if (message.isolatedBy != null && !string.IsNullOrWhiteSpace(message.isolatedBy.ToString()))
                        {
                            var user = message.isolatedBy;
                            form.Isolation.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }

                        if (message.deisolatedBy != null && !string.IsNullOrWhiteSpace(message.deisolatedBy.ToString()))
                        {
                            var user = message.deisolatedBy;
                            form.Deisolation.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }

                        if (message.isolationMarkupCompletedBy != null && !string.IsNullOrWhiteSpace(message.isolationMarkupCompletedBy.ToString()))
                        {
                            var user = message.isolationMarkupCompletedBy;
                            form.IsolationMarkup.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }


                        if (message.extra != null && !string.IsNullOrWhiteSpace(message.extra.ToString()))
                        {
                            var extra = message.extra;
                            var isoPermit = form.Permits.FirstOrDefault(x => x.Id == "ISO");
                            if (isoPermit != null)
                            {
                                var lockbox = isoPermit.Extras.FirstOrDefault(x => x.Name == "Lockbox Number");
                                if (lockbox != null)
                                {
                                    lockbox.Value = extra.value;
                                    form.Lockbox = extra.value;
                                }
                            }
                        }

                        if (form.Open == null)
                        {
                            form.Open = new AuthorityToWorkForm.AuthorityToWorkFormOpen();
                        }

                        if (form.Open.PermitAcceptor != null)
                        {
                            form.Open.PermitAcceptor.ActionedAt = DateTime.Now;
                            form.Open.PermitAcceptor.IsApproved = true;
                            form.Open.PermitAcceptor.Message = message.message;
                            form.Open.PermitAcceptor.Status = FormState.OPENED;
                        }
                        else
                        {
                            form.Open.PermitAcceptor = new FormUserAction()
                            {
                                ActionedAt = DateTime.UtcNow,
                                IsApproved = true,
                                Message = message.message,
                                Status = FormState.OPENED,
                                User = form.CreatedBy
                            };
                        }


                        form.Open.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.OPENED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {

                            await _authorityToWorkService.UpdateAsync(form);
                            await CreateOrUpdateIsolationPermit(form);
                            await CreateOrUpdateWorkingAtHeightsPermit(form);
                            await CreateOrUpdateConfinedSpaceEntryPermit(form);
                            await CreateOrUpdateExcavationPermit(form);
                            await CreateOrUpdateHighVoltagePermit(form);

                            await _mediator.Publish(new FormApprovedEvent(form));
                            await _mediator.Publish(new FormOpenedEvent(form));

                            return Ok(new { Message = "Form approved & opened." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }

                    }
                }

                if (status == "deny")
                {
                    var validation = _formAuthorisation.CanDeny(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {

                        form.Status = FormState.DENIED;

                        if (form.Open == null)
                        {
                            form.Open = new AuthorityToWorkForm.AuthorityToWorkFormOpen();
                        }

                        form.Open.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.DENIED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormDeniedEvent(form));
                        return Ok(new { Message = "Form denied." });
                    }
                }

                if (status == "more information")
                {
                    var validation = _formAuthorisation.CanRequestMoreInformation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.INFORMATION_REQUIRED;

                        if (form.Open == null)
                        {
                            form.Open = new AuthorityToWorkForm.AuthorityToWorkFormOpen();
                        }

                        form.Open.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.INFORMATION_REQUIRED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormRequiresInformationEvent(form));
                        return Ok(new { Message = "Form requested for more information." });
                    }
                }

                if (status == "request completion")
                {
                    var validation = _formAuthorisation.CanRequestCompletion(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.COMPLETED;
                        form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion());
                        var record = form.Completions.Last();

                        record.PermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.COMPLETED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormCompletedEvent(form));
                            return Ok(new { Message = "Form submitted for completion." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise completion")
                {
                    var validation = _formAuthorisation.CanAuthoriseCompletion(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.COMPLETED;
                        var record = form.Completions.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.COMPLETED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormCompletedEvent(form));

                        return Ok(new { Message = "Form completed." });
                    }
                }

                if (status == "request test run")
                {
                    var validation = _formAuthorisation.CanRequestTestRun(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.TESTING;
                        form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion());
                        var record = form.Completions.Last();

                        record.PermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.TESTING,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormTestRunningEvent(form));
                            return Ok(new { Message = "Form submitted for test run." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise test run")
                {
                    var validation = _formAuthorisation.CanAuthoriseTestRun(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.TESTING;
                        var record = form.Completions.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.TESTING,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormTestRunningEvent(form));

                        return Ok(new { Message = "Form in testing." });
                    }
                }

                if (status == "request cancellation")
                {
                    var validation = _formAuthorisation.CanRequestCancellation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.CANCELLED;
                        form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion());
                        var record = form.Completions.Last();

                        record.PermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.CANCELLED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormCancelledEvent(form));
                            return Ok(new { Message = "Form submitted for cancellation." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise cancellation")
                {
                    var validation = _formAuthorisation.CanAuthoriseCancellation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.CANCELLED;
                        var record = form.Completions.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.CANCELLED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormCancelledEvent(form));

                        return Ok(new { Message = "Form cancelled." });
                    }
                }

                if (status == "request transfer")
                {
                    var validation = _formAuthorisation.CanRequestTransfer(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {

                        form.Status = FormState.PENDING_TRANSFER;
                        form.Transfers.Add(new AuthorityToWorkForm.AuthorityToWorkFormTransfer());
                        var record = form.Transfers.Last();

                        record.OriginalPermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.PENDING_TRANSFER,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };


                        record.NewPermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.PENDING_TRANSFER,
                            User = new User()
                            {
                                Email = message.newPermitAcceptor.email,
                                Id = message.newPermitAcceptor.id,
                                Name = message.newPermitAcceptor.name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormPendingTransferEvent(form));
                            return Ok(new { Message = "Form submitted for transfer." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise transfer")
                {
                    var validation = _formAuthorisation.CanAuthoriseTransfer(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.TRANSFERRED;

                        var record = form.Transfers.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.TRANSFERRED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        form.CreatedBy = record.NewPermitAcceptor.User;
                        form.PermitAcceptor = new AuthorityToWorkForm.AuthorityToWorkPerson()
                        {
                            Email = record.NewPermitAcceptor.User.Email,
                            Name = record.NewPermitAcceptor.User.Name
                        };


                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormTransferredEvent(form));
                            return Ok(new { Message = "Form transferred." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "request suspension")
                {
                    var validation = _formAuthorisation.CanRequestSuspension(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {

                        form.Status = FormState.PENDING_SUSPENSION;
                        form.Suspensions.Add(new AuthorityToWorkForm.AuthorityToWorkFormSuspension());
                        var record = form.Suspensions.Last();

                        record.PermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.PENDING_SUSPENSION,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormPendingSuspensionEvent(form));
                            return Ok(new { Message = "Form submitted for suspension." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise suspension")
                {
                    var validation = _formAuthorisation.CanAuthoriseSuspension(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.SUSPENDED;
                        var record = form.Suspensions.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.SUSPENDED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        await _authorityToWorkService.UpdateAsync(form);
                        await _mediator.Publish(new FormSuspendedEvent(form));

                        return Ok(new { Message = "Form suspended." });
                    }
                }

                if (status == "request revalidation")
                {
                    var validation = _formAuthorisation.CanRequestRevalidation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.PENDING_REVALIDATION;
                        form.Revalidations.Add(new AuthorityToWorkForm.AuthorityToWorkFormRevalidation());
                        var record = form.Revalidations.Last();

                        record.PermitAcceptor = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.PENDING_REVALIDATION,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormPendingRevalidationEvent(form));
                            return Ok(new { Message = "Form submitted for revalidation." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "authorise revalidation")
                {
                    var validation = _formAuthorisation.CanAuthoriseRevalidation(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.OPENED;
                        var record = form.Revalidations.Last();

                        record.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.OPENED,
                            User = new User()
                            {
                                Email = _currentUserService.Email,
                                Id = _currentUserService.Id,
                                Name = _currentUserService.Name
                            }
                        };

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormOpenedEvent(form));
                            return Ok(new { Message = "Form submitted revalidated." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "open")
                {
                    var validation = _formAuthorisation.CanOpen(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {
                        form.Status = FormState.OPENED;

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormOpenedEvent(form));
                            return Ok(new { Message = "Form opened." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                if (status == "close")
                {
                    var validation = _formAuthorisation.CanClose(form);

                    if (!validation.Authorised)
                    {
                        return BadRequest(new { Message = validation.Message });
                    }
                    else
                    {


                        form.Status = FormState.CLOSED;

                        if (message.isolatedBy != null && !string.IsNullOrWhiteSpace(message.isolatedBy.ToString()) && !string.IsNullOrWhiteSpace(message.isolatedBy.id.ToString()))
                        {
                            var user = message.isolatedBy;
                            form.Isolation.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }

                        if (message.deisolatedBy != null && !string.IsNullOrWhiteSpace(message.deisolatedBy.ToString()) && !string.IsNullOrWhiteSpace(message.deisolatedBy.id.ToString()))
                        {
                            var user = message.deisolatedBy;
                            form.Deisolation.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }

                        if (message.isolationMarkupCompletedBy != null && !string.IsNullOrWhiteSpace(message.isolationMarkupCompletedBy.ToString()) && !string.IsNullOrWhiteSpace(message.isolationMarkupCompletedBy.id.ToString()))
                        {
                            var user = message.isolationMarkupCompletedBy;
                            form.IsolationMarkup.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }

                        if (form.Close == null)
                        {
                            form.Close = new AuthorityToWorkForm.AuthorityToWorkFormClose();
                        }

                        form.Close.PermitAuthoriser = new FormUserAction()
                        {
                            ActionedAt = DateTime.UtcNow,
                            IsApproved = true,
                            Message = message.message,
                            Status = FormState.CLOSED
                        };

                        if (message.permitAuthoriser != null && !string.IsNullOrWhiteSpace(message.permitAuthoriser.ToString()) && !string.IsNullOrWhiteSpace(message.permitAuthoriser.id.ToString()))
                        {
                            var user = message.permitAuthoriser;
                            form.Close.PermitAuthoriser.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }
                        else
                        {
                            form.Close.PermitAuthoriser.User = new User()
                            {
                                Id = _currentUserService.Id,
                                Email = _currentUserService.Email,
                                Name = _currentUserService.Name
                            };
                        }

                        var entry = form.Completions.FirstOrDefault();
                        if (entry == null)
                            form.Completions.Add(new AuthorityToWorkForm.AuthorityToWorkFormCompletion());
                        entry = form.Completions.First();
                        entry.PermitAcceptor.ActionedAt = DateTime.UtcNow;
                        entry.PermitAcceptor.IsApproved = true;

                        if (message.permitAcceptor != null && !string.IsNullOrWhiteSpace(message.permitAcceptor.ToString()) && !string.IsNullOrWhiteSpace(message.permitAcceptor.id.ToString()))
                        {
                            var user = message.permitAcceptor;
                            entry.PermitAcceptor.User = new User()
                            {
                                Id = user.id,
                                Name = user.name,
                                Email = user.email
                            };
                        }
                        else
                        {
                            entry.PermitAcceptor.User = form.Open.PermitAcceptor.User;
                        }

                        try
                        {
                            await _authorityToWorkService.UpdateAsync(form);
                            await _mediator.Publish(new FormClosedEvent(form));
                            return Ok(new { Message = "Form closed." });
                        }
                        catch (Exception ex)
                        {
                            return BadRequest(new { Message = ex.Message });
                        }
                    }
                }

                return NotFound();
            }
            else if (formType == "isolation-permit")
            {
                var form = await _isoService.FindAsync(formId);


                if (status == "cancel")
                {
                    foreach(var link in form.LinkedForms.Where(x => x.Id == "ATW"))
                    {
                        var friendlyId = link.FormId;
                        var atw = (await _fetchService.QueryAsync(x => x.FriendlyId == friendlyId)).FirstOrDefault() as AuthorityToWorkForm;

                        if (atw == null) continue;

                        foreach (var permit in atw.Permits.Where(x => x.Id == "ISO" && x.PermitId == form.FriendlyId))
                        {
                            permit.Required = false;
                            permit.PermitId = null;
                            permit.Generate = false;
                        }

                        await _authorityToWorkService.UpdateAsync(atw);
                    }

                    form.FriendlyId = null;
                    form.LinkedForms = new List<FormLink>();
                    form.Inactive = true;
                    form.Status = FormState.CANCELLED;

                    await _isoService.UpdateAsync(form);
                    //await _mediator.Publish(new FormDeniedEvent(form));
                    return Ok(new { Message = "Permit cancelled." });
                }

                return NotFound();
            }
            else if (formType == "working-at-heights-permit")
            {
                var form = await _wahService.FindAsync(formId);


                if (status == "cancel")
                {
                    foreach (var link in form.LinkedForms.Where(x => x.Id == "ATW"))
                    {
                        var friendlyId = link.FormId;
                        var atw = (await _fetchService.QueryAsync(x => x.FriendlyId == friendlyId)).FirstOrDefault() as AuthorityToWorkForm;

                        if (atw == null) continue;

                        foreach (var permit in atw.Permits.Where(x => x.Id == "WAH" && x.PermitId == form.FriendlyId))
                        {
                            permit.Required = false;
                            permit.PermitId = null;
                            permit.Generate = false;
                        }

                        await _authorityToWorkService.UpdateAsync(atw);
                    }

                    form.FriendlyId = null;
                    form.LinkedForms = new List<FormLink>();
                    form.Inactive = true;
                    form.Status = FormState.CANCELLED;

                    await _wahService.UpdateAsync(form);
                    //await _mediator.Publish(new FormDeniedEvent(form));
                    return Ok(new { Message = "Permit cancelled." });
                }

                return NotFound();
            }
            else
            {
                return BadRequest();
            }

        }

        private async Task AddReferenceIntoLinkedForms(IForm form)
        {
            // Get other forms linked to this form
            // Add link to this form on other form
            foreach (var formLink in form.LinkedForms.Where(x => x.Completed))
            {
                var otherForm = (await _fetchService.QueryAsync(x => x.FriendlyId == formLink.FormId)).FirstOrDefault();
                if (otherForm == null) continue;

                // Go through other form links and check for this form
                var existingFormLink = otherForm.LinkedForms.FirstOrDefault(x => x.FormId == form.FriendlyId);
                if (existingFormLink != null) continue;

                var blankFormLink = otherForm.LinkedForms.FirstOrDefault(x => x.Id == form.Kind.Code && string.IsNullOrWhiteSpace(x.FormId));
                if (blankFormLink != null)
                {
                    blankFormLink.Id = form.Kind.Code;
                    blankFormLink.Name = form.Kind.Name;
                    blankFormLink.FormId = form.FriendlyId;
                    blankFormLink.Completed = true;
                    blankFormLink.Reason = null;
                }
                else
                {
                    otherForm.LinkedForms.Add(new FormLink()
                    {
                        Id = form.Kind.Code,
                        Name = form.Kind.Name,
                        FormId = form.FriendlyId,
                        Completed = true,
                        Reason = null,
                    });
                }


                await _fetchService.UpdateAsync(otherForm);
            }
        }

        private Func<IQueryable<T>, IOrderedQueryable<T>> GetOrderBy<T>(string orderColumn, string orderType) where T : IForm
        {
            Type typeQueryable = typeof(IQueryable<T>);
            ParameterExpression argQueryable = Expression.Parameter(typeQueryable, "p");
            var outerExpression = Expression.Lambda(argQueryable, argQueryable);
            string[] props = orderColumn.Split('.');
            IQueryable<T> query = new List<T>().AsQueryable<T>();
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");

            Expression expr = arg;
            foreach (string prop in props)
            {
                PropertyInfo pi = type.GetPublicProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            LambdaExpression lambda = Expression.Lambda(expr, arg);
            string methodName = orderType == "ascending" ? "OrderBy" : "OrderByDescending";

            MethodCallExpression resultExp =
                Expression.Call(typeof(Queryable), methodName, new Type[] { typeof(T), type }, outerExpression.Body, Expression.Quote(lambda));
            var finalLambda = Expression.Lambda(resultExp, argQueryable);
            return (Func<IQueryable<T>, IOrderedQueryable<T>>)finalLambda.Compile();
        }

        private static void SetExpiry(PersonTraining personTraining, VisitorTraining trainingTemplate)
        {
            if (trainingTemplate.Expiry != null && trainingTemplate.ExpiryUnit != null)
            {
                if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "hour")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddHours(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "day")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddDays(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "month")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddMonths(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "year")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddYears(trainingTemplate.Expiry.Value);
                }
            }
        }
    }
}
