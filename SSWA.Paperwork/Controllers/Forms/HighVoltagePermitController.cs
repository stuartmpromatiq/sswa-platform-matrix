using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SSWA.Paperwork.Common.Forms.HighVoltagePermit;

namespace SSWA.Paperwork.Controllers
{
    public class HighVoltagePermitController : FormControllerBase
    {
        private readonly DocumentService<HighVoltagePermit> _permitService;

        public HighVoltagePermitController(DocumentService<HighVoltagePermit> wahService,
                                         ICurrentUserService currentUserService,
                                         IFormAuthorisation formAuthorisation)
            : base(currentUserService, formAuthorisation)
        {
            _permitService = wahService;
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[HttpGet("/api/forms/confined-space-entry-permit/")]
        //public async Task<IActionResult> Get()
        //{
        //    try
        //    {
        //        return Ok(new HighVoltagePermit());
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest();
        //    }
        //}

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/high-voltage-permit/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] HighVoltagePermit form)
        {
            var existingForm = await _permitService.FindAsync(id);
            var canEdit = FormAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            existingForm.UpdatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };

            existingForm.Revision += 1;

            existingForm.SwitchingProgamNumber = form.SwitchingProgamNumber;
            existingForm.Contractor = form.Contractor;
            existingForm.ReferenceDrawingNumbers = form.ReferenceDrawingNumbers;
            existingForm.WorkLocation = form.WorkLocation;
            existingForm.WorksDescription = form.WorksDescription;
            existingForm.PermitAccess = form.PermitAccess;
            existingForm.PermitPurpose = form.PermitPurpose;
            existingForm.IsolationPoints = form.IsolationPoints;
            existingForm.EquipmentEarthedAt = form.EquipmentEarthedAt;

            try
            {
                await _permitService.UpdateAsync(existingForm);

                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }
    }
}
