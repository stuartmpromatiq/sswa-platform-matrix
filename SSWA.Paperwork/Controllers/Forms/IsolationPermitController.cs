using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Controllers
{
    public class IsolationPermitController : FormControllerBase
    {
        private readonly DocumentService<IsolationPermit> _isoService;
        private readonly DocumentService<IsolationPermitTemplate> _isoTemplateService;

        public IsolationPermitController(DocumentService<IsolationPermit> isoService,
                                         DocumentService<IsolationPermitTemplate> isoTemplateService,
                                         ICurrentUserService currentUserService,
                                         IFormAuthorisation formAuthorisation)
            : base(currentUserService, formAuthorisation)
        {
            _isoService = isoService;
            _isoTemplateService = isoTemplateService;
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/forms/isolation-permit/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] IsolationPermit form)
        {
            var existingForm = await _isoService.FindAsync(id);
            var canEdit = FormAuthorisation.CanEdit(existingForm);
            if (!canEdit.Authorised) return Unauthorized();

            existingForm.UpdatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };

            existingForm.Revision += 1;

            existingForm.IsolationMethod = form.IsolationMethod;
            existingForm.Attachments = form.Attachments;
            existingForm.WorksDescription = form.WorksDescription;
            existingForm.PhysicalLocation = form.PhysicalLocation;
            existingForm.SubContractor = form.SubContractor;
            existingForm.SwitchingProgramNumber = form.SwitchingProgramNumber;

            try
            {
                await _isoService.UpdateAsync(existingForm);

                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("/api/form-templates/isolation-permit")]
        public async Task<IActionResult> PostTemplate([FromBody] IsolationPermitTemplate form)
        {
            //var canEdit = FormAuthorisation.CanEdit(existingForm);
            //if (!canEdit.Authorised) return Unauthorized();

            form.CreatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };
            form.UpdatedBy = form.CreatedBy;
            
            try
            {
                await _isoTemplateService.InsertAsync(form);
                return Ok(form);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }

        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut("/api/form-templates/isolation-permit/{id}")]
        public async Task<IActionResult> PutTemplate(string id, [FromBody] IsolationPermitTemplate form)
        {
            var existingForm = await _isoTemplateService.FindAsync(id);
            //var canEdit = FormAuthorisation.CanEdit(existingForm);
            //if (!canEdit.Authorised) return Unauthorized();

            existingForm.UpdatedBy = new User()
            {
                Id = CurrentUserService.Id,
                Name = CurrentUserService.Name,
                Email = CurrentUserService.Email
            };

            existingForm.Revision += 1;

            existingForm.Lookup.Level1 = form.Lookup.Level1;
            existingForm.Lookup.Level2 = form.Lookup.Level2;
            existingForm.Lookup.Level3 = form.Lookup.Level3;
            existingForm.Lookup.Name = form.Lookup.Name;

            existingForm.IsolationMethod = form.IsolationMethod;
            existingForm.Attachments = form.Attachments;

            try
            {
                await _isoTemplateService.UpdateAsync(existingForm);
                return Ok(existingForm);
            }
            catch (Exception ex)
            {
                return BadRequest(form);
            }

           
        }
    }
}
