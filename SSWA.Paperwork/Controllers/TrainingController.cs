using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Paperwork.Models;
using SSWA.ExternalUserService;
using SSWA.Paperwork.Common;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.Models;

namespace SSWA.Paperwork.Controllers
{

    [Produces("application/json")]
    public class TrainingController : Controller
    {
        private readonly IExternalUserService _externalUserService;
        private readonly NotificationService _notificationService;
        private readonly ICurrentUserService _currentUserService;
        private readonly CosmosDbService<VisitorTraining> _trainingService;

        public TrainingController(
            IExternalUserService externalUserService,
            NotificationService notificationService,
            ICurrentUserService currentUserService,
            CosmosDbService<VisitorTraining> trainingService
            )
        {
            _externalUserService = externalUserService;
            _notificationService = notificationService;
            _currentUserService = currentUserService;
            _trainingService = trainingService;
        }

        [HttpGet("/api/training/")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [ResponseCache(Duration = 3600)]
        public async Task<IEnumerable<VisitorTraining>> Get()
        {
            var training = await _trainingService.QueryAsync("visitor_training");

            return training.OrderBy(x => x.CanonicalName);
        }
    }
}
