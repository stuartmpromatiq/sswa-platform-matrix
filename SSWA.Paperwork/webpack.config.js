const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const bundleOutputDir = './wwwroot/dist';

module.exports = () => {
  const isDevBuild = !(
    process.env.NODE_ENV && process.env.NODE_ENV === 'production'
  );

  return [{
    stats: {
      modules: false
    },
    entry: {
      main: './ClientApp/boot-app.js'
    },
    resolve: {
      extensions: ['.js', '.vue'],
      alias: isDevBuild ?
        {
          vue$: 'vue/dist/vue',
          components: path.resolve(__dirname, './ClientApp/components'),
          views: path.resolve(__dirname, './ClientApp/views'),
          utils: path.resolve(__dirname, './ClientApp/utils'),
          api: path.resolve(__dirname, './ClientApp/store/api')
        } :
        {
          components: path.resolve(__dirname, './ClientApp/components'),
          views: path.resolve(__dirname, './ClientApp/views'),
          utils: path.resolve(__dirname, './ClientApp/utils'),
          api: path.resolve(__dirname, './ClientApp/store/api')
        }
    },
    output: {
      path: path.join(__dirname, bundleOutputDir),
      filename: '[name].js',
      publicPath: '/dist/'
    },
    module: {
      rules: [{
          test: /\.vue$/,
          //include: /ClientApp/,
          use: 'vue-loader'
        },
        {
          test: /\.js$/,
          include: /ClientApp/,
          use: 'babel-loader'
        },
        {
          test: /\.(s*)css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [{
                loader: 'css-loader'
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: function () {
                    return [require('precss'), require('autoprefixer')];
                  }
                }
              },
              {
                loader: 'sass-loader'
              }
            ]
          })
        },
        {
          test: /\.(png|jpg|jpeg|gif|svg|eot|woff|ttf)$/,
          use: 'url-loader?limit=25000'
        }
        // {
        //   test: /\.(eot|woff|ttf)$/,
        //   use: 'file-loader'
        // }
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          PAPERWORK_AUTH0_CLIENT_ID: JSON.stringify(
            process.env.PAPERWORK_AUTH0_CLIENT_ID
          ),
          PAPERWORK_AUTH0_DOMAIN: JSON.stringify(
            process.env.PAPERWORK_AUTH0_DOMAIN
          ),
          PAPERWORK_LOGIN_REDIRECT_URI: JSON.stringify(
            process.env.PAPERWORK_LOGIN_REDIRECT_URI
          ),
          PAPERWORK_LOGOUT_REDIRECT_URI: JSON.stringify(
            process.env.PAPERWORK_LOGOUT_REDIRECT_URI
          ),
          PAPERWORK_AUTH0_AUDIENCE: JSON.stringify(
            process.env.PAPERWORK_AUTH0_AUDIENCE
          )
        }
      }),
      new webpack.NormalModuleReplacementPlugin(
        /element-ui[/\\]lib[/\\]locale[/\\]lang[/\\]zh-CN/,
        'element-ui/lib/locale/lang/en'
      ),
      new ExtractTextPlugin({
        filename: 'app.bundle.css'
      })
      // new webpack.DllReferencePlugin({
      //   context: __dirname,
      //   manifest: require('./wwwroot/dist/vendor-manifest.json')
      // }),
    ].concat(
      isDevBuild ?
      [
        // Plugins that apply in development builds only
        new webpack.SourceMapDevToolPlugin({
          filename: '[file].map', // Remove this line if you prefer inline source maps
          moduleFilenameTemplate: path.relative(
            bundleOutputDir,
            '[resourcePath]'
          ) // Point sourcemap entries to the original file locations on disk
        })
      ] :
      [
        // Plugins that apply in production builds only
        new webpack.optimize.UglifyJsPlugin(),
        // Compress extracted CSS.
        new OptimizeCSSPlugin({
          cssProcessorOptions: {
            safe: true
          }
        })
      ]
    )
  }];
};
