import format from "date-fns/format";

function dateFormat(date, formatOption) {
  if (!Date.parse(date)) return '';

  let minDate = Date.parse('1950-01-01 00:00:00.000000Z');
  let parsedDate = Date.parse(date);

  if (parsedDate < minDate) return '__ / __ / __';

  return format(parsedDate, formatOption);
}

export { dateFormat };
