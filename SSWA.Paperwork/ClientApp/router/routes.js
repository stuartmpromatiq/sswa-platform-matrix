import MainContent from 'components/main-content';

import Form from 'components/forms/form';
import AuthorityToWorkRegister from 'components/forms/authority-to-work-register';
import IsolationPermitTemplateReigster from 'components/forms/isolation-permit-template-register';

import FindFormByFriendlyId from 'components/forms/find-form-by-friendly-id';

import Visitors from 'components/admin/visitors';
import PersonDetail from 'components/admin/person';


import AuthorityToWorkFormCreate from 'components/forms/authority-to-work/create';
import AuthorityToWorkFormDetail from 'components/forms/authority-to-work/detail';
import AuthorityToWorkFormEdit from 'components/forms/authority-to-work/edit';

import PlantEntryRequestFormCreate from 'components/forms/plant-entry-request/create';
import PlantEntryRequestFormDetail from 'components/forms/plant-entry-request/detail';
import PlantEntryRequestFormEdit from 'components/forms/plant-entry-request/edit';

import PlantWorksRequestFormCreate from 'components/forms/plant-works-request/create';
import PlantWorksRequestFormDetail from 'components/forms/plant-works-request/detail';
import PlantWorksRequestFormEdit from 'components/forms/plant-works-request/edit';

import IsolationPermitCreate from 'components/forms/isolation-permit/create';
import IsolationPermitDetail from 'components/forms/isolation-permit/detail';
import IsolationPermitEdit from 'components/forms/isolation-permit/edit';

import WorkingAtHeightsPermitCreate from 'components/forms/working-at-heights-permit/create';
import WorkingAtHeightsPermitDetail from 'components/forms/working-at-heights-permit/detail';
import WorkingAtHeightsPermitEdit from 'components/forms/working-at-heights-permit/edit';

import ConfinedSpaceEntryPermitCreate from 'components/forms/confined-space-entry-permit/create';
import ConfinedSpaceEntryPermitDetail from 'components/forms/confined-space-entry-permit/detail';
import ConfinedSpaceEntryPermitEdit from 'components/forms/confined-space-entry-permit/edit';

import ExcavationPermitCreate from 'components/forms/excavation-permit/create';
import ExcavationPermitDetail from 'components/forms/excavation-permit/detail';
import ExcavationPermitEdit from 'components/forms/excavation-permit/edit';

import HighVoltagePermitCreate from 'components/forms/high-voltage-permit/create';
import HighVoltagePermitDetail from 'components/forms/high-voltage-permit/detail';
import HighVoltagePermitEdit from 'components/forms/high-voltage-permit/edit';

import ProcessLocks from 'components/process-locks/process-locks';
import ProcessLockCreate from 'components/process-locks/create';
import ProcessLockEdit from 'components/process-locks/edit';

import Home from 'components/home';

import Dashboard from 'components/dashboard/dashboard';

// import MyForms from 'components/dashboard/my-forms';
// import MyTasks from 'components/dashboard/my-tasks';
// import MyApprovals from 'components/dashboard/my-approvals';

import Notifications from 'components/notifications';
import Calendar from 'components/calendar';
import Reports from 'components/reports';
import Help from 'components/help';

import Users from 'components/admin/users';

import Login from 'components/login';
import Callback from 'components/callback';

export const routes = [{
    name: 'home',
    path: '/',
    component: Home,
    meta: {
      display: 'Home',
      breadcrumbs: []
    }
  },
  {
    name: 'main',
    path: '/app',
    component: MainContent,
    meta: {
      display: 'Home',
      breadcrumbs: []
    },
    children: [{
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard,
        meta: {
          display: 'Dashboard',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'notifications',
        path: '/notifications',
        component: Notifications,
        meta: {
          display: 'Notifications',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'calendar',
        path: '/calendar',
        component: Calendar,
        meta: {
          display: 'Calendar',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'reports',
        path: '/reports',
        component: Reports,
        meta: {
          display: 'Reports',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'help',
        path: '/help',
        component: Help,
        meta: {
          display: 'Help',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'login',
        path: '/login/:returnPath',
        component: Login,
        props: true,
        meta: {
          display: 'Login',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'callback',
        path: '/callback',
        component: Callback,
        meta: {
          display: 'Callback',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'admin',
        path: '/admin',
        component: Home,
        meta: {
          display: 'Admin',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'admin-users',
        path: '/admin/users',
        component: Users,
        meta: {
          display: 'Users & Permissions',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'admin',
              display: 'Administration'
            }
          ]
        }
      },
      {
        name: 'visitors',
        path: '/admin/people',
        component: Visitors,
        meta: {
          display: 'People',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'admin',
            display: 'Administration'
          }
          ]
        }
      },
      {
        name: 'person',
        path: '/admin/people/:id',
        component: PersonDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'admin',
            display: 'Administration'
            },
            {
              routeName: 'visitors',
              display: 'People'
            }
          ]
        }
      },
      {
        name: 'forms',
        path: '/forms',
        component: Form,
        meta: {
          display: 'Forms',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'authority-to-work-register',
        path: '/authority-to-work-register',
        component: AuthorityToWorkRegister,
        meta: {
          display: 'Authority To Work Register',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'isolation-permit-template-register',
        path: '/isolation-permit-template-register',
        component: IsolationPermitTemplateReigster,
        meta: {
          display: 'Isolation Permit Template Register',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'find-form-by-friendly-id',
        path: '/forms/find-by-friendly-id/:friendlyId',
        component: FindFormByFriendlyId,
        meta: {
          display: 'Forms',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }]
        }
      },
      {
        name: 'authority-to-work-create',
        path: '/forms/authority-to-work/create',
        component: AuthorityToWorkFormCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'authority-to-work-register',
              display: 'Authority To Work'
            }
          ]
        }
      },
      {
        name: 'authority-to-work-form-detail',
        path: '/forms/authority-to-work/detail/:formId',
        component: AuthorityToWorkFormDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'authority-to-work-register',
              display: 'Authority To Work'
            }
          ]
        }
      },
      {
        name: 'authority-to-work-form-edit',
        path: '/forms/authority-to-work/edit/:formId',
        component: AuthorityToWorkFormEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'authority-to-work-register',
              display: 'Authority To Work'
            }
          ]
        }
      },
      {
        name: 'plant-entry-request-create',
        path: '/forms/plant-entry-request/create',
        component: PlantEntryRequestFormCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Entry Request'
            }
          ]
        }
      },
      {
        name: 'plant-entry-request-form-detail',
        path: '/forms/plant-entry-request/detail/:formId',
        component: PlantEntryRequestFormDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Entry Request'
            }
          ]
        }
      },
      {
        name: 'plant-entry-request-form-edit',
        path: '/forms/plant-entry-request/edit/:formId',
        component: PlantEntryRequestFormEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Entry Request'
            }
          ]
        }
      },
      {
        name: 'plant-works-request-create',
        path: '/forms/plant-works-request/create',
        component: PlantWorksRequestFormCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Works Request'
            }
          ]
        }
      },
      {
        name: 'plant-works-request-form-detail',
        path: '/forms/plant-works-request/detail/:formId',
        component: PlantWorksRequestFormDetail,
        meta: {
          display: 'Detail',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Works Request'
            }
          ]
        }
      },
      {
        name: 'plant-works-request-form-edit',
        path: '/forms/plant-works-request/edit/:formId',
        component: PlantWorksRequestFormEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
              routeName: 'home',
              display: 'Home'
            },
            {
              routeName: 'forms',
              display: 'Forms'
            },
            {
              routeName: 'forms',
              display: 'Plant Works Request'
            }
          ]
        }
      },
      {
        name: 'isolation-permit-form-create',
        path: '/forms/isolation-permit/create',
        component: IsolationPermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'isolation-permit-form-detail',
        path: '/forms/isolation-permit/detail/:formId',
        component: IsolationPermitDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'isolation-permit-form-edit',
        path: '/forms/isolation-permit/edit/:formId',
        component: IsolationPermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'isolation-permit-template-create',
        path: '/forms/isolation-permit-template/create',
        component: IsolationPermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'isolation-permit-template-register',
            display: 'Isolation Permit Templates'
          }
          ]
        }
      },
      {
        name: 'isolation-permit-template-edit',
        path: '/forms/isolation-permit-template/edit/:formId',
        component: IsolationPermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
            {
              routeName: 'isolation-permit-template-register',
              display: 'Isolation Permit Templates'
            }
          ]
        }
      },
      {
        name: 'working-at-heights-permit-form-create',
        path: '/forms/working-at-heights-permit/create',
        component: WorkingAtHeightsPermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'working-at-heights-permit-form-detail',
        path: '/forms/working-at-heights-permit/detail/:formId',
        component: WorkingAtHeightsPermitDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'working-at-heights-permit-form-edit',
        path: '/forms/working-at-heights-permit/edit/:formId',
        component: WorkingAtHeightsPermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'confined-space-entry-permit-form-create',
        path: '/forms/confined-space-entry-permit/create',
        component: ConfinedSpaceEntryPermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'confined-space-entry-permit-form-detail',
        path: '/forms/confined-space-entry-permit/detail/:formId',
        component: ConfinedSpaceEntryPermitDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'confined-space-entry-permit-form-edit',
        path: '/forms/confined-space-entry-permit/edit/:formId',
        component: ConfinedSpaceEntryPermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'excavation-permit-form-create',
        path: '/forms/excavation-permit/create',
        component: ExcavationPermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'excavation-permit-form-detail',
        path: '/forms/excavation-permit/detail/:formId',
        component: ExcavationPermitDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'excavation-permit-form-edit',
        path: '/forms/excavation-permit/edit/:formId',
        component: ExcavationPermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'high-voltage-permit-form-create',
        path: '/forms/high-voltage-permit/create',
        component: HighVoltagePermitCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'high-voltage-permit-form-detail',
        path: '/forms/high-voltage-permit/detail/:formId',
        component: HighVoltagePermitDetail,
        meta: {
          display: 'Details',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'high-voltage-permit-form-edit',
        path: '/forms/high-voltage-permit/edit/:formId',
        component: HighVoltagePermitEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'forms',
            display: 'Forms'
          },
          {
            routeName: 'authority-to-work-register',
            display: 'Authority To Work'
          }
          ]
        }
      },
      {
        name: 'process-lock-register',
        path: '/process-lock-register',
        component: ProcessLocks,
        meta: {
          display: 'Process Lock Register',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          }
          ]
        }
      },
      {
        name: 'process-lock-create',
        path: '/process-lock-register/create',
        component: ProcessLockCreate,
        meta: {
          display: 'New',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'process-lock-register',
            display: 'Process Lock Register'
          }
          ]
        }
      },
      {
        name: 'process-lock-edit',
        path: '/process-lock-register/edit/:id',
        component: ProcessLockEdit,
        meta: {
          display: 'Edit',
          breadcrumbs: [{
            routeName: 'home',
            display: 'Home'
          },
          {
            routeName: 'process-lock-register',
            display: 'Process Lock Register'
          }
          ]
        }
      },
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
];
