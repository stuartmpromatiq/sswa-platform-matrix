import Vue from 'vue';
import VueRouter from 'vue-router';
import {
  routes
} from './routes';
import {
  store
} from './../app';

Vue.use(VueRouter);

let router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0
      };
    }
  }
});

//very basic "setup" of a global guard
router.beforeEach((to, from, next) => {
  if (
    to.name == 'callback' ||
    to.name == 'login' ||
    to.name == 'home' ||
    store.getters.isAuthenticated ||
    to.path == '/'
  ) {
    next();
  } else {
    next({
      name: 'login',
      params: {
        returnPath: to.path
      }
    });
  }
});

export default router;
