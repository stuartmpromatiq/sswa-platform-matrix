import axios from 'axios';
const API_URL = '/api';

export default class Api {
  constructor() {

  }

  async getForms(
    pageSize = 20,
    token = null,
    filters = {
      formType: 'any',
      status: 'any',
      startDate: null,
      endDate: null,
      description: null,
    },
    sort = {
      prop: 'updatedAt',
      order: 'descending'
    }
  ) {
    try {
      let response = await axios({
        method: 'get',
        url: `/api/forms/${filters.formType}`,
        params: {
          pageSize: pageSize,
          token: token,
          status: filters.status,
          startDate: filters.startDate,
          endDate: filters.endDate,
          description: filters.description,
          sortBy: sort.prop,
          orderBy: sort.order
        },
        headers: {
          Authorization: 'Bearer ' + this.$store.getters.accessToken
        }
      });

      return {
        data: response.data.data,
        nextPageToken: response.data.nextPageToken,
        hasMoreData: response.data.hasMoreData
      }

    } catch (err) {
      console.log(err);
    }
  }

}
