let options_requestTypes = [{
    code: 'V',
    name: 'Visitors Request',
    description: ' 1) Individuals or groups are the responsibility of the Plant host and have restricted access in the administration building and must be accompanied at all times when accessing any parts of the Plant including the workshops, plus are unable to conduct any work activities. 2) Tour groups must be accompanied at all times by the Plant host in the administration area and the Plant.',
    length: {
      timeUnit: 'hours',
      value: 12
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: ['reception-lockbox-boardroom', 'office'],
    induction: 'Visitors (3-5mins) - Reception PC and tour groups the Pindjarup Room.',
    pwr: false,
    atw: false
  },
  {
    code: 'STW',
    name: 'Short Term Works Request',
    description: 'Work by individuals or a team of workers engaged to conduct short term maintenance or project work of any nature on the Plant with restricted access according to the areas of work and are the responsibility of the Permit Acceptor/Nominee, Maintenance Planner or Project Engineer.',
    length: {
      timeUnit: 'month',
      value: 1
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: [
      'reception-lockbox-boardroom', 'office', 'chemical-building', 'workshop', 'gate-two', 'gate-three-four',
      'stage-one-ro', 'stage-one-mf', 'stage-one-swips', 'stage-one-dwps', 'stage-one-potab',
      'stage-two-ro', 'stage-two-mf', 'stage-two-swips', 'stage-two-dwps', 'stage-two-potab', 'marine-assets'
    ],
    induction: 'Short Term Workers (5-10mins) - Reception PC',
    pwr: true,
    atw: true
  },
  {
    code: 'PCW',
    name: 'Period Contract Works Request',
    description: 'Work by individuals or a group of workers engaged to conduct regular work on the Plant with restricted access according to the areas of work and are the responsibility of the Permit Acceptor /Nominee.',
    length: {
      timeUnit: 'months',
      value: 12
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: [
      'reception-lockbox-boardroom', 'office', 'chemical-building', 'workshop', 'gate-two', 'gate-three-four',
      'stage-one-ro', 'stage-one-mf', 'stage-one-swips', 'stage-one-dwps', 'stage-one-potab',
      'stage-two-ro', 'stage-two-mf', 'stage-two-swips', 'stage-two-dwps', 'stage-two-potab', 'marine-assets'
    ],
    induction: 'Period Contract Workers (10-15mins) - Reception PC',
    pwr: true,
    atw: true
  },
  {
    code: 'DWRD',
    name: 'Delivery and Waste Removal Drivers',
    description: 'A minimum requirement for any chemical or waste removal driver to enter the Plant.',
    length: {
      timeUnit: 'months',
      value: 12
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: [
      'reception-lockbox-boardroom', 'office', 'chemical-building', 'workshop', 'gate-two', 'gate-three-four',
      'stage-one-ro', 'stage-one-mf', 'stage-one-swips', 'stage-one-dwps', 'stage-one-potab',
      'stage-two-ro', 'stage-two-mf', 'stage-two-swips', 'stage-two-dwps', 'stage-two-potab', 'marine-assets'
    ],
    induction: 'Delivery and Waste Removal Drivers - Reception PC',
    pwr: true,
    atw: false
  },
  {
    code: 'EDD',
    name: 'Escorted Delivery Drivers',
    description: 'If escorted, delivery drivers who do not fall into the category of Delivery and Waste Removal Drivers, are able to enter the Plant under a PER without requiring a PWR, if not work is carried out.',
    length: {
      timeUnit: 'hours',
      value: 12
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: ['reception-lockbox-boardroom', 'office', 'chemical-building', 'workshop', 'gate-two', 'gate-three-four', 'stage-one-ro', 'stage-one-mf', 'stage-one-swips', 'stage-one-dwps', 'stage-one-potab', 'stage-two-ro', 'stage-two-mf', 'stage-two-swips', 'stage-two-dwps', 'stage-two-potab', 'marine-assets'],
    induction: 'Escorted Delivery Drivers - Reception PC',
    pwr: false,
    atw: false
  },
  {
    code: 'MW',
    description: 'A team of marine workers engaged to conduct marine work (e.g. maintenance or  inspection activities) ON or OFF shore on SSDP infrastructure with access according to the areas of work and are the responsibility of the Permit Acceptor/Nominee and/or Mechanical Planner.',
    name: 'Marine Works Request',
    length: {
      timeUnit: 'months',
      value: 3
    },
    approvers: ['Operations Manager or Maintenance Manager', 'Alliance Manager'],
    plantAreas: ['reception-lockbox-boardroom', 'office', 'stage-one-swips', 'stage-two-swips', 'marine-assets'],
    induction: 'Marine Workers (10-15mins) - Reception PC and/or Bunbury harbour jetty',
    pwr: true,
    atw: true
  }
];

let options_lockboxes = [{
    number: 'PP'
  },
  {
    number: 'SS'
  },
  {
    number: 'GG'
  },
  {
    number: 'UU'
  },
  {
    number: 'HH'
  },
  {
    number: 'RR'
  },
  {
    number: 'EE'
  },
  {
    number: 'CC'
  },
  {
    number: 'MM'
  },
  {
    number: 'NN'
  },
  {
    number: 'W'
  },
  {
    number: 'LL'
  },
  {
    number: 'DD'
  },
  {
    number: 'QQ'
  },
  {
    number: 'KK'
  },
  {
    number: 'G'
  },
  {
    number: 'WW'
  },
  {
    number: 'XX'
  },
  {
    number: 'V'
  },
  {
    number: 'VV'
  },
  {
    number: 'L'
  },
  {
    number: 'X'
  },
  {
    number: 'I'
  },
  {
    number: 'AA'
  },
  {
    number: 'FF'
  },
  {
    number: 'TT'
  }
];

let options_quickDates = {
  shortcuts: [{
      text: 'Today',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        picker.$emit('pick', [start, end]);
      }
    },
    {
      text: 'Tomorrow',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        start.setTime(start.getTime() + 3600 * 1000 * 24 * 1);
        end.setTime(end.getTime() + 3600 * 1000 * 24 * 1);
        picker.$emit('pick', [start, end]);
      }
    },
    {
      text: 'Next 7 days',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        end.setTime(end.getTime() + 3600 * 1000 * 24 * 7);
        picker.$emit('pick', [start, end]);
      }
    },
    {
      text: 'Next 30 days',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        end.setTime(end.getTime() + 3600 * 1000 * 24 * 30);
        picker.$emit('pick', [start, end]);
      }
    },
    {
      text: 'Next 90 days',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        end.setTime(end.getTime() + 3600 * 1000 * 24 * 90);
        picker.$emit('pick', [start, end]);
      }
    },
    {
      text: 'Next 365 days',
      onClick(picker) {
        const end = new Date();
        const start = new Date();
        end.setTime(end.getTime() + 3600 * 1000 * 24 * 365);
        picker.$emit('pick', [start, end]);
      }
    }
  ]
};

let options_awarenessTraining = [
  'High Pressure',
  'High Voltage',
  'Chemicals',
  'Chlorine'
];

let options_approvers = {
  paperwork_operations_manager: [],
  paperwork_maintenance_manager: [],
  paperwork_alliance_manager: []
};

let options_swipeCardAreas = [{
    id: 'general',
    label: 'General',
    children: [{
        id: 'plant-wide',
        label: 'Plant Wide'
      },
      {
        id: 'reception-lockbox-boardroom',
        label: 'Reception, Lockbox Area, and Boardroom'
      },
      {
        id: 'office',
        label: 'Office Area'
      },
      {
        id: 'chemical-building',
        label: 'Chemical Building'
      },
      {
        id: 'workshop',
        label: 'Workshop'
      },
      {
        id: 'gate-two',
        label: 'Gate 2'
      },
      {
        id: 'gate-three-four',
        label: 'Gate 3 & 4'
      },
      {
        id: 'marine-assets',
        label: 'Marine Assets'
      }
    ]
  },
  {
    id: 'stage-one',
    label: 'Stage 1',
    children: [{
        id: 'stage-one-ro',
        label: 'RO'
      },
      {
        id: 'stage-one-mf',
        label: 'MF'
      },
      {
        id: 'stage-one-swips',
        label: 'SWIPS'
      },
      {
        id: 'stage-one-dwps',
        label: 'DWPS'
      },
      {
        id: 'stage-one-potab',
        label: 'POTABILISATION'
      }
    ]
  },
  {
    id: 'stage-two',
    label: 'Stage 2',
    children: [{
        id: 'stage-two-ro',
        label: 'RO'
      },
      {
        id: 'stage-two-mf',
        label: 'MF'
      },
      {
        id: 'stage-two-swips',
        label: 'SWIPS'
      },
      {
        id: 'stage-two-dwps',
        label: 'DWPS'
      },
      {
        id: 'stage-two-potab',
        label: 'POTABILISATION'
      }
    ]
  }
];

let options_plantAccessAreas = [{
    id: 'general',
    label: 'General',
    children: [{
        id: 'plant-wide',
        label: 'Plant Wide'
      },
      {
        id: 'reception-lockbox-boardroom',
        label: 'Reception, Lockbox Area, and Boardroom'
      },
      {
        id: 'office',
        label: 'Office Area'
      },
      {
        id: 'chemical-building',
        label: 'Chemical Building'
      },
      {
        id: 'workshop',
        label: 'Workshop'
      },
      {
        id: 'gate-two',
        label: 'Gate 2'
      },
      {
        id: 'gate-three-four',
        label: 'Gate 3 & 4'
      },
      {
        id: 'marine-assets',
        label: 'Marine Assets'
      },
      {
        id: 'external-to-buildings-only',
        label: 'External to buildings only'
      }
    ]
  },
  {
    id: 'stage-one',
    label: 'Stage 1',
    children: [{
        id: 'stage-one-ro',
        label: 'RO'
      },
      {
        id: 'stage-one-mf',
        label: 'MF'
      },
      {
        id: 'stage-one-swips',
        label: 'SWIPS'
      },
      {
        id: 'stage-one-dwps',
        label: 'DWPS'
      },
      {
        id: 'stage-one-potab',
        label: 'POTABILISATION'
      }
    ]
  },
  {
    id: 'stage-two',
    label: 'Stage 2',
    children: [{
        id: 'stage-two-ro',
        label: 'RO'
      },
      {
        id: 'stage-two-mf',
        label: 'MF'
      },
      {
        id: 'stage-two-swips',
        label: 'SWIPS'
      },
      {
        id: 'stage-two-dwps',
        label: 'DWPS'
      },
      {
        id: 'stage-two-potab',
        label: 'POTABILISATION'
      }
    ]
  }
];

export {
  options_requestTypes,
  options_quickDates,
  options_awarenessTraining,
  options_approvers,
  options_swipeCardAreas,
  options_plantAccessAreas,
  options_lockboxes
};
