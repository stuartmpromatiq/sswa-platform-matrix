import Vue from 'vue';
import Vuex from 'vuex';
import addSeconds from 'date-fns/add_seconds';
import isFuture from 'date-fns/is_future';

Vue.use(Vuex);

export default new Vuex.Store({
  state() {
    return {
      token: localStorage.getItem('id_token') || '',
      accessToken: localStorage.getItem('access_token') || '',
      expiresAt: localStorage.getItem('expires_at') || new Date(1900, 1, 1),
      user: localStorage.getItem('user') || '{}',
      redirectUrl: localStorage.getItem('redirect_url') || '/'
    };
  },
  modules: {},
  mutations: {
    update_auth_data(state, tokenData) {
      localStorage.setItem(
        'access_token',
        tokenData.access_token || tokenData.accessToken
      );
      state.accessToken = localStorage.getItem('access_token');

      localStorage.setItem('id_token', tokenData.id_token || tokenData.idToken);
      state.token = localStorage.getItem('id_token');

      const tokensExpiry = addSeconds(
        new Date(),
        tokenData.expires_in || tokenData.expiresIn
      );
      localStorage.setItem('expires_at', tokensExpiry);
      state.expiresAt = localStorage.getItem('expires_at');

      localStorage.setItem(
        'user',
        JSON.stringify(tokenData.idTokenPayload || tokenData.id_token_payload)
      );
      state.user = localStorage.getItem('user');
    }
  },
  actions: {},
  getters: {
    expiresAt: state => state.expiresAt,
    accessToken: state => state.accessToken,
    token: state => state.token,
    user: state => {
      let user = JSON.parse(state.user);
      user.phone = JSON.parse(state.user)['http://api.sswa/claims/phone'];
      user.name = JSON.parse(state.user)['http://api.sswa/claims/given_name'] + ' ' + JSON.parse(state.user)['http://api.sswa/claims/family_name'];
      return user;
    },
    redirectUrl: state => state.redirectUrl,
    isAuthenticated: state => isFuture(state.expiresAt),
    roles: state => JSON.parse(state.user)['http://api.sswa/claims/roles']
  }
});
