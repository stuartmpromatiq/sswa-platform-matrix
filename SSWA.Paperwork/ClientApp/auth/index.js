import Store from '../store/index';
import Router from '../router/index';
import subtractMinutes from 'date-fns/sub_minutes';
import addSeconds from 'date-fns/add_seconds';
import differenceInMilliSeconds from 'date-fns/difference_in_milliseconds';
import { WebAuth } from 'auth0-js';

let baseUri =
  window.location.protocol +
  '//' +
  window.location.hostname +
  (window.location.port ? ':' + window.location.port : '');
let refreshTimeout = null;
const auth0 = new WebAuth({
  responseType: 'token id_token',
  domain: process.env.PAPERWORK_AUTH0_DOMAIN || 'sswa.au.auth0.com',
  clientID:
    process.env.PAPERWORK_AUTH0_CLIENT_ID || 'jjtzavPmJcQknLfSmoSyDLaoQD2eZCAN',
  redirectUri:
    process.env.PAPERWORK_LOGIN_REDIRECT_URI || baseUri + '/callback',
  audience: process.env.PAPERWORK_AUTH0_AUDIENCE || 'https://api.sswa',
  scope: 'openid profile email'
});

export { auth0 };
export { initSession };
export { refreshTokens };
export { logout };
export { parseHash };
export default {
  initSession,
  refreshTokens,
  auth0,
  parseHash
};

function logout() {
  auth0.logout({
    returnTo: process.env.PAPERWORK_LOGOUT_REDIRECT_URI || baseUri
  });
  Store.commit('update_auth_data', {
    id_token: '',
    accessToken: '',
    expiresIn: new Date(1900, 1, 1),
    idTokenPaylad: {}
  }); //clear our tokens
  clearTimeout(refreshTimeout);
  refreshTimeout = null;
}

function initSession() {
  return new Promise(resolve => {
    // let tokenExpiryDate = Store.getters.expiresAt;
    // if (!tokenExpiryDate) {
    //   console.log('No token expiry date. user probably never logged in');
    //   return Router.push('/login');
    // }
    // let tenMinutesBeforeExpiry = subtractMinutes(tokenExpiryDate, 10); //If the token has expired or will expire in the next 30 minutes
    // const now = new Date();
    // if (isAfter(now, tenMinutesBeforeExpiry)) {
    //   //If the token has expired or will expire in the next 10 minutes
    //   console.log('Token expired/will expire in the next 1 minutes');
    //   return Router.push('/login');
    // }
    // console.log('Token Ok. Expiring at ' + tokenExpiryDate);
    // refreshTimeout = setTimeout(
    //   refreshTokens,
    //   differenceInMilliSeconds(tenMinutesBeforeExpiry, now)
    // );
  });
}

function parseHash() {
  return new Promise(resolve => {
    auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        resolve(authResult);
      } else if (err) {
        return Router.push('/login');
      }
    });
  });
}

function refreshTokens() {
  return new Promise(resolve => {
    auth0.checkSession({}, function(err, authResult) {
      if (err) {
        Router.push('/login');
      }
      Store.commit('update_auth_data', authResult);
      const tokenExpiryDate = addSeconds(new Date(), authResult.expiresIn);
      const tenMinutesBeforeExpiry = subtractMinutes(tokenExpiryDate, 10);
      const now = new Date();
      refreshTimeout = setTimeout(
        refreshTokens,
        differenceInMilliSeconds(tenMinutesBeforeExpiry, now)
      );
      resolve();
    });
  });
}

// let webAuth = new auth0.WebAuth({
//   domain: 'sswa.au.auth0.com',
//   clientID: 'jjtzavPmJcQknLfSmoSyDLaoQD2eZCAN',
//   redirectUri: baseUri + '/callback',
//   audience: 'https://api.sswa',
//   //audience: 'https://sswa.au.auth0.com/userinfo',
//   responseType: 'token id_token',
//   scope: 'openid profile email'
// });

// let tokenRenewalTimeout = {};

// let auth = new Vue({
//   computed: {
//     token: {
//       get: function() {
//         return localStorage.getItem('id_token');
//       },
//       set: function(id_token) {
//         localStorage.setItem('id_token', id_token);
//       }
//     },
//     accessToken: {
//       get: function() {
//         return localStorage.getItem('access_token');
//       },
//       set: function(accessToken) {
//         localStorage.setItem('access_token', accessToken);
//       }
//     },
//     expiresAt: {
//       get: function() {
//         return localStorage.getItem('expires_at');
//       },
//       set: function(expiresIn) {
//         let expiresAt = JSON.stringify(expiresIn * 1000 + new Date().getTime());
//         localStorage.setItem('expires_at', expiresAt);
//       }
//     },
//     user: {
//       get: function() {
//         return JSON.parse(localStorage.getItem('user'));
//       },
//       set: function(user) {
//         localStorage.setItem('user', JSON.stringify(user));
//       }
//     }
//   },
//   created() {
//     this.scheduleRenewal();
//   },
//   methods: {
//     login(redirectPath) {
//       localStorage.setItem('redirectUrl', redirectPath);
//       webAuth.authorize();
//     },
//     logout() {
//       return new Promise((resolve, reject) => {
//         localStorage.removeItem('access_token');
//         localStorage.removeItem('id_token');
//         localStorage.removeItem('expires_at');
//         localStorage.removeItem('user');
//         localStorage.removeItem('redirectUrl');

//         window.location.href = '/';
//       });
//     },
//     isAuthenticated() {
//       return new Date().getTime() < this.expiresAt;
//     },
//     renewToken() {
//       return new Promise((resolve, reject) => {
//         webAuth.checkSession({}, (err, authResult) => {
//           if (authResult && authResult.accessToken && authResult.idToken) {
//             this.expiresAt = authResult.expiresIn;
//             this.accessToken = authResult.accessToken;
//             this.token = authResult.idToken;
//             this.user = authResult.idTokenPayload;

//             this.scheduleRenewal();

//             resolve(authResult);
//           } else if (err) {
//             this.logout();
//             reject(err);
//           }
//         });
//       });
//     },
//     handleAuthentication() {
//       return new Promise((resolve, reject) => {
//         webAuth.parseHash((err, authResult) => {
//           if (authResult && authResult.accessToken && authResult.idToken) {
//             this.expiresAt = authResult.expiresIn;
//             this.accessToken = authResult.accessToken;
//             this.token = authResult.idToken;
//             this.user = authResult.idTokenPayload;

//             this.scheduleRenewal();

//             resolve(authResult);
//           } else if (err) {
//             this.logout();
//             reject(err);
//           }
//         });
//       });
//     },
//     scheduleRenewal() {
//       const expiresAt = this.expiresAt;
//       const delay = expiresAt - Date.now();
//       tokenRenewalTimeout = {};

//       if (delay >= -60) {
//         tokenRenewalTimeout = setTimeout(() => {
//           this.renewToken()
//             .then(data => {
//               console.log(data);
//             })
//             .catch(error => {
//               console.log(error);
//             });
//         }, delay);
//       }
//     }
//   }
// });

// export default {
//   install: function(Vue) {
//     Vue.prototype.$auth = auth;
//   }
// };
