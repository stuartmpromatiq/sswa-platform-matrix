using System.Linq;
using Microsoft.AspNetCore.Http;
using SSWA.Admin.App.Extensions;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Services
{


    public class CurrentRequestService : ICurrentRequestService
    {
        public CurrentRequestService(IHttpContextAccessor contextAccessor)
        {
            var request = contextAccessor.HttpContext.Request;
            Domain = $"{request.Scheme}://{request.Host}";
        }

        public string Domain { get; }

    }
}
