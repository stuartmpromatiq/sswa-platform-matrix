using System.Linq;
using Microsoft.AspNetCore.Http;
using SSWA.Admin.App.Extensions;
using SSWA.Paperwork.Core;

namespace SSWA.Paperwork.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor contextAccessor)
        {
            var user = contextAccessor.HttpContext.User;

            Id = user.GetId();
            Name = user.GetName();
            Email = user.GetEmail();
            Phone = user.GetPhone();

            Roles = user.GetRoles();
            Permissions = user.GetPermissions();
        }

        public string Id { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string[] Roles { get; }
        public string[] Permissions { get; }

        public bool UserInRole(string role)
        {
            return Roles.Any(x => x == role);
        }

        public bool UserInRoles(params string[] roles)
        {
            foreach (var role in roles)
            {
                if (Roles.Any(x => x == role))
                {
                    return true;
                }
                else
                {
                    continue;
                }
            }

            return false;
        }

        public bool UserHasPermission(string permission)
        {
            return Permissions.Any(x => x == permission);
        }
    }
}
