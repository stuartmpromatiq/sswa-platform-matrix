using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Paperwork.Models
{
    public class UserListViewModel
    {
        public string ID { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        public string Phone { get; set; }

        [DisplayName("Account Activated")]
        public bool AccountActivated { get; set; }

        [DisplayName("Last Login")]
        public DateTime? LastLogin { get; set; }

        [DisplayName("Login Count")]
        public string LoginCount { get; set; }

        public bool Blocked { get; set; }
    }
}
