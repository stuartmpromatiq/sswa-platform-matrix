using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Models.ViewModels
{
    public class FormTemplateLookup
    {
        public string Type { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Level1 { get; set; }

        public string Level2 { get; set; }

        public string Level3 { get; set; }
    }
}
