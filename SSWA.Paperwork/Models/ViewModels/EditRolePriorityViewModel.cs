using SSWA.ExternalUserService;
using SSWA.Paperwork.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Paperwork.Models
{
    public class EditRolePriorityViewModel
    {
        public EditRolePriorityViewModel()
        {
            Users = new List<UserRolePriority>();
        }

        [Required]
        public string RoleId { get; set; }

        public string RoleName { get; set; }

        public string RoleDisplayName { get; set; }


        public IList<UserRolePriority> Users { get; set; }

        public class RolePriorityUserViewModel
        {
            public RolePriorityUserViewModel()
            {
                Priority = 1;
            }

            [Required]
            public string UserId { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            [Required]
            public int Priority { get; set; }
        }

        public class RolePriorityViewModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
