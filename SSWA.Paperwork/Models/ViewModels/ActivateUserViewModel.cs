using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Paperwork.Models
{
    public class ActivateUserViewModel
    {
        public string Email { get; set; }

        [Required]
        public string UserToken { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        [MinLength(6, ErrorMessage = "Password must be at least 6 characters long")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        [Compare("Password", ErrorMessage = "Password and Confirm Password do not match.")]
        [MinLength(6, ErrorMessage = "Password must be at least 6 characters long")]
        public string ConfirmPassword { get; set; }

        [Required]
        public bool PendingActivation { get; set; }
    }
}
