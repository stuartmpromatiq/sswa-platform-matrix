using Newtonsoft.Json;
using SSWA.Paperwork.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Paperwork.Models
{
    public class UserRolePriority : BaseEntity
    {
        public override string Type => "user_role_priority";

        [JsonProperty("userId")]
        public string UserId { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [JsonProperty("roleId")]
        public string RoleId { get; set; }

        [JsonProperty("priority")]
        public int Priority { get; set; }
    }
}
