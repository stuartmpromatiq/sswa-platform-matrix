﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace SSWA.Database
{
    public class SswaContextFactory : IDesignTimeDbContextFactory<SswaContext>
    {
        public SswaContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<SswaContext>();
            builder.UseSqlServer(config.GetConnectionString("DefaultConnection"));

            return new SswaContext(builder.Options);
        }
    }
}
