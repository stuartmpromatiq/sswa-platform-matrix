﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;

namespace SSWA.Paperwork.Functions
{
    public static class JobStartup
    {
        public static CosmodDbOptions GetCosmosDbOptions(IConfigurationRoot config) => new CosmodDbOptions(
                        config["CosmosDb:DatabaseName"],
                        config["CosmosDb:CollectionName"]
                    );
        public static DocumentClient GetDocClient(IConfigurationRoot config) =>
                    new DocumentClient(
                                new Uri(config["CosmosDb:Domain"]),
                                config["CosmosDb:ApiKey"],
                                serializerSettings: new JsonSerializerSettings()
                                {
                                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                                    Converters = new List<JsonConverter>() { new FormConverter(), new FormTemplateConverter() }
                                }
                            );
        public static IConfigurationRoot GetIConfigurationRoot(ExecutionContext context) => new ConfigurationBuilder()
                        .SetBasePath(context.FunctionAppDirectory)
                        .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                        .AddEnvironmentVariables()
                        .Build();

        public static IMemoryCache GetMemoryCache()
        {
            return new MemoryCache(new MemoryCacheOptions());
        }
    }
}
