using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using System.Linq;
using Microsoft.Azure.Documents.SystemFunctions;
using Microsoft.Extensions.Caching.Memory;

namespace SSWA.Paperwork.Functions
{
    public static class AddNextSiteVisit
    {
        [FunctionName("add-next-site-visit")]
        public static async Task Run(
            [TimerTrigger("0 0 * * * *")] TimerInfo myTimer,
            ILogger log,
            ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();


            var formService = new CosmosDbService<PlantEntryRequestForm>(docClient, cosmosDbOptions, cache);
            var personService = new CosmosDbService<Person>(docClient, cosmosDbOptions, cache);

            var forms = await formService.QueryAsync("form", x => x.Kind.Code == "PER" && x.Status == FormState.APPROVED);

            foreach (var form in forms)
            {
                foreach (var visitor in form.Visitors.Where(x => !string.IsNullOrWhiteSpace(x.Id)))
                {

                    var startDate = (visitor.VisitDuration.FirstOrDefault() == DateTime.MinValue ? form.StartDate : visitor.VisitDuration.FirstOrDefault()).Value;

                    Person person = null;
                    try
                    {
                        person = await personService.GetAsync(visitor.Id);
                    }
                    catch (Exception ex)
                    {
                        log.LogError(ex, $"Could not find visitor with id {visitor.Id}");
                        continue;
                    }

                    if (person == null)
                        continue;

                    if (person.NextVisit == null || person.NextVisit > startDate)
                    {
                        person.NextVisit = startDate;
                        person.NextPerId = form.Id;
                        person.NextPerFriendlyId = form.FriendlyId;

                        try
                        {
                            await personService.UpdateAsync(person);
                            log.LogInformation($"{person.Name} updated with next site visit {person.NextVisit}");
                        }
                        catch (Exception ex)
                        {
                            log.LogError(ex, $"{person.Name} not updated.");
                        }

                    }

                }
            }



        }

    }


}
