using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using System.Linq;
using SSWA.Paperwork.Common;
using static SSWA.Paperwork.Common.Notification;
using SSWA.Paperwork.Common.Forms;
using Microsoft.Extensions.Caching.Memory;

namespace SSWA.Paperwork.Functions
{
    public static class SendReminderForUnapprovedForms
    {
        private static EmailService _emailService;
        [FunctionName("send-reminder-for-unapproved-forms")]
        public static async Task Run([TimerTrigger("0 0 * * * *")] TimerInfo myTimer, ILogger<EmailService> log, ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();

            _emailService = new EmailService(config["SendGrid:ApiKey"], log);

            var service = new DocumentService<IForm>(
                new CosmosDbService<IForm>(
                    docClient,
                    cosmosDbOptions,
                    cache
                )
            );
            var userNotificationService = new CosmosDbService<Notification>(docClient, cosmosDbOptions, cache);

            var predicates = new List<Expression<Func<IForm, bool>>>();

            predicates.Add(x => x.Kind.Code != "ATW");
            predicates.Add(x => x.Status == FormState.PENDING_APPROVAL || x.Status == FormState.PARTIALLY_APPROVED);
            predicates.Add(x => x.StartDate <= DateTime.Now.ToUniversalTime().Date.AddDays(3) && x.StartDate >= DateTime.Now.ToUniversalTime().Date.AddDays(-1));

            var forms = await service.QueryAsync(predicates.ToArray());

            foreach (var form in forms)
            {
                if (form.Kind.Code == "PER")
                {
                    var mgrApproval = form.Approvers
                                                .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER)
                                                .All(x => x.IsApproved.GetValueOrDefault(false));
                    var opsOrMntApproval = form.Approvers
                                               .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                               .Any(x => x.IsApproved.GetValueOrDefault(false));

                    if (!opsOrMntApproval)
                    {
                        var approvers = form.Approvers
                                               .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER) && x.ActionedAt == null);
                        await Remind(log, userNotificationService, form, approvers);
                    }

                    if (opsOrMntApproval && !mgrApproval)
                    {
                        var approvers = form.Approvers
                                               .Where(x => x.User != null && (x.Role.Id == Roles.ALLIANCE_MANAGER) && x.ActionedAt == null);
                        await Remind(log, userNotificationService, form, approvers);
                    }
                }
                else if (form.Kind.Code == "PWR")
                {
                    var mgrApproval = form.Approvers
                            .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER)
                            .All(x => x.IsApproved.GetValueOrDefault(false));

                    var opsAndMntApproval = form.Approvers
                                               .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                               .All(x => x.IsApproved.GetValueOrDefault(false));

                    if (!opsAndMntApproval)
                    {
                        var approvers = form.Approvers
                                               .Where(x => x.User != null)
                                               .Where(x => x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER)
                                               .Where(x => x.IsApproved == null);
                        await Remind(log, userNotificationService, form, approvers);

                    }

                    if (opsAndMntApproval && !mgrApproval)
                    {
                        var approvers = form.Approvers
                            .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER && x.IsApproved == null);

                        await Remind(log, userNotificationService, form, approvers);
                    }
                }
            }
        }

        private static async Task Remind(ILogger log, CosmosDbService<Notification> userNotificationService, IForm form, IEnumerable<FormApproval> approvers)
        {
            foreach (var approver in approvers)
            {
                var lastNotification = await userNotificationService.QueryPagedAsync("notification", 1, null, x => x.OrderByDescending(y => y.CreatedAt), x => x.RecipientId == approver.User.Id, x => x.Message.Contains(form.FriendlyId));

                var lastSent = lastNotification.Data.FirstOrDefault()?.CreatedAt;

                if (lastSent == null || DateTime.Now.ToUniversalTime() - lastSent.Value >= TimeSpan.FromHours(12))
                {
                    var company = "";

                    switch (form.Kind.Code)
                    {
                        case "PER":
                            var per = (PlantEntryRequestForm)form;
                            company = per.Visitors.FirstOrDefault()?.Company;
                            break;
                        case "PWR":
                            var pwr = (PlantWorksRequestForm)form;
                            company = pwr.PrincipalContractor;
                            break;
                    }

                    company = string.IsNullOrWhiteSpace(company) ? "N/A" : company;
                    var subject = $"[REMINDER] {form.Kind.Code} by {form.CreatedBy.Name} for {company}";

                    var userNotification = new Notification()
                    {
                        CreatedAt = DateTime.UtcNow,
                        UpdatedAt = DateTime.UtcNow,
                        RecipientId = approver.User.Id,
                        Recipient = approver.User,
                        Subject = subject,
                        Message = $"{form.CreatedBy.Name} has submitted a {form.Kind.Name} ({form.FriendlyId}) for you to review & approve." +
                              $"<br /><br />Request: <em>{form.Description}</em>",
                        Action = new NotificationAction()
                        {
                            Domain = "https://sswa.azurewebsites.net",
                            Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                            Display = "View Form"
                        }
                    };

                    try
                    {
                        await _emailService.SendAsync(userNotification);
                        await userNotificationService.AddAsync(userNotification);

                        log.LogInformation($"Reminder notification for {form.FriendlyId} sent to {approver.User.Name}");
                    }
                    catch (Exception ex)
                    {
                        log.LogError(ex, $"Could not send reminder notification for {form.FriendlyId} to {approver.User.Name}");
                    }
                }
            }
        }
    }
}
