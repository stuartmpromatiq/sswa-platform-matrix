using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using static SSWA.Paperwork.Common.Notification;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;

namespace SSWA.Paperwork.Functions
{
    public static class ExpirePCWInductionsWhereLastPERMoreThanTwoMonthsAgo
    {
        class LatestPer : BaseEntity, IEntity
        {
            public string Name { get; set; }
            public DateTime? LatestPerEndDate { get; set; }

            public override string Type => "latest_per";
        }

        class VisitorInduction : BaseEntity, IEntity
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? ExpiryDate { get; set; }

            public override string Type => "visitor_induction";
        }

        [FunctionName("expire-pcw-inductions-where-last-per-more-than-two-months-ago")]
        public static async Task Run([TimerTrigger("0 8 * * *")] TimerInfo myTimer, ILogger<EmailService> log, ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();


            var service = new DocumentService<IForm>(
                new CosmosDbService<IForm>(
                    docClient,
                    cosmosDbOptions,
                    cache
                )
            );

            var latestPer = new CosmosDbService<LatestPer>(
                    docClient,
                    cosmosDbOptions,
                    cache
                );

            var visitorInduction = new CosmosDbService<VisitorInduction>(
                    docClient,
                    cosmosDbOptions,
                    cache
                );

            var sql = @"
                SELECT a.id, a.name, max(c.endDate) as latestPerEndDate 
                FROM c 
                JOIN a IN c.visitors 
                WHERE a.id != null and c.requestType = 'PCW' 
                GROUP BY a.id, a.name
            ";
            var latestPers = await latestPer.SqlQueryAsync(sql);

            sql = @"
                SELECT c.id, c.name, c.email, t.startDate, t.expiryDate 
                FROM c 
                JOIN t IN c.latestTraining 
                WHERE c.type = 'person' and t.type = 'Induction' and t.trainingName = 'PCW' and t.expiryDate > (select value GetCurrentDateTime())
            ";
            var futureExpiries = await visitorInduction.SqlQueryAsync(sql);

            foreach(var induction in futureExpiries)
            {
                log.LogInformation(induction.Name);

                if (!latestPers.Any(x => x.Id == induction.Id) || 
                    latestPers.FirstOrDefault(x => x.Id == induction.Id)?.LatestPerEndDate < DateTime.UtcNow.AddMonths(-2))
                {
                    log.LogInformation(latestPers.FirstOrDefault(x => x.Id == induction.Id)?.LatestPerEndDate?.ToString());
                    log.LogInformation(induction.ExpiryDate?.ToString());

                    Console.WriteLine(induction.Name + " " + induction.ExpiryDate);
                }
            }

            var emailService = new EmailService(config["SendGrid:ApiKey"], log);

           

            //log.LogInformation($"Start Param: {startDate}");
            //log.LogInformation($"End Param: {endDate}");
            //log.LogInformation($"End Plus One Param: {endDatePlusOne}");

            //var forms = await service.QueryAsync(predicates.ToArray());



            //foreach (var form in forms)
            //{
            //    log.LogInformation(form.FriendlyId);
            //    var userNotification = new Notification()
            //    {
            //        CreatedAt = DateTime.UtcNow,
            //        UpdatedAt = DateTime.UtcNow,
            //        RecipientId = form.CreatedBy.Id,
            //        Recipient = new User() { Id = "123123", Name = "Liam", Email = "liamw@promatiq.com" },
            //        Subject = $"{form.FriendlyId} finishes in 7 days",
            //        Message = $"Your {form.Kind.Name} ({form.FriendlyId}) is due to be completed in 7 days.",
            //        Action = new NotificationAction()
            //        {
            //            Domain = "https://sswa.azurewebsites.net",
            //            Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
            //            Display = "View Form"
            //        }
            //    };

            //    await emailService.SendAsync(userNotification);
            //}
        }
    }
}
