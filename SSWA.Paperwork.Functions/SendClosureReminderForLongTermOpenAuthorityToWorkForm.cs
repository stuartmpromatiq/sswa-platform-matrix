using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using static SSWA.Paperwork.Common.Notification;
using Microsoft.Extensions.Caching.Memory;

namespace SSWA.Paperwork.Functions
{
    public static class SendClosureReminderForLongTermOpenAuthorityToWorkForm
    {
        [FunctionName("send-closure-reminder-for-long-term-open-authority-to-work-form")]
        public static async Task Run([TimerTrigger("0 8 * * *")] TimerInfo myTimer, ILogger<EmailService> log, ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();


            var service = new DocumentService<IForm>(
                new CosmosDbService<IForm>(
                    docClient,
                    cosmosDbOptions,
                    cache
                )
            );

            var emailService = new EmailService(config["SendGrid:ApiKey"], log);

            var predicates = new List<Expression<Func<IForm, bool>>>();
            var startDate = DateTime.Now.AddDays(-23).Date.ToUniversalTime();
            var endDate = DateTime.Now.AddDays(7).Date.ToUniversalTime();
            var endDatePlusOne = endDate.AddDays(1);

            predicates.Add(x => x.Kind.Code == "ATW");
            predicates.Add(x => x.Status == FormState.OPENED);
            predicates.Add(x => x.StartDate.Value <= startDate);
            predicates.Add(x => x.EndDate.Value >= endDate);
            predicates.Add(x => x.EndDate.Value < endDatePlusOne);

            log.LogInformation($"Start Param: {startDate}");
            log.LogInformation($"End Param: {endDate}");
            log.LogInformation($"End Plus One Param: {endDatePlusOne}");

            var forms = await service.QueryAsync(predicates.ToArray());



            foreach (var form in forms)
            {
                log.LogInformation(form.FriendlyId);
                var userNotification = new Notification()
                {
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    RecipientId = form.CreatedBy.Id,
                    Recipient = new User() { Id = "123123", Name = "Liam", Email = "liamw@promatiq.com" },
                    Subject = $"{form.FriendlyId} finishes in 7 days",
                    Message = $"Your {form.Kind.Name} ({form.FriendlyId}) is due to be completed in 7 days.",
                    Action = new NotificationAction()
                    {
                        Domain = "https://sswa.azurewebsites.net",
                        Path = $"/forms/{form.Kind.Slug}/detail/{form.Id}",
                        Display = "View Form"
                    }
                };

                await emailService.SendAsync(userNotification);
            }
        }
    }
}
