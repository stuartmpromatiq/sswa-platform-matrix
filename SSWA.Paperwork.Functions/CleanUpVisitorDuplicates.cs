using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using System.Linq;

namespace SSWA.Paperwork.Functions
{
    public static class CleanUpVisitorDuplicates
    {
        //[FunctionName("clean-up-visitor-duplicates")]
        //public static async Task Run(
        //    [TimerTrigger("0 0 * * * *")] TimerInfo myTimer,
        //    ILogger log,
        //    ExecutionContext context)
        //{
        //    IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
        //    DocumentClient docClient = JobStartup.GetDocClient(config);
        //    CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);

        //    var personService = new CosmosDbService<Person>(docClient, cosmosDbOptions);

        //    var people = await personService.QueryAsync("person");

        //    var duplicates = people.GroupBy(x => x.CanonicalName).Where(x => x.Count() > 1);

        //    foreach (var duplicateGroup in duplicates)
        //    {
        //        var rankedDuplicates = duplicateGroup.Select(x => new { Person = x, Rank = RankPerson(x) }).OrderByDescending(x => x.Rank);
        //        var toDelete = rankedDuplicates.Last().Person;

        //        await personService.RemoveAsync(toDelete.Id);
        //    }         
        //}

        public static int RankPerson(Person person)
        {
            int rank = 0;

            if (!string.IsNullOrWhiteSpace(person.CanonicalCompany))
            {
                rank += 10;
            }

            if (!string.IsNullOrWhiteSpace(person.CanonicalCategory))
            {
                rank += 10;
            }

            if (!string.IsNullOrWhiteSpace(person.Email))
            {
                rank += 10;
            }

            if (!string.IsNullOrWhiteSpace(person.ContactNumber))
            {
                rank += 10;
            }

            if (!string.IsNullOrWhiteSpace(person.VehicleRegistration))
            {
                rank += 10;
            }

            if (person.LatestTraining.Any())
            {
                rank += 10;
            }

            if (person.ArchivedTraining.Any())
            {
                rank += 10;
            }

            return rank;
        }



    }


}
