using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using System.Linq;
using Microsoft.Azure.Documents.SystemFunctions;
using Microsoft.Extensions.Caching.Memory;

namespace SSWA.Paperwork.Functions
{
    public static class AddSearchableDescriptions
    {
        [FunctionName("add-searchable-descriptions")]
        public static async Task Run(
            [TimerTrigger("0 0 * * * *")] TimerInfo myTimer,
            ILogger log,
            ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();


            var processLockService = new CosmosDbService<ProcessLock>(docClient, cosmosDbOptions, cache);

            var locks = await processLockService.QueryAsync("process_lock", x => !x.CanonicalReason.IsDefined());

            foreach (var @lock in locks)
            {
                await processLockService.UpdateAsync(@lock);
            }

            var formService = new CosmosDbService<IForm>(docClient, cosmosDbOptions, cache);

            string continuationToken = null;
            PagedResult<IForm> forms = null;

            do
            {
                forms = await formService.QueryPagedAsync("form", pageSize: 100, continuationToken: continuationToken, sort: null, predicates: x => !x.CanonicalDescription.IsDefined());
                continuationToken = forms.NextPageToken;

                foreach (var form in forms.Data)
                {
                    await formService.UpdateAsync(form);
                }

            } while (forms.HasMoreData);




        }

    }


}
