using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using SSWA.Paperwork.Common;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace SSWA.Paperwork.Functions
{
    public static class AddInductionsToApprovedForms
    {
        [FunctionName("add-inductions-to-approved-forms")]
        public static async Task Run(
            [TimerTrigger("0 0 * * * *")] TimerInfo myTimer,
            ILogger log,
            ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();


            var service = new DocumentService<PlantEntryRequestForm>(
                new CosmosDbService<PlantEntryRequestForm>(
                    docClient,
                    cosmosDbOptions,
                    cache
                )
            );

            var trainingService = new CosmosDbService<VisitorTraining>(docClient, cosmosDbOptions, cache);
            var personService = new CosmosDbService<Person>(docClient, cosmosDbOptions, cache);

            var predicates = new List<Expression<Func<PlantEntryRequestForm, bool>>>();

            predicates.Add(x => x.Kind.Code == "PER");
            predicates.Add(x => x.Status == FormState.APPROVED);
            predicates.Add(x => x.StartDate == DateTime.Now.ToUniversalTime().Date);


            var forms = await service.QueryAsync(predicates.ToArray());

            foreach (var form in forms)
            {
                foreach (var visitor in form.Visitors)
                {
                    var trainingTemplates = await trainingService.QueryAsync("visitor_training");
                    var training = new PersonTraining();

                    if (!string.IsNullOrWhiteSpace(visitor.AccessCardNo))
                    {
                        var person = await personService.GetAsync(visitor.Id);

                        if (!person.CheckInduction(form))
                        {
                            var template = trainingTemplates.Single(x => x.TrainingType == "Induction" && x.Name == form.RequestType);

                            training = new PersonTraining()
                            {
                                Type = "Induction",
                                TrainingName = form.RequestType,
                                StartDate = form.StartDate.GetValueOrDefault(DateTime.Today),
                            };

                            SetExpiry(training, template);

                            person.AddTraining(training);
                        }

                        foreach (var awareness in form.AwarenessTraining)
                        {
                            if (!person.CheckAwareness(form, awareness))
                            {
                                var template = trainingTemplates.Single(x => x.TrainingType == "Awareness" && x.Name == awareness);

                                training = new PersonTraining()
                                {
                                    Type = "Awareness",
                                    TrainingName = awareness,
                                    StartDate = form.StartDate.GetValueOrDefault(DateTime.Today),
                                };

                                person.AddTraining(training);
                            }
                        }

                        await personService.UpdateAsync(person);
                    }
                }

                await service.UpdateAsync(form);
            }
        }

        private static void SetExpiry(PersonTraining personTraining, VisitorTraining trainingTemplate)
        {
            if (trainingTemplate.Expiry != null && trainingTemplate.ExpiryUnit != null)
            {
                if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "hour")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddHours(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "day")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddDays(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "month")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddMonths(trainingTemplate.Expiry.Value);
                }
                else if (trainingTemplate.ExpiryUnit.ToLowerInvariant() == "year")
                {
                    personTraining.ExpiryDate = personTraining.StartDate.Value.AddYears(trainingTemplate.Expiry.Value);
                }
            }
        }
    }


}
