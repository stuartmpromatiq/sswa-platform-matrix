using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SSWA.Paperwork.Common.Infrastructure;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;
using SSWA.Paperwork.Common.Forms;

namespace SSWA.Paperwork.Functions
{
    public static class MarkFormsAsComplete
    {
        [FunctionName("mark-forms-as-complete")]
        public static async Task Run([TimerTrigger("0 */5 * * * *")] TimerInfo myTimer, ILogger log, ExecutionContext context)
        {
            IConfigurationRoot config = JobStartup.GetIConfigurationRoot(context);
            DocumentClient docClient = JobStartup.GetDocClient(config);
            CosmodDbOptions cosmosDbOptions = JobStartup.GetCosmosDbOptions(config);
            IMemoryCache cache = JobStartup.GetMemoryCache();

            var service = new DocumentService<IForm>(
                new CosmosDbService<IForm>(
                    docClient,
                    cosmosDbOptions,
                    cache
                )
            );

            var predicates = new List<Expression<Func<IForm, bool>>>();

            predicates.Add(x => x.Kind.Code != "ATW");
            predicates.Add(x => x.Status == FormState.APPROVED);
            predicates.Add(x => x.EndDate < DateTime.Now.ToUniversalTime().Date.AddDays(-2));

            var forms = await service.QueryAsync(predicates.ToArray());

            foreach (var form in forms)
            {
                log.LogInformation($"Completing form {form.FriendlyId} ending on {form.EndDate}.");
                form.Status = FormState.COMPLETED;
                await service.UpdateAsync(form);
            }


            predicates.Clear();
            predicates.Add(x => x.Kind.Code != "ATW");
            predicates.Add(x => x.Status == FormState.PARTIALLY_APPROVED || x.Status == FormState.PENDING_APPROVAL);

            var partiallyApprovedForms = await service.QueryAsync(predicates.ToArray());

            foreach (var form in partiallyApprovedForms)
            {
                var mgrApproval = form.Approvers
                                                  .Where(x => x.User != null && x.Role.Id == Roles.ALLIANCE_MANAGER)
                                                  .All(x => x.IsApproved.GetValueOrDefault(false));
                var opsOrMntApproval = form.Approvers
                                           .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                           .Any(x => x.IsApproved.GetValueOrDefault(false));

                var opsAndMntApproval = form.Approvers
                                          .Where(x => x.User != null && (x.Role.Id == Roles.OPERATIONS_MANAGER || x.Role.Id == Roles.MAINTENANCE_MANAGER))
                                          .All(x => x.IsApproved.GetValueOrDefault(false));


                if (form is PlantEntryRequestForm && mgrApproval && opsOrMntApproval)
                {
                    log.LogInformation($"Approving form {form.FriendlyId}.");
                    form.Status = FormState.APPROVED;
                    await service.UpdateAsync(form);
                }
                else if (form is PlantWorksRequestForm && mgrApproval && opsAndMntApproval)
                {
                    log.LogInformation($"Approving form {form.FriendlyId}.");
                    form.Status = FormState.APPROVED;
                    await service.UpdateAsync(form);
                }
            }
        }
    }
}
