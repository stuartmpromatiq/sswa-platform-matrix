﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.ExternalUserService
{
    public class AuthorizationApiClient : IAuthorizationApiClient
    {
        private HttpClient _client;

        public AuthorizationApiClient(string accessToken, string api)
        {
            _client = new HttpClient();

            if (!api.EndsWith("/"))
                api += "/";

            _client.BaseAddress = new Uri(api);
            _client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessToken}");
        }

        public async Task<IEnumerable<ExternalRole>> GetUserRolesAsync(string userId)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_client.BaseAddress, $"users/{userId}/roles"));
            request.Headers.Add("Accept", "application/json");

            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var payload = JsonConvert.DeserializeObject<IEnumerable<ExternalRole>>(json);
                return payload;
            }

            return new List<ExternalRole>();
        }

        public async Task UpdateUserRolesAsync(string userId, IEnumerable<string> roleIds)
        {
            var request = new HttpRequestMessage(new HttpMethod("PATCH"), new Uri(_client.BaseAddress, $"users/{userId}/roles"));
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent(JsonConvert.SerializeObject(roleIds), Encoding.UTF8, "application/json");

            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return;
            }
        }

        public async Task DeleteUserRolesAsync(string userId, IEnumerable<string> roleIds)
        {
            var request = new HttpRequestMessage(new HttpMethod("DELETE"), new Uri(_client.BaseAddress, $"users/{userId}/roles"));
            request.Headers.Add("Accept", "application/json");
            request.Content = new StringContent(JsonConvert.SerializeObject(roleIds), Encoding.UTF8, "application/json");

            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return;
            }
        }

        public async Task<IEnumerable<ExternalRole>> GetRolesAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_client.BaseAddress, "roles"));
            request.Headers.Add("Accept", "application/json");

            try
            {
                var response = await _client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var payload = JsonConvert.DeserializeObject<GetAllRolesResponse>(json);
                    return payload.Roles;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }


            return new List<ExternalRole>();
        }

        public async Task GetUsersAsync()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_client.BaseAddress, "users"));
            request.Headers.Add("Accept", "application/json");

            var response = await _client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                var payload = JsonConvert.DeserializeObject<GetAllRolesResponse>(json);
            }

        }
    }
}
