﻿using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Mail;
using System.Net;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Diagnostics;
using System.Dynamic;
using Newtonsoft.Json;

namespace SSWA.ExternalUserService
{
    public class Auth0UserService : IExternalUserService
    {
        private Auth0UserServiceConfiguration _configuration;

        private IAuthenticationApiClient _authenticationApiClient;
        private IManagementApiClient _managementApiClient;
        private IAuthorizationApiClient _authorizationApiClient;

        public Auth0UserService(Auth0UserServiceConfiguration configuration)
        {
            _configuration = configuration;
            _authenticationApiClient = new AuthenticationApiClient(_configuration.Domain);
        }

        public async Task<IEnumerable<ExternalUser>> GetAllUsersAsync()
        {
            var client = await CreateManagementClientAsync();
            var getUsersRequest = new GetUsersRequest()
            {
                Connection = _configuration.Connection,
                SearchEngine = "v3",
                Sort = "email:1",
            };

            var auth0Users = await client.Users.GetAllAsync(getUsersRequest, new PaginationInfo(0, 100, false));
            return auth0Users.Select(x => MapFromAuth0User(x));
        }

        public async Task<IEnumerable<ExternalUser>> GetUsersByQueryAsync(string query)
        {
            var client = await CreateManagementClientAsync();
            var getUsersRequest = new GetUsersRequest()
            {
                Connection = _configuration.Connection,
                SearchEngine = "v3",
                Sort = "email:1",
                Query = query
            };

            var auth0Users = await client.Users.GetAllAsync(getUsersRequest, new PaginationInfo(0, 100, false));
            return auth0Users.Select(x => MapFromAuth0User(x));
        }

        public async Task<ExternalUser> GetUserAsync(string id)
        {
            var client = await CreateManagementClientAsync();
            var auth0User = await client.Users.GetAsync(id);

            return MapFromAuth0User(auth0User);
        }

        public async Task<ExternalUser> GetUserByEmailAsync(string email)
        {
            var client = await CreateManagementClientAsync();
            var auth0User = await client.Users.GetUsersByEmailAsync(email);

            return MapFromAuth0User(auth0User.FirstOrDefault());
        }

        public async Task<IEnumerable<ExternalRole>> GetAllRolesAsync()
        {
            var client = await CreateAuthorizationClient();

            var roles = await client.GetRolesAsync();

            return roles.Where(x => x.ApplicationId == _configuration.AppId).ToList();
        }

        public async Task GetAuthUsers()
        {
            var client = await CreateAuthorizationClient();

            await client.GetUsersAsync();
        }

        public async Task<IEnumerable<ExternalRole>> GetUserRolesAsync(string userId)
        {
            var client = await CreateAuthorizationClient();

            var roles = await client.GetUserRolesAsync(userId);

            return roles.Where(x => x.ApplicationId == _configuration.AppId).ToList();
        }

        public async Task<IEnumerable<ExternalRole>> UpdateUserRolesAsync(string userId, IEnumerable<ExternalRole> addRoles, IEnumerable<ExternalRole> deleteRoles)
        {
            var client = await CreateAuthorizationClient();

            var addRoleIds = addRoles.Select(x => x.Id);
            var deleteRoleIds = deleteRoles.Select(x => x.Id);
            await client.UpdateUserRolesAsync(userId, addRoleIds);

            await client.DeleteUserRolesAsync(userId, deleteRoleIds);

            return await GetUserRolesAsync(userId);
        }

        public async Task<ExternalUser> InviteUserAsync(ExternalUser user, string activationCallback)
        {
            var client = await CreateManagementClientAsync();
            var randomPassword = Guid.NewGuid().ToString();
            var metadata = new
            {
                activation_pending = true
            };

            var request = new UserCreateRequest()
            {
                Connection = _configuration.Connection,
                Email = user.Email,
                Password = randomPassword,
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailVerified = true,
                AppMetadata = metadata,
                UserMetadata = new
                {
                    given_name = user.FirstName,
                    family_name = user.LastName,
                    phone = user.Phone
                }
            };
            User auth0User = new User();
            try
            {
                auth0User = await client.Users.CreateAsync(request);
            }
            catch (Exception ex)
            {
                var users = await client.Users.GetUsersByEmailAsync(user.Email);
                auth0User = users.FirstOrDefault();

                if (auth0User == null)
                    throw ex;
            }

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var payload = new { id = auth0User.UserId, email = auth0User.Email };
            var token = encoder.Encode(payload, _configuration.SigningKey);

            var verificationUrlTicket = await client.Tickets.CreateEmailVerificationTicketAsync(
                new EmailVerificationTicketRequest
                {
                    UserId = auth0User.UserId,
                    ResultUrl = $"{activationCallback}?userToken={token}"
                }
            );


            var htmlMessage = $"Hello {auth0User.FirstName} {auth0User.LastName},<br /><br />" +
                              $"You have been invited to join the {_configuration.AppName}.<br /><br />" +
                              $"To login you first need to activate your account. You can do this by clicking on the button below and setting up a password.<br/>" +
                              $"You can then login using your email address ({auth0User.Email}) and password.<br /><br />" +
                              $"If you forget your password you can reset it by clicking on the Login button and following the 'Don't remember your password?' prompts.";

            var message = new SendGridMessage();
            message.AddTo(auth0User.Email);
            message.From = new EmailAddress("sswa-platform@iqdatasoftware.com", "SSWA Platform");
            message.Subject = _configuration.AppName + " Invitation";
            message.HtmlContent = String.Format(EMAIL_BODY, $@"Welcome!", htmlMessage, verificationUrlTicket.Value, "Activate Account");
            var sendgrid = new SendGridClient(_configuration.SendgridApiKey);

            var response = await sendgrid.SendEmailAsync(message);

            return MapFromAuth0User(auth0User);
        }

        public async Task<ExternalUser> ActivateUserAsync(string id, string password)
        {
            var client = await CreateManagementClientAsync();
            var request = new UserUpdateRequest()
            {
                Password = password
            };
            var auth0User = await client.Users.UpdateAsync(id, request);

            return MapFromAuth0User(auth0User);
        }

        public async Task<ExternalUser> UpdateMetadataAsync(string id, dynamic metadata)
        {
            var client = await CreateManagementClientAsync();
            var request = new UserUpdateRequest()
            {
                AppMetadata = metadata
            };
            var auth0User = await client.Users.UpdateAsync(id, request);

            return MapFromAuth0User(auth0User);
        }

        public async Task<ExternalUser> UpdateUserAsync(ExternalUser user)
        {

            var client = await CreateManagementClientAsync();
            var jsonMetadata = JsonConvert.SerializeObject(user.AppMetadata);
            ExternalAppMetadata appMetadata = JsonConvert.DeserializeObject<ExternalAppMetadata>(jsonMetadata);

            if (appMetadata.authorization == null)
            {
                appMetadata.authorization = new List<ExternalAuthorization>();
            }

            var appAuthorization = appMetadata.authorization.FirstOrDefault(x => x.client == _configuration.AppId);
            if (appAuthorization == null)
            {
                appMetadata.authorization.Add(new ExternalAuthorization()
                {
                    client = _configuration.AppId,
                    roles = user.AppRoles.Select(x => x.Name).ToArray(),
                    permissions = new string[0]
                });
            }
            else
            {
                appAuthorization.roles = user.AppRoles.Select(x => x.Name).ToArray();
            }

            var request = new UserUpdateRequest()
            {
                UserMetadata = new { given_name = user.FirstName, family_name = user.LastName, phone = user.Phone },
                AppMetadata = appMetadata
            };

            try
            {
                var auth0User = await client.Users.UpdateAsync(user.Identifier, request);

                return MapFromAuth0User(auth0User);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ExternalUser> BlockUserAsync(string id)
        {
            var client = await CreateManagementClientAsync();
            var request = new UserUpdateRequest()
            {
                Blocked = true
            };
            var auth0User = await client.Users.UpdateAsync(id, request);

            return await GetUserAsync(id);
        }

        public async Task<ExternalUser> UnblockUserAsync(string id)
        {
            var client = await CreateManagementClientAsync();
            var request = new UserUpdateRequest()
            {
                Blocked = false
            };
            var auth0User = await client.Users.UpdateAsync(id, request);

            return await GetUserAsync(id);
        }

        private async Task<IManagementApiClient> CreateManagementClientAsync()
        {
            if (_managementApiClient == null)
            {
                var tokenResponse = await GetAccessTokenAsync($"https://{_configuration.Domain}/api/v2/");
                _managementApiClient = new ManagementApiClient(tokenResponse.AccessToken, _configuration.Domain);
            }

            return _managementApiClient;
        }

        private async Task<IAuthorizationApiClient> CreateAuthorizationClient()
        {
            //var api = "https://sswa.au8.webtask.io/adf6e2f2b84784b57522e3b19dfc9201/api";
            var api = "https://sswa.au.webtask.io/adf6e2f2b84784b57522e3b19dfc9201/api";

            if (_authorizationApiClient == null)
            {
                var tokenResponse = await GetAccessTokenAsync("urn:auth0-authz-api");
                _authorizationApiClient = new AuthorizationApiClient(tokenResponse.AccessToken, api);
            }

            return _authorizationApiClient;
        }

        private async Task<AccessTokenResponse> GetAccessTokenAsync(string audience)
        {
            return await _authenticationApiClient.GetTokenAsync(new ClientCredentialsTokenRequest()
            {
                Audience = audience,
                ClientId = _configuration.ClientId,
                ClientSecret = _configuration.ClientSecret
            });
        }

        private ExternalUser MapFromAuth0User(Auth0.ManagementApi.Models.User auth0User)
        {
            var user = new ExternalUser(
                auth0User.UserId,
                auth0User.Email,
                (string)auth0User.UserMetadata?["phone"],
                (string)auth0User.UserMetadata?["given_name"] ?? auth0User.FirstName,
                (string)auth0User.UserMetadata?["family_name"] ?? auth0User.LastName,
                new string[0],
                auth0User.AppMetadata,
                auth0User.Blocked);

            user.LastLogin = auth0User.LastLogin;
            user.LoginsCount = auth0User.LoginsCount;
            user.EmailVerified = auth0User.EmailVerified.GetValueOrDefault(false);
            return user;
        }

        public const string EMAIL_BODY = @"
        <!DOCTYPE HTML PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional //EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"" xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"">

<head>
    <!--[if gte mso 9]><xml>
     <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
    <meta name=""viewport"" content=""width=device-width"">
    <!--[if !mso]><!-->
    <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-- -->
    <link href=""https://fonts.googleapis.com/css?family=Montserrat"" rel=""stylesheet"" type=""text/css"">
    <!--<![endif]-->

    <style type=""text/css"" id=""media-query"">
        body {{
            margin: 0;
            padding: 0;
        }}

        table,
        tr,
        td {{
            vertical-align: top;
            border-collapse: collapse;
        }}

        .ie-browser table,
        .mso-container table {{
            table-layout: fixed;
        }}

        * {{
            line-height: inherit;
        }}

        a[x-apple-data-detectors=true] {{
            color: inherit !important;
            text-decoration: none !important;
        }}

        [owa] .img-container div,
        [owa] .img-container button {{
            display: block !important;
        }}

        [owa] .fullwidth button {{
            width: 100% !important;
        }}

        [owa] .block-grid .col {{
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }}

        .ie-browser .num12,
        .ie-browser .block-grid,
        [owa] .num12,
        [owa] .block-grid {{
            width: 600px !important;
        }}

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass a,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {{
            line-height: 100%;
        }}

        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {{
            width: 200px !important;
        }}

        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {{
            width: 400px !important;
        }}

        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {{
            width: 300px !important;
        }}

        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {{
            width: 200px !important;
        }}

        .ie-browser .block-grid.four-up .col,
        [owa] .block-grid.four-up .col {{
            width: 150px !important;
        }}

        .ie-browser .block-grid.five-up .col,
        [owa] .block-grid.five-up .col {{
            width: 120px !important;
        }}

        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {{
            width: 100px !important;
        }}

        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {{
            width: 85px !important;
        }}

        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {{
            width: 75px !important;
        }}

        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {{
            width: 66px !important;
        }}

        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {{
            width: 60px !important;
        }}

        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {{
            width: 54px !important;
        }}

        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {{
            width: 50px !important;
        }}

        @media only screen and (min-width: 620px) {{
            .block-grid {{
                width: 600px !important;
            }}
            .block-grid .col {{
                vertical-align: top;
            }}
            .block-grid .col.num12 {{
                width: 600px !important;
            }}
            .block-grid.mixed-two-up .col.num4 {{
                width: 200px !important;
            }}
            .block-grid.mixed-two-up .col.num8 {{
                width: 400px !important;
            }}
            .block-grid.two-up .col {{
                width: 300px !important;
            }}
            .block-grid.three-up .col {{
                width: 200px !important;
            }}
            .block-grid.four-up .col {{
                width: 150px !important;
            }}
            .block-grid.five-up .col {{
                width: 120px !important;
            }}
            .block-grid.six-up .col {{
                width: 100px !important;
            }}
            .block-grid.seven-up .col {{
                width: 85px !important;
            }}
            .block-grid.eight-up .col {{
                width: 75px !important;
            }}
            .block-grid.nine-up .col {{
                width: 66px !important;
            }}
            .block-grid.ten-up .col {{
                width: 60px !important;
            }}
            .block-grid.eleven-up .col {{
                width: 54px !important;
            }}
            .block-grid.twelve-up .col {{
                width: 50px !important;
            }}
        }}

        @media (max-width: 620px) {{
            .block-grid,
            .col {{
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }}
            .block-grid {{
                width: calc(100% - 40px) !important;
            }}
            .col {{
                width: 100% !important;
            }}
            .col>div {{
                margin: 0 auto;
            }}
            img.fullwidth,
            img.fullwidthOnMobile {{
                max-width: 100% !important;
            }}
            .no-stack .col {{
                min-width: 0 !important;
                display: table-cell !important;
            }}
            .no-stack.two-up .col {{
                width: 50% !important;
            }}
            .no-stack.mixed-two-up .col.num4 {{
                width: 33% !important;
            }}
            .no-stack.mixed-two-up .col.num8 {{
                width: 66% !important;
            }}
            .no-stack.three-up .col.num4 {{
                width: 33% !important;
            }}
            .no-stack.four-up .col.num3 {{
                width: 25% !important;
            }}
            .mobile_hide {{
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }}
        }}
    </style>
</head>

<body class=""clean-body"" style=""margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #F5F5Fd"">
    <style type=""text/css"" id=""media-query-bodytag"">
        @media (max-width: 520px) {{
            .block-grid {{
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }}

            .col {{
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }}

            .col>div {{
                margin: 0 auto;
            }}

            img.fullwidth {{
                max-width: 100% !important;
            }}
            img.fullwidthOnMobile {{
                max-width: 100% !important;
            }}
            .no-stack .col {{
                min-width: 0 !important;
                display: table-cell !important;
            }}
            .no-stack.two-up .col {{
                width: 50% !important;
            }}
            .no-stack.mixed-two-up .col.num4 {{
                width: 33% !important;
            }}
            .no-stack.mixed-two-up .col.num8 {{
                width: 66% !important;
            }}
            .no-stack.three-up .col.num4 {{
                width: 33% !important;
            }}
            .no-stack.four-up .col.num3 {{
                width: 25% !important;
            }}
            .mobile_hide {{
                min-height: 0px !important;
                max-height: 0px !important;
                max-width: 0px !important;
                display: none !important;
                overflow: hidden !important;
                font-size: 0px !important;
            }}
        }}
    </style>
    <!--[if IE]><div class=""ie-browser""><![endif]-->
    <!--[if mso]><div class=""mso-container""><![endif]-->
    <table class=""nl-container"" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #F5F5Fd;width: 100%""
        cellpadding=""0"" cellspacing=""0"">
        <tbody>
            <tr style=""vertical-align: top"">
                <td style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top"">
                    <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td align=""center"" style=""background-color: #F5F5Fd;""><![endif]-->

                    <div style=""background-color:transparent;"">
                        <div style=""Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;""
                            class=""block-grid "">
                            <div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
                                <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""background-color:transparent;"" align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 600px;""><tr class=""layout-full-width"" style=""background-color:transparent;""><![endif]-->

                                <!--[if (mso)|(IE)]><td align=""center"" width=""600"" style="" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><![endif]-->
                                <div class=""col num12"" style=""min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;"">
                                    <div style=""background-color: transparent; width: 100% !important;"">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style=""border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"">
                                            <!--<![endif]-->



                                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""divider "" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                <tbody>
                                                    <tr style=""vertical-align: top"">
                                                        <td class=""divider_inner"" style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 30px;padding-left: 30px;padding-top: 30px;padding-bottom: 30px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                            <table class=""divider_content"" height=""0px"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                <tbody>
                                                                    <tr style=""vertical-align: top"">
                                                                        <td style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                            <span>&#160;</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style=""background-color:transparent;"">
                        <div style=""Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;""
                            class=""block-grid "">
                            <div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
                                <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""background-color:transparent;"" align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 600px;""><tr class=""layout-full-width"" style=""background-color:transparent;""><![endif]-->

                                <!--[if (mso)|(IE)]><td align=""center"" width=""600"" style=""background-color:#0091d5; width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><![endif]-->
                                <div class=""col num12"" style=""min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;"">
                                    <div style=""background-color: #0091d5; width: 100% !important;"">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style=""border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;"">
                                            <!--<![endif]-->



                                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""divider "" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                <tbody>
                                                    <tr style=""vertical-align: top"">
                                                        <td class=""divider_inner"" style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 4px;padding-bottom: 4px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                            <table class=""divider_content"" height=""0px"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                <tbody>
                                                                    <tr style=""vertical-align: top"">
                                                                        <td style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                            <span>&#160;</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style=""background-color:transparent;"">
                        <div style=""Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;""
                            class=""block-grid "">
                            <div style=""border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;"">
                                <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""background-color:transparent;"" align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 600px;""><tr class=""layout-full-width"" style=""background-color:#FFFFFF;""><![endif]-->

                                <!--[if (mso)|(IE)]><td align=""center"" width=""600"" style="" width:600px; padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><![endif]-->
                                <div class=""col num12"" style=""min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;"">
                                    <div style=""background-color: transparent; width: 100% !important;"">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style=""border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"">
                                            <!--<![endif]-->


                                            <div align=""center"" class=""img-container center  autowidth  "" style=""padding-right: 0px;  padding-left: 0px;"">
                                                <!--[if mso]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr style=""line-height:0px;line-height:0px;""><td style=""padding-right: 0px; padding-left: 0px;"" align=""center""><![endif]-->
                                                <img class=""center  autowidth "" align=""center"" border=""0"" src=""https://promatiqsswa.blob.core.windows.net/assets/SSWA_logo_lg.png"" alt=""Image""
                                                    title=""Image"" style=""outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 160px""
                                                    width=""160"">
                                                <!--[if mso]></td></tr></table><![endif]-->
                                            </div>


                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style=background-color:transparent;"">
                        <div style=""Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;""
                            class=""block-grid "">
                            <div style=""border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;"">
                                <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""background-color:transparent;"" align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 600px;""><tr class=""layout-full-width"" style=""background-color:#FFFFFF;""><![endif]-->

                                <!--[if (mso)|(IE)]><td align=""center"" width=""600"" style="" width:600px; padding-right: 0px; padding-left: 0px; padding-top:50px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><![endif]-->
                                <div class=""col num12"" style=""min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;"">
                                    <div style=""background-color: transparent; width: 100% !important;"">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style=""border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:50px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"">
                                            <!--<![endif]-->


                                            <div class="""">
                                                <!--[if mso]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;""><![endif]-->
                                                <div style=""line-height:120%;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;"">
                                                    <div style=""line-height:14px;font-size:12px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;"">
                                                        <p style=""margin: 0;line-height: 14px;text-align: center;font-size: 12px"">
                                                            <span style=""font-size: 28px; line-height: 33px;"">
                                                                <b>{0}</b>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                            </div>




                                            <div class="""">
                                                <!--[if mso]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;""><![endif]-->
                                                <div style=""line-height:150%;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; padding-right: 10px; padding-left: 10px; padding-top: 20px; padding-bottom: 10px;"">
                                                    <div style=""font-size:12px;line-height:18px;color:#0D0D0D;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:left;"">
                                                        <p style=""margin: 0;font-size: 14px;line-height: 21px;text-align: center"">{1}</p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                            </div>



                                            <div align=""center"" class=""button-container center "" style=""padding-right: 10px; padding-left: 10px; padding-top:25px; padding-bottom:10px;"">
                                                <div style=""color: #ffffff; background-color: #1c4e80; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 230px; width: 200px;width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent; padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; text-align: center; mso-border-alt: none;"">
                                                    <a href=""{2}"" style=""text-decoration: none; color: #FFFFFF; font-size:16px;line-height:32px;"">{3}</a>
                                                </div>
                                            </div>




                                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""divider "" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                <tbody>
                                                    <tr style=""vertical-align: top"">
                                                        <td class=""divider_inner"" style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 10px;padding-left: 10px;padding-top: 30px;padding-bottom: 10px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                            <table class=""divider_content"" height=""0px"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                <tbody>
                                                                    <tr style=""vertical-align: top"">
                                                                        <td style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                            <span>&#160;</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style=""background-color:transparent;"">
                        <div style=""Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;""
                            class=""block-grid "">
                            <div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
                                <!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""background-color:transparent;"" align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 600px;""><tr class=""layout-full-width"" style=""background-color:transparent;""><![endif]-->

                                <!--[if (mso)|(IE)]><td align=""center"" width=""600"" style="" width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><![endif]-->
                                <div class=""col num12"" style=""min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;"">
                                    <div style=""background-color: transparent; width: 100% !important;"">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style=""border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"">
                                            <!--<![endif]-->



                                            <table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" class=""divider "" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                <tbody>
                                                    <tr style=""vertical-align: top"">
                                                        <td class=""divider_inner"" style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-right: 30px;padding-left: 30px;padding-top: 30px;padding-bottom: 30px;min-width: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                            <table class=""divider_content"" height=""0px"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;border-top: 0px solid transparent;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                <tbody>
                                                                    <tr style=""vertical-align: top"">
                                                                        <td style=""word-break: break-word;border-collapse: collapse !important;vertical-align: top;font-size: 0px;line-height: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%"">
                                                                            <span>&#160;</span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
    <!--[if (mso)|(IE)]></div><![endif]-->


</body>

</html>";
    }
}
