﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.ExternalUserService
{
    public class GetAllRolesResponse
    {
        [JsonProperty("roles")]
        public IEnumerable<ExternalRole> Roles { get; set; }
    }
}
