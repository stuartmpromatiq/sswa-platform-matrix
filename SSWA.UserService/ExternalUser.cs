﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.ExternalUserService
{
    public class ExternalUser
    {
        public ExternalUser(string id, string email, string phone, string firstName, string lastName, ICollection<string> roles, dynamic appMetadata = null, bool blocked = false)
        {
            Id = id;
            Identifier = id;
            Email = email;
            Phone = phone;
            FirstName = firstName;
            LastName = lastName;
            Roles = roles;
            AppMetadata = appMetadata;
            IsActivated = !((bool?)appMetadata?["activation_pending"]).GetValueOrDefault(true);
            Blocked = blocked;
        }
        public string Id { get; set; }
        public string Identifier { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<string> Roles { get; set; }
        public bool IsActivated { get; set; }
        public DateTime? LastLogin { get; set; }
        public string LoginsCount { get; set; }
        public bool EmailVerified { get; set; }
        public dynamic AppMetadata { get; set; }
        public bool Blocked { get; set; }

        public IEnumerable<ExternalRole> AppRoles { get; set; }
        public string Name =>
            !string.IsNullOrWhiteSpace(FirstName) ?
                $"{FirstName} {LastName}" :
                Email;

        public int Priority { get; set; }
    }
}
