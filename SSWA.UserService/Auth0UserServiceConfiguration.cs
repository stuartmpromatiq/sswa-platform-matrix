﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.ExternalUserService
{
    public class Auth0UserServiceConfiguration
    {
        public string AppId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AppName { get; set; }
        public string Domain { get; set; }
        public string Connection { get; set; }
        public string SigningKey { get; set; }
        public string ActivationCallback { get; set; }
        public string SendgridApiKey { get; set; }
        public string AuthzApiUrl { get; set; }
    }
}
