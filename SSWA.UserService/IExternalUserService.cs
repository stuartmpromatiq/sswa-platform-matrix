﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.ExternalUserService
{
    public interface IExternalUserService
    {
        Task<IEnumerable<ExternalUser>> GetAllUsersAsync();
        Task<IEnumerable<ExternalUser>> GetUsersByQueryAsync(string query);
        Task GetAuthUsers();
        Task<ExternalUser> GetUserAsync(string id);
        Task<ExternalUser> ActivateUserAsync(string id, string password);
        Task<ExternalUser> UpdateMetadataAsync(string id, dynamic metadata);
        Task<ExternalUser> InviteUserAsync(ExternalUser user, string activationCallback);
        Task<ExternalUser> UpdateUserAsync(ExternalUser user);
        Task<ExternalUser> BlockUserAsync(string id);
        Task<ExternalUser> UnblockUserAsync(string id);
        Task<IEnumerable<ExternalRole>> GetAllRolesAsync();
        Task<IEnumerable<ExternalRole>> GetUserRolesAsync(string userId);
        Task<IEnumerable<ExternalRole>> UpdateUserRolesAsync(string userId, IEnumerable<ExternalRole> addRoles, IEnumerable<ExternalRole> deleteRoles);
    }
}
