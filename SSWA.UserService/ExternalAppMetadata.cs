﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.ExternalUserService
{
    public class ExternalAppMetadata
    {
        public bool activation_pending { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string name { get; set; }
        public List<ExternalAuthorization> authorization { get; set; }
    }

    public class ExternalAuthorization
    {
        public string client { get; set; }
        public string[] roles { get; set; }
        public string[] permissions { get; set; }
    }

}
