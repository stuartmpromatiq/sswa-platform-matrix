﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.ExternalUserService
{
    public interface IAuthorizationApiClient
    {
        Task<IEnumerable<ExternalRole>> GetRolesAsync();
        Task<IEnumerable<ExternalRole>> GetUserRolesAsync(string userId);
        Task UpdateUserRolesAsync(string userId, IEnumerable<string> roleIds);
        Task DeleteUserRolesAsync(string userId, IEnumerable<string> roleIds);

        Task GetUsersAsync();
    }
}
