﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.ExternalUserService
{
    public class ExternalRole
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("users")]
        public IEnumerable<string> Users { get; set; }

        [JsonProperty("permissions")]
        public IEnumerable<string> Permissions { get; set; }

    }
}
