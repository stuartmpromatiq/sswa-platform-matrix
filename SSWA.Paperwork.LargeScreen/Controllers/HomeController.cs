﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using LazyCache;
using Microsoft.AspNetCore.Mvc;
using SSWA.Paperwork.Common.Forms;
using SSWA.Paperwork.Common.Services;
using SSWA.Paperwork.Core;
using SSWA.Paperwork.LargeScreen.Models;

namespace SSWA.Paperwork.LargeScreen.Controllers
{
    public class HomeController : Controller
    {
        private readonly DocumentService<AuthorityToWorkForm> _fetchService;
        private readonly IAppCache _cache;

        public HomeController(DocumentService<AuthorityToWorkForm> fetchService, IAppCache cache)
        {
            _fetchService = fetchService;
            _cache = cache;
        }


        public async Task<IActionResult> Index(string mode = null, int delay = 15, decimal interval = 5)
        {
            Task<IEnumerable<AuthorityToWorkForm>> showObjectFactory() => PopulateForms();
            var forms = await _cache.GetOrAddAsync("forms", showObjectFactory, DateTimeOffset.Now.AddMinutes(10));

            if (mode?.ToLowerInvariant() == "today")
            {
                forms = forms.Where(x => x.StartDate?.ToLocalTime().Date == DateTime.Today);
            }

            ViewBag.StartDelay = delay;
            ViewBag.ScrollSpeed = interval;

            return View(forms.OrderBy(x => x.StartDate).ToList());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task<IEnumerable<AuthorityToWorkForm>> PopulateForms()
        {
            IEnumerable<AuthorityToWorkForm> forms =
                await _fetchService
                    .QueryAsync(
                        x => x.Kind.Code == "ATW",
                        x => x.Status == FormState.OPENED || x.Status == FormState.PENDING_AUTHORISATION || x.Status == FormState.PENDING_OPEN);
            return forms;
        }
    }
}
